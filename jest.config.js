module.exports = {
    rootDir: ".",
    "roots": [
        "<rootDir>/src/",
        "<rootDir>/test/"
    ],
    "testMatch": [
        "**/?(*.)+(spec|test).+(ts|tsx|js)"
    ],
    "transform": {
        "^.+\\.(ts|tsx)$": "ts-jest"
    },
    moduleDirectories: [
        "<rootDir>/node_modules"
    ],
    setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
    setupFiles: ["<rootDir>/jest.env.js"],
    testPathIgnorePatterns: [
        "/node_modules/",
        "/dist/"
    ]
}
