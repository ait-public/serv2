-- CreateTable
CREATE TABLE "Setting" (
    "id" TEXT NOT NULL,
    "code" TEXT NOT NULL,
    "value" TEXT,
    "createdAt" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,
    "deleted" TIMESTAMP(3),
    "tenant_id" UUID NOT NULL DEFAULT public.ait_default_tenant(),

    CONSTRAINT "Setting_pkey" PRIMARY KEY ("id")
);
