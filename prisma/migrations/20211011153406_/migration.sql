create or replace function public.ait_default_tenant()
	returns uuid
	immutable
	language plpgsql
as
$$
begin
   return current_setting('app.current_tenant')::uuid;
end;
$$;
