# Server
server library for backend development App service
https://medportal-demo.ru/monitor2

## todo
    remove in Postgress ait_tenant_table_grant  - check Ait installation

## App example (how usage)

index.ts
```typescript

import { createApp } from '@ait/serv2';

import schema from './graphql';

import { createPrisma } from './Prisma';
import { seedPlatform, seedTenant } from './seed';
import permissions from './permissions';

import {} from './graphql/Cron';

import { AppOptions } from '@ait/serv2/dist/engine/options';

export const APP_OPTIONS: AppOptions = {
  // App mode
  mode: {
    isTenant: true,
  },
  // GraphQl API
  schema,
  // Factories
  core: {
    prisma: createPrisma,
  },
  // Prisma Options
  prisma: {
    isLogged: true,
    middlewares: {
      rls: true,
    },
  },
  // Permissons
  shield: {
    ruleTree: permissions,
    options: {
      allowExternalErrors: true,
      debug: true,
    },
  },
  // Migrate options
  migration: {
    // seed // AIT User Connection
    seedTenant, // seed for each tanants
    seedPlatform, // tenant platform
  },
};


async function main() {
  // server init
  const server = await createApp(APP_OPTIONS);
  // server run
  await server.run(undefined, (c) => {
    // write server info
    console.log('NODE_ENV:', process.env.NODE_ENV);
  });
}

main();

```


### graphql (controllers)

typeDefs.ts
```typescript
import gql from 'graphql-tag';

export default gql`
  type Query {
    foo: String
  }
`;
```
resolvers.ts
```typescript
import { Resolvers } from '../../resolversTypes';

const resolvers: Resolvers = {
  Query: {
    foo: async (_parent, args, { gqlClient, role, options }) => {
      return 'foo';
    },
  },
};
export default resolvers;
```

### Permission example

```typescript
import { allow, /* deny, rule,*/ or } from 'graphql-shield';
import { shield } from '@ait/serv2';

export default {
  Query: {
    foo: allow,
    findLookup: shield.rules.isRolePatientOrMore,
    findLookupList: shield.rules.isRoleEmployeeOrMore,
    '*': or(shield.rules.isAuthenticated, shield.rules.isModeDev),
  },
  Mutation: {
    xqImport: allow,
    runCronJob: allow,
    '*': shield.rules.isRoleEmployeeOrMore,
  },
};
```

## Migration
todo

## Clients (http)
todo

## Cron 
todo

## Settings
todo

## Lookups
Регистрация лукапа

```typescript
import { Lookup } from '@ait/serv2';
import { PrepareOpts } from '@ait/serv2/dist/engine/lookup/Lookup';

const lookup = new Lookup();

lookup.add({
  type: 'foo',                                // <-- prisma table name
  propId: 'id',                               // <--- PK
  propText: (c) => `${c.shortName}   ${c.name} (${c.role})`, // <---- `_display` name
  searchFields: ['code', 'name', 'shortName'],  // <--- Поиск по OR 
                                                // (+ сортировка ASC по умолчанию (code, name, shortName))
  select: {id: true, name: true, role: true, shortName: true}, // <-- Призма select
  prepareArgs: {
    list: async (options: PrepareOpts) => {   // <----- intercept for manipulation
      const { args } = options;               // <------ Prisma expression (with Order,Pagination,Filter)
      args.where.enabled = true;              // <------ Показать неудаленные  (+ AND enabled = true)
      return args;
    },
  },
});
```
## Logs
todo

## Error
todo

## EBus
todo

## see also

1. Frontent Vue UI library - Aitify2 based on Vuetify https://gitlab.com/ait-public/aitify2
2. Common library - https://gitlab.com/ait-public/share2


