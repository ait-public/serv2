import { createTestContext } from '../src/engine/testing';
import { PrismaClient } from '@prisma/client';
import { helper } from '../src';



const ctx = createTestContext<PrismaClient>({
  tenant: { forceTenantId: "00000000-0000-0000-0000-000000000001" },
  prisma: {
    middlewares: {
      rls: true
    }
  },
  test: { suppressLog: ["log"] },
  migration: {
    seedPlatform: async ({ migrationName, prisma, tenantId }) => {
      const p: PrismaClient = prisma;
      await p.setting.create({ data: { id: migrationName, code: migrationName, value: tenantId } });
    },
    seed: async ({ prisma }) => {
      const p: PrismaClient = prisma;
      await p.setting.create({ data: { id: "seed all", code: "seed all" } });
    },
    seedTenant: async ({ prisma, tenantId, tenantIds }) => {
      const p: PrismaClient = prisma;
      await p.setting.create({ data: { id: tenantId, code: tenantId } });
    }
  },
  core: {
    prisma: (opts) => new PrismaClient({
      datasources: {
        db: {
          url: opts?.dbUrl
        }
      }
    })
  }
});

describe('migration prisma', () => {
  test('check db schema', async () => {
    const r = await helper.db.checkSchema(ctx.dbUrl);
    expect(r).toBeTruthy();
  });

  test('seed', async () => {
    const p = ctx.prisma;
    const d = await p.setting.findFirst({ where: { id: "seed all" } });
    expect(d?.id === "seed all").toBeTruthy();
  });

  test('seed Tenant', async () => {

    const tenants = ctx.options?.core?.tenants;
    if (!tenants) throw new Error("error tenants");
    const p = ctx.prisma;
    const tenantIds = await tenants(ctx.options);
    let i = 0;
    for (let index = 0; index < tenantIds.length; index++) {
      const id = tenantIds[index];
      const d = await p.setting.findFirst({ where: { id } });
      if (d && d.id === id) i++;
    }
    expect(i === tenantIds.length).toBeTruthy();
  });

  test('seed Platform', async () => {
    const r1 = await helper.db.lastMigration(ctx.dbUrl);
    expect(r1).toHaveProperty("id");
    const p = ctx.prisma;
    const d = await p.setting.findFirst({ where: { id: r1.migration_name } });
    expect(r1.migration_name === d?.id).toBeTruthy();
    expect(d?.value === ctx.options?.tenant?.platformTenantId).toBeTruthy();
  });

});

