import { PrismaClient } from '@prisma/client'
import { createTestContext } from '../src/engine/testing';

const ctx = createTestContext<any>({
  test: { suppressLog: ['log', 'error'] },
  mode: {
    isTenant: true,
  },
  prisma: {
    middlewares: {
      rls: true
    }
  },
  database: {
    db: "dev"
  },
  core: {
    prisma: (opts) => new PrismaClient({
      datasources: {
        db: {
          url: opts?.dbUrl
        }
      }
    })
  },
  migration: { skipDeploy: true }
});

describe('core', () => {
  test('tenats', async () => {
    const opts = ctx.options;
    const ids = opts.core?.tenants && (await opts.core.tenants(opts));
    expect(ids?.length).toBeDefined();
  });

});
