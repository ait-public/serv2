import { createTestContext } from '../src/engine/testing';
import { PrismaClient } from '@prisma/client';
import { helper } from '../src/engine';
import { TenantScopeContext } from '../src/engine/_helper/ctx';


const ctx = createTestContext<PrismaClient>({
  test: { nodrop: false, suppressLog: "log" },
  tenant: {
    strict: true
  },
  mode: {
    isTenant: true,
  },
  prisma: {
    middlewares: {
      rls: true
    }
  },
  core: {
    prisma: (opts) => new PrismaClient({
      datasources: {
        db: {
          url: opts?.dbUrl
        }
      }
    })
  }
});

describe('prisma', () => {
  test('$transaction', async () => {

    const options = ctx.options;

    const TENANTID_2 = "df7232ee-b614-4bf7-b47f-5bfc6a804b63";
    const TENANTID_3 = "df7232ee-b614-4bf7-b47f-5bfc6a804b64";

    await helper.ctx.inTenantScope(options, TENANTID_2, async ({ prisma }: TenantScopeContext<PrismaClient>) => {

      await helper.ctx.inTenantScope(options, TENANTID_3, ({ prisma }: TenantScopeContext<PrismaClient>) =>
        prisma.setting.create({ data: { code: "11" } }));

      const createOne = prisma.setting.create({ data: { code: "11" } });
      const createTwo = prisma.setting.create({ data: { code: "12" } });

      const t = [createTwo, createOne];

      // $transaction API
      const r = await prisma.$transaction(t);

      expect(r.length).toBe(2);

    });

    const r2 = await helper.ctx.inTenantScope(options, TENANTID_3, ({ prisma }: TenantScopeContext<PrismaClient>) => prisma.setting.findMany());
    expect(r2.length).toBe(1);

  });
});
