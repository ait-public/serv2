import { PrismaClient } from '.prisma/client';
import { allow } from 'graphql-shield';
import { LookupType } from '../src/engine/lookups/lookups.options';
import { createTestContext } from '../src/engine/testing';


const FOO_LIST = [
  { id: "1", value: "value 1" },
  { id: "2", value: "value 2" },
  { id: "3", value: "value 3" }
];

const foo: LookupType = {
  propId: 'id',
  propText: (c) => c.id + " " + c.value,
  resolvers: {
    ids: ({ ids }) => Promise.resolve(FOO_LIST.filter(c => ids.includes(c.id))),
    list: () => Promise.resolve(FOO_LIST)
  },
};

const ctx = createTestContext<any>({
  test: { suppressLog: ['log'] },
  lookups: {
    types: {
      foo
    }
  },
  shield: {
    ruleTree: {
      Query: {
        '*': allow
      },
    },
    options: {
      allowExternalErrors: true,
      debug: true,
    },
  },
  core: {
    prisma: (opts) => new PrismaClient({
      datasources: {
        db: {
          url: opts?.dbUrl
        }
      }
    })
  },
  migration: { skipDeploy: true }
});



describe('gql', () => {


  test('check', async () => {

    const client = ctx.gqlClient.get("serv2");

    const r = await client.request(`
      query Query($ids: LookupInput!, $type: String!) {
        findLookupList(type: $type)
        findLookup(ids: $ids) {
          foo {
            id
            record
          }
        }
        getLookupTypes
      }
      `, {
      "type": "foo",
      "ids": {
        "foo": ["2", "1"]
      }
    });

    expect(r).toBeDefined();
    expect(r.findLookup.foo.length).toBe(2);
    expect(r.findLookup.foo[0]).toMatchObject({
      id: "2",
      record: {
        ...FOO_LIST[1],
        _display: "2 value 2"
      }
    });
    expect(r.findLookup.foo[1].id).toBe("1");


    expect(r.getLookupTypes.foo).toBeDefined();

    expect(r.findLookupList.length).toBe(FOO_LIST.length);


  });


});
