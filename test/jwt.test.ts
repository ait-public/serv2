import { helper } from '../src/engine';

describe("Jwt", () => {
  //
  test("createToken", async () => {
    const token = await helper.jwt.createToken({
      clientId: "foo",
      name: "zoo",
      secret: "secret",
      role: "role",
      userId: "userID",
      profileId: "profileId",
      tenantId: "tenantId",
      sessionId: "123"
    });

    expect(token).toBeDefined();

    if (token) {
      const payload = await helper.jwt.verifyToken({
        secret: "secret",
        token
      });

      expect(payload?.clientId).toBe("foo");
      expect(payload?.name).toBe("zoo");
      expect(payload?.role).toBe("role");
      expect(payload?.sub).toBe("userID");
      expect(payload?.profileId).toBe("profileId");
      expect(payload?.tenantId).toBe("tenantId");
      expect(payload?.sessionId).toBe("123");
    }

  });
})