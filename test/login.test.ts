import { PrismaClient } from '@prisma/client';
import { gql } from 'graphql-request';
import { Context } from '../src/engine/Context';
import { createTestContext } from '../src/engine/testing';

const TENANT_ID = "790d50b2-bef5-4152-a280-295e7e6fb84c";
const TENANTS = ["6dd34d35-efe6-49fa-8bb6-9fd4a3788a6a", "790d50b2-bef5-4152-a280-295e7e6fb84c"];
const PROFILE_ID = "1d2bd9a4-418a-474e-bca3-741f35b18952"



const ctx = createTestContext<PrismaClient>({
  tenant: {
    strict: true,
  },
  mode: {
    isTenant: true,
  },
  prisma: {
    middlewares: {
      rls: true
    }
  },
  database: {
    schema: "serv2_"
  },
  test: { suppressLog: ["log", "error"], nodrop: false },
  schema: {
    typeDefs: `type Query {getRls: Json}`,
    resolvers: [{
      Query: {
        getRls: async (p: any, args: any, { prisma, dbUrl, tenant }: Context<any>, meta: any) => {
          console.log("db", dbUrl, "tenant", tenant);
          return await (prisma as PrismaClient).setting.findMany();
        },
      },
    }]
  },
  migration: {
    seedPlatform: async ({ prisma, tenantId, migrationName }) => {

      const p: PrismaClient = prisma;
      await p.setting.upsert({ where: { id: tenantId }, create: { id: tenantId, code: "" }, update: {} });

    },
    seedTenant: async ({ prisma, tenantId, migrationName }) => {

      const p: PrismaClient = prisma;
      await p.setting.upsert({ where: { id: tenantId }, create: { id: tenantId, code: "" }, update: {} });

    }
  },
  core: {
    prisma: (opts) => new PrismaClient({ datasources: { db: { url: opts?.dbUrl } } }),
    tenants: async () => TENANTS
  }
});


const gqlLOGIN = gql`
  mutation login($name: String!, $password: String!, $tenantId: String!, $profileId: String ) {
    login(name: $name, password: $password, tenantId: $tenantId, profileId: $profileId) {
      accessToken
      refreshToken
    }
  }
`;

const gqlME = gql`{me}`;

const gqlREFRESH_TOKEN = gql`
  mutation refreshToken($refreshToken: String!) {
    refreshToken(refreshToken: $refreshToken) {
      accessToken
      refreshToken
    }
  }
`;

describe("login", () => {
  test("check me", async () => {
    //
    const client0 = ctx.gqlClient.get("");
    const rLogin = await client0.request(gqlLOGIN, { name: "ADMIN", password: "ADMIN", profileId: PROFILE_ID, tenantId: TENANT_ID });

    expect(rLogin).toHaveProperty("login");
    expect(rLogin.login?.accessToken).toBeDefined();
    expect(rLogin.login?.refreshToken).toBeDefined();
    console.log("t", rLogin);

    const clientA = ctx.gqlClient.get("", {
      headers: { ['Authorization']: 'Bearer ' + rLogin.login?.accessToken }
    });

    const rme = await clientA.request(gqlME);
    expect(rme.me?.profile?.id).toBe(PROFILE_ID);


    const rRefr = await clientA.request(gqlREFRESH_TOKEN, { refreshToken: rLogin.login?.refreshToken });
    expect(rRefr.refreshToken?.accessToken).toBeDefined();
    expect(rRefr.refreshToken?.refreshToken).toBeDefined();

    const clientB = ctx.gqlClient.get("", {
      headers: { ['Authorization']: 'Bearer ' + rRefr.refreshToken?.accessToken }
    });

    const rme2 = await clientB.request(gqlME);
    expect(rme2.me?.profile?.id).toBe(PROFILE_ID);


    const { getRls } = await clientB.request(`{getRls}`);
    console.log(getRls);


    await clientB.request("mutation{logout}");

    try {
      const r = await clientB.request(gqlME);
      expect(r).not.toBeDefined();
    } catch (e) {
      expect(true).toBeTruthy();
    }
  })
})
