import { initApp } from '../src/engine/createApp';
import { helper } from '../src/engine';
import { utils } from '@ait/share2';


const consoleLog = console.log;
beforeEach(() => {
  console.log = consoleLog
})

describe('install', () => {

  // test('checkAitInstallation', async () => {
  //   const app = initApp();
  //   let dbUrl = app.dbConnection({ mode: { isTenant: false, isMigration: true }, database: { db: "dev", host: "92.63.98.135" } });
  //   expect(await helper.db.checkAitInstallation(dbUrl)).toBeTruthy();
  // });

  test('install new Db', async () => {

    console.log = jest.fn()

    const app = initApp();

    let dbUrl = app.dbConnection({ mode: { isTenant: false, isMigration: true } });
    const db = `__test_install_` + utils.getRandomInt(0, 100);
    console.log("test new", db);
    try {
      await helper.db.query({ dbUrl, queryText: `CREATE DATABASE ${db}` });
      const newDbUrl = await app.dbInstall({ database: { db } })
      expect(await helper.db.checkAitInstallation(newDbUrl)).toBeTruthy();

    } finally {
      helper.db.query({ dbUrl, queryText: `DROP DATABASE IF EXISTS ${db}` });
    }
  });
});


