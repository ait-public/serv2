import { createApp, helper } from '../src/engine';

const consoleLog = console.log;
beforeEach(() => {
    console.log = consoleLog
})

describe('server', () => {
    test('start & stop', async () => {
        console.log = jest.fn()
        const app = await createApp({ core: { prisma: () => null } });
        await app.run();
        await app.stop();
        expect(true).toBeDefined();
    });



    test('2 servers', async () => {
        console.log = jest.fn()

        const port1 = 3389;
        const port2 = 3390;

        const url1 = `http://localhost:${port1}/serv2/ping`;
        const url2 = `http://localhost:${port2}/serv2/ping`;

        const ctx1 = { server: { port: port1 }, core: { prisma: () => null }, mode: { isTest: false, isMigration: false } };
        const ctx2 = { server: { port: port2 }, core: { prisma: () => null }, mode: { isTest: false, isMigration: false } };

        const app1 = await createApp(ctx1);
        const app2 = await createApp(ctx2);

        await Promise.all([app1.run(), app2.run()]);
        try {
            const [r1, r2] = await Promise.all([helper.fetch(url1), helper.fetch(url2)]);
            expect(r1).toHaveProperty("now");
            expect(r2).toHaveProperty("now");
        } finally {
            await Promise.all([app1.stop(), app2.stop()]);
        }
    });

});

