import { createTestContext } from '../src/engine/testing';
import { PrismaClient } from '@prisma/client';
import { helper } from '../src/engine';
import { SeedContext } from '../src/engine/Context';
import { TenantScopeContext } from '../src/engine/_helper/ctx';


const TENANTID = "66372315-b952-40ee-ad0d-14511350df99";
const TENANTID_1 = "edfc7695-2fd7-4040-accb-284ac543c9f2";

const ctx = createTestContext<PrismaClient>({
  test: { nodrop: false, suppressLog: "log" },
  tenant: {
    strict: true
  },
  mode: {
    isTenant: true,
  },
  prisma: {
    middlewares: {
      rls: true
    }
  },
  migration: {
    seed: async ({ prisma }: SeedContext<PrismaClient>) => {
      await prisma.setting.create({
        data: {
          code: "1",
          tenantId: TENANTID
        }
      });
      await prisma.setting.create({
        data: {
          code: "2",
          tenantId: TENANTID_1
        }
      })
    },
  },
  core: {
    prisma: (opts) => new PrismaClient({
      datasources: {
        db: {
          url: opts?.dbUrl
        }
      }
    })
  }
});

describe('inScope', () => {
  test('inTenantScope', async () => {
    await helper.ctx.inTenantScope(ctx.options, TENANTID, async ({ prisma }: TenantScopeContext<PrismaClient>) => {
      const items = await prisma.setting.findMany();
      expect(items.length).toBe(1);
      expect(items[0].tenantId).toBe(TENANTID);
    });
  });


  test('inAitScope', async () => {
    await helper.ctx.inAitScope(ctx.options, TENANTID, async ({ prisma }: TenantScopeContext<PrismaClient>) => {
      const items = await prisma.setting.findMany();
      expect(items.length).toBe(2);
    });
  });
});
