import { PrismaClient } from '@prisma/client'
import { clients, helper } from '../src/engine';
import { createClientToken } from '../src/engine/client/httpHeader.factory';
import { createTestContext } from '../src/engine/testing';

const ctx = createTestContext<any>({
  test: { suppressLog: ['log'] },
  core: {
    prisma: (opts) => new PrismaClient({
      datasources: {
        db: {
          url: opts?.dbUrl
        }
      }
    })
  },
  migration: { skipDeploy: true }
});


describe('gqlClient.getUrl', () => {
  test('getUrl("")', async () => {
    expect(ctx.gqlClient.getUrl("")).toBe(`http://localhost:${ctx.options?.server?.port}/${ctx.options?.root?.schema}`);
  });

  test('serv2', async () => {
    expect(ctx.gqlClient.getUrl("serv2")).toBe(`http://localhost:${ctx.options?.server?.port}/${ctx.options?.root?.schema}`);
  });

  test('monitor2', async () => {
    expect(ctx.gqlClient.getUrl("monitor2")).toBe(`${ctx.options.dev?.medportalUrl ?? ''}/monitor2`);
  });
});


describe('pingApi', () => {

  test('ping()', async () => {
    const api = clients.ping.create(ctx.gqlClient);
    const res = await api.ping();
    expect(res).toBeDefined();
  });

  test('ping', async () => {
    const api = clients.ping.create(ctx.gqlClient);
    const res = await api.ping({ schema: "serv2", json: true });
    expect(res).toHaveProperty('now');
  });

  test('version', async () => {
    const api = clients.ping.create(ctx.gqlClient);
    const res = await api.ping({ schema: "serv2", version: true, json: true });
    expect(res).toHaveProperty('app');
  });

  test('api/ping', async () => {
    const api = clients.ping.create(ctx.gqlClient);
    const res = await api.ping({ schema: "serv2", api: true, json: true });
    expect(res).toHaveProperty('now');
  });


  test('api/version', async () => {
    const api = clients.ping.create(ctx.gqlClient);
    if (await helper.db.checkSchema(ctx.dbUrl)) {
      const res = await api.ping({ schema: "serv2", api: true, version: true, json: true });
      expect(res).toHaveProperty('app');
    }
  });

  test('ping medportal', async () => {
    const api = clients.ping.create(ctx.gqlClient);
    const res = await api.ping({ schema: "monitor2" });
    expect(res).toBeDefined();
  });
});



describe('lookup', () => {
  test('getIds()', async () => {

    const t = await createClientToken({ clientId: "serv2", tenantId: "00000000-0000-0000-1000-000000000002" });
    const api = clients.lookup.create(ctx.gqlClient);
    const res = await api.getIds(
      {
        schema: "monitor2",
        ids: {
          user: ["26385788-5b41-4b1a-b85c-8561b1d3d5cb"]
        },
        headers: {
          "Authorization": "Bearer " + t
        }
      });
    expect(res?.user).toHaveProperty(["26385788-5b41-4b1a-b85c-8561b1d3d5cb"]);
  });
});
