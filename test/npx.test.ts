import { prismaDeploy } from '../src/engine/prisma/npx/prismaDeploy';
import { prismaVersion } from '../src/engine/prisma/npx/prismaVersion';

const consoleLog = console.log;
beforeEach(() => {
  console.log = consoleLog
})

describe('migration npx', () => {


  test('npx version', async () => {
    console.log = jest.fn()
    const b = await prismaVersion({});
    expect(b).toBeTruthy();
  });

  test('npx deploy', async () => {
    console.log = jest.fn()
    const b = await prismaDeploy({});
    expect(b).toBeTruthy();
  });
});

