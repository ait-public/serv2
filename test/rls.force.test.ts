import { createTestContext } from '../src/engine/testing';
import { PrismaClient } from '@prisma/client';


const FORCETENANTID = "66372315-b952-40ee-ad0d-14511350df99";

const ctx = createTestContext<PrismaClient>({
  test: { suppressLog: "log" },
  tenant: {
    forceTenantId: FORCETENANTID
  },
  prisma: {
    middlewares: {
      rls: true
    }
  },
  core: {
    prisma: (opts) => new PrismaClient({
      datasources: {
        db: {
          url: opts?.dbUrl
        }
      }
    })
  }
});

describe('Rls forceTenantId', () => {
  test('def', async () => {

    const p = ctx.options?.prisma?.prisma;
    if (!p) throw new Error("!");
    await p.setting.create({ data: { id: "forceTenantId", code: "seed" } });
    const item = await p.setting.findUnique({ where: { id: "forceTenantId" } });
    expect(item?.tenantId === FORCETENANTID).toBeTruthy();
  });

  test('custom', async () => {

    const p = ctx.prisma;
    await p.setting.create({ data: { id: "2", code: "seed", tenantId: "66372315-b952-40ee-ad0d-145113500000" } });
    const item = await p.setting.findUnique({ where: { id: "2" } });
    expect(item?.tenantId === "66372315-b952-40ee-ad0d-145113500000").toBeTruthy();
  });
});

