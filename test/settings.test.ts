import { createTestContext } from '../src/engine/testing';
import { PrismaClient } from '@prisma/client';
import { KeySetting } from '../src/engine/settings/settings.options';

let _settings: any = null;
let _list: KeySetting[] = [];

createTestContext<PrismaClient>({
  test: { suppressLog: "log" },
  mode: {
    isTenant: true,
    isMigration: true
  },
  settings: {
    keys: {
      "serv2.var0": { value: "Привет!" },
      "serv2.var1": { defValue: "var1", label: "var 1", note: "test settings", meta: { foo: "some" }, public: true, sort: 3 },
      "xxx.foo": null
    }
  },
  prisma: {
    middlewares: {
      rls: true
    }
  },
  migration: {
    seedPlatform: async ({ settings, options }) => {
      _settings = settings;
      const getSettingList = options?.settings?.provider?.getSettingList;
      if (getSettingList) {
        _list = await getSettingList();
      }
    },
  },
  core: {
    prisma: (opts) => new PrismaClient({
      datasources: {
        db: {
          url: opts?.dbUrl
        }
      }
    })
  }
});

describe('settings', () => {
  test('settings defined', async () => {

    expect(_settings?.["serv2.var0"]).toBeDefined();

    const s0 = _list.find(c => c.key === 'serv2.var1');
    expect(s0).toBeDefined();
    expect(s0?.public).toBeTruthy();
  });
});

