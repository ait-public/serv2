
import { utils } from '@ait/share2';
// import { AsyncLocalStorage } from 'async_hooks';
import { presets } from '../src/engine/presets'



describe("context storage", () => {
  const storage = presets.storage({});

  test("check context in async func", async () => {

    let tid: string | undefined;

    async function work() {
      await utils.sleep(10);
      tid = storage.getStore()?.tenantId;
    }

    if (!storage) throw new Error("storage is null");
    await storage.run({ tenantId: 'baz' }, async () => {
      return await work()
    });

    expect(tid).toBe("baz");
  })
});
