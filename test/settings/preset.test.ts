import { presetSettings } from "../../src/engine/settings/settings.preset";

describe('preset', () => {
  test('env+keys', async () => {

    // throw new Error("!")
    const val = "It`s test!";
    process.env["ait.test.serv2.test"] = val;

    const settings = await presetSettings({
      settings: {
        constants: {
          ENV_LOCAL_PREFIX: "ait.test."
        }
      }
    });

    expect(presetSettings()).toBeDefined();

    if (!settings.keys) throw new Error("key not initialized");

    expect(settings.keys).toBeDefined()
    expect(settings.keys?.["serv2.test"]).toBeDefined();
    expect(settings.keys?.["serv2.test"]?.defValue).toEqual(val);

    settings.keys["serv2.test"] = {
      label: "label",
    };

    const mySettings1 = await presetSettings({ settings });
    if (!mySettings1.keys) throw new Error("key not initialized");

    expect(mySettings1.keys).toBeDefined()
    expect(mySettings1.keys?.["serv2.test"]).toBeDefined();
    expect(mySettings1.keys?.["serv2.test"]?.defValue).toEqual(val);
    expect(mySettings1.keys?.["serv2.test"]?.label).toEqual("label");


    const myVal = "My test!";

    settings.keys["serv2.test"] = {
      defValue: myVal,
      note: "note"
    };

    const mySettings2 = await presetSettings({ settings });
    if (!mySettings2.keys) throw new Error("key not initialized");

    expect(mySettings2.keys).toBeDefined()
    expect(mySettings2.keys?.["serv2.test"]).toBeDefined();
    expect(mySettings2.keys?.["serv2.test"]?.defValue).toEqual(myVal);
    expect(mySettings2.keys?.["serv2.test"]?.note).toEqual("note");

  });
});
