// import { createGetSettings } from "../../src/engine/settings/settings.helper";
// import { PrismaClient } from '.prisma/client';
// import { Context } from '../../src/engine/Context';
// import { AppOptions } from '../../src/engine/options';
// import { createTestContext } from '../../src/engine/testing';

// describe('getSettings', () => {


//   const appOpts: AppOptions = {
//     root: { schema: "serv2" },
//     mode: {
//       isTenant: true,
//       isTest: false
//     },
//     tenant: {
//       forceTenantId: "00000000-0000-0000-1000-000000000002"
//     },
//     database: {
//       db: "dev",
//       host: "localhost",
//     },
//     schema: {
//       typeDefs: `type Query {getSettings: Json}`,
//       resolvers: [{
//         Query: {
//           getSettings: async (p: any, args: any, { getSettings }: Context<any>, meta: any) => {
//             return await getSettings();
//           },
//         },
//       }]
//     },
//     prisma: {
//       middlewares: {
//         rls: true
//       }
//     },
//     settings: {
//       keys: {
//         "org2.name": null,
//         "serv2.test": {
//           defValue: "test!!!",
//           label: "label",
//           public: true,
//           note: "@@@",
//           sort: 10
//         },
//         "serv2.test1": {
//           defValue: "test1!",
//           label: "label 1",
//           meta: { ttt: 345, name: "caption" },
//           public: false,
//           note: "@@@",
//           sort: 20
//         }
//       }
//     },
//     core: {
//       prisma: (opts) => new PrismaClient({ datasources: { db: { url: opts?.dbUrl } } })
//     }
//   };


//   const ctx = createTestContext<PrismaClient>(appOpts);


//   test('should load settings from context', async () => {
//     const client0 = ctx.gqlClient.get("");
//     const rLogin = await client0.request("getSettings");
//   });

// });
