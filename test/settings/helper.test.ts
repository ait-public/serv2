import { createGetSettings, createSaveSettings } from "../../src/engine/settings/settings.helper";
import { initApp } from "../../src/engine/createApp";
import { AppOptions } from '../../src/engine/options';
import { nanoid } from 'nanoid';

describe('settings.helper', () => {


  const appOpts: AppOptions = {
    root: { schema: "serv2" },
    mode: {
      isTenant: true,
      isTest: false
    },
    tenant: {
      forceTenantId: "00000000-0000-0000-1000-000000000002"
    },
    database: {
      db: "dev",
      host: "localhost",
    },
    prisma: {
      middlewares: {
        rls: true
      }
    },
    settings: {
      keys: {
        "org2.name": null,
        "xxx.foo": null,
        "serv2.test": {
          defValue: "test!!!",
          label: "label",
          public: true,
          note: "@@@",
          sort: 10
        },
        "serv2.test1": {
          defValue: "test1!",
          label: "label 1",
          meta: { ttt: 345, name: "caption" },
          public: false,
          note: "@@@",
          sort: 20
        }
      }
    },
    core: {
      // prisma: (opts) => new PrismaClient({ datasources: { db: { url: opts?.dbUrl } } })
    }
  };

  async function testOptions() {
    return await initApp().config(appOpts);
  }


  test('should load settings from db', async () => {
    const opts = await testOptions();
    const getSettings = createGetSettings(opts);
    expect(getSettings).toBeDefined();
    const d = await getSettings();
    expect(d).toBeDefined();
    expect(d["serv2.test1"]).toBeDefined();
    expect(d["serv2.test"]).toBeDefined();
  });


  test('should save settings to db', async () => {
    const CHECK_VAL = nanoid();
    const opts = await testOptions();
    const saveSettings = createSaveSettings(opts);
    expect(saveSettings).toBeDefined();
    const c = await saveSettings({
      "serv2.test1": CHECK_VAL,
      "xxx.foo": "FOO"
    });
    expect(c).toBe(2);
    const getSettings = createGetSettings(opts);
    const d = await getSettings();
    expect(d["serv2.test1"]).toBe(CHECK_VAL);
    expect(d["xxx.foo"]).toBeUndefined();
  });

});
