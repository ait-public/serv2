import { createTestContext } from '../src/engine/testing';
import { PrismaClient } from '@prisma/client';
import { helper } from '../src/engine';
import { SeedContext } from '../src/engine/Context';
import { TenantScopeContext } from '../src/engine/_helper/ctx';


const TENANTID_0 = "66372315-b952-40ee-ad0d-14511350df99";
const TENANTID_1 = "edfc7695-2fd7-4040-accb-284ac543c9f2";
const TENANTID_2 = "edfc7695-2fd7-4040-accb-284ac543c9f4";


const ctx = createTestContext<PrismaClient>({
  test: { nodrop: false, suppressLog: "log" },
  tenant: {
    strict: true
  },
  database: {
    schema: "serv2_prisma_test"
  },
  mode: {
    isTenant: true,
  },
  prisma: {
    middlewares: {
      rls: true
    }
  },
  migration: {
    seed: async ({ prisma }: SeedContext<PrismaClient>) => {
      await prisma.setting.create({
        data: {
          code: "1",
          tenantId: TENANTID_0
        }
      });
      await prisma.setting.create({
        data: {
          code: "2",
          tenantId: TENANTID_0
        }
      });
      await prisma.setting.create({
        data: {
          code: "30",
          tenantId: TENANTID_1
        }
      })
    },
  },
  core: {
    prisma: (opts) => new PrismaClient({
      datasources: {
        db: {
          url: opts?.dbUrl
        }
      }
    })
  }
});

describe('prisma', () => {
  test('$queryRaw', async () => {
    await helper.ctx.inTenantScope(ctx.options, TENANTID_0, async ({ prisma }: TenantScopeContext<PrismaClient>) => {

      const r = await prisma.$queryRawUnsafe<any[]>(`SELECT * FROM serv2_prisma_test."Setting" where code=$1`, 2);
      expect(r.length).toBe(1);

      const r1 = await prisma.$queryRawUnsafe<any[]>(`SELECT * FROM serv2_prisma_test."Setting"`);
      expect(r1.length).toBe(2);

      const r2 = await prisma.$queryRaw<any[]>`SELECT * FROM serv2_prisma_test."Setting" where code=${1}`;
      expect(r2.length).toBe(1);
    });

    await helper.ctx.inTenantScope(ctx.options, TENANTID_1, async ({ prisma }: TenantScopeContext<PrismaClient>) => {

      const r = await prisma.$queryRawUnsafe<any[]>(`SELECT * FROM serv2_prisma_test."Setting" where code=$1`, "30");
      expect(r.length).toBe(1);

      const r1 = await prisma.$queryRawUnsafe<any[]>(`SELECT * FROM serv2_prisma_test."Setting"`);
      expect(r1.length).toBeGreaterThanOrEqual(1);

      const r2 = await prisma.$queryRaw<any[]>`SELECT * FROM serv2_prisma_test."Setting" where code=${"30"}`;
      expect(r2.length).toBe(1);
    });

  });

  test('$executeRaw', async () => {
    await helper.ctx.inTenantScope(ctx.options, TENANTID_2, async ({ prisma }: TenantScopeContext<PrismaClient>) => {
      const r = await prisma.$executeRawUnsafe<number>(`
      INSERT INTO serv2_prisma_test."Setting" (id, code)
      VALUES($1, 'edfc7695-2fd7-4040-accb-284ac543c9f4')`, "edfc7695-2fd7-4040-accb-284ac543c9f4");
      expect(r).toBe(1);

      const r1 = await prisma.$executeRaw<number>`
      UPDATE serv2_prisma_test."Setting"
        SET code='!!!'
        WHERE id=${"edfc7695-2fd7-4040-accb-284ac543c9f4"}`;
      expect(r1).toBe(1);

      const r2 = await prisma.setting.findFirst({ where: { id: TENANTID_2 } });
      expect(r2?.code).toBe('!!!');
    });
  });

});
