import { helper } from "../src";

describe('db', () => {
  test('parse C# Connection', () => {
    const r = helper.db.parseUrl("Server=92.63.98.130;Port=5432;Database=aitjwt;User Id=ait;Password=475a!2;Pooling=true;MinPoolSize=10;MaxPoolSize=100;Enlist=true;Timeout=300;CommandTimeout=300")

    expect(r).toBeDefined();
    expect(r).toMatchObject({
      db: 'aitjwt',
      host: '92.63.98.130',
      match: true,
      password: '475a!2',
      port: '5432',
      schema: undefined,
      user: 'ait',
    })
  });

  test('parse Url', () => {
    const r = helper.db.parseUrl("postgresql://ait:475aiOswd@localhost:5432/dev?schema=my")

    expect(r).toBeDefined();
    expect(r).toMatchObject({
      db: 'dev',
      match: true,
      host: 'localhost',
      password: '475aiOswd',
      port: '5432',
      schema: 'my',
      user: 'ait',
    })
  });
});
