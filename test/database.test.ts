import { initApp } from '../src/engine/createApp';
import { constants, helper } from '../src/engine';

describe('core', () => {

  test('database', async () => {
    const core = initApp();
    let dbUrl = core.dbConnection({ mode: { isMigration: true, isTenant: true }, database: { db: "dev" } });
    let parse = helper.db.parseUrl(dbUrl);
    expect(parse.match).toBeTruthy();
    expect(parse.db).toBe("dev");
    expect(parse.user).toBe("tenant");
    expect(parse.bouncer).toBeFalsy();

    dbUrl = core.dbConnection({ mode: { isMigration: true, isTenant: false }, database: { db: undefined } });
    parse = helper.db.parseUrl(dbUrl);
    expect(parse.user).toBe("ait");
    expect(parse.db).toBe(constants.ENV.AIT_POSTGRES_DB);

    dbUrl = core.dbConnection({ mode: { isMigration: false, isTenant: true }, database: { host: "192.182.6.1" } });
    parse = helper.db.parseUrl(dbUrl);
    expect(parse.user).toBe("tenant");
    expect(parse.bouncer).toBeTruthy();

    dbUrl = core.dbConnection({ mode: { isMigration: false, isTenant: false }, database: { host: "192.182.6.1" } });
    parse = helper.db.parseUrl(dbUrl);
    expect(parse.user).toBe("ait");
    expect(parse.bouncer).toBeTruthy();

    dbUrl = core.dbConnection({ mode: { isMigration: false, isTenant: true }, database: { host: "localhost" } });
    parse = helper.db.parseUrl(dbUrl);
    expect(parse.bouncer).toBeFalsy();
  });
});
