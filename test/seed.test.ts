import { createTestContext } from '../src/engine/testing';
import { PrismaClient } from '@prisma/client';
import { helper } from '../src';
import { presetCore } from '../src/engine/core/core.preset';


let seedUser = '';
let seedPlatform = '';
let seedPlatformTenant = '';

const ctx = createTestContext<PrismaClient>({
  test: { suppressLog: "log" },
  mode: {
    isTenant: true,
    isMigration: true
  },
  settings: {
    keys: {
      "serv2.var0": { value: "Привет!" },
      "serv2.var1": { defValue: "var1", label: "var 1", note: "test settings", meta: { foo: "some" }, public: true, sort: 3 },
      "xxx.foo": null
    }
  },
  prisma: {
    middlewares: {
      rls: true
    }
  },
  migration: {
    seed: async ({ dbUrl }) => {
      seedUser = helper.db.parseUrl(dbUrl).user || "";
    },
    seedPlatform: async ({ dbUrl, tenantId, prisma }) => {
      try {
        seedPlatform = helper.db.parseUrl(dbUrl).user || "";
        seedPlatformTenant = tenantId;
        const p: PrismaClient = prisma;
        await p.setting.create({ data: { id: "seedPlatform", code: "", value: tenantId } });
      } catch (e) {
        console.error("seedPlatform", e);
      }
    },
    seedTenant: async ({ tenantId, prisma, settings }) => {
      const p: PrismaClient = prisma;
      const v = settings?.["serv2.var0"] ?? "";

      await p.setting.create({ data: { id: tenantId, code: v, value: tenantId, } });
    },
  },
  core: {
    prisma: (opts) => new PrismaClient({
      datasources: {
        db: {
          url: opts?.dbUrl
        }
      }
    })
  }
});

describe('seed', () => {

  test('seed AIT', async () => {
    expect(seedUser).toBe('ait');
  });

  test('seed Platform', async () => {
    expect(seedPlatform).toBe('tenant');
    expect(seedPlatformTenant).toBe(ctx.options?.tenant?.platformTenantId);
    const r = await ctx.prisma.setting.findUnique({ where: { id: "seedPlatform" } });
    expect(r?.tenantId).toBe(ctx.options?.tenant?.platformTenantId);
  });

  test('seed Tenants', async () => {

    const core = presetCore(ctx.options);
    const ids = await core.tenants(ctx.options);
    expect(ids.length).toBeTruthy();

    const id1 = ids[0]
    if (id1) {
      const r = await ctx.prisma.setting.findUnique({ where: { id: id1 } });
      expect(r?.tenantId).toBe(id1);
    }

    const id2 = ids[1]
    if (id2) {
      const r = await ctx.prisma.setting.findUnique({ where: { id: id2 } });
      expect(r?.tenantId).toBe(id2);
    }
  });

});

