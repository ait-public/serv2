import { PrismaClient } from '@prisma/client'
import { createTestContext } from '../src/engine/testing';

const ctx = createTestContext<any>({
  test: { suppressLog: ['log', 'error'] },
  core: {
    prisma: (opts) => new PrismaClient({
      datasources: {
        db: {
          url: opts?.dbUrl
        }
      }
    })
  },
  migration: { skipDeploy: true }
});

describe('tesing', () => {
  test('ping', async () => {
    const url = ctx.gqlClient.getUrl('serv2') + '/ping';
    const res = await ctx.gqlClient.post(url);
    expect(res).toHaveProperty('now');
  });

  test('version', async () => {
    const url = ctx.gqlClient.getUrl('serv2') + '/version';
    const res = await ctx.gqlClient.post(url);
    expect(res).toHaveProperty('app');
  });

  test('api/ping', async () => {
    const url = ctx.gqlClient.getUrl('serv2') + '/api/ping';
    const res = await ctx.gqlClient.post(url);
    expect(res).toHaveProperty('now');
  });


  test('api/version', async () => {
    const url = ctx.gqlClient.getUrl('serv2') + '/api/version';
    await expect(ctx.gqlClient.post(url)).rejects.toThrow()
  });
});
