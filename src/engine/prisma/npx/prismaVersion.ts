import constants from '../../constants';
import { npxPrisma } from './npxPrisma';

export async function prismaVersion({ cmdVersion }: { cmdVersion?: string }) {
  cmdVersion = cmdVersion ?? constants.MIGRATION.PRISMA_VERSION_CMD;
  // log info...
  return await npxPrisma(cmdVersion);
}
