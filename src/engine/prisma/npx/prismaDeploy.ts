import constants from '../../constants';
import { npxPrisma } from './npxPrisma';

// or prisma migrate deploy --preview-feature

export async function prismaDeploy({
  dbUrl,
  cmdDeploy,
}: {
  dbUrl?: string;
  cmdDeploy?: string;
}) {
  // console.log('db deploy - start');
  cmdDeploy = cmdDeploy ?? constants.MIGRATION.PRISMA_DEPLOY_CMD;
  // Set the required environment variable to contain the connection string
  // to our database test schema
  // Run the migrations to ensure our schema has the required structure
  const ENV = dbUrl && { DATABASE_URL: dbUrl };
  return await npxPrisma(cmdDeploy, ENV);
}
