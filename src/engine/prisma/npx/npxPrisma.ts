import util from 'util';
import { join } from 'path';
import fs from 'fs';

const exec = util.promisify(require('child_process').exec);

export async function npxPrisma(cmd?: string, env?: any) {
  if (!cmd) {
    console.error('cmd is empty');
    return false;
  }

  const ENV = {
    ...process.env,
    ...env,
  };

  console.log(`npx prisma ${cmd}`);

  try {
    const p = await exec(`${getPrismaBinPath()} ${cmd}`, { env: ENV });
    if (p.stderr) {
      if (
        p.stderr &&
        p.stderr.startsWith &&
        !p.stderr.startsWith('Debugger attached.')
      ) {
        console.error(p.stderr);
      }
    }
    if (p.stdout) console.log(p.stdout);
    return !!p.stdout;
  } catch (e) {
    console.error('npx', e);
    return false;
  }
}

export function getPrismaBinPath() {
  let PRISMA_BINARY = join(process.cwd(), 'node_modules', '.bin', 'prisma');

  if (!fs.existsSync(PRISMA_BINARY)) {
    PRISMA_BINARY = join(
      process.cwd(),
      'server',
      'node_modules',
      '.bin',
      'prisma'
    );
  }

  if (!fs.existsSync(PRISMA_BINARY))
    throw new Error(`Путь к .bin\prisma не найден '${PRISMA_BINARY}'!`);

  return PRISMA_BINARY;
}
