import { prismaVersion } from './prismaVersion';
import { prismaDeploy } from './prismaDeploy';
import { npxPrisma } from './npxPrisma';

export default {
  prismaDeploy,
  prismaVersion,
  npxPrisma,
};
