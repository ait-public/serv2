import { PrismaOptions } from '../options';
import { presetCore } from '../core/core.preset';
import { presetPrisma } from './prisma.preset';
import { PrismaOptionsInput } from './prisma.options.input';

export async function createPrismaOptions(
  opts?: PrismaOptionsInput
): Promise<PrismaOptions> {
  const core = presetCore(opts);
  const dbUrl = core.dbConnection(opts);
  const prisma: PrismaOptions = presetPrisma({
    ...opts,
    prisma: { ...opts?.prisma, dbUrl },
  });

  if (!prisma.prisma) {
    prisma.prisma = core.prisma(prisma, opts?.contextStorage);
    if (prisma.prisma) {
      if (prisma.middlewares?.rls)
        await core.useRlsMiddleware({ ...opts, prisma });
    }
  }

  return prisma;
}
