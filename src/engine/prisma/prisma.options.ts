export type PrismaOptions = {
  dbUrl?: string;
  isLogged?: boolean;
  prisma?: any; // Prisma instance
  middlewares?: {
    rls?: boolean;
  };
};
