import constants from '../../constants';
import { presetPrisma } from '../prisma.preset';
import { userRlsMiddlewareOptionsInput as useRlsMiddlewareOptionsInput } from './useRlsMiddleware.options.input';
import { presetTenant } from '../../tenant/tenant.preset';
import { presetStorage } from '../../storage/storage.preset';

namespace PrismaTypes {
  export type MiddlewareParams = {
    model?: any;
    action: any;
    args: any;
    dataPath: string[];
    runInTransaction: boolean;
  };
}

const RLS = Symbol('RLS');

export async function useRlsMiddleware(opts?: useRlsMiddlewareOptionsInput) {
  const { prisma } = presetPrisma(opts);

  if (!prisma) throw new Error('prisma should be set');

  const tenant = presetTenant(opts);
  const storage = presetStorage(opts);

  const currentTenantParamName =
    tenant.pgCurrentTenantParamName ||
    constants.TENANT.PG_CURRENT_TENANT_PARAM_NAME;

  const forceTenantId = tenant.forceTenantId;

  // wrap func $transaction<P extends PrismaPromise<any>[]>(arg: [...P]): Promise<UnwrapTuple<P>>;
  if (!prisma[RLS]) {
    prisma[RLS] = true;
    // save base method
    const $BASE_TRANSACTION: any = prisma.$transaction.bind(prisma);
    // override $transaction
    prisma.$transaction = async (...arg: any) => {
      //
      if (!Array.isArray(arg[0])) return $BASE_TRANSACTION(...arg);

      const tenantId = forceTenantId ?? storage.getStore()?.tenantId;

      if (!tenantId) throw new Error('tenant_id should be set');
      const values = arg[0];
      const SQL_SET_LOCAL = `SET LOCAL ${currentTenantParamName} TO '${tenantId}'`;
      const setLocal = prisma.$queryRawUnsafe(SQL_SET_LOCAL);
      const results: any[] = await $BASE_TRANSACTION([setLocal, ...values]);
      results.shift();
      return results;
    };
  }

  const rls = async (
    params: PrismaTypes.MiddlewareParams,
    next: (params: PrismaTypes.MiddlewareParams) => Promise<any>
  ) => {
    if (params.runInTransaction) return next(params);

    switch (params.action) {
      case 'queryRaw':
        const values = params.args?.parameters?.values || '[]';
        const vals = JSON.parse(values);
        const queryRaw = prisma.$queryRawUnsafe(params.args.query, ...vals);
        const r = await prisma.$transaction([queryRaw]);
        return r[0];
      case 'executeRaw':
        const values1 = params.args?.parameters?.values || '[]';
        const vals1 = JSON.parse(values1);
        const executeRaw = prisma.$executeRawUnsafe(
          params.args.query,
          ...vals1
        );
        const r1 = await prisma.$transaction([executeRaw]);
        return r1[0];

      default:
        const model = params.model;
        if (model == null) return next(params);
        // Generate model class name from model params (PascalCase to camelCase)
        const modelName = model.charAt(0).toLowerCase() + model.slice(1);
        // @ts-ignore
        const prismaAction = prisma[modelName][params.action](params.args);

        const res = await prisma.$transaction([prismaAction]);
        return res[0];
    }
  };

  prisma.$use(rls);
}
