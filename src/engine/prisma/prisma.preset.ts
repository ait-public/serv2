import { PrismaOptions } from '../options';

export function presetPrisma(opts?: { prisma?: PrismaOptions }): PrismaOptions {
  const prisma = opts?.prisma;
  return {
    ...prisma,
    middlewares: {
      ...prisma?.middlewares,
    },
  };
}
