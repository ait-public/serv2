import {
  DbConnectionOptions,
  DbOptions,
  PrismaOptions,
  userRlsMiddlewareOptionsInput,
} from '../options';
import { IContextStorage } from '../storage/IContextStorage';
import { TenantOptions } from '../tenant/tenant.options';

export type PrismaOptionsInput = {
  database?: DbOptions;
  mode?: {
    isTest?: boolean;
    /** if true direct connection AIT_POSTGRES_PORT=5432 else AIT_PGBOUNCER_PORT=6432 */
    isMigration?: boolean;
    /** if true  AIT_POSTGRES_USER_TENANT = "tenant" else AIT_POSTGRES_USER="ait" */
    isTenant?: boolean;
  };
  root?: {
    schema?: string;
  };
  prisma?: PrismaOptions;

  tenant?: TenantOptions;
  contextStorage?: IContextStorage;

  core?: {
    dbConnection?: (opts?: DbConnectionOptions) => string;
    prisma?: (opts?: PrismaOptions, storage?: IContextStorage) => any;
    rlsMiddleware?: (
      opts?: userRlsMiddlewareOptionsInput
    ) => (params: any, next: (params: any) => Promise<any>) => Promise<any>;
  };
};
