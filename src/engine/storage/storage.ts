import { AsyncLocalStorage } from 'async_hooks';
import { TenantContext } from '../Context';
import { IContextStorage } from './IContextStorage';

const storage: IContextStorage = new AsyncLocalStorage<TenantContext>();

export default storage;
