import { IContextStorage } from './IContextStorage';
import storage from './storage';

export function presetStorage(opts?: {
  contextStorage?: IContextStorage;
}): IContextStorage {
  return opts?.contextStorage ?? storage;
}
