import { TenantContext } from '../Context';

/**AsyncLocalStorage wrapper */
export interface IContextStorage {
  /**
   * This method returns the current store. If this method is called outside of an
   * asynchronous context initialized by calling `asyncLocalStorage.run`, it will
   * return `undefined`.
   */
  getStore(): TenantContext | undefined;
  /**
   * This methods runs a functi
   * on synchronously within a context and return its
   * return value. The store is not accessible outside of the callback function or
   * the asynchronous operations created within the callback.
   *
   * Optionally, arguments can be passed to the function. They will be passed to the
   * callback function.
   *
   * I the callback function throws an error, it will be thrown by `run` too. The
   * stacktrace will not be impacted by this call and the context will be exited.
   */
  run: (
    state: TenantContext,
    callback: (...args: any[]) => any,
    ...args: any[]
  ) => any;
}
