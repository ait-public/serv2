export type RootOptions = {
  schema?: string;
  externalUrl?: string;
  /** ENV production version */
  appVersion?: string;
  /** npm server package.json */
  version?: string;
  mailTestAddress?: string;
};
