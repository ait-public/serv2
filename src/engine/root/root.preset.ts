import constants from '../constants';
import { RootOptions } from '../options';
import { presetMode } from '../mode/mode.preset';

export function presetRoot(opts?: {
  root?: RootOptions;
  mode?: { isTest?: boolean };
}): RootOptions {
  const mode = presetMode(opts);
  const isTest = mode.isTest;

  const schema =
    opts?.root?.schema ?? constants.ENV.APP_SCHEMA ?? constants.ROOT.SCHEMA;

  const externalUrl =
    opts?.root?.externalUrl ??
    constants.ENV.AIT_EXTERNAL_URL ??
    constants.ROOT.AIT_EXTERNAL_URL;

  const mailTestAddress =
    opts?.root?.mailTestAddress ?? isTest
      ? constants.ENV.AIT_MAIL_TEST_ADDRESS ??
        constants.ROOT.AIT_MAIL_TEST_ADDRESS
      : constants.ENV.AIT_MAIL_TEST_ADDRESS;

  const appVersion =
    opts?.root?.appVersion ??
    constants.ENV.AIT_APP_VERSION ??
    constants.ROOT.AIT_APP_VERSION;

  const version = opts?.root?.version ?? constants.ENV.npm_package_version;

  return {
    schema,
    externalUrl,
    appVersion,
    version,
    mailTestAddress,
    ...opts?.root,
  };
}
