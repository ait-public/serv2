export default {
  PORT: 3011,
  PUBLIC_DIR: 'public',
  JSON_LIMIT: '50mb',
};
