export default {
  SCHEMA: 'ait',
  AIT_EXTERNAL_URL: 'https://medportal-demo.ru',
  AIT_APP_VERSION: '',
  AIT_MAIL_TEST_ADDRESS: 'support@ait.ru',
};
