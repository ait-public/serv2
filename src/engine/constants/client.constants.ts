export default {
  INGRESS_REWRITE_SCHEMAS: [
    'fserv',
    'cron',
    'xqconv',
    'egisz',
    'printx',
    'cmon',
    'bash',
  ],
  ROLE: 'SYSTEM',
  EXP: 52560000,
};
