const ISSUER = 'ev2.ru';
const SECRET = 'ait+ait+ait+ait+';

export default {
  ISSUER,
  SECRET,
  ANONYM: 'ANONYM',

  TOKEN: {
    SECRET,
    EXPIRES_IN: 60 * 60 * 24,
  },
  /**
   * Configuration of refresh token.
   * expiresIn - The time in minutes before the code token expires.  Default is 100 years.  Most if
   *             all refresh tokens are expected to not expire.  However, I give it a very long shelf
   *             life instead.
   */
  REFRESH_TOKEN: {
    SECRET,
    EXPIRES_IN: 52560000,
  },
  /**
   * Configuration of code token.
   * expiresIn - The time in minutes before the code token expires.  Default is 5 minutes.
   */
  CODE_TOKEN: {
    EXPIRES_IN: 5 * 60,
  },

  // /**
  //  * Session configuration
  //  *
  //  * maxAge - The maximum age in milliseconds of the session.  Use null for web browser session only.
  //  *          Use something else large like 3600000 * 24 * 7 * 52 for a year.
  //  * secret - The session secret that you should change to what you want
  //  */
  // session: {
  //     maxAge: 3600000 * 24 * 7 * 52,
  //     secret: 'A Secret That Should Be Changed', // TODO: You need to change this secret to something that you choose for your secret
  // },
};
