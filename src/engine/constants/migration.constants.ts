export default {
  PRISMA_DEPLOY_CMD: 'migrate deploy',
  PRISMA_VERSION_CMD: '-v',
};
