export default {
  typeDefs: 'type Query{hello:String} type Mutation{ping:String}',
  resolvers: [
    {
      Query: { hello: () => 'Hi!' },
      Mutation: { ping: () => 'Pong' },
    },
  ],
};
