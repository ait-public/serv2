import ROOT from './root.constants';
import JWT from './jwt.constants';
import APOLLO from './apollo.constants';
import SERVER from './server.constants';
import DEV from './dev.constants';
import SHIELD from '../shield/shield.constants';
import SCHEMA from './schema.constants';
import ENV from './env.constants';
import MIGRATION from './migration.constants';
import CLIENT from './client.constants';
import TENANT from './tenant.constants';
import SETTINGS from './settings.constants';

export default {
  ROOT,
  JWT,
  APOLLO,
  SERVER,
  DEV,
  SHIELD,
  SCHEMA,
  ENV,
  MIGRATION,
  CLIENT,
  TENANT,
  SETTINGS,
};
