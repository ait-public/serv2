import { ApolloServer, ExpressContext } from 'apollo-server-express';

import { IGqlClient } from '../types/index';

import {
  AppOptions,
  ExpressOptions,
  DbConnectionOptions,
  PrismaOptions,
  SchemaOptions,
  SchemaOptionsInput,
  ServerOptions,
  UrlOptions,
  GqlClientOptions,
  ShieldOptions,
  HttpHeaderOptions,
  GetUrl,
  PrismaOptionsInput,
  ClientOptionsInput,
  ClientOptions,
  SeedOptionsInput,
  DeployOptionsInput,
  MigrateOptionsInput,
  ApolloOptionsInput,
  ApolloOptions,
  ExpressOptionsInput,
  ServerOptionsInput,
  DbInstallOptionsInput,
  userRlsMiddlewareOptionsInput,
  SettingsOptionsInput,
  LookupsOptionsInput,
  LookupsOptions,
} from '../options';

import { IContextStorage } from '../storage/IContextStorage';
import { SettingsOptions } from '../settings/settings.options';
import { IPrisma } from '../_helper/ctx';

export type Core = {
  app: (opts?: AppOptions) => Promise<AppOptions>;
  config: (opts?: AppOptions) => Promise<AppOptions>;
  express: (opts?: ExpressOptionsInput) => Promise<ExpressOptions>;
  apollo: (opts?: ApolloOptionsInput) => Promise<ApolloServer<ExpressContext>>;
  prismaOptions: (opts?: PrismaOptionsInput) => Promise<PrismaOptions>;
  prisma: (opts?: PrismaOptions, storage?: IContextStorage) => IPrisma;
  dbConnection: (opts?: DbConnectionOptions) => string;
  context: (opts?: AppOptions) => Promise<ApolloOptions>;
  schema: (opts?: SchemaOptionsInput) => Promise<SchemaOptions>;
  server: (opts?: ServerOptionsInput) => Promise<ServerOptions>;
  getUrl: (opts?: UrlOptions) => Promise<GetUrl>;
  clientOptions: (opts?: ClientOptionsInput) => Promise<ClientOptions>;
  gqlClient: (opts?: GqlClientOptions) => Promise<IGqlClient>;
  shield: (opts?: SchemaOptionsInput) => ShieldOptions;
  httpHeader: (opts?: HttpHeaderOptions) => Promise<Record<string, string>>;
  migrate: (opts?: MigrateOptionsInput) => Promise<void>;
  dbInstall: (opts?: DbInstallOptionsInput) => Promise<string>;
  dbDeploy: (opts?: DeployOptionsInput) => Promise<void>;
  dbSeed: (opts?: SeedOptionsInput) => Promise<void>;
  tenants: (opts?: AppOptions) => Promise<string[]>;
  useRlsMiddleware: (opts?: userRlsMiddlewareOptionsInput) => Promise<void>;
  settings: (opts?: SettingsOptionsInput) => Promise<SettingsOptions>;
  lookups: (opts?: LookupsOptionsInput) => Promise<LookupsOptions>;
};
