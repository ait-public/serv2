import { Core } from './Core';
import { CoreOptions } from './core.options';

const dummy = (o?: any): any => {
  throw new Error('Core is not initialized!');
};

let _core: Core = {
  app: dummy,
  apollo: dummy,
  config: dummy,
  express: dummy,
  dbConnection: dummy,
  prismaOptions: dummy,
  prisma: dummy,
  context: dummy,
  schema: dummy,
  server: dummy,
  getUrl: dummy,
  clientOptions: dummy,
  gqlClient: dummy,
  shield: dummy,
  httpHeader: dummy,
  migrate: dummy,
  dbInstall: dummy,
  dbSeed: dummy,
  dbDeploy: dummy,
  tenants: dummy,
  useRlsMiddleware: dummy,
  settings: dummy,
  lookups: dummy,
};

export function initCore(core: Core): Core {
  _core = {
    ..._core,
    ...core,
  };
  return _core;
}

export function presetCore(opts?: { core?: CoreOptions }): Core {
  return {
    ..._core,
    ...opts?.core,
  };
}
