import constants from '../constants';
import { GetUrl, UrlOptions } from '../options';
import { presetDev } from '../dev/dev.preset';
import { presetMode } from '../mode/mode.preset';
import { presetRoot } from '../root/root.preset';
import { presetServer } from '../server/server.preset';

const INGRESS_REWRITE_SCHEMAS = constants.CLIENT.INGRESS_REWRITE_SCHEMAS;

export async function createUrl(opts?: UrlOptions): Promise<GetUrl> {
  const dev = presetDev(opts);
  const mode = presetMode(opts);
  const root = presetRoot({ ...opts, mode });
  const server = await presetServer({ ...opts, root, mode });

  const SCHEMA = root?.schema || constants.ROOT.SCHEMA;

  return (schema: string) => {
    schema = schema || SCHEMA;

    if (schema?.startsWith('http:') || schema?.startsWith('https:'))
      return schema;

    if (mode?.isKube) {
      if (INGRESS_REWRITE_SCHEMAS.includes(schema)) {
        return `http://everest2${schema}-service`;
      }

      return `http://everest2${schema}-service/${schema}`;
    }

    if (schema === SCHEMA) {
      return `http://localhost:${server?.port}/${schema}`;
    }

    return `${dev?.medportalUrl ?? ''}/${schema}`;
  };
}
