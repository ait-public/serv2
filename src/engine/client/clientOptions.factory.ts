import { ClientOptions } from '../options';
import { presetCore } from '../core/core.preset';
import { presetClient } from './client.preset';
import { ClientOptionsInput } from './client.options.input';

export async function createClientOptions(
  opts?: ClientOptionsInput
): Promise<ClientOptions> {
  const core = presetCore(opts);
  const headers = await core.httpHeader(opts);

  const client = presetClient(opts);
  client.headers = { ...client?.headers, ...headers };
  client.getUrl = client.getUrl ?? (await core.getUrl(opts));

  client.gqlClient =
    client.gqlClient ??
    (await core.gqlClient({
      ...opts,
      client,
      core,
    }));

  return client;
}
