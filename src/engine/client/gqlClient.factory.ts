import { GraphQLClient } from 'graphql-request';

import { IGqlClient } from '../types';
import { GqlClientOptions } from '../options';

import fetchEx from '../_helper/fetch.helper';
import { presetCore } from '../core/core.preset';
import { presetClient } from './client.preset';
import { presetStorage } from '../storage/storage.preset';
import { utils } from '@ait/share2';
import { createClientToken } from './httpHeader.factory';

export async function createGqlClient(
  opts?: GqlClientOptions
): Promise<IGqlClient> {
  const core = presetCore(opts);
  const options = await core.config(opts);

  const clientOpts = presetClient(options);
  const globalHeaders = clientOpts?.headers;
  const storage = presetStorage(options);

  // per request call
  function getHeaders(
    headers?: Record<string, string>,
    noProxyTenant?: boolean
  ): Record<string, string> {
    const xRequestId = utils.uuidv4();

    const h: any = {
      'x-request-id': xRequestId,
      'x-trace-id': xRequestId,
      Authorization: globalHeaders?.Authorization,
    };

    const tenantContext = storage.getStore();
    if (tenantContext) {
      if (tenantContext.traceId) h['x-trace-id'] = tenantContext.traceId;
      if (tenantContext.sessionId) h['x-session-id'] = tenantContext.sessionId;
      if (tenantContext.token)
        h['Authorization'] = 'Bearer ' + tenantContext.token;
    }

    if (noProxyTenant) {
      h['Authorization'] =
        'Bearer ' +
        createClientToken({
          sessionId: tenantContext?.sessionId,
          profileId: tenantContext?.profileId,
          tenantId: tenantContext?.tenantId,
          userId: tenantContext?.userId,
        });
    }

    return {
      ...globalHeaders,
      ...h,
      ...headers,
    };
  }

  const getUrl =
    options?.client?.getUrl ?? (await presetCore(options).getUrl(options));

  function get(
    schema: string,
    options?: {
      headers?: Record<string, string>;
      noProxyJwt?: boolean;
    }
  ): GraphQLClient {
    schema = schema || '';

    const url = `${getUrl(schema)}/graphql`;
    const headers = getHeaders(options?.headers, options?.noProxyJwt);

    return new GraphQLClient(url, {
      headers,
    });
  }

  function post(
    url: string,
    data: any,
    init?: {
      method?: string;
      headers?: Record<string, string>;
      contentType?: string;
      responseText?: boolean;
      noProxyJwt?: boolean;
      body?: any;
    }
  ) {
    const headers = getHeaders(init?.headers, init?.noProxyJwt);
    return fetchEx(url, data, {
      ...init,
      headers,
    });
  }

  return {
    options,
    get,
    post,
    getUrl,
  };
}
