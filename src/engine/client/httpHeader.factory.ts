import { HttpHeaderOptions } from '../options';
import constants from '../constants';
import jwtHelper, { CreateTokenInput } from '../auth/jwt.helper';
import { presetRoot } from '../root/root.preset';
import { presetMode } from '../mode/mode.preset';
import { presetAuth } from '../auth/auth.preset';
import { presetClient } from './client.preset';

/**
 * Global header
 */
export async function createHttpHeader(
  opts?: HttpHeaderOptions
): Promise<Record<string, string>> {
  const mode = presetMode(opts);
  const root = presetRoot({ ...opts, mode });
  const auth = presetAuth(opts);
  const client = presetClient(opts);

  const clientId = root?.schema ?? constants.ROOT.SCHEMA;

  const headers: Record<string, string> = {
    'x-client-id': clientId,
    ...client?.headers,
  };

  const token = await createClientToken({
    clientId,
    secret: auth?.token?.secret,
  });

  if (token) {
    headers['Authorization'] = `Bearer ${token}`;
  }

  return headers;
}

export async function createClientToken(opts?: CreateTokenInput) {
  return jwtHelper.createToken({
    ...opts,
    role: constants.CLIENT.ROLE,
    exp: constants.CLIENT.EXP,
  });
}
