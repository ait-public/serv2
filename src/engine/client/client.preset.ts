import { ClientOptions } from '../options';

export function presetClient(opts?: { client?: ClientOptions }): ClientOptions {
  return {
    ...opts?.client,
    headers: opts?.client?.headers,
  };
}
