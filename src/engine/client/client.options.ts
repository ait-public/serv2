import { GetUrl } from '../options';
import { IGqlClient } from '../types';

export type ClientOptions = {
  headers?: Record<string, string>;
  gqlClient?: IGqlClient;
  getUrl?: GetUrl;
};
