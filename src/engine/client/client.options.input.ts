import { AuthOptions, GetUrl, HttpHeaderOptions, UrlOptions } from '../options';
import { IGqlClient } from '../types';

export type ClientOptionsInput = {
  root?: {
    schema?: string;
  };
  mode?: {
    isTest?: boolean;
    isKube?: boolean;
  };
  dev?: {
    medportalUrl?: string;
  };
  server?: {
    port?: string | number;
  };
  auth?: AuthOptions;
  client?: {
    headers?: Record<string, string>;
    gqlClient?: IGqlClient;
    getUrl?: GetUrl;
  };
  core?: {
    httpHeader?: (opts?: HttpHeaderOptions) => Promise<Record<string, string>>;
    getUrl?: (opts?: UrlOptions) => Promise<GetUrl>;
  };
};
