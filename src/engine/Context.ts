import DataLoader from 'dataloader';
import { GraphQLSchema } from 'graphql';
import { AppOptions } from './options';
import { SettingValue } from './settings/settings.options';
import { IGqlClient } from './types';

export type WeakDataLooader = WeakMap<object, DataLoader<string, object>>;

/** Seed db Context */
export type SeedContext<T> = {
  /**  db access */
  prisma: T;
  /**  db connection string */
  dbUrl: string;
  /** lpu tenant id */
  tenantId: string;
  /** all tenant ids */
  tenantIds?: string[];
  /** last migration name */
  migrationName: string;
  /** App env */
  options?: AppOptions;
  /** tenant state */
  tenant?: TenantContext;
  /** Tenant settings */
  settings?: Record<string, SettingValue>;
};

/** TenantState by request headers */
export type TenantContext = {
  /**
   * lpu tenant id
   */
  tenantId?: string;
  /**
   * current client - app id
   */
  clientId?: string;
  /**
   * per call Id
   */
  requestId?: string;
  /**
   * first request id
   */
  traceId?: string;
  /**
   * session id by login
   */
  sessionId?: string;
  /** user role */
  role?: string;
  /** user id */
  userId?: string;
  /**
   * profile Id
   */
  profileId?: string;
  /** JWT token */
  token?: string;
  /**
   * request ip
   */
  ip?: string;
};

/**
 * Apollo Context
 */
export type Context<T> = {
  /** Текущая объединенная схема */
  schema?: GraphQLSchema;
  /**  db access */
  prisma: T;
  // // current request
  // request: express.Request;
  /** DataLoader — правильно решаем проблему N+1 запросов - https://github.com/nodkz/conf-talks/tree/master/articles/graphql/dataloader */
  dataloaders: WeakDataLooader;
  /**  tenant state */
  tenant?: TenantContext;
  /**  tenantId from tenant context */
  tenantId?: string;
  /**  userId or clientId defined */
  isAuthenticated: boolean;
  /**  copy from tenat*/
  userId?: string;
  /**  role from tenant state if empty ANONYM */
  role: string;
  /**  config*/
  options: AppOptions;
  /**  paljs - supporting */
  select: any;
  /** store Map - for custom used*/
  store: Map<string, any>;
  /**  db connection string */
  dbUrl?: string;
  /**  gql client */
  gqlClient: IGqlClient;
  /** load settings by tenant */
  getSettings: () => Promise<Record<string, SettingValue>>;
};
