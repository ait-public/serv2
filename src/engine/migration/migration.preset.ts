import constants from '../constants';
import { MigrationOptions } from '../options';

export function presetMigration(opts?: {
  migration?: MigrationOptions<any>;
}): MigrationOptions<any> {
  const cmdDeploy =
    opts?.migration?.cmdDeploy ?? constants.MIGRATION.PRISMA_DEPLOY_CMD;
  const cmdVersion =
    opts?.migration?.cmdVersion ?? constants.MIGRATION.PRISMA_VERSION_CMD;

  return {
    ...opts?.migration,
    cmdDeploy,
    cmdVersion,
  };
}
