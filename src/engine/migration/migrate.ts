import { presetCore } from '../core/core.preset';
import { MigrateOptionsInput } from './migrate.options.input';
import { afterDeploy } from './after/afterDeploy';

export async function migrate(opts?: MigrateOptionsInput) {
  const core = presetCore(opts);
  const options = await core.config(opts);

  console.log(`----- Start migration: ${options.prisma?.dbUrl} ------`);
  try {
    await core.dbInstall(options);
    await core.dbDeploy(options);

    // *******************************
    // SET GRANT for Tenant user
    // *******************************
    await afterDeploy(options);

    await core.dbSeed(options);
    console.log('MIGRATE - OK');
  } catch (e) {
    console.error('MIGRATE - FAIL', e);
    throw e;
  } finally {
    console.log('----- End migration ------');
  }
}
