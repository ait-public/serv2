import { presetMigration } from '../migration.preset';
import { presetCore } from '../../core/core.preset';
import { SeedOptionsInput } from './seed.options.input';
import helper from '../../_helper';

export async function dbSeed(opts?: SeedOptionsInput) {
  const core = presetCore(opts);
  const options = await core.config(opts);
  const migration = presetMigration(options);

  const settings = await core.settings(options);

  options.settings = settings;

  // const isSeed =
  //   migration?.seed || migration?.seedPlatform || migration.seedTenant;

  // if (!isSeed) {
  //   console.log('SEED - IGNORE...');
  //   return;
  // }

  try {
    console.log('----- Start SEED');
    const schema = options.root?.schema || '.';

    const getSettings = settings?.provider?.getSettings;

    const settingKeys = getSettings
      ? Object.keys(settings?.keys || {}).filter((c) => c.startsWith(schema))
      : [];

    const tenantIds = await core.tenants(options);
    const platformTenantId = options.tenant?.platformTenantId || '';

    const migrationName = await helper.ctx.inAitScope(
      options,
      platformTenantId,
      async ({ dbUrl, prisma, tenant, tenantId }) => {
        let migrName: string = '';

        if (!migration?.skipDeploy) {
          try {
            if (await helper.db.checkSchema(dbUrl)) {
              const migrationInfo = await helper.db.lastMigration(dbUrl);
              migrName = migrationInfo?.migration_name || '';
            }
          } catch {
            console.error('Ошибка получения версии', dbUrl);
          }
        }

        if (migration.seed) {
          console.log(`----- ----- seed AIT "${dbUrl}"`);
          await migration.seed({
            dbUrl,
            prisma,
            migrationName: migrName,
            options,
            tenant,
            tenantId,
            tenantIds,
          });
          console.log('----- ----- seed AIT - OK');
        }

        return migrName;
      }
    );

    if (migration.seedPlatform || settingKeys.length) {
      await helper.ctx.inTenantScope(
        options,
        platformTenantId,
        async ({ dbUrl, prisma, tenant, tenantId }) => {
          console.log(`----- ----- seed PLATFORM "${dbUrl}"`);

          const setts = getSettings ? await getSettings() : {};

          if (migration.seedPlatform) {
            await migration.seedPlatform({
              dbUrl,
              migrationName,
              prisma,
              tenantId,
              options,
              tenant,
              tenantIds,
              settings: setts,
            });
          }
          console.log('----- -----  OK');
        }
      );
    }

    const len = tenantIds.length;
    if (len && (migration.seedTenant || settingKeys.length)) {
      console.log(`----- ----- seed TENANTS = ${len}`);
      for (let index = 0; index < len; index++) {
        await helper.ctx.inTenantScope(
          options,
          tenantIds[index],
          async ({ dbUrl, prisma, tenant, tenantId }) => {
            console.log(
              `----- ----- ----- seed ${index + 1}/${len} "${tenantId}`
            );

            const setts = getSettings ? await getSettings() : {};

            if (migration.seedTenant) {
              await migration.seedTenant({
                dbUrl,
                migrationName,
                prisma,
                tenantId,
                options,
                tenant,
                tenantIds,
                settings: setts,
              });
            }
            console.log('----- ----- -----  OK');
          }
        );
      }
    }

    console.log('----- SEED - OK');
  } catch (e) {
    console.error('SEED - FAIL', e);
    throw e;
  }
}
