import helper from '../../_helper';

export async function grantTableTenant(
  dbUrl: string,
  schema: string,
  tables: string[]
) {
  console.log('ait_tenant_table_grant', tables);

  for (let index = 0; index < tables.length; index++) {
    const t = tables[index];
    await helper.db.query({
      dbUrl,
      queryText: `CALL public.ait_tenant_table_grant('${schema}','${t}')`,
    });
  }
}
