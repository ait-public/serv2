import helper from '../../_helper';

export async function grantSchemaTenant(dbUrl: string, schema: string) {
  await helper.db.query({
    dbUrl,
    queryText: `CALL public.ait_tenant_schema_grant('${schema}')`,
  });
}
