import helper from '../../_helper';
import { AppOptions } from '../../options';
import { grantSchemaTenant } from './grantSchemaTenant';
import { grantTableTenant } from './grantTableTenant';

export async function grantTenant(dbUrl: string, options: AppOptions) {
  const schema = options.database?.schema;

  if (schema && (await helper.db.checkSchema(dbUrl, schema))) {
    await grantSchemaTenant(dbUrl, schema);

    const models = options.prisma?.prisma?._dmmf?.datamodel?.models;

    if (Array.isArray(models)) {
      const tables: string[] = models.map((c: any) => c.dbName || c.name);

      await grantTableTenant(dbUrl, schema, tables);
    }
  }
}
