import { presetCore } from '../../core/core.preset';
import { AppOptions } from '../../options';
import { grantTenant } from './grantTenant';

export async function afterDeploy(options: AppOptions) {
  const core = presetCore(options);

  const dbUrl = core.dbConnection({
    ...options,
    database: { ...options.database, schema: 'public' },
    mode: { isMigration: true, isTenant: false },
  });

  await grantTenant(dbUrl, options);
}
