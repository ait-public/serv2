import { presetCore } from '../../core/core.preset';
import { presetDatabase } from '../../database/database.preset';
import { DbInstallOptionsInput } from './dbInstall.options.input';
import execSql from './execSql';

export async function dbInstall(opts?: DbInstallOptionsInput): Promise<string> {
  const core = presetCore(opts);
  const database = presetDatabase(opts);
  // set AIT + off bouncer
  let dbUrl = core.dbConnection({
    ...opts,
    mode: { ...opts?.mode, isTenant: false, isMigration: true },
    database: { ...database, pgbouncer: undefined, schema: 'public' },
  });

  await execSql({
    dbUrl,
    passwordTenant: database.passwordTenant || 'tenant',
    userTenant: database.userTenant || 'tenant',
  });

  return dbUrl;
}
