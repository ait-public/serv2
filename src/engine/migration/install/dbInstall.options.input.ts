import { DbConnectionOptions, DbOptions } from '../../options';

export type DbInstallOptionsInput = {
  mode?: { isTest?: boolean };
  root?: { schema?: string };
  database?: DbOptions;
  core?: {
    dbConnection?: (opts?: DbConnectionOptions) => string;
  };
};
