export default () => `
-- ----------------------------------------
  CREATE SCHEMA IF NOT EXISTS core2;

  CREATE TABLE IF NOT EXISTS core2."Setting" (
    id text NOT NULL,
    value text NULL,
    "label" text NULL,
    note text NULL,
    meta jsonb NULL,
    sort int4 NULL DEFAULT 0,
    public bool NOT NULL DEFAULT false,
    "createdAt" timestamp(3) NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" timestamp(3) NULL,
    "isDisabled" bool NULL DEFAULT false,
    "key" text NOT NULL DEFAULT 'dummy'::text,
    tenant_id uuid NOT NULL DEFAULT ait_default_tenant(),
    CONSTRAINT "Setting_pkey" PRIMARY KEY (id)
  );

  CREATE UNIQUE INDEX  IF NOT EXISTS  "Setting_key_tenantId_unique_constraint" ON core2."Setting" USING btree (tenant_id, key);
  CREATE INDEX  IF NOT EXISTS "Setting_tenant_id_idx" ON core2."Setting" USING btree (tenant_id);

  -- Permissions
  CALL public.ait_tenant_schema_grant('core2');
  CALL public.ait_tenant_table_grant('core2','"Setting"');

`;
