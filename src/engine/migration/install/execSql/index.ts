import helper from '../../../_helper';
import sql_0 from './sql.0';
import sql_1 from './sql.1';

export default async function (opts: {
  dbUrl: string;
  userTenant: string;
  passwordTenant: string;
}) {
  let result = 1;
  const dbUrl = opts.dbUrl;
  const hasInstallation = await helper.db.checkAitInstallation(dbUrl);

  if (!hasInstallation) {
    console.log(`----- INSTALL - "${dbUrl}" START...`);
    try {
      const queryText = sql_0();
      console.log(queryText);
      await helper.db.query({ dbUrl, queryText });

      await helper.db.query({
        dbUrl,
        queryText: `CALL public.ait_tenant_role_check('${opts.userTenant}','${opts.passwordTenant}')`,
      });

      console.log(`----- INSTALL - OK`);
    } catch (e) {
      console.error('INSTALL', e);
      throw new Error('Ошибка инсталяции');
    }
    result = 2;
  }

  const hasCore2 = await helper.db.checkSchema(dbUrl, 'core2');

  if (!hasCore2) {
    console.log(`----- INIT CORE2 START...`);
    try {
      const queryText = sql_1();
      console.log(queryText);
      await helper.db.query({ dbUrl, queryText });
      console.log(`----- INIT - OK`);
    } catch (e) {
      console.error('INIT CORE', e);
      throw new Error('Ошибка установки core2');
    }

    result = 4;
  }

  return result;
}
