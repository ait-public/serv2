export default () => `

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE OR REPLACE PROCEDURE public.ait_tenant_role_check(rolename text, psw text)
 LANGUAGE plpgsql
AS $$
begin
	IF NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = rolename) THEN
		EXECUTE format('CREATE ROLE %s NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN PASSWORD ''%s'' NOREPLICATION NOBYPASSRLS', rolename, psw);
	END IF;
end
$$
;


create or replace function public.ait_default_tenant()
	returns uuid
	immutable
	language plpgsql
as
$$
begin
   return current_setting('app.current_tenant')::uuid;
end;
$$;


CREATE OR REPLACE PROCEDURE public.ait_tenant_schema_grant(schema_name text) AS $$
DECLARE
	sql varchar(1024);
	tenant_user text := 'tenant';
BEGIN
    RAISE NOTICE USING MESSAGE=format('Granting schema: %s', schema_name);

	-- grant access to schema for tenant user (CREATE | USAGE)
	sql := format('GRANT ALL ON SCHEMA %s TO %s;', schema_name, tenant_user);
    RAISE NOTICE USING MESSAGE=sql;
	EXECUTE(sql);

	-- grant access to tables for tenant user
	sql := format('GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA %s TO %s;', schema_name, tenant_user);
    RAISE NOTICE USING MESSAGE=sql;
	EXECUTE(sql);
END
$$

LANGUAGE plpgsql;

create or replace function public.ait_rls_check_dict(tenant_id uuid)
  returns bool
  stable
  cost 100000
as
$$
  select case
      when tenant_id::text = current_setting('app.current_tenant')::text
          or tenant_id::text = '00000000-0000-0000-1000-000000000001' --current_setting('app.platform')::text
      then true
      else false
  end;
$$
language sql;

CREATE OR REPLACE PROCEDURE public.ait_tenant_table_grant(p_schemaname text, p_tablename text)
    LANGUAGE plpgsql
AS $procedure$
DECLARE
    sql varchar(1024);
BEGIN

    IF NOT EXISTS (SELECT 1
        FROM  information_schema.columns
            join information_schema.tables t on t.table_schema = columns.table_schema and t.table_name = columns.table_name and t.table_type <> 'VIEW'
        WHERE  columns.table_schema = p_schemaname
        AND  columns.table_name = p_tablename
        AND  columns.column_name = 'tenant_id') THEN
            RETURN;
    END IF;

    sql := format('ALTER TABLE %s."%s" ENABLE ROW LEVEL SECURITY;', p_schemaname, p_tablename);
    RAISE NOTICE USING MESSAGE=sql;
    EXECUTE(sql);
    IF NOT EXISTS (select 1 from pg_policies where schemaname = p_schemaname and tablename = p_tablename and policyname = 'tenant_isolation_policy') THEN
        IF (p_schemaname = 'dict2') THEN
            sql := format('CREATE POLICY tenant_isolation_policy ON %s."%s" USING (public.ait_rls_check_dict(tenant_id));', p_schemaname, p_tablename);
        ELSE
            sql := format('CREATE POLICY tenant_isolation_policy ON %s."%s" USING (tenant_id = current_setting(''app.current_tenant'')::uuid);', p_schemaname, p_tablename);
        END IF;
        RAISE NOTICE USING MESSAGE=sql;
        EXECUTE(sql);
    END IF;
END
$procedure$;
;

`;
