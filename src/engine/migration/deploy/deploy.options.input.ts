import {
  AppOptions,
  DbConnectionOptions,
  DbOptions,
  MigrationOptions,
} from '../../options';

export type DeployOptionsInput = {
  mode?: { isTest?: boolean; isMigration?: boolean; isTenant?: boolean };
  root?: { schema?: string };
  migration?: MigrationOptions<any>;
  database?: DbOptions;
  core?: {
    config?: (opts?: AppOptions) => Promise<AppOptions>;
    dbConnection?: (opts?: DbConnectionOptions) => string;
  };
};
