import { presetMigration } from '../migration.preset';
import { prismaVersion } from '../../prisma/npx/prismaVersion';
import { prismaDeploy } from '../../prisma/npx/prismaDeploy';
import { DeployOptionsInput } from './deploy.options.input';
import { presetCore } from '../../core/core.preset';

export async function dbDeploy(opts?: DeployOptionsInput) {
  // set AIT Connection + bouncer off
  opts = {
    ...opts,
    mode: { ...opts?.mode, isTenant: false, isMigration: true },
  };
  const core = presetCore(opts);
  const options = await core.config(opts);
  const migration = presetMigration(options);
  const dbUrl = core.dbConnection(options);

  if (migration?.skipDeploy) {
    console.log('db deploy - SKIPPED...');
  } else {
    const v = await prismaVersion({ cmdVersion: migration?.cmdVersion });
    if (!v) throw new Error('db version - FAIL');
    console.log(`----- ----- DEPLOY - "${dbUrl}" START...`);
    const up = await prismaDeploy({
      dbUrl,
      cmdDeploy: migration?.cmdDeploy,
    });
    if (!up) throw new Error('db deploy - FAIL');
    console.log(`----- ----- DEPLOY - OK`);
  }
}
