import { AppOptions } from '../options';

export type MigrateOptionsInput = AppOptions;
