import { presetApp } from '../app/app.preset';
import { AppOptions } from '../app/app.options';

export function createConfig(opts?: AppOptions): Promise<AppOptions> {
  return presetApp(opts);
}
