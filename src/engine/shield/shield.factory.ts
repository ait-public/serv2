import { shield } from 'graphql-shield';
import { ShieldOptions } from './shield.options';
import { presetShield } from './shield.preset';

export function createShield(opts?: { shield?: ShieldOptions }): ShieldOptions {
  const _scheld = presetShield(opts);
  const permissions =
    _scheld.permissions ?? shield(_scheld.ruleTree ?? {}, _scheld.options);
  _scheld.permissions = permissions;
  return _scheld;
}
