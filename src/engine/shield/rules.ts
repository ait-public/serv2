import { or, rule } from 'graphql-shield';

export const isAuthenticated = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => ctx.isAuthenticated
);

export const isModeDev = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => !!ctx.options.mode?.isDev
);

export const isModeProd = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => !!ctx.options.mode?.isProd
);

export const isModeTest = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => !!ctx.options.mode?.isTest
);

export const isRoleSystem = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => ctx.role === 'SYSTEM'
);

export const isRoleSysAdmin = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => ctx.role === 'SYSADMIN'
);

export const isRoleAdmin = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => ctx.role === 'ADMIN'
);

export const isRoleEmployee = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => ctx.role === 'EMPLOYEE'
);

export const isRolePatient = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => ctx.role === 'PATIENT'
);

export const isRoleUser = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => ctx.role === 'USER'
);

export const isRoleAnonym = rule({ cache: 'contextual' })(
  async (parent, args, ctx, info) => !ctx.role || ctx.role === 'ANONYM'
);

export const isRoleSysAdminOrMore = or(isRoleSysAdmin, isRoleSystem);

export const isRoleAdminOrMore = or(isRoleAdmin, isRoleSysAdmin, isRoleSystem);

export const isRoleEmployeeOrMore = or(
  isRoleEmployee,
  isRoleAdmin,
  isRoleSysAdmin,
  isRoleSystem
);

export const isRolePatientOrMore = or(
  isRolePatient,
  isRoleEmployee,
  isRoleAdmin,
  isRoleSysAdmin,
  isRoleSystem
);

export const isRoleUserOrMore = or(
  isRoleUser,
  isRolePatient,
  isRoleEmployee,
  isRoleAdmin,
  isRoleSysAdmin,
  isRoleSystem
);
