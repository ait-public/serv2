import { ShieldOptions } from '../options';

export function presetShield(opts?: { shield?: ShieldOptions }): ShieldOptions {
  return {
    ruleTree: {},
    ...opts?.shield,
  };
}
