import { ShieldOptions } from './shield.options';

export type ShieldOptionsInput = {
  shield?: ShieldOptions;
};
