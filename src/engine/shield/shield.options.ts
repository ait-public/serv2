import { GraphQLResolveInfo } from 'graphql';
import { IMiddlewareGenerator } from 'graphql-middleware';
import { IRules } from 'graphql-shield';

export declare type IFallbackErrorMapperType = (
  err: unknown,
  parent: object,
  args: object,
  ctx: any,
  info: GraphQLResolveInfo
) => Promise<Error> | Error;
export declare type IFallbackErrorType = Error | IFallbackErrorMapperType;
export declare type IHashFunction = (arg: { parent: any; args: any }) => string;

export interface IOptions {
  debug?: boolean;
  allowExternalErrors?: boolean;
  fallbackRule?: any;
  fallbackError?: IFallbackErrorType;
  hashFunction?: IHashFunction;
}

export type ShieldOptions = {
  ruleTree?: IRules;
  options?: IOptions;
  permissions?: IMiddlewareGenerator<any, any, any>;
};
