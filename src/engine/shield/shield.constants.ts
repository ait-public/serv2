import { or } from 'graphql-shield';
import { isAuthenticated, isModeTest, isModeDev } from './rules';

const ruleAllow = or(isModeDev, isModeTest, isAuthenticated);

export default {
  ruleTree: {
    Query: {
      '*': ruleAllow,
    },
    Mutation: {
      '*': ruleAllow,
    },
  },
  options: {
    debug: true,
    allowExternalErrors: true,
  },
};
