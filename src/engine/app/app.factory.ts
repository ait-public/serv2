import { presetCore } from '../core/core.preset';

import helper from '../_helper';
import { AppOptions } from './app.options';

export async function createApp(opts?: AppOptions): Promise<AppOptions> {
  const core = presetCore(opts);
  // load congif
  const options = await core.config(opts);

  // db access ---------------------------
  const { prisma, dbUrl } = (options.prisma = await core.prismaOptions(
    options
  ));
  // http access --------------------------
  options.client = await core.clientOptions(options);

  options.server = await core.server(options);

  const appServer = options.server.appServer;
  if (!appServer) throw new Error('appServer not initialized');

  const { isMigration, isTest } = options.mode || {};
  const _run = appServer.run;

  // override run
  appServer.run = async (
    opts?: any,
    listeningListener?: (opts: any) => void
  ) => {
    if (isMigration) {
      if (!isTest) {
        listeningListener && listeningListener({});
      }
      try {
        await core.migrate(options);
      } catch (e) {
        if (!isTest) {
          console.error(e, () => process.exit(1));
        } else {
          console.error(e);
          throw e;
        }
      }
    }

    if (isTest || !isMigration) {
      await _run(null, listeningListener);
    }

    if (isMigration && !isTest) {
      await appServer.stop();
    }

    return options;
  };

  const _stop = appServer.stop;

  appServer.stop = async () => {
    if (prisma?.$disconnect) await prisma.$disconnect();

    if (_stop) await _stop();
    if (isTest) {
      if (!options?.test?.nodrop && options?.mode?.isMigration && dbUrl) {
        if (await helper.db.checkSchema(dbUrl))
          await helper.db.dropSchema(dbUrl);
      }
    }
  };

  const app = options.server?.express?.app;
  const baseUrl = options.server?.express?.baseUrl;

  if (!app) throw new Error('express mot initialized!');

  app.all(`${baseUrl}/api/ping`, async function (req: any, res: any) {
    try {
      if (!dbUrl) throw new Error('dbUrl not initialized');
      const r = await helper.db.ping(dbUrl);
      res.json(r);
    } catch (e) {
      const error = e instanceof Error ? e?.message : e + '';
      console.error(error);
      res.status(500).json({ error });
    }
  });

  app.all(`${baseUrl}/api/version`, async function (req: any, res: any) {
    try {
      if (!dbUrl) throw new Error('dbUrl not initialized');
      const r = await helper.db.lastMigration(dbUrl);
      res.json(r);
    } catch (e) {
      const error = e instanceof Error ? e?.message : e + '';
      console.error(error);
      res.status(500).json({ error });
    }
  });

  return options;
}
