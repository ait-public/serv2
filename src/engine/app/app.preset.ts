import { AppOptions } from '../options';
import { presetAuth } from '../auth/auth.preset';
import { presetDatabase } from '../database/database.preset';
import { presetDev } from '../dev/dev.preset';
import { presetLog } from '../log/log.preset';
import { presetMigration } from '../migration/migration.preset';
import { presetMode } from '../mode/mode.preset';
import { presetPrisma } from '../prisma/prisma.preset';
import { presetRoot } from '../root/root.preset';
import { presetSchema } from '../schema/schema.preset';
import { presetServer } from '../server/server.preset';
import { presetShield } from '../shield/shield.preset';
import { presetTest } from '../testing/test.preset';
import { presetCore } from '../core/core.preset';
import { presetTenant } from '../tenant/tenant.preset';
import { presetStorage } from '../storage/storage.preset';
import { presetSettings } from '../settings/settings.preset';
import { presetLookups } from '../lookups/lookups.preset';

export async function presetApp(opts?: AppOptions): Promise<AppOptions> {
  const mode = presetMode(opts);
  const auth = presetAuth(opts);
  const log = presetLog(opts);
  const dev = presetDev(opts);
  const test = presetTest(opts);
  const prisma = presetPrisma(opts);
  const shield = presetShield(opts);
  const migration = presetMigration(opts);
  const core = presetCore(opts);
  const tenant = presetTenant(opts);
  const contextStorage = presetStorage(opts);
  const settings = await presetSettings(opts);
  const lookups = presetLookups(opts);

  opts = {
    ...opts,
    mode,
    auth,
    log,
    dev,
    test,
    prisma,
    shield,
    tenant,
    migration,
    core,
    contextStorage,
    settings,
    lookups,
  };

  const root = presetRoot(opts);
  const schema = presetSchema(opts);

  opts = {
    ...opts,
    root,
    schema,
  };

  const server = await presetServer(opts);
  const database = presetDatabase(opts);

  opts = {
    ...opts,
    server,
    database,
  };

  return opts;
}
