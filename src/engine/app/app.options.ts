import { LogOptions } from '../log/log.options';
import { SchemaOptions } from '../schema/schema.options';
import { ModeOptions } from '../mode/mode.options';
import { DbOptions } from '../database/db.options';
import { RootOptions } from '../root/root.options';
import { ServerOptions } from '../server/server.options';
import { AuthOptions } from '../auth/auth.options';
import { MigrationOptions } from '../migration/migration.options';
import { CoreOptions } from '../core/core.options';
import { PrismaOptions } from '../prisma/prisma.options';
import { DevOptions } from '../dev/dev.options';
import { TestOptions } from '../testing/test.options';
import { ShieldOptions } from '../shield/shield.options';
import { ClientOptions } from '../client/client.options';
import { TenantOptions } from '../tenant/tenant.options';
import { IContextStorage } from '../storage/IContextStorage';
import { SettingsOptions } from '../settings/settings.options';
import { LookupsOptions } from '../lookups/lookups.options';

export type AppOptions = {
  root?: RootOptions;
  mode?: ModeOptions;
  database?: DbOptions;
  prisma?: PrismaOptions;
  shield?: ShieldOptions;
  schema?: SchemaOptions;
  log?: LogOptions;
  server?: ServerOptions;
  auth?: AuthOptions;
  migration?: MigrationOptions<any>;
  dev?: DevOptions;
  core?: CoreOptions;
  test?: TestOptions;
  client?: ClientOptions;
  tenant?: TenantOptions;
  contextStorage?: IContextStorage;
  settings?: SettingsOptions;
  lookups?: LookupsOptions;
};
