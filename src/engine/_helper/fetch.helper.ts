import fetch, { Response } from 'node-fetch';
import https from 'https';
import FormData from 'form-data';

const httpsAgent = new https.Agent({
  rejectUnauthorized: false,
});

export class PostError extends Error {
  url: string;
  status: number;

  constructor(url: string, status: number, message: string) {
    super(message);
    this.name = 'AitPostError';
    this.url = url;
    this.status = status;
  }
}

async function getResult(r: Response, asText?: boolean) {
  if (r.status >= 200 && r.status < 300) {
    if (r.status === 204) return;

    return asText ? r.text() : r.json();
  }
  const data = await r.text();
  const headers = r.headers || {};
  const msg = `${r.url} status: ${r?.status}
    headers:
${Object.keys(headers)
  .map((k) => `   ${k}: ${headers.get(k)}`)
  .join('\r\n')}

    data:
${data}

`;

  throw new PostError(r.url, r.status, msg);
}

async function fetchEx(
  url: string,
  data?: any,
  init?: {
    method?: string;
    headers?: Record<string, string>;
    contentType?: string;
    responseText?: boolean;
    getResult?: (r: Response, init?: any) => Promise<any>;
    body?: any;
  }
) {
  let contentType: string | undefined = init?.contentType ?? 'application/json';
  const method = init?.method ?? 'POST';

  let body = init?.body;

  if (!body) {
    if (typeof data === 'string') {
      body = data;
    } else if (contentType) {
      switch (contentType) {
        case 'application/json':
          if (method !== 'GET') body = JSON.stringify(data);
          break;
        case 'multipart/form-data':
          if (typeof data === 'object') {
            // DO NOT supply headers with 'Content-Type' if it's using FormData.
            contentType = undefined;
            const formData = new FormData();
            for (const name in data) {
              if (data[name] !== undefined) formData.append(name, data[name]);
            }
            body = formData;
          }
          break;
      }
    }
  }

  let headers: any;
  if (contentType || init?.headers) {
    headers = {
      ...(contentType && { 'Content-Type': contentType }),
      ...init?.headers,
    };
  }

  const res = await fetch(url, {
    method,
    headers,
    body,
    agent: url.startsWith('https://') ? httpsAgent : undefined,
  });

  if (init?.getResult) {
    return init.getResult(res, init);
  }

  return getResult(res, init?.responseText);
}

export default fetchEx;
