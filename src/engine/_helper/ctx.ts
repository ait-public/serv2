import { TenantContext } from '../Context';
import { presetCore } from '../core/core.preset';
import { AppOptions } from '../options';
import { presetStorage } from '../storage/storage.preset';

export interface IPrisma {
  $disconnect(): Promise<any>;
  $executeRawUnsafe(query: string, ...values: any[]): Promise<any>;
  $queryRawUnsafe(query: string, ...values: any[]): Promise<any>;
}

export type TenantScopeContext<TPrisma extends IPrisma> = {
  tenantId: string;
  tenant: TenantContext;
  prisma: TPrisma;
  dbUrl: string;
};

function createScope(
  create: (options: AppOptions) => Promise<{ prisma: IPrisma; dbUrl: string }>
) {
  return async function inTenantScope<T extends IPrisma, R = any>(
    opts: AppOptions,
    tenantId: string,
    callback: (ctx: TenantScopeContext<T>) => Promise<R>
  ): Promise<R> {
    const storage = presetStorage(opts);
    const tenant = { ...storage.getStore(), tenantId };

    return storage.run(tenant, async () => {
      const { prisma, dbUrl } = await create(opts);
      try {
        return await callback({ tenantId, tenant, prisma: prisma as T, dbUrl });
      } finally {
        if (prisma) await prisma.$disconnect();
      }
    });
  };
}

async function newAitPrisma<TPrisma extends IPrisma>(
  options: AppOptions
): Promise<{ prisma: TPrisma; dbUrl: string }> {
  const core = presetCore(options);
  const { prisma, dbUrl } = await core.prismaOptions({
    ...options,
    mode: {
      ...options.mode,
      isMigration: false,
      isTenant: false,
    },
    prisma: {
      isLogged: options.prisma?.isLogged,
      middlewares: {
        ...options.prisma?.middlewares,
        rls: true, // SET LOCAL tenant
      },
    },
    database: {
      ...options.database,
      dbUrl: undefined,
    },
  });

  if (!dbUrl) throw new Error('dbUrl is null');

  return { prisma, dbUrl };
}

async function newTenantPrisma<TPrisma extends IPrisma>(
  options: AppOptions
): Promise<{ prisma: TPrisma; dbUrl: string }> {
  const core = presetCore(options);

  const { prisma, dbUrl } = await core.prismaOptions({
    ...options,
    mode: {
      ...options.mode,
      isMigration: false,
      isTenant: options.mode?.isTenant, // by tenant user
    },
    prisma: {
      isLogged: options.prisma?.isLogged,
      middlewares: {
        ...options.prisma?.middlewares,
        rls: true, // SET LOCAL tenant
      },
    },
    database: {
      ...options.database,
      dbUrl: undefined,
    },
  });

  if (!dbUrl) throw new Error('dbUrl is null');

  return { prisma, dbUrl };
}

export default {
  inTenantScope: createScope(newTenantPrisma),
  inAitScope: createScope(newAitPrisma),
};
