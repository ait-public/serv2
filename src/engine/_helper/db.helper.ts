import { Client } from 'pg';
import constants from '../constants';

function parseUrl(dbUrl: string): {
  match: boolean;
  port?: string | number;
  db?: string;
  user?: string;
  password?: string;
  schema?: string;
  host?: string;
  bouncer?: boolean;
} {
  if (!dbUrl) return { match: false };

  // postgresql://userId:Password@Server:5489/DbName?schema=dbSchema&pgbouncer=true

  const re =
    /^(postgresql):\/\/([^:]*):([^:]*)@([^:]*):(\d+)\/([^?]*)\?([^#]+)/;

  const r = dbUrl.match(re);

  if (r) {
    const p = split(r[7], '&');

    return {
      match: true,
      host: r[4],
      port: r[5],
      db: r[6],
      user: r[2],
      password: r[3],
      schema: p['schema'],
      bouncer:
        p['pgbouncer'] !== undefined ? p['pgbouncer'] === 'true' : undefined,
    };
  }

  // "Server=92.63.98.130;Port=5432;Database=aitjwt;User Id=ait;Password=475@1;Pooling=true;MinPoolSize=10;MaxPoolSize=100;Enlist=true;Timeout=300;CommandTimeout=300"
  const sobj = split(dbUrl, ';');

  return {
    match: !!sobj['database'] && !!sobj['server'],
    host: sobj['server'],
    port: sobj['port'],
    db: sobj['database'],
    user: sobj['user id'],
    password: sobj['password'],
    schema: sobj['schema'],
  };
}

function split(dbUrl: string, splitter: string) {
  return dbUrl.split(splitter).reduce((acc, c) => {
    const kv = c.split('=');
    if (kv.length === 2) acc[kv[0].trim().toLowerCase()] = kv[1].trim();
    return acc;
  }, {} as { [k: string]: string });
}

function buildUrl(opts: {
  dbUrl?: string;
  port?: string | number;
  db?: string;
  user?: string;
  password?: string;
  schema?: string;
  host?: string;
  bouncer?: boolean;
}): string {
  let { dbUrl, port, db, user, password, schema, host: server, bouncer } = opts;

  if (dbUrl) {
    const m = parseUrl(dbUrl);
    if (!m.match) {
      throw new Error(`Parse DbUrl error '${dbUrl}'`);
    }

    port = m.port ?? port;
    db = m.db ?? db;
    user = m.user ?? user;
    password = m.password ?? password;
    schema = m.schema ?? schema;
    server = m.host ?? server;
    bouncer = m.bouncer ?? bouncer;
  }

  const pgbouncer = bouncer ? 'true' : 'false';
  return `postgresql://${user}:${password}@${server}:${port}/${db}?schema=${schema}&pgbouncer=${pgbouncer}`;
}

async function dropSchema(dbUrl: string, schema?: string) {
  if (!dbUrl) throw new Error('Cannot read property  dbUrl');
  const p = parseUrl(dbUrl);
  schema = schema || p.schema;
  if (!schema) throw new Error('schema not named');

  dbUrl = buildUrl({
    ...p,
    schema: 'public',
    user: constants.ENV.AIT_POSTGRES_USER,
    password: constants.ENV.AIT_POSTGRES_PASSWORD,
    bouncer: false,
    port: constants.ENV.AIT_POSTGRES_PORT,
  });

  await query({
    dbUrl: dbUrl,
    queryText: `DROP SCHEMA IF EXISTS "${schema}" CASCADE`,
  });
}

async function ping(dbUrl: string) {
  if (!dbUrl) throw new Error('Cannot read property  dbUrl');
  const r = await query({ dbUrl: dbUrl, queryText: `SELECT NOW()` });
  return r.rows[0];
}

async function checkSchema(dbUrl: string, schema?: string) {
  const p = parseUrl(dbUrl);
  if (!p.match) throw new Error('Cannot read property dbUrl');
  schema = schema ?? p.schema;

  dbUrl = buildUrl({
    ...p,
    schema: 'public',
    user: constants.ENV.AIT_POSTGRES_USER,
    password: constants.ENV.AIT_POSTGRES_PASSWORD,
    bouncer: false,
    port: constants.ENV.AIT_POSTGRES_PORT,
  });

  const r = await query({
    dbUrl: dbUrl,
    queryText: `SELECT EXISTS(SELECT 1 FROM pg_namespace WHERE nspname = '${schema}');`,
  });
  return !!r.rows[0].exists;
}

async function lastMigration(dbUrl: string, schema?: string) {
  const p = parseUrl(dbUrl);
  if (!p.match) throw new Error('Cannot read property dbUrl');

  schema = schema ?? p.schema ?? constants.ENV.APP_SCHEMA ?? 'public';

  dbUrl = buildUrl({
    ...p,
    schema,
    user: constants.ENV.AIT_POSTGRES_USER,
    password: constants.ENV.AIT_POSTGRES_PASSWORD,
    bouncer: false,
    port: constants.ENV.AIT_POSTGRES_PORT,
  });

  const r = await query({
    dbUrl: dbUrl,
    queryText: `select id, migration_name, started_at, finished_at, applied_steps_count
  FROM ${schema}."_prisma_migrations"
  order by started_at desc limit 1`,
  });
  return r.rows[0];
}

async function checkAitInstallation(dbUrl: string) {
  const p = parseUrl(dbUrl);
  if (!p.match) throw new Error('Cannot read property dbUrl');

  const queryText = `
  SELECT EXISTS (
    SELECT *
    FROM pg_catalog.pg_proc
    JOIN pg_namespace ON pg_catalog.pg_proc.pronamespace = pg_namespace.oid
            WHERE proname = 'ait_tenant_table_grant'
        AND pg_namespace.nspname = 'public'
)
`;

  try {
    const r = await query({
      dbUrl,
      queryText,
    });
    return !!r.rows[0].exists;
  } catch (e) {
    return false;
  }
}

async function query({
  dbUrl,
  queryText,
  values,
}: {
  dbUrl: string;
  queryText: string;
  values?: any[];
}) {
  //
  if (!dbUrl) throw new Error('Cannot read property  dbUrl');
  const client = new Client({ connectionString: dbUrl });

  // `CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
  await client.connect();
  try {
    return await client.query(queryText, values);
  } finally {
    await client.end();
  }
}

export default {
  parseUrl,
  buildUrl,
  dropSchema,
  ping,
  checkSchema,
  lastMigration,
  checkAitInstallation,
  query,
};
