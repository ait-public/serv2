type endTimer = () => number;
import { performance } from 'perf_hooks';

export function startTimer(): endTimer {
  // import 'acemodes'
  let startTime = 0;
  let endTime = 0;

  function endT() {
    endTime = performance.now();

    return (
      Math.round(((endTime - startTime) / 1000 + Number.EPSILON) * 100) / 100
    );
  }

  function start() {
    startTime = performance.now();
    return endT;
  }

  return start();
}
