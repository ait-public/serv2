import { print } from 'graphql';
import { fetch } from 'cross-fetch';
import { SubschemaConfig } from '@graphql-tools/delegate';
import { introspectSchema } from '@graphql-tools/wrap';
import { stitchSchemas } from '@graphql-tools/stitch';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { TypeMergingOptions } from '@graphql-tools/stitch/types';

function makeRemoteExecutor(
  url: string,
  options: { log?: boolean; headers?: Record<string, string> } = { log: false }
): ({
  document,
  variables,
}: {
  document: any;
  variables: any;
}) => Promise<any> {
  const headers = { ...options.headers, 'Content-Type': 'application/json' };
  return async ({ document, variables }) => {
    const query = typeof document === 'string' ? document : print(document);
    if (options.log)
      console.log(`# -- OPERATION ${new Date().toISOString()}:\n${query}`);

    const fetchResult = await fetch(url, {
      method: 'POST',
      headers,
      body: JSON.stringify({ query, variables }),
    });
    return fetchResult.json();
  };
}

class ExecutorStore {
  // static executors: Map<{ url: string }, any> = new Map<{ url: string }, any>();
  static executors: { [id: string]: any } = {};
}

export const defaultRemoteTypeFuncArgs: (
  ids: any[],
  remoteTypePkName: string
) => object = (ids, remoteTypePkName) => ({
  where: { [remoteTypePkName]: { in: ids } },
});

export async function subSchema(
  url: string,
  remotes: {
    remoteTypeName: string;
    remoteTypePkName: string;
    remoteTypeFunc: string;
    remoteTypeFuncArgs: (ids: any[]) => object;
  }[],
  options: {
    log?: boolean;
    batch?: boolean;
    valuesFromResults?: (results: any[], keys: any[]) => any[];
    headers?: Record<string, string>;
  } = { log: false, batch: true, valuesFromResults: undefined }
): Promise<SubschemaConfig> {
  if (options.log) console.log('# -- MERGING URL: ' + url);

  let executor = ExecutorStore.executors[url];
  if (!executor) {
    executor = makeRemoteExecutor(url, {
      log: options.log,
      headers: options.headers,
    }) as any;
    ExecutorStore.executors[url] = executor;
  }

  const config = {
    schema: introspectSchema(executor),
    executor: executor,
    batch: options.batch,
    merge: {
      //   [remoteTypeName]: {
      //     selectionSet: `{ ${remoteTypePkName} }`,
      //     fieldName: remoteTypeFunc,
      //     key: (obj) => obj[remoteTypePkName], // pluck a key from each record in the array
      //     argsFromKeys: remoteTypeFuncArgs, // format all plucked keys into query args
      //   },
    },
  } as SubschemaConfig;

  remotes.forEach((remote) => {
    Object.assign(config.merge, {
      [remote.remoteTypeName]: {
        selectionSet: `{ ${remote.remoteTypePkName} }`,
        fieldName: remote.remoteTypeFunc,
        key: (obj: any) => obj[remote.remoteTypePkName], // pluck a key from each record in the array
        argsFromKeys: remote.remoteTypeFuncArgs, // format all plucked keys into query args
        valuesFromResults: (results: any[], keys: any[]) => {
          // юзаем внешнюю сотртировалку если есть
          if (options.valuesFromResults)
            return options.valuesFromResults(results, keys);
          // graphql-tools bugfix: при сортировке по внешнему полю подтянутые в batching зависимости не сопоставляются как надо
          // поэтому сами сортируем массив объектов results в соответствии с массивом keys
          var sortedArray = keys.map((i) =>
            results.find((j) => j[remote.remoteTypePkName] === i)
          );
          return sortedArray;
        },
      },
    });
  });

  if (options.log) console.log('# -- MERGE: ', config.merge);

  return config;
}

export async function makeGatewaySchema(
  typeDefs: any,
  resolvers: any,
  subSchemas: Promise<SubschemaConfig>[],
  typeMergingOptions?: TypeMergingOptions
) {
  let localSchema = makeExecutableSchema({
    typeDefs: typeDefs,
    resolvers: resolvers ?? [],
  });
  // TODO: ?? Promise.allSettled(all);
  const resolvedSubSchemas = await Promise.all(subSchemas);

  const stitchedSchema = stitchSchemas({
    subschemas: [...resolvedSubSchemas, localSchema],
    typeMergingOptions: typeMergingOptions,
  });

  return stitchedSchema;
}
