import { PrismaSelect, generateGraphQlSDLFile } from '@paljs/plugins';
import { GraphQLResolveInfo } from 'graphql';
import { applyMiddleware } from 'graphql-middleware';
import { mergeTypeDefs } from '@graphql-tools/merge';
import {
  defaultRemoteTypeFuncArgs,
  makeGatewaySchema,
  subSchema,
} from './graphqlTools';
import { Context } from '../Context';
import { SchemaOptionsInput } from '../options';
import { makeExecutableSchema } from '@graphql-tools/schema';

import constants from '../constants';
import { presetSchema } from './schema.preset';
import { presetCore } from '../core/core.preset';
import graphql from '../graphql';

// https://paljs.com/generator/sdl
export async function createSchema(opts?: SchemaOptionsInput) {
  const schemaOpts = presetSchema(opts);
  // as is
  let schema: any = schemaOpts?.schema;

  if (!schema) {
    const lookup = opts?.lookups?.lookup;

    schemaOpts.typeDefs = mergeTypeDefs([
      lookup?.typeDefs ?? '',
      graphql.typeDefs,
      schemaOpts?.typeDefs ?? constants.SCHEMA.typeDefs,
    ]);

    schemaOpts.resolvers = [
      lookup?.resolvers() || {},
      ...graphql.resolvers,
      ...(schemaOpts?.resolvers ?? constants.SCHEMA.resolvers),
    ];

    if (opts && schemaOpts?.subSchemas?.length) {
      // stitch
      schema = await makeStitchSchema(opts);
    } else {
      // standart
      schema = makeExecutableSchema<any>({
        typeDefs: schemaOpts?.typeDefs, // ?? constants.SCHEMA.typeDefs,
        resolvers: schemaOpts?.resolvers, // ?? constants.SCHEMA.resolvers,
      });
    }
  }

  // Build one sdl file have all types you can delete if you not need
  if (schema && schemaOpts?.isPaljs && schemaOpts?.isGenerateGraphQlSDLFile)
    generateGraphQlSDLFile(schema);

  const middlewares: any = [
    schemaOpts?.isPaljs ? paljsMiddleware : null,
    ...(schemaOpts?.middlewares ?? []),
    opts?.shield?.permissions,
  ].filter((c) => !!c);

  // paljs preset...
  schemaOpts.schema = applyMiddleware(schema, ...middlewares);

  return schemaOpts;
}

// stitch
async function makeStitchSchema(opts: SchemaOptionsInput) {
  const schemaOpts = presetSchema(opts);
  const getUrl = opts.client?.getUrl ?? presetCore(opts).getUrl;

  const headers = opts.client?.headers;

  const subSchemas = (schemaOpts?.subSchemas || []).map((c) => {
    //
    const remotes = c.remotes.map((cr) => {
      return {
        ...cr,
        remoteTypeFunc: cr.remoteTypeFunc || `findMany${cr.remoteTypeName}`,
        remoteTypeFuncArgs:
          cr.remoteTypeFuncArgs ||
          ((ids) => defaultRemoteTypeFuncArgs(ids, cr.remoteTypePkName)),
      };
    });

    const url = `${getUrl(c.schemaName)}/graphql`;
    const options = { log: false, batch: true, headers, ...c.options };

    return subSchema(url, remotes, options);
  });

  const schema = await makeGatewaySchema(
    schemaOpts?.typeDefs,
    schemaOpts?.resolvers ?? [],
    subSchemas,
    { validationSettings: { validationLevel: 'off' } as any }
  );

  return schema;
}

// paljs preset...
function paljsMiddleware(
  resolve: (
    root: any,
    args: any,
    context: Context<any>,
    info: GraphQLResolveInfo
  ) => any,
  root: any,
  args: any,
  context: Context<any>,
  info: GraphQLResolveInfo
) {
  const result = new PrismaSelect(info).value;
  if (!result.select || Object.keys(result.select).length > 0) {
    args = {
      ...args,
      ...result,
    };
  }
  return resolve(root, args, context, info);
}
