import { IMiddlewareGenerator } from 'graphql-middleware';
import { Lookup } from '..';
import { GetUrl, UrlOptions } from '../options';
import { SchemaOptions } from './schema.options';

export type SchemaOptionsInput = {
  client?: {
    headers?: Record<string, string>;
    getUrl?: GetUrl;
  };
  schema?: SchemaOptions;
  shield?: {
    permissions?: IMiddlewareGenerator<any, any, any>;
  };
  lookups?: {
    lookup?: Lookup;
  };
  core?: {
    getUrl?: (opts?: UrlOptions) => Promise<GetUrl>;
  };
};
