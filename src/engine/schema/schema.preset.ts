import { SchemaOptions } from '../options';

export function presetSchema(opts?: {
  schema?: SchemaOptions;
  mode?: { isDev?: boolean };
}): SchemaOptions {
  return {
    ...opts?.schema,
  };
}
