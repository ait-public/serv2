import constants from '../constants';
import { AuthOptions } from '../options';

export function presetAuth(opts?: { auth?: AuthOptions }): AuthOptions {
  const secretKey =
    opts?.auth?.secretKey ??
    constants.ENV.AIT_JWT_SECRET ??
    constants.JWT.SECRET;

  return {
    secretKey,
    token: {
      secret: secretKey,
    },
    refreshToken: {
      secret: secretKey,
    },
  };
}
