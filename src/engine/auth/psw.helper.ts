import bcrypt from 'bcrypt';

async function hashPassword(password: string) {
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);
  return hashedPassword;
}

async function isValidPassword(noencrypted: string, encrypted: string) {
  try {
    return await bcrypt.compare(noencrypted, encrypted);
  } catch (error) {
    return false;
  }
}

export default {
  hashPassword,
  isValidPassword,
};
