import * as JWT from 'jsonwebtoken';
import createError from 'http-errors';

import constants from '../constants';
import { ID } from '../types';
import { AuthOptions } from './auth.options';
import { RootOptions } from '../root/root.options';

import { utils } from '@ait/share2';

export type JwtPayload = {
  [key: string]: string | number | undefined;
  jti: ID;
  // userId or clientId
  sub: ID;
  exp: number;
  name: string;
  role: string;
  clientId: string;
  sessionId?: string;
  profileId?: string;
  tenantId?: string;
};

export type CreateTokenInput = {
  [key: string]: string | number | undefined;
  userId?: string;
  name?: string;
  role?: string;
  secret?: string;
  clientId?: string;
  issuer?: string;
  exp?: number;
  sessionId?: string;
  profileId?: string;
  tenantId?: string;
  ip?: string;
};

function createToken(opts?: CreateTokenInput): Promise<string | undefined> {
  const sub = opts?.userId || '';
  const name = opts?.name || '';
  const secret = opts?.secret || constants.JWT.SECRET;
  const exp = opts?.exp || 3600;
  const role = opts?.role || constants.JWT.ANONYM;
  const clientId = opts?.clientId;
  const issuer = opts?.issuer || constants.JWT.ISSUER;

  const sessionId = opts?.sessionId;
  const profileId = opts?.profileId;
  const tenantId = opts?.tenantId;

  return new Promise<string | undefined>((resolve, reject) => {
    const payload: JwtPayload = {
      jti: utils.uuidv4(),
      sub,
      // jwt (iat, exp) измеряется в секундах
      exp: Math.floor(Date.now() / 1000) + exp,
      name,
      role,
      clientId: clientId || '',
      sessionId,
      tenantId,
      profileId,
    };

    const options: JWT.SignOptions = {
      // expiresIn: `1d`,
      issuer,
    };

    JWT.sign(payload, secret, options, (err, token) => {
      if (err) {
        console.log(err.message);
        reject(new createError.InternalServerError());
        return;
      }
      resolve(token);
    });
  });
}

export type TokenSecretPair = {
  token: string;
  secret?: string;
};

function verifyToken(opts: TokenSecretPair) {
  const { token, secret } = opts;
  return new Promise<JwtPayload | undefined>((resolve, reject) => {
    const secretOrPublicKey = secret || constants.JWT.TOKEN.SECRET;
    JWT.verify(token, secretOrPublicKey, (err, payload) => {
      if (err) {
        const message =
          err.name === 'JsonWebTokenError' ? 'Unauthorized' : err.message;
        reject(new createError.Unauthorized(message));
        return;
      }
      resolve(payload as JwtPayload);
    });
  });
}

function createAccessToken(opts?: CreateTokenInput) {
  return createToken({
    userId: opts?.userId,
    name: opts?.name,
    role: opts?.role,
    exp: opts?.exp || constants.JWT.TOKEN.EXPIRES_IN,
    secret: opts?.secret || constants.JWT.TOKEN.SECRET,
    clientId: opts?.clientId,
    issuer: opts?.issuer,
    sessionId: opts?.sessionId,
    profileId: opts?.profileId,
    tenantId: opts?.tenantId,
    ip: opts?.ip,
  });
}

function createRefreshToken(opts?: CreateTokenInput) {
  return createToken({
    userId: opts?.userId,
    name: opts?.name,
    role: opts?.role,
    exp: opts?.exp || constants.JWT.REFRESH_TOKEN.EXPIRES_IN,
    secret: opts?.secret || constants.JWT.REFRESH_TOKEN.SECRET,
    clientId: opts?.clientId,
    issuer: opts?.issuer,
    sessionId: opts?.sessionId,
    profileId: opts?.profileId,
    tenantId: opts?.tenantId,
    ip: opts?.ip,
  });
}

async function getSafePayload(
  opts: TokenSecretPair
): Promise<JwtPayload | undefined> {
  try {
    return await verifyToken({ token: opts.token, secret: opts.secret });
  } catch {}
  return undefined;
}

async function getPayload(opts: {
  token: string;
  auth: AuthOptions;
}): Promise<JwtPayload | undefined> {
  const { token, auth } = opts;
  if (!token) return undefined;

  const refreshTokenSecret =
    auth.refreshToken?.secret ?? constants.JWT.REFRESH_TOKEN.SECRET;
  const tokenSecret = auth.token?.secret ?? constants.JWT.TOKEN.SECRET;

  return (
    (await getSafePayload({ token, secret: refreshTokenSecret })) ??
    getSafePayload({ token, secret: tokenSecret })
  );
}

async function createTokens(opts: {
  /** deprecated use data instead*/
  user?: { id?: ID; name?: string; role?: string };
  data?: CreateTokenInput;
  auth?: AuthOptions;
  root?: RootOptions;
}) {
  const { user, auth, root } = opts;

  const tOpts: CreateTokenInput = {
    userId: user?.id,
    name: user?.name,
    role: user?.role,
    secret: auth?.token?.secret || constants.JWT.TOKEN.SECRET,
    clientId: root?.schema ?? constants.ROOT.SCHEMA,
    ...opts?.data,
  };

  const accessToken = await createAccessToken({
    ...tOpts,
    secret: auth?.token?.secret || constants.JWT.TOKEN.SECRET,
  });

  const refreshToken = await createRefreshToken({
    ...tOpts,
    secret: auth?.refreshToken?.secret || constants.JWT.REFRESH_TOKEN.SECRET,
  });

  return { accessToken, refreshToken };
}

export default {
  createToken,
  createTokens,
  verifyToken,
  getPayload,
  // getUserId,
};
