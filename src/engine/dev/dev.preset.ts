import constants from '../constants';
import { DevOptions } from './dev.options';

export function presetDev(opts?: { dev?: DevOptions }): DevOptions {
  return {
    medportalUrl:
      opts?.dev?.medportalUrl ??
      constants.ENV.AIT_MEDPORTAL_URL ??
      constants.DEV.MEDPORTAL_URL,
  };
}
