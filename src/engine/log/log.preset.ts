import constants from '../constants';
import { LogOptions } from '../options';

export function presetLog(opts?: { log?: LogOptions }): LogOptions {
  return {
    level: opts?.log?.level ?? parseInt(constants.ENV.AIT_LOGLEVEL ?? '2', 10),
  };
}
