import { LookupsOptions } from './lookups.options';

export type LookupsOptionsInput = {
  lookups?: LookupsOptions;
};
