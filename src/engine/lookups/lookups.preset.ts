import { LookupsOptions } from './lookups.options';

export function presetLookups(opts?: {
  lookups?: LookupsOptions;
}): LookupsOptions {
  const lookups = opts?.lookups;
  return {
    ...lookups,
    types: {
      ...lookups?.types,
    },
  };
}
