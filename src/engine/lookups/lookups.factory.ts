import Lookup from '../lookup/Lookup';
import { LookupsOptions } from './lookups.options';
import { LookupsOptionsInput } from './lookups.options.input';
import { presetLookups } from './lookups.preset';

export async function createLookups(
  opts?: LookupsOptionsInput
): Promise<LookupsOptions> {
  const lookupsOptions = presetLookups(opts);

  if (lookupsOptions.lookup) return lookupsOptions;

  const lookup = new Lookup();
  const types = lookupsOptions.types || {};
  // fill lookup
  Object.keys(types).forEach((type) => {
    lookup.add({ type, ...types[type] });
  });

  return {
    ...lookupsOptions,
    types,
    lookup,
  };
}
