import DataLoader from 'dataloader';

import { ApolloOptions } from '../server/apollo/apollo.options';
import helper from '../_helper';
import constants from '../constants';
import { TenantContext } from '../Context';
import { utils } from '@ait/share2';
import { AppOptions } from '../options';
import { presetAuth } from '../auth/auth.preset';
import { presetStorage } from '../storage/storage.preset';
import { presetCore } from '../core/core.preset';
import { presetPrisma } from '../prisma/prisma.preset';
import { presetClient } from '../client/client.preset';
import { presetExpress } from '../server/express/express.preset';
import { presetApollo } from '../server/apollo/apollo.preset';
import { CallBackReqContext } from '../types';
import { presetSchema } from '../schema/schema.preset';

export async function createContext(opts?: AppOptions): Promise<ApolloOptions> {
  // set GLOBAL context for App instance
  const core = presetCore(opts);
  const options = await core.config(opts);
  const { app } = presetExpress(options);

  const apollo = presetApollo(options);
  // check context exsits
  if (apollo.context) return apollo;

  const auth = presetAuth(options);
  const storage = presetStorage(options);

  const settings = await core.settings(options);

  const getSettings =
    settings.provider?.getSettings ?? (() => Promise.resolve({}));

  // **************************************
  // prepare Tenant Context
  // **************************************
  if (app) {
    app.use(async (req, res, next) => {
      const clientId = req.headers['x-client-id'] as string | undefined;
      const requestId =
        (req.headers['x-request-id'] as string) || utils.uuidv4();
      const traceId = (req.headers['x-trace-id'] as string) || requestId;

      const token = (req.get('Authorization') ?? '').replace('Bearer ', ''); // fix Bearer

      const payload = auth
        ? await helper.jwt.getPayload({ token, auth })
        : undefined;

      const userId = payload?.sub;
      const role = payload?.role || constants.JWT.ANONYM;
      const tenantId = payload?.tenantId;
      const sessionId = payload?.sessionId;
      const profileId = payload?.profileId;
      const ip = req.ip;

      const ctx: TenantContext = {
        clientId,
        requestId,
        traceId,
        tenantId,
        userId,
        sessionId,
        role,
        profileId,
        token,
        ip,
      };

      storage.run(ctx, next);
    });
  }

  let { gqlClient } = presetClient(options);
  const { prisma, dbUrl } = presetPrisma(options);
  const { schema } = presetSchema(options);

  // *******************************************
  // prepare Apollo Context
  // *******************************************

  const context: CallBackReqContext = async (/*{ req }*/) => {
    if (!gqlClient) throw new Error('gqlClient is not initialized');

    const tenant = storage.getStore();
    const role = (tenant && tenant.role) || constants.JWT.ANONYM;
    const tenantId = tenant?.tenantId;
    // TODO checkUser
    const userId = tenant?.userId;
    const isAuthenticated = role !== constants.JWT.ANONYM;

    return {
      schema,
      prisma,
      dataloaders: new WeakMap<object, DataLoader<string, object>>(),
      isAuthenticated,
      role,
      options,
      tenant,
      tenantId,
      userId,
      select: {},
      // request: req,
      gqlClient,
      store: new Map<string, any>(),
      dbUrl,
      getSettings,
    };
  };

  return {
    ...apollo,
    context,
  };
}
