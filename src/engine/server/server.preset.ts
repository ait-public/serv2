import constants from '../constants';
import { ServerOptions } from '../options';
import getPort from 'get-port';
import { utils } from '@ait/share2';
import { presetMode } from '../mode/mode.preset';
import { presetApollo } from './apollo/apollo.preset';
import { presetExpress } from './express/express.preset';

export async function presetServer(opts?: {
  server?: ServerOptions;
  mode?: { isTest?: boolean };
  root?: { schema?: string };
}): Promise<ServerOptions> {
  let port = opts?.server?.port;
  if (!port) {
    let mode = presetMode(opts);
    if (mode.isTest) {
      port = await getPort({
        port: utils.getRandomInt(5000, 5500), //    makeRange(utils.getRandomInt(5000, 5500), 6000),
      });
    } else {
      port = constants.ENV.PORT ?? constants.SERVER.PORT;
    }
  }

  return {
    ...opts?.server,
    port,
    express: presetExpress(opts),
    apollo: presetApollo(opts),
  };
}
