import constants from '../../constants';
import { ExpressOptions } from '../../options';
import { presetRoot } from '../../root/root.preset';

export function presetExpress(opts?: {
  server?: { express?: ExpressOptions };
  root?: { schema?: string };
}): ExpressOptions {
  const root = presetRoot(opts);
  const express = opts?.server?.express;

  let baseUrl = express?.baseUrl || root?.schema || '';

  if (baseUrl && !baseUrl.startsWith(`/`)) {
    baseUrl = `/${baseUrl}`;
  }

  const publicDir = express?.publicDir ?? constants.SERVER.PUBLIC_DIR;
  const jsonLimit = express?.jsonLimit ?? constants.SERVER.JSON_LIMIT;

  return {
    ...express,
    baseUrl,
    publicDir,
    jsonLimit,
  };
}
