import express from 'express';

export type ExpressOptions = {
  baseUrl?: string;
  app?: express.Application;
  publicDir?: string;
  /**
   * Controls the maximum request body size. If this is a number,
   * then the value specifies the number of bytes; if it is a string,
   * the value is passed to the bytes library for parsing. Defaults to '100kb'.
   */
  jsonLimit?: number | string | undefined;
};
