import { utils } from '@ait/share2';
import express from 'express';
import compression from 'compression';

import { presetExpress } from './express.preset';
import { ExpressOptionsInput } from '../../options';
import { presetRoot } from '../../root/root.preset';

const cors = require('cors');

export async function createExpress(opts?: ExpressOptionsInput) {
  const root = presetRoot(opts);
  const expessOptions = presetExpress({ ...opts, root });

  const app = (expessOptions.app = expessOptions.app ?? express());
  // compress all responses
  app.use(compression());
  app.use(express.json({ limit: expessOptions.jsonLimit }));
  app.use(express.urlencoded({ extended: true }));
  app.options('*', cors());

  const baseUrl = expessOptions?.baseUrl ?? ``;
  const publicDir = expessOptions.publicDir || '';

  if (baseUrl) {
    app.use(baseUrl, express.static(publicDir));
  } else {
    app.use(express.static(publicDir));
  }

  app.all(`${baseUrl}/ping`, (req: any, res: any) => {
    const d = new Date();
    const data = { now: utils.formatDateTime(d), utc: d };
    console.log(`${baseUrl}/ping`, data);
    res.json(data);
  });

  app.all(`${baseUrl}/version`, (req: any, res: any) => {
    const now = new Date();
    res.json({
      npm: root.version,
      app: root.appVersion,
      today: now.toString(),
      todayISO: now.toISOString(),
      todayUTC: now.toUTCString(),
      timeZone: now.getTimezoneOffset(),
    });
  });

  return expessOptions;
}
