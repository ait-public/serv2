import { ExpressOptions } from './express.options';

export type ExpressOptionsInput = {
  server?: {
    express?: ExpressOptions;
  };
  root?: {
    schema?: string;
    /** ENV production version */
    appVersion?: string;
    /** npm server package.json */
    version?: string;
  };
};
