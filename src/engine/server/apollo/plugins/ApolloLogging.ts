export const ApolloLogging = {
  async requestDidStart(requestContext: any) {
    return {
      async parsingDidStart(reg: any) {
        return (...errors: any): any => {
          // If there are no errors, we log in willSendResponse instead.
          if (errors.length) {
            errors.forEach((err: any) => console.error('parsingDidStart', err));
          }
        };
      },
      async validationDidStart() {
        return (errs: any) => {
          if (errs) {
            errs.forEach((err: any) =>
              console.error('validationDidStart', err)
            );
          }
        };
      },
      async executionDidStart() {
        return (err: any) => {
          if (err) {
            console.error('executionDidStart', err);
          }
        };
      },
      async didEncounterErrors(reg: any) {
        if (reg?.errors.length) {
          // TODO: traceId

          console.error(
            '\n',
            '🎈 errors:',
            `${reg.operationName}\n`,
            reg?.errors
          );

          console.log(
            reg.source,
            '\nvariables:',
            reg?.request.variables,
            '\n-----------\n'
          );
        }
      },
    };
  },
};
