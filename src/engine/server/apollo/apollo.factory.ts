import {
  ApolloServer,
  ApolloServerExpressConfig,
  ExpressContext,
} from 'apollo-server-express';
import { ApolloServerPluginLandingPageLocalDefault } from 'apollo-server-core';

import { ApolloLogging } from './plugins/ApolloLogging';
import { presetApollo } from './apollo.preset';
import { ApolloOptionsInput } from './apollo.options.input';
import { presetSchema } from '../../schema/schema.preset';

export async function createApollo(
  opts?: ApolloOptionsInput
): Promise<ApolloServer<ExpressContext>> {
  const apollo = presetApollo(opts);
  const { schema } = presetSchema(opts);

  if (apollo.apolloServer) return apollo.apolloServer;

  const apolloConfig: ApolloServerExpressConfig = {
    schema,
    context: apollo?.context,
    plugins: [
      ApolloLogging as any,
      // process.env.NODE_ENV === 'production' ?
      //   ApolloServerPluginLandingPageProductionDefault({ footer: false }) :
      ApolloServerPluginLandingPageLocalDefault({ footer: false }),
    ],
    introspection: true,
  };
  return new ApolloServer(apolloConfig);
}
