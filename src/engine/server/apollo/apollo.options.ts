import express from 'express';
import { ValueOrPromise } from '../../types';
import { Context } from '../../Context';
import { ApolloServer, ExpressContext } from 'apollo-server-express';

export type ContextParameters = {
  req: express.Request;
  res: express.Response;
};

export declare type ContextFunction = (
  params: ContextParameters
) => ValueOrPromise<Context<any>>;

export type ApolloOptions = {
  apolloServer?: ApolloServer<ExpressContext>;
  context?: ContextFunction;
  playground?: string;
  graphqlPostfixUrl?: string;
  graphqlUrl?: string;
  // bg?: 'dark' | 'light' | string;
};
