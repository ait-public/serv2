import constants from '../../constants';
import { ApolloOptions } from '../../options';
import { ExpressOptions } from '../express/express.options';
import { presetExpress } from '../express/express.preset';

export function presetApollo(opts?: {
  server?: {
    express?: ExpressOptions;
    apollo?: ApolloOptions;
  };
  root?: { schema?: string };
}): ApolloOptions {
  const { baseUrl } = presetExpress(opts);
  const apollo = { ...opts?.server?.apollo };
  apollo.graphqlPostfixUrl =
    apollo.graphqlPostfixUrl ?? constants.APOLLO.GRAPHQL_POSTFIX_URL;

  if (!apollo.graphqlUrl) {
    apollo.graphqlUrl = baseUrl
      ? `${baseUrl}${apollo.graphqlPostfixUrl}`
      : apollo.graphqlPostfixUrl;
  }

  return apollo;
}
