import { ServerOptions } from './server.options';
import { presetServer } from './server.preset';
import { presetCore } from '../core/core.preset';
import { Core } from '../core/Core';
import { ServerOptionsInput } from './server.options.input';

export async function createServer(
  opts?: ServerOptionsInput
): Promise<ServerOptions> {
  const core: Core = presetCore(opts);

  const options = await core.config(opts);
  const server = (options.server = await presetServer(options));

  const express = (server.express = await core.express(options));
  const app = express.app;
  if (!app) throw new Error('express.Application not initialize');

  options.lookups = await core.lookups(options);

  options.shield = core.shield(options);
  options.schema = await core.schema(options);

  const apollo = (options.server.apollo = await core.context(options));
  const apolloServer = (apollo.apolloServer = await core.apollo(options));

  // check server exsist
  if (server.appServer) return server;

  if (!apolloServer) throw Error('apolloServer should be init');

  const port = server?.port ?? 0;
  // handles
  let started: any = null;
  let httpServer: any;

  server.appServer = {
    run: async (_: any, listeningListener?: (opts: any) => void) => {
      started = await apolloServer.start();
      apolloServer.applyMiddleware({ app, path: apollo.graphqlUrl });

      return new Promise((resolve) => {
        httpServer = app.listen({ port }, () => {
          listeningListener && listeningListener(core);

          console.log(`

******************************************************************************************
      🚀 Server v${options?.root?.version} ready at http://localhost:${port}${apolloServer.graphqlPath}
                          www - http://localhost:${port}${express?.baseUrl}
******************************************************************************************
`);
        });

        resolve(options);
      });
    },

    stop: async () => {
      if (httpServer?.close) httpServer.close();
      if (started) await apolloServer?.stop();
    },
  };

  return server;
}
