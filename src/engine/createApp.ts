import { Core } from './core/Core';
import { initCore } from './core/core.preset';

import { AppOptions } from './options';
import { createApp as app } from './app/app.factory';
import { IServer } from './types';
import { createSchema } from './schema/schema.factory';
import { createApollo } from './server/apollo/apollo.factory';
import { createContext } from './context/context.factory';
import { createExpress } from './server/express/express.factory';
import { createServer } from './server/server.factory';
import { createShield } from './shield/shield.factory';

import { createDbConnection } from './database/dbConnection.factory';
import { createPrismaOptions } from './prisma/prismaOptions.factory';
import { dbDeploy } from './migration/deploy/deploy';
import { migrate } from './migration/migrate';
import { dbSeed } from './migration/seed/seed';

import { getTenants } from './tenant/getTenants';

import { createClientOptions } from './client/clientOptions.factory';
import { createGqlClient } from './client/gqlClient.factory';
import { createHttpHeader } from './client/httpHeader.factory';
import { createUrl } from './client/url.factory';
import { createConfig } from './config/config.factory';
import { dbInstall } from './migration/install/install';
import { useRlsMiddleware } from './prisma/middlewares/useRls.middleware';
import { createSettings } from './settings/settings.factory';
import { createLookups } from './lookups/lookups.factory';

const dummy = (o?: any): any => {
  throw new Error('Core is not initialized!');
};

let _core: Core | undefined;

export function initApp(): Core {
  _core =
    _core ??
    initCore({
      app: app,
      context: createContext,
      apollo: createApollo,
      config: createConfig,
      express: createExpress,
      dbConnection: createDbConnection,
      prismaOptions: createPrismaOptions,
      prisma: dummy,
      schema: createSchema,
      server: createServer,
      getUrl: createUrl,
      clientOptions: createClientOptions,
      gqlClient: createGqlClient,
      shield: createShield,
      httpHeader: createHttpHeader,
      dbInstall: dbInstall,
      dbSeed: dbSeed,
      dbDeploy: dbDeploy,
      migrate: migrate,
      tenants: getTenants,
      useRlsMiddleware: useRlsMiddleware,
      settings: createSettings,
      lookups: createLookups,
    });

  return _core;
}

export async function createApp(opts?: AppOptions): Promise<IServer> {
  const appOpts = await initApp().app(opts);
  if (!appOpts.server?.appServer) throw new Error('appServer not initialized');
  return appOpts.server.appServer;
}
