import { mergeTypeDefs } from '@graphql-tools/merge';

import Auth from './Auth/typeDefs';
import scalars from './scalars';
import { CronTypeDefs } from '../cron';

export default mergeTypeDefs([scalars.typeDefs, Auth, CronTypeDefs]);
