import Auth from './Auth/resolvers';
import scalars from './scalars';
import { CronResolvers } from '../cron';

export default [scalars.resolvers, Auth, CronResolvers];
