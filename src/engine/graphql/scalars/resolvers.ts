import {
  // DateTimeResolver,
  JSONObjectResolver,
  LocalDateResolver,
  JSONResolver,
  GUIDResolver,
  LocalTimeResolver,
  EmailAddressResolver,
} from 'graphql-scalars';
import { LocalDateTimeResolver } from './LocalDateTime';
import { DateTimeResolver } from './DateTime';

export default {
  LocalDateTime: LocalDateTimeResolver,
  // JSON: JSONResolver,
  GUID: GUIDResolver,
  EmailAddress: EmailAddressResolver,
  LocalTime: LocalTimeResolver,
  // ----------
  DateTime: DateTimeResolver,
  JSONObject: JSONObjectResolver,
  LocalDate: LocalDateResolver,
  Json: {
    ...JSONResolver,
    name: 'Json',
  },
};
