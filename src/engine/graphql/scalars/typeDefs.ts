export default `
  scalar DateTime
  scalar JSONObject
  scalar LocalDateTime
  scalar LocalDate
  scalar GUID
  scalar EmailAddress
  scalar LocalTime
  scalar Json
`;
