import { GraphQLScalarType, Kind } from 'graphql';

const returnOnError = (operation: () => any, alternative: any) => {
  try {
    return operation();
  } catch (e) {
    return alternative;
  }
};

// serialize: Эта функция вызывается, когда значение передается клиенту. Здесь вы можете вернуть все, что угодно, если это может быть действительный JSON. Это означает, что вы можете сериализовать его в строку, числа, объекты и массивы.
// parseValue: Эта функция вызывается, когда необходимо проанализировать входной параметр.
// parseLiteral: Эта функция вызывается, когда необходимо проанализировать встроенный входной параметр. Вместо того, чтобы возвращать значение, он вернет узел AST. (GraphQL использует абстрактное синтаксическое дерево для анализа запроса)

export const DateTimeResolver = new GraphQLScalarType({
  name: 'DateTime',
  description:
    'Дата-время без часового пояса в календарной системе ISO-8601, например 2007-12-03T10:15:30, используется для дней рождений...',
  parseValue(value) {
    // console.log('parseValue', value);
    return returnOnError(() => (value == null ? null : new Date(value)), null);
  },
  serialize(value) {
    // console.log("serialize", value);
    if (value instanceof Date) {
      return getYYYYMMDD(value);
    } else if (typeof value === 'string') {
      return value;
      // const m = moment(value);
      // return m.format("DD.MM.YYYY HH:mm:ss");
    } else if (typeof value === 'number') {
      returnOnError(() => (value == null ? null : new Date(value)), null);
    }
    // tslint:disable-next-line:no-null-keyword
    return undefined;
  },
  parseLiteral(ast) {
    // console.log("parseLiteral", ast);

    if (ast.kind === Kind.STRING) {
      let sdate = ast.value;
      //
      // yyyy-mm-dd
      if (sdate?.length === 10) {
        // fixed 2000-01-01  => 2000-01-01T00:00:00
        sdate = sdate + 'T00:00:00';
      }

      return getYYYYMMDD(new Date(sdate)); // oddValue(parseInt(ast.value, 10));
    } else if (ast.kind === Kind.INT) {
      return new Date(ast.value);
    }
    return null;
  },
});

function getYYYYMMDD(value: Date) {
  const yyyy = value.getFullYear();
  const MM = padLeft(value.getMonth() + 1);
  const dd = padLeft(value.getDate());

  const hh = padLeft(value.getHours());
  const mm = padLeft(value.getMinutes());
  const ss = padLeft(value.getSeconds());
  return `${yyyy}-${MM}-${dd}T${hh}:${mm}:${ss}`;
}

function padStart(value: any, length: number, char: string): string {
  value = value + '';
  const len = length - value.length;
  if (len <= 0) {
    return value;
  }
  return Array(len + 1).join(char) + value;
}

function padLeft(num: number): string {
  return padStart(num, 2, '0');
}
