import { utils } from '@ait/share2';
import createError from 'http-errors';
import helper from '../../../_helper';
import { CreateTokenInput } from '../../../auth/jwt.helper';
import { Context } from '../../../Context';
import { AppOptions } from '../../../options';

const ROLES = [
  'ANONYM',
  'USER',
  'PATIENT',
  'EMPLOYEE',
  'ADMIN',
  'SYSADMIN',
  'SYSTEM',
];

const _sessions = new Map<string, CreateTokenInput>();

const resolvers = {
  Query: {
    me: async (_: any, args: any, { userId }: Context<any>, _any: any) => {
      const cti = _sessions.get(userId || '');

      if (!cti)
        throw new createError.NotFound('Пользователь не зарегистрирован');

      const user = {
        id: userId,
        name: cti.name,
        createdAt: cti.createdAt,
        email: `${cti.name}@test.ru`,
        isDisabled: false,
        personId: cti.userId,
        role: cti.role,
        profile: {
          id: cti.profileId,
          role: cti.role,
          name: 'Тестовый ' + cti.role,
        },
      };

      return user;
    },
  },
  Mutation: {
    login: async (
      _: any,
      args: any,
      { options: { auth, root } }: { options: AppOptions },
      _any: any
    ) => {
      try {
        const { name, password, tenantId, profileId } = args;

        if (password && password !== name) {
          throw new createError.Unauthorized(
            'Введено неверное имя пользователя или пароль'
          );
        }

        let role = 'SYSADMIN';

        if (ROLES.includes(password)) {
          role = password;
        }

        const userId = utils.uuidv4();

        const data: CreateTokenInput = {
          sessionId: userId,
          userId,
          name,
          tenantId,
          profileId,
          role,
        };

        _sessions.set(userId, data);

        const { accessToken, refreshToken } = await helper.jwt.createTokens({
          data,
          auth,
          root,
        });

        return { accessToken, refreshToken };
      } catch (error) {
        console.error('login', error);
        throw error;
      }
    },
    register: async () => {
      throw new createError.NotImplemented();
    },
    refreshToken: async (
      _: any,
      { refreshToken }: any,
      { options: { auth, root } }: Context<any>,
      __: any
    ) => {
      // try {
      if (!refreshToken) throw new createError.BadRequest();

      const payload = await helper.jwt.verifyToken({
        token: refreshToken,
        secret: auth?.refreshToken?.secret,
      });

      if (!payload)
        throw new createError.NotFound('Пользователь не зарегистрирован');

      const userId = payload.sub; //userId
      const cti = _sessions.get(userId);

      if (!cti)
        throw new createError.NotFound('Пользователь не зарегистрирован');

      return helper.jwt.createTokens({ data: cti, auth, root });
      // } catch (error) {
      //   throw;
      // }
    },
    logout: async (_: any, __any: any, { userId }: Context<any>, __: any) => {
      return _sessions.delete(userId || '');
    },
  },
};

export default resolvers;
