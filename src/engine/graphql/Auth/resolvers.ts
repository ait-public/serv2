import createError from 'http-errors';
import { Context } from '../../Context';
import resolvers from './test/resolvers';

export default {
  Query: {
    me: async (p: any, args: any, ctx: Context<any>, meta: any) => {
      if (ctx.options.mode?.isTest)
        return resolvers.Query.me(p, args, ctx, meta);
      throw new createError.NotImplemented();
    },
  },
  Mutation: {
    login: async (p: any, args: any, ctx: Context<any>, meta: any) => {
      if (ctx.options.mode?.isTest)
        return resolvers.Mutation.login(p, args, ctx, meta);
      throw new createError.NotImplemented();
    },
    register: async () => {
      throw new createError.NotImplemented();
    },
    refreshToken: async (p: any, args: any, ctx: Context<any>, meta: any) => {
      if (ctx.options.mode?.isTest)
        return resolvers.Mutation.refreshToken(p, args, ctx, meta);
      throw new createError.NotImplemented();
    },
    logout: async (p: any, args: any, ctx: Context<any>, meta: any) => {
      if (ctx.options.mode?.isTest)
        return resolvers.Mutation.logout(p, args, ctx, meta);
      throw new createError.NotImplemented();
    },
  },
};
