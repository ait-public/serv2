import gql from 'graphql-tag';

export default gql`
  type Token {
    accessToken: String
    refreshToken: String
  }

  type Query {
    me: JSONObject
  }

  type Mutation {
    login(
      name: String!
      password: String!
      tenantId: String!
      profileId: String
    ): Token
    register(name: String!, password: String!, email: String): Token
    refreshToken(refreshToken: String!): Token
    logout: Boolean
  }
`;
