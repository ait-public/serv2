import { GraphQLResolveInfo } from 'graphql';
import { JSONObjectResolver } from 'graphql-scalars';
import { startTimer } from '../_helper/startTimer';
import gql from 'graphql-tag';

import { createClientToken } from '../client/httpHeader.factory';
import { Context } from '../Context';
import { presetCore } from '../core/core.preset';

const POOL_JOB_SIZE = 5;

interface Jobs<T> {
  [Key: string]: T;
}

interface IJob {
  id: string;
  name?: string | null;
  cronTime: string;
  curl: string;
  enabled: boolean;
}

export type Resolver<TContext> = (
  parent: any,
  args: any,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<any>;

export type JobOpts<TContext> = {
  /**
   * id исполняемая-команда
   */
  id: string;
  /**
   * наименование команды
   */
  name: string;
  /**
   * Формат cronjob-выражения '*\/30 * * * * *'
   */
  cronTime: string;
  /**
   * доп. данные
   */
  data?: any;
  /**
   * пользовательский заголовок
   */
  headers?: Record<string, string>;
  /**
   * автоматический запуск при первой регистрации службы
   */
  runFirst?: boolean;

  /**
   * Выполнение команды
   */
  resolve?: Resolver<TContext>;
};

export type JobActivity = {
  /**
   * id исполняемая-команда
   */
  id: string;
  /**
   * длина массива
   */
  len?: number;
  /**
   * результат
   */
  result?: any;
  /**
   * доп. данные команды
   */
  data?: any;
  /**
   * длительность выполнения
   */
  duration: number;
  /**
   * Дата исполнения
   */
  time: Date;
  /**
   * Ошибка
   */
  error?: string;
};

const GET_JOBS = gql`
  query jobs($ids: [String!]) {
    jobs(where: { id: { in: $ids } }) {
      id
      name
      cronTime
      curl
      enabled
    }
  }
`;

const SAVE_JOB = gql`
  mutation saveJob(
    $id: ID!
    $name: String
    $cronTime: String!
    $curl: String!
    $enabled: Boolean!
  ) {
    saveJob(
      job: {
        id: $id
        cronTime: $cronTime
        name: $name
        curl: $curl
        enabled: $enabled
      }
    ) {
      id
      name
      curl
      enabled
      cronTime
    }
  }
`;

export type JobDump = {
  jobs: {
    id: string;
    name: string;
    lastActivity?: JobActivity;
  }[];
};

/**
 * Команда для периодического выполнения заданий
 * @example
const c = new Job<any>();
c.add({
      id: 'monitor2.mail-dispatch',
      cronTime: "* * * * * *",
      name: 'рассылка писем',
      runFirst: true,
      resolve: async (_parent, args, context) => {...}
});
 */
export class Job<TContext> {
  private static jobs: Jobs<{
    opts: JobOpts<any>;
    item?: IJob;
    lastActivity?: JobActivity;
    dump: {
      errors: JobActivity[];
      logs: JobActivity[];
    };
  }> = {};

  public static readonly typeDefs = `
  type Query {
    getCronJobs: JSONObject
  }
  type Mutation {
    "CRON Команда для периодического выполнения заданий в определённое время"
    runCronJob(
      "id исполняемая-команда"
      id: String!
      "доп. данные"
      data: JSONObject
    ): JSONObject
    "инициализация (регистрация) CRON служб"
    initCronJobs: JSONObject
  }
  `;

  public static getCurl(
    url: string,
    id: string,
    data?: any,
    headers?: { [key: string]: string }
  ) {
    const h =
      (headers &&
        Object.keys(headers)
          .map((k) => `-H "${k}: ${headers[k]}"`)
          .join(' ')) ||
      '';

    return `curl -X POST ${url} -H "Content-Type: application/json" ${h} -d '${JSON.stringify(
      {
        query: `mutation runCronJob($id:String!,$data:JSONObject){runCronJob(id:$id,data:$data)}`,
        variables: { data, id },
      }
    )}'`;
  }
  /**
  * Добавить задачу
  * @example
  c.add({
        id: 'monitor2.mail-dispatch',
        cronTime: "* * * * * *",
        name: 'рассылка писем',
        runFirst: true,
        resolve: async (_parent, args, context) => {...}
  });
  */
  public add(opts: JobOpts<TContext>) {
    Job.jobs[opts.id] = { opts, dump: { errors: [], logs: [] } };
    return this;
  }

  private static async initJobs(
    parent: any,
    args: any,
    context: Context<any>,
    info: any
  ) {
    const jobs = Job.jobs;
    const client = context.gqlClient.get('cron');

    const clientId = context.options.root?.schema || '';
    const url = context.gqlClient.getUrl(clientId) + '/graphql';

    const ids = Job.getIds();
    const items: IJob[] = (await client.request(GET_JOBS, ids)).jobs;

    items.forEach((item: any) => {
      const c = jobs[item.id];
      if (c) c.item = item;
    });

    const jwt = await createClientToken({
      clientId: 'cron',
      tenantId: context.tenantId,
      secret: context.options.auth?.token?.secret,
    });

    const h = await presetCore(context.options).httpHeader();
    h['Authorization'] = 'Bearer ' + jwt;

    for (const cid in jobs) {
      const copts = jobs[cid].opts;
      const headers = { ...h, ...copts?.headers };

      try {
        const curl = Job.getCurl(url, cid, copts.data, headers);
        const item = (
          await client.request(SAVE_JOB, {
            id: cid,
            name: copts.name,
            cronTime: copts.cronTime,
            curl,
            enabled: !!copts.runFirst,
          })
        ).saveJob;

        jobs[cid].item = item;
      } catch (e) {
        console.error('initCronJobs', cid, e);
      }
    }

    return {
      date: new Date(),
      jobs: ids
        .map((cid) => jobs[cid]?.item)
        .filter((c) => !!c)
        .map((c) => ({
          id: c?.id,
          name: c?.name,
          enabled: c?.enabled,
          cronTime: c?.cronTime,
        })),
    };
  }

  private static getIds() {
    return Object.keys(Job.jobs);
  }

  private static async run(
    parent: any,
    args: { id: string; data?: any },
    context: any,
    info: any
  ): Promise<JobActivity> {
    const { id, data } = args;
    const endTimer = startTimer();
    const time = new Date();
    try {
      const job = Job.jobs[id];
      if (!job) {
        throw new Error('Job not found');
      }

      const resolve = job.opts.resolve;

      const result = resolve && (await resolve(parent, args, context, info));
      const duration = endTimer();
      const len = Array.isArray(result) ? result.length : undefined;

      return {
        id,
        len,
        result,
        data,
        duration,
        time,
      };
    } catch (e) {
      const duration = endTimer();
      console.error('runCronJob', id, e);
      return {
        id,
        duration,
        data,
        error: e instanceof Error ? e?.message : e + '',
        time,
      };
    }
  }

  private static trimSize(items: any[]) {
    if (items.length > POOL_JOB_SIZE) return items.shift();
  }

  private static async runJob(
    parent: any,
    args: { id: string; data?: any },
    context: any,
    info: any
  ): Promise<JobActivity> {
    const ativity = await Job.run(parent, args, context, info);
    const job = Job.jobs[args.id];
    if (job) {
      const prev = job.lastActivity;
      job.lastActivity = ativity;
      if (prev) {
        job.dump.logs.push(prev);
        if (prev.error) {
          job.dump.errors.push(prev);
        }
        Job.trimSize(job.dump.logs);
        Job.trimSize(job.dump.errors);
      }
    }
    return ativity;
  }

  private static getJobs(): JobDump {
    return {
      jobs: Job.getIds().map((id: string) => {
        const j = Job.jobs[id];
        return {
          id,
          name: j.item?.name ?? j.opts.name,
          lastActivity: j.lastActivity,
          dump: j.dump,
        };
      }),
    };
  }

  public static resolvers = {
    JSONObject: JSONObjectResolver,
    Mutation: {
      runCronJob: Job.runJob,
      initCronJobs: Job.initJobs,
    },
    Query: {
      getCronJobs: Job.getJobs,
    },
  };
}

export const CronResolvers = Job.resolvers;
export const CronTypeDefs = Job.typeDefs;
