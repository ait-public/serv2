import { Context } from '../Context';
import { JSONObjectResolver } from 'graphql-scalars';

function getTypeName(type: string) {
  type = type.charAt(0).toUpperCase() + type.slice(1);
  return `Lookup${type}Payload`;
}

function getTypeDefs(type: string) {
  return `
  type ${getTypeName(type)} {
    id: ID!
    record: JSONObject
    error: String
  }
`;
}

const DEF = `
  scalar JSONObject

  input LookupSortInput {
    by: String
    desc: Boolean
  }

  input LookupPaginationInput {
    page: Int
    perPage: Int
  }

  input LookupSearchInput {
    filter: JSONObject
    pagination: LookupPaginationInput
    sort: [LookupSortInput!]
  }

  type Query {
    findLookup(
      ids: LookupInput!
    ): LookupResult!
    findLookupList(
      type: String!
      where: LookupSearchInput
    ): [JSONObject!]
    getLookupTypes: JSONObject!
  }
`;

export interface LookupDict<T> {
  [Key: string]: T | any;
}

type LookupTypeInfo = {
  type: string;
  propId: string;
  propText: string | ((item: any) => string);
  typeDefs: string;
  typeName: string;
  select: LookupDict<boolean>;
  resolvers: LookupResolvers;
  // searchFields: string[] | PrepareSearchCondition;
};

export type LookupSort = {
  by: string;
  desc: boolean;
};
/**
 * @default
 *  {page: 1, perPage: 100}
 */
export type LookupPaginationInput = {
  page?: number;
  perPage?: number;
};

export type LookupSearchInput = {
  filter?: LookupDict<any>;
  pagination?: LookupPaginationInput;
  sort?: LookupSort[];
};

export type LookupGetByIdsParams = {
  source: any;
  args: any;
  context: Context<any>;
  info: any;
  ids: string[];
};

export type LookupGetByIds = (ctx: LookupGetByIdsParams) => Promise<any[]>;

export type LookupGetListParams = {
  source: any;
  context: Context<any>;
  info: any;
  type: string;
  where?: LookupSearchInput;
};

export type LookupGetList = (ctx: LookupGetListParams) => Promise<any[]>;

export type LookupResolvers = {
  ids?: LookupGetByIds;
  list?: LookupGetList;
};

export type FindManyArgs =
  | {
      where?: any;
      skip?: number;
      take?: number;
      orderBy?: any;
      select?: LookupDict<boolean>;
    }
  | undefined;

export type PrepareOpts<TFindManyArgs = FindManyArgs> = {
  input?: string | string[];
  args: TFindManyArgs;
  type: string;
  context: Context<any>;
  opts: LookupGetListParams | LookupGetByIdsParams;
};

export type PrepareFindManyArgs<TFindManyArgs = FindManyArgs> = (
  opts: PrepareOpts<TFindManyArgs>
) => Promise<TFindManyArgs>;

export type PrepareArgs<TFindManyArgs = FindManyArgs> = {
  list?: PrepareFindManyArgs<TFindManyArgs>;
  ids?: PrepareFindManyArgs<TFindManyArgs>;
};

export type PrepareSearchCondition = (opts: PrepareOpts) => Promise<any>;

/**
 * @default
 *  {propId: "id", propText: "name"}
 */
export type LookupOptions = {
  type: string;
  propId?: string;
  propText?: string | ((item: any) => string);
  searchFields?: string[] | PrepareSearchCondition;
  select?: LookupDict<boolean>;
  resolvers?: LookupResolvers;
  prepareArgs?: PrepareArgs;
  map?: (item: any) => any;
};

function createGetList(
  type: string,
  propText: string | ((item: any) => string),
  select: LookupDict<boolean>,
  searchFields: string[] | PrepareSearchCondition,
  prepareArgs?: PrepareFindManyArgs
): LookupGetList {
  return async (opts: LookupGetListParams) => {
    const pagination = opts?.where?.pagination;
    const page = pagination?.page ?? 1;
    const take = pagination?.perPage ?? 100;
    const skip = (page - 1) * take;
    const sort = opts?.where?.sort;

    const filter = opts?.where?.filter || {};
    const { q, ...otherFilter } = { q: undefined, ...filter };

    const where: any = { ...otherFilter };

    const orderBy =
      (sort?.length &&
        sort.map((c) => ({
          [c.by]: c.desc ? 'desc' : 'asc',
        }))) ||
      (Array.isArray(searchFields) &&
        searchFields.map((c) => ({ [c]: 'asc' })));

    const context = opts.context;

    let args: FindManyArgs = {
      where,
      skip,
      take,
      orderBy,
      select,
    };

    if (q) {
      if (Array.isArray(searchFields)) {
        if (searchFields.length) {
          const OR = searchFields.map((c) => ({
            [c]: { startsWith: q, mode: 'insensitive' },
          }));
          args.where.OR = [...where.OR, ...OR];
        }
      } else if (typeof searchFields === 'function') {
        //
        const OR = await searchFields({ args, input: q, context, type, opts });
        args.where.OR = [...where.OR, OR];
      }
    }

    if (prepareArgs) {
      args =
        (await prepareArgs({ args, input: q, context, type, opts })) ?? args;
    }

    return context.prisma[type].findMany(args);
  };
}

function buildDisplay(propText: string | ((item: any) => string), list: any) {
  if (propText) {
    if (typeof propText === 'function') {
      list.forEach((c: any) => (c['_display'] = propText(c)));
    } else {
      list.forEach((c: any) => (c['_display'] = c[propText]));
    }
  }
  return list;
}

function createGetByIds(
  type: string,
  propId: string,
  propText: string | ((item: any) => string),
  select: LookupDict<boolean>,
  prepareArgs?: PrepareFindManyArgs
  // where?: any
): LookupGetByIds {
  return async (opts: LookupGetByIdsParams) => {
    const { context, ids } = opts;
    let pargs: FindManyArgs = {
      where: { [propId]: { in: ids } },
      select,
    };

    if (prepareArgs) {
      pargs =
        (await prepareArgs({ args: pargs, context, type, input: ids, opts })) ??
        pargs;
    }

    return context.prisma[type].findMany(pargs);
  };
}
/**
 *@example
  const lookup = new Lookup();
  lookup.add({
    type: "gender",
    propId: "genderId",
    select: { name: true },
  })
  //gql
  {
    findLookup(ids: { gender: ["00000000-0000-0000-0006-000000000001"] }) {
      gender {
        id
        record
      }
    }
    findLookupList(type: "gender")
  }
 */
class Lookup {
  readonly types: LookupDict<LookupTypeInfo> = {};

  public add(opts: LookupOptions) {
    const { type } = opts;
    if (type) {
      const propId = opts.propId ?? 'id';
      const propText = opts.propText ?? 'name';
      const typeName = getTypeName(type);
      const mapper = opts.map;

      const select: any = { ...opts.select };
      select[propId] = true;

      let arrSearchFields: string[] | undefined =
        (Array.isArray(opts.searchFields) && [...opts.searchFields]) ||
        undefined;

      if (typeof propText === 'string') {
        select[propText] = true;

        if (arrSearchFields && !arrSearchFields.includes(propText))
          arrSearchFields.push(propText);
      }

      if (arrSearchFields) arrSearchFields.forEach((c) => (select[c] = true));

      const ids =
        opts?.resolvers?.ids ??
        createGetByIds(type, propId, propText, select, opts?.prepareArgs?.ids);

      const searchFields = arrSearchFields ?? opts?.searchFields ?? [];

      const list =
        opts?.resolvers?.list ??
        createGetList(
          type,
          propText,
          select,
          searchFields,
          opts?.prepareArgs?.list
        );

      // buildDisplay
      const resolvers = {
        ids: async (idsOpts: LookupGetByIdsParams) => {
          let items = await ids(idsOpts);
          if (mapper) items = items.map((c) => mapper(c));
          return buildDisplay(propText, items);
        },
        list: async (listOpts: LookupGetListParams) => {
          let items = await list(listOpts);
          if (mapper) items = items.map((c) => mapper(c));
          return buildDisplay(propText, items);
        },
      };

      const typeDefs = getTypeDefs(type);

      this.types[type] = {
        type,
        propId,
        // propText,
        typeName,
        resolvers,
        typeDefs,
        // select,
        // searchFields,
      };
    }
    return this;
  }

  getTypes() {
    return Object.getOwnPropertyNames(this.types);
  }

  toPayload() {
    return this.getTypes()
      .map((t) => this.types[t].typeDefs)
      .join();
  }

  toInput() {
    const flds = this.getTypes()
      .map((t) => `    ${t}: [ID!]`)
      .join('\r\n');
    return `
  input LookupInput {
${flds}
  }
`;
  }

  toResult() {
    const flds = this.getTypes()
      .map((t) => `    ${t}: [${this.types[t].typeName}!]`)
      .join('\r\n');
    return `
  type LookupResult {
${flds}
  }
`;
  }

  public get typeDefs() {
    if (!this.getTypes().length) return '';

    return `
    ${this.toInput()}
    ${this.toPayload()}
    ${this.toResult()}
    ${DEF}
`;
  }

  public toString(): string {
    return this.typeDefs;
  }

  public resolvers() {
    if (!this.getTypes().length) return {};

    return {
      JSONObject: JSONObjectResolver,
      Query: {
        findLookup: async (source: any, args: any, context: any, info: any) => {
          const typesIds = args.ids || {};
          const promises = Object.keys(typesIds).map(async (ctype: string) => {
            const ids = args?.ids[ctype] ?? [];

            const ti = this.types[ctype];
            const propId = ti?.propId;
            const getIds = ti?.resolvers?.ids;

            let items: any[] = [];
            let error: any = null;

            if (getIds) {
              try {
                items = await getIds({
                  source,
                  args,
                  context,
                  info,
                  ids: [...ids],
                });
              } catch (e) {
                console.error('findLookup getIds', e);
                if (e instanceof Error) {
                  error = e.message;
                } else {
                  error = 'error';
                }
              }
            }

            const r = ids.map((id: any) => ({
              id,
              record:
                items.find((citem) => propId && citem?.[propId] === id) || null,
              error,
            }));

            return {
              ctype,
              r,
            };
          });

          const results = await Promise.all(promises);

          const result = results.reduce((p, c) => {
            p[c.ctype] = c.r;
            return p;
          }, {} as any);

          return result;
        },
        findLookupList: (source: any, args: any, context: any, info: any) => {
          const getList = this.types[args.type].resolvers?.list;
          if (!getList) return null;
          return (
            getList({
              source,
              context,
              info,
              type: args.type,
              where: args?.where,
            }) ?? null
          );
        },
        getLookupTypes: () =>
          this.getTypes().reduce((p, t) => {
            p[t] = { propId: this.types[t] };
            return p;
          }, {} as any),
      },
      // ...payloads,
    };
  }
}

export default Lookup;
