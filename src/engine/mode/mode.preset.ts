import constants from '../constants';
import { ModeOptions } from '../options';

export function presetMode(opts?: { mode?: ModeOptions }): ModeOptions {
  const isDev = opts?.mode?.isDev ?? constants.ENV.NODE_ENV === 'development';
  const isProd = opts?.mode?.isProd ?? constants.ENV.NODE_ENV === 'production';
  const isTest = opts?.mode?.isTest ?? constants.ENV.NODE_ENV === 'test';
  const isKube = opts?.mode?.isKube ?? constants.ENV.AIT_KUBE === 'true';
  const isMigration =
    opts?.mode?.isMigration ?? constants.ENV.AIT_MIGRATE_ON_STARTUP === 'true';

  const isTenant =
    opts?.mode?.isTenant ?? constants.ENV.AIT_MODE_TENANT === 'true';

  return {
    isKube,
    isMigration,
    isDev,
    isProd,
    isTest,
    isTenant,
  };
}
