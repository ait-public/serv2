import { AppOptions } from '../options';
import { presetCore } from '../core/core.preset';
import { presetTenant } from '../tenant/tenant.preset';
import { utils } from '@ait/share2';
import helper from '../_helper';

export async function getTenants(opts?: AppOptions): Promise<string[]> {
  const core = presetCore(opts);
  const options = await core.config(opts);
  const tenant = presetTenant(options);

  const platformTenantId = tenant.platformTenantId || '';

  let tenantIds: string[] | null = await helper.ctx.inAitScope(
    options,
    platformTenantId,
    async ({ dbUrl }) => {
      try {
        if (await helper.db.checkSchema(dbUrl, 'platform')) {
          const rows = await helper.db.query({
            dbUrl,
            queryText: `SELECT id FROM platform."Tenant" WHERE "isDisabled"=false`,
          });
          return rows.rows.map((c) => c.id);
        }
      } catch {
        console.error('Ошибка получения списка platform."Tenant"', dbUrl);
      }
      return [];
    }
  );

  if (!tenantIds || !tenantIds.length) {
    // default lpu ids if platform was not initialized
    tenantIds = [
      '00000000-0000-0000-1000-000000000002',
      '00000000-0000-0000-1000-000000000003',
    ];
  }

  return utils.distinct(tenantIds.filter((c) => c && c !== platformTenantId));
}
