import constants from '../constants';
import { TenantOptions } from './tenant.options';

export function presetTenant(opts?: {
  tenant?: TenantOptions;
  // mode?: { isTest?: boolean };
}): TenantOptions {
  const platformTenantId =
    opts?.tenant?.platformTenantId ??
    constants.ENV.AIT_PLATFORM_TENANT_ID ??
    constants.TENANT.AIT_PLATFORM_TENANT_ID;
  const pgCurrentTenantParamName =
    constants.TENANT.PG_CURRENT_TENANT_PARAM_NAME;

  return {
    platformTenantId,
    pgCurrentTenantParamName,
    ...opts?.tenant,
  };
}
