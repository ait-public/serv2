import { IGqlClient } from '../types';
import { gql } from 'graphql-request';

export interface IDocNum {
  value: string;
  numerator_id: string;
  issued_number_id: string;
}

export interface ICommonApi {
  //getNumeratorCode(docType: string): string;
  getDocNum(numerator_id: string, onDate?: Date): Promise<IDocNum>;
}

// локальный контекст, отделен от глобального
export interface ICommonContext {
  deptCode?: string;
}

// function getNumeratorCode(docType: string, context?: ICmonContext) {
//   let res = docType;
//   const deptCode = context?.deptCode ?? process.env.AIT_DEV_DEPT_CODE; // TODO: из глобального контекста здесь?
//   if (deptCode) res += `.${deptCode}`;
//   return res;
// }

async function getDocNum(
  numerator_id: string,
  client: IGqlClient,
  context?: ICommonContext,
  onDate?: Date
) {
  const q = gql`
    mutation execNumerator($input: ExecNumeratorInput!) {
      execNumerator(input: $input) {
        ... on ExecNumeratorResultSuccess {
          value
          numerator_id
          issued_number_id
        }
        ... on ResultError {
          errorCode
          errorText
        }
      }
    }
  `;

  const res = (
    await client
      .get('common')
      .request(q, { input: { numerator_id: numerator_id, onDate: onDate } })
  ).execNumerator;
  console.log('execNumerator res', res);
  return res;

  //const numeratorCode = getNumeratorCode(docType);
  //const docNumUrl = client.getUrl('common') + '/num/api/numerator/execute';
  //return client.post(docNumUrl, { code: numeratorCode });
}

export function create(
  client: IGqlClient,
  context?: ICommonContext
): ICommonApi {
  return {
    //getNumeratorCode: (docType: string) => getNumeratorCode(docType, context),
    getDocNum: (numerator_id: string, onDate?: Date) =>
      getDocNum(numerator_id, client, context, onDate),
  };
}
