import { IGqlClient } from '../types';

export interface IBashApi {
  exec(data: { script: string; database: string; params: any }): Promise<any>;
}

async function exec(
  client: IGqlClient,
  data: { script: string; database: string; params: any }
) {
  const url = client.getUrl('bash') + '/Scripts/execute';
  return client.post(url, data, {
    contentType: 'application/json',
    responseText: true,
  });
}

export function create(client: IGqlClient): IBashApi {
  return {
    exec: (data: { script: string; database: string; params: any }) => {
      return exec(client, data);
    },
  };
}
