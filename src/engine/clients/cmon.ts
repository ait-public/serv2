import { IGqlClient } from '../types';

export interface IDocNum {
  value: string;
  numeratorId: string;
  issuedNumberId: string;
}

export interface ICmonApi {
  getNumeratorCode(docType: string): string;
  getDocNum(docType: string): Promise<IDocNum>;
}

// локальный контекст, отделен от глобального
export interface ICmonContext {
  deptCode?: string;
}

function getNumeratorCode(docType: string, context?: ICmonContext) {
  let res = docType;
  const deptCode = context?.deptCode ?? process.env.AIT_DEV_DEPT_CODE; // TODO: из глобального контекста здесь?
  if (deptCode) res += `.${deptCode}`;
  return res;
}

async function getDocNum(
  docType: string,
  client: IGqlClient,
  context?: ICmonContext
) {
  const numeratorCode = getNumeratorCode(docType);
  const docNumUrl = client.getUrl('cmon') + '/num/api/numerator/execute';
  return client.post(docNumUrl, { code: numeratorCode });
}

export function create(client: IGqlClient, context?: ICmonContext): ICmonApi {
  return {
    getNumeratorCode: (docType: string) => getNumeratorCode(docType, context),
    getDocNum: (docType: string) => getDocNum(docType, client, context),
  };
}
