import { IGqlClient } from '../types';

export interface IPostmanApi {
  mail(data: {
    addresses: string[] | string;
    subject: string;
    html?: string;
  }): Promise<any>;
}

function sendMail(
  gqlClient: IGqlClient,
  data: {
    addresses: string;
    subject: string;
    body?: string;
  }
) {
  // post api
  const url = gqlClient.getUrl('fserv') + '/postman/api/send_mail';
  return gqlClient.post(url, data, {
    contentType: 'multipart/form-data',
    responseText: true,
  });
}

export function create(client: IGqlClient): IPostmanApi {
  return {
    mail: (data: {
      addresses: string[] | string;
      subject: string;
      html?: string;
    }) => {
      let addresses = client.options?.root?.mailTestAddress;

      if (!addresses) {
        addresses = Array.isArray(data.addresses)
          ? data.addresses.join(';')
          : data.addresses;
      }

      return sendMail(client, {
        addresses,
        subject:
          data.subject.length > 255
            ? data.subject.substring(0, 254) + '…'
            : data.subject,
        body: data.html,
      });
    },
  };
}
