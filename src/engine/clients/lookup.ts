import { IGqlClient } from '../types';

export type LookupIdsInput = {
  schema: string;
  ids: { [type: string]: string[] };
  headers?: Record<string, string>;
};

export type LookupIdsResult = {
  [type: string]: { [id: string]: any } | null;
};

export interface ILookupApi {
  getIds(data: LookupIdsInput): Promise<LookupIdsResult>;
}

const getQuery = (types: string[]) => `query FindLookup($ids: LookupInput!) {
  findLookup(ids: $ids) {
    ${types.map((k) => `${k} {id record}`).join(' ')}
  }
}`;

async function getIds(client: IGqlClient, data: LookupIdsInput) {
  if (!data || !data.schema) return {};

  const r: { [key: string]: [{ id: string; record: any }] } = (
    await client
      .get(data.schema || '')
      .request(getQuery(Object.keys(data.ids)), { ids: data.ids }, data.headers)
  ).findLookup;

  const result = Object.keys(r).reduce((p, typ) => {
    const payload = r[typ].reduce((p1, idRecord) => {
      p1[idRecord.id] = idRecord.record || null;
      return p1;
    }, {} as { [key: string]: any });

    p[typ] = payload;

    return p;
  }, {} as LookupIdsResult);

  return result;
}

export function create(client: IGqlClient): ILookupApi {
  return {
    getIds: (data: LookupIdsInput) => getIds(client, data),
  };
}
