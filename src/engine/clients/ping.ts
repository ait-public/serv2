import { IGqlClient } from '../types';

export type PingOptionsInput = {
  schema: string;
  api?: boolean;
  version?: boolean;
  json?: boolean;
};

export interface IPingApi {
  ping(data?: PingOptionsInput): Promise<any>;
}

async function ping(client: IGqlClient, data?: PingOptionsInput) {
  const cmd = data?.version ? 'version' : 'ping';
  const path = data?.api ? `/api/${cmd}` : `/${cmd}`;
  const url = client.getUrl(data?.schema || '') + path;
  return client.post(url, undefined, {
    method: 'GET',
    responseText: !data?.json,
  });
}

export function create(client: IGqlClient): IPingApi {
  return {
    ping: (data?: PingOptionsInput) => ping(client, data),
  };
}
