import { presetAuth } from './auth/auth.preset';
import { presetClient } from './client/client.preset';
import { presetCore } from './core/core.preset';
import { presetDatabase } from './database/database.preset';
import { presetDev } from './dev/dev.preset';
import { presetLog } from './log/log.preset';
import { presetMigration } from './migration/migration.preset';
import { presetMode } from './mode/mode.preset';
import { presetPrisma } from './prisma/prisma.preset';
import { presetRoot } from './root/root.preset';
import { presetSchema } from './schema/schema.preset';
import { presetApollo } from './server/apollo/apollo.preset';
import { presetExpress } from './server/express/express.preset';
import { presetServer } from './server/server.preset';
import { presetShield } from './shield/shield.preset';
import { presetTenant } from './tenant/tenant.preset';
import { presetTest } from './testing/test.preset';
import { presetStorage } from './storage/storage.preset';
import { presetSettings } from './settings/settings.preset';

export const presets = {
  core: presetCore,
  database: presetDatabase,
  auth: presetAuth,
  dev: presetDev,
  log: presetLog,
  migration: presetMigration,
  mode: presetMode,
  prisma: presetPrisma,
  root: presetRoot,
  shield: presetShield,
  test: presetTest,
  tenant: presetTenant,
  client: presetClient,
  server: presetServer,
  schema: presetSchema,
  apollo: presetApollo,
  express: presetExpress,
  storage: presetStorage,
  settings: presetSettings,
};
