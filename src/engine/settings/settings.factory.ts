import { AppOptions } from '../options';

import { presetCore } from '../core/core.preset';

import { SettingsOptions, SettingsOptionsInput } from './settings.options';

import { presetSettings } from './settings.preset';

import {
  createGetSettingList,
  createGetSettings,
  createSaveSettings,
} from './settings.helper';

export async function createSettings(
  opts?: SettingsOptionsInput
): Promise<SettingsOptions> {
  const core = presetCore(opts);
  opts = await core.config(opts);

  const settings = await presetSettings(opts);

  settings.provider = buildProvider(settings, opts);

  return settings;
}

function buildProvider(settings: SettingsOptions, opts: AppOptions) {
  const provider = settings.provider || {};

  if (!provider.getSettings) {
    provider.getSettings = createGetSettings(opts);
  }

  if (!provider.saveSettings) {
    provider.saveSettings = createSaveSettings(opts);
  }

  if (!provider.getSettingList) {
    provider.getSettingList = createGetSettingList(opts);
  }

  return provider;
}
