import { AppOptions } from '../options';

export type Setting = {
  label?: string;
  defValue?: string;
  value?: string;
  note?: string;
  meta?: any;
  sort?: number;
  public?: boolean;
};

export type KeySetting = { id: string; key: string } & Setting;

export type SettingNullable = Setting | undefined | null;

export type KeySettings = Record<string, SettingNullable>;

export type SettingValue = string | undefined | null;

export type SettingsProvider = {
  getSettings?: (appId?: string) => Promise<Record<string, SettingValue>>;
  saveSettings?: (data: Record<string, string>) => Promise<number>;
  getSettingList?: (options?: {
    appId?: string;
    public?: boolean;
  }) => Promise<KeySetting[]>;
};

export type SettingsOptions = {
  keys?: KeySettings;
  provider?: SettingsProvider;
  constants?: {
    ENV_LOCAL_PREFIX?: string;
  };
};

export type SettingsOptionsInput = AppOptions;
