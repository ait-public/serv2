import constants from '../constants';
import { Setting, SettingsOptions } from './settings.options';
import { utils } from '@ait/share2';

let ENV: any = null;

export async function presetSettings(opts?: {
  settings?: SettingsOptions;
}): Promise<SettingsOptions> {
  const consts = {
    ENV_LOCAL_PREFIX: constants.SETTINGS.ENV_LOCAL_PREFIX,
    ...opts?.settings?.constants,
  };

  const env = ENV || (ENV = loadEnv(consts.ENV_LOCAL_PREFIX));
  const settings = opts?.settings?.keys || {};
  const keys = utils.mergeDeep(env, settings);

  return {
    constants: consts,
    ...opts?.settings,
    keys,
  };
}

function loadEnv(prefix: string) {
  if (!prefix || !process?.env) return {};
  return Object.keys(process.env)
    .filter((key) => key.startsWith(prefix))
    .reduce((r, ckey) => {
      const v = process.env[ckey];
      const pureKey = ckey.substring(prefix.length);
      if (pureKey) {
        r[pureKey] = { defValue: v };
      }
      return r;
    }, {} as Record<string, Setting>);
}
