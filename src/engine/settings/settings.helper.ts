import helper from '../_helper';
import { AppOptions } from '../options';
import { utils } from '@ait/share2';
import { presetCore } from '../core/core.preset';
import constants from '../constants';

const TABLE_NAME = 'core2."Setting"';

function escapeString(val?: string) {
  if (!val) return '';
  val = val.replace(/[\0\n\r\b\t\\'"\x1a]/g, function (s) {
    switch (s) {
      case '\0':
        return '\\0';
      case '\n':
        return '\\n';
      case '\r':
        return '\\r';
      case '\b':
        return '\\b';
      case '\t':
        return '\\t';
      case '\x1a':
        return '\\Z';
      case "'":
        return "''";
      case '"':
        return '""';
      default:
        return '\\' + s;
    }
  });

  return val;
}

export function createGetSettings(opts: AppOptions, tablename = TABLE_NAME) {
  // cache
  const allKeys = Object.keys(opts.settings?.keys || {});
  if (!allKeys.length) return () => Promise.resolve({});

  const sqlIn = allKeys
    .filter((c) => c)
    .map((c) => "'" + escapeString(c) + "'")
    .join(',');

  return async (appId?: string) => {
    if (!sqlIn && !appId) return {};

    const { tenantId, dbUrl, sql_SET_LOCAL } = getDbContext(opts);

    let OR_LIKE = '';
    if (appId) {
      OR_LIKE = ` OR "key" LIKE '${escapeString(appId)}.%' `;
    }

    const sql_SELECT = `
    SELECT
      "key", value
    FROM
      ${tablename}
    WHERE
      tenant_id='${tenantId}'
      AND ("key" IN (${sqlIn}) ${OR_LIKE})
    ORDER BY sort, "label";
    `;

    const recs: any = await helper.db.query({
      dbUrl,
      queryText: sql_SET_LOCAL + sql_SELECT,
    });

    let rdata = (recs[1].rows ?? []).reduce((d: any, c: any) => {
      d[c.key] = c.value;
      return d;
    }, {} as any);

    // check new settings for registered
    const newValues = allKeys
      .filter(
        (key) =>
          opts.root?.schema &&
          key.startsWith(opts.root.schema) &&
          !utils.has(rdata, key)
      )
      .map((key) => {
        const sett = opts.settings?.keys?.[key];
        return { key, sett };
      });

    if (newValues?.length) {
      const values = newValues
        .map(
          (kv) =>
            `
(
'${utils.uuidv4()}',
'${escapeString(kv.key)}',
'${escapeString(kv.sett?.value || kv.sett?.defValue)}',
'${escapeString(kv.sett?.label)}',
${kv.sett?.public ? 'true' : 'false'},
'${escapeString(kv.sett?.note)}',
${kv.sett?.sort || 10},
${kv.sett?.meta ? `'${JSON.stringify(kv.sett?.meta)}'` : 'NULL'},
'${tenantId}'
)`
        )
        .join(',');

      const queryText = `
${sql_SET_LOCAL}
INSERT INTO ${tablename}
  (id,"key",value,label,public,note,sort,meta,tenant_id)
VALUES
  ${values}
ON CONFLICT (tenant_id,"key") DO NOTHING;
${sql_SELECT}`;

      const r: any = await helper.db.query({
        dbUrl,
        queryText,
      });

      // reread
      rdata = (r[2].rows || []).reduce((d: any, c: any) => {
        d[c.key] = c.value;
        return d;
      }, {} as any);
    }

    return rdata;
  };
}

export function createGetSettingList(opts: AppOptions, tablename = TABLE_NAME) {
  return async (options?: { appId?: string; public?: boolean }) => {
    const { tenantId, dbUrl, sql_SET_LOCAL } = getDbContext(opts);

    let AND = '';
    if (options?.appId) {
      AND = ` AND "key" LIKE '${escapeString(options?.appId)}.%' `;
    }
    if (options?.public) {
      AND = ` AND "public"=true `;
    } else if (options?.public === false) {
      AND = ` AND "public"=false `;
    }

    const sql_SELECT = `
  SELECT
    id, value, "label", note, meta, sort, public, "key"
  FROM
    ${tablename}
  WHERE
    tenant_id='${tenantId}' ${AND}
  ORDER BY sort, "label";
  `;

    const recs: any = await helper.db.query({
      dbUrl,
      queryText: sql_SET_LOCAL + sql_SELECT,
    });

    return recs?.[1].rows;
  };
}

export function createSaveSettings(opts: AppOptions, tablename = TABLE_NAME) {
  return async function (data: Record<string, string>) {
    const keys = data && Object.keys(data);
    if (!keys?.length) return Promise.resolve(0);

    const { tenantId, dbUrl, sql_SET_LOCAL } = getDbContext(opts);
    const upd = keys.length;

    for (const key of keys) {
      const sql_UPDATE = `UPDATE ${tablename}
      SET value='${escapeString(data[key])}'
      WHERE
        ("key"='${escapeString(key)}'
          OR id='${escapeString(key)}')
        AND tenant_id='${tenantId}'`;

      await helper.db.query({
        dbUrl,
        queryText: sql_SET_LOCAL + sql_UPDATE,
      });
    }
    return upd;
  };
}

function getDbContext(opts: AppOptions) {
  const core = presetCore(opts);
  const dbUrl = core.dbConnection(opts);
  const currentTenantParamName =
    opts?.tenant?.pgCurrentTenantParamName ||
    constants.TENANT.PG_CURRENT_TENANT_PARAM_NAME;

  const tenantId =
    opts.tenant?.forceTenantId ||
    opts.contextStorage?.getStore()?.tenantId ||
    '';

  const sql_SET_LOCAL = tenantId
    ? `SET LOCAL ${currentTenantParamName} TO '${tenantId}';`
    : '';
  return { tenantId, dbUrl, sql_SET_LOCAL };
}
