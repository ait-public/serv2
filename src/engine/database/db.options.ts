export type DbOptions = {
  dbUrl?: string;
  host?: string;
  db?: string;
  user?: string;
  password?: string;
  schema?: string;
  port?: string | number;
  // BOUNCER PORT
  pgbouncer?: string | number;
  userTenant?: string;
  passwordTenant?: string;
};
