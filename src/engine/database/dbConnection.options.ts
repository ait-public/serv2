import { DbOptions } from './db.options';

export type DbConnectionOptions = {
  database?: DbOptions;
  mode?: { isTest?: boolean; isMigration?: boolean; isTenant?: boolean };
  root?: { schema?: string };
};
