import helper from '../_helper';
import { DbOptions } from '../options';
import { DbConnectionOptions } from './dbConnection.options';
import { presetDatabase } from './database.preset';
import { presetMode } from '../mode/mode.preset';
import { presetRoot } from '../root/root.preset';

export function createDbConnection(opts?: DbConnectionOptions) {
  const mode = presetMode(opts);
  const root = presetRoot({ ...opts, mode });
  const database: DbOptions = presetDatabase({ ...opts, mode, root });

  // BOUNCER PORT
  const hasBouncer =
    !mode?.isMigration &&
    database?.pgbouncer &&
    database.host !== 'localhost' &&
    database.host !== '127.0 0.1';

  // PG PORT
  const port = hasBouncer ? database?.pgbouncer : database?.port;

  // миграцию делаем напрямую в порт 5432
  const dbOpts = { ...database, bouncer: !!hasBouncer, port };

  // check tenant user connection
  if (mode.isTenant && database.userTenant) dbOpts.user = database.userTenant;
  if (mode.isTenant && database.passwordTenant)
    dbOpts.password = database.passwordTenant;

  return helper.db.buildUrl(dbOpts);
}
