import constants from '../constants';
import { DbOptions } from '../options';
import { customAlphabet } from 'nanoid';
import { utils } from '@ait/share2';
import { presetMode } from '../mode/mode.preset';
import { presetRoot } from '../root/root.preset';

const rand = customAlphabet('qwertyuioplkjhgfdsazxcvbnm', 3);

export function presetDatabase(opts?: {
  database?: DbOptions;
  mode?: { isTest?: boolean };
  root?: { schema?: string };
}): DbOptions {
  let dbSchema = opts?.database?.schema;
  if (!dbSchema) {
    const mode: { isTest?: boolean } = presetMode(opts);
    const root = presetRoot({ ...opts, mode });

    dbSchema = root.schema;
    if (mode?.isTest) {
      dbSchema = `${dbSchema}_${utils.formatDate(
        new Date(),
        'yyyyMMddHHmmss'
      )}_${rand()}`;
    }
  }

  return {
    host: opts?.database?.host ?? constants.ENV.AIT_POSTGRES_HOST ?? '',
    db: opts?.database?.db ?? constants.ENV.AIT_POSTGRES_DB ?? '',
    user: opts?.database?.user ?? constants.ENV.AIT_POSTGRES_USER ?? '',
    password:
      opts?.database?.password ?? constants.ENV.AIT_POSTGRES_PASSWORD ?? '',
    schema: dbSchema,
    port: opts?.database?.port ?? constants.ENV.AIT_POSTGRES_PORT ?? 5432,
    // BOUNCER PORT
    pgbouncer: opts?.database?.pgbouncer ?? constants.ENV.AIT_PGBOUNCER_PORT,
    userTenant:
      opts?.database?.userTenant ?? constants.ENV.AIT_POSTGRES_USER_TENANT,
    passwordTenant:
      opts?.database?.passwordTenant ??
      constants.ENV.AIT_POSTGRES_PASSWORD_TENANT,
  };
}
