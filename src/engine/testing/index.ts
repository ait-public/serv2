import { IGqlClient, IServer } from '../types';
import { AppOptions } from '../options';
import { createApp } from '../createApp';
import { presetCore } from '../core/core.preset';

export type TestContext<TPrisma> = {
  // http client
  gqlClient: IGqlClient;
  // db access
  prisma: TPrisma;
  // app config
  options: AppOptions;
  // db connection string
  dbUrl: string;
};

export function mockConsole(method?: any, value?: any) {
  if (!method) return null;

  const methods = Array.isArray(method) ? method : [method];

  return methods.map(
    (c) => jest.spyOn<any, any>(console, c).mockReturnValue(value).mockRestore
  );
}

export function createTestContext<TPrisma>(
  opts?: AppOptions
): TestContext<TPrisma> {
  let context: any = {};
  let server: IServer | null = null;
  let restores: any[] | null = null;

  opts = {
    ...opts,
    mode: { isMigration: true, isTest: true, ...opts?.mode },
    shield: {
      ...opts?.shield,
      options: {
        allowExternalErrors: true,
        debug: true,
      },
    },
  };

  if (opts?.test?.isBeforeEach) {
    beforeEach(doBefore());
    afterEach(doAfter());
  } else {
    beforeAll(doBefore());
    afterAll(doAfter());
  }

  return context;

  function doBefore(): jest.ProvidesCallback {
    return async () => {
      restores = mockConsole(opts?.test?.suppressLog);
      server = await createApp(opts);
      const options: AppOptions = await server.run();

      const core = presetCore(options);
      const { dbUrl, prisma } = await core.prismaOptions({
        ...options,
        mode: { isTenant: false },
        prisma: {},
      });

      context.dbUrl = dbUrl;
      context.prisma = prisma;
      context.options = options;
      context.gqlClient = options.client?.gqlClient;
    };
  }

  function doAfter(): jest.ProvidesCallback {
    return async () => {
      if (context?.prisma && context.prisma.$disconnect) {
        await context.prisma.$disconnect();
      }
      context = null;
      if (server) {
        await server.stop();
        server = null;
        await sleep(1000);
      }
      // log restore
      if (restores) restores.forEach((c) => c());
    };
  }
}

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
