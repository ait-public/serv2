export type TestOptions = {
  nodrop?: boolean;
  suppressLog?: string | string[] | object;
  isBeforeEach?: boolean;
};
