import { TestOptions } from '../options';

export function presetTest(opts?: { test?: TestOptions }): TestOptions {
  return {
    ...opts?.test,
  };
}
