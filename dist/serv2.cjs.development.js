'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var graphqlShield = require('graphql-shield');
var pg = require('pg');
var JWT$1 = require('jsonwebtoken');
var createError = _interopDefault(require('http-errors'));
var share2 = require('@ait/share2');
var bcrypt = _interopDefault(require('bcrypt'));
var fetch = _interopDefault(require('node-fetch'));
var https = _interopDefault(require('https'));
var FormData = _interopDefault(require('form-data'));
var util = _interopDefault(require('util'));
var path = require('path');
var fs = _interopDefault(require('fs'));
var async_hooks = require('async_hooks');
var perf_hooks = require('perf_hooks');
var plugins = require('@paljs/plugins');
var graphqlMiddleware = require('graphql-middleware');
var merge = require('@graphql-tools/merge');
var graphql$1 = require('graphql');
var crossFetch = require('cross-fetch');
var wrap = require('@graphql-tools/wrap');
var stitch = require('@graphql-tools/stitch');
var schema = require('@graphql-tools/schema');
var graphqlScalars = require('graphql-scalars');
var gql = _interopDefault(require('graphql-tag'));
var apolloServerExpress = require('apollo-server-express');
var apolloServerCore = require('apollo-server-core');
var express = _interopDefault(require('express'));
var compression = _interopDefault(require('compression'));
var getPort = _interopDefault(require('get-port'));
var nanoid = require('nanoid');
var graphqlRequest = require('graphql-request');

var ROOT = {
  SCHEMA: 'ait',
  AIT_EXTERNAL_URL: 'https://medportal-demo.ru',
  AIT_APP_VERSION: '',
  AIT_MAIL_TEST_ADDRESS: 'support@ait.ru'
};

var ISSUER = 'ev2.ru';
var SECRET = 'ait+ait+ait+ait+';
var JWT = {
  ISSUER: ISSUER,
  SECRET: SECRET,
  ANONYM: 'ANONYM',
  TOKEN: {
    SECRET: SECRET,
    EXPIRES_IN: 60 * 60 * 24
  },

  /**
   * Configuration of refresh token.
   * expiresIn - The time in minutes before the code token expires.  Default is 100 years.  Most if
   *             all refresh tokens are expected to not expire.  However, I give it a very long shelf
   *             life instead.
   */
  REFRESH_TOKEN: {
    SECRET: SECRET,
    EXPIRES_IN: 52560000
  },

  /**
   * Configuration of code token.
   * expiresIn - The time in minutes before the code token expires.  Default is 5 minutes.
   */
  CODE_TOKEN: {
    EXPIRES_IN: 5 * 60
  }
};

var APOLLO = {
  BG: 'dark',
  GRAPHQL_POSTFIX_URL: "/graphql"
};

var SERVER = {
  PORT: 3011,
  PUBLIC_DIR: 'public',
  JSON_LIMIT: '50mb'
};

var DEV = {
  MEDPORTAL_URL: 'https://medportal-demo.ru'
};

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  Object.defineProperty(Constructor, "prototype", {
    writable: false
  });
  return Constructor;
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _inheritsLoose(subClass, superClass) {
  subClass.prototype = Object.create(superClass.prototype);
  subClass.prototype.constructor = subClass;

  _setPrototypeOf(subClass, superClass);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _isNativeReflectConstruct() {
  if (typeof Reflect === "undefined" || !Reflect.construct) return false;
  if (Reflect.construct.sham) return false;
  if (typeof Proxy === "function") return true;

  try {
    Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    return true;
  } catch (e) {
    return false;
  }
}

function _construct(Parent, args, Class) {
  if (_isNativeReflectConstruct()) {
    _construct = Reflect.construct;
  } else {
    _construct = function _construct(Parent, args, Class) {
      var a = [null];
      a.push.apply(a, args);
      var Constructor = Function.bind.apply(Parent, a);
      var instance = new Constructor();
      if (Class) _setPrototypeOf(instance, Class.prototype);
      return instance;
    };
  }

  return _construct.apply(null, arguments);
}

function _isNativeFunction(fn) {
  return Function.toString.call(fn).indexOf("[native code]") !== -1;
}

function _wrapNativeSuper(Class) {
  var _cache = typeof Map === "function" ? new Map() : undefined;

  _wrapNativeSuper = function _wrapNativeSuper(Class) {
    if (Class === null || !_isNativeFunction(Class)) return Class;

    if (typeof Class !== "function") {
      throw new TypeError("Super expression must either be null or a function");
    }

    if (typeof _cache !== "undefined") {
      if (_cache.has(Class)) return _cache.get(Class);

      _cache.set(Class, Wrapper);
    }

    function Wrapper() {
      return _construct(Class, arguments, _getPrototypeOf(this).constructor);
    }

    Wrapper.prototype = Object.create(Class.prototype, {
      constructor: {
        value: Wrapper,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    return _setPrototypeOf(Wrapper, Class);
  };

  return _wrapNativeSuper(Class);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function _taggedTemplateLiteralLoose(strings, raw) {
  if (!raw) {
    raw = strings.slice(0);
  }

  strings.raw = raw;
  return strings;
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _createForOfIteratorHelperLoose(o, allowArrayLike) {
  var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"];
  if (it) return (it = it.call(o)).next.bind(it);

  if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
    if (it) o = it;
    var i = 0;
    return function () {
      if (i >= o.length) return {
        done: true
      };
      return {
        done: false,
        value: o[i++]
      };
    };
  }

  throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var runtime_1 = createCommonjsModule(function (module) {
/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined$1; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function define(obj, key, value) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
    return obj[key];
  }
  try {
    // IE 8 has a broken Object.defineProperty that only works on DOM objects.
    define({}, "");
  } catch (err) {
    define = function(obj, key, value) {
      return obj[key] = value;
    };
  }

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  define(IteratorPrototype, iteratorSymbol, function () {
    return this;
  });

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = GeneratorFunctionPrototype;
  define(Gp, "constructor", GeneratorFunctionPrototype);
  define(GeneratorFunctionPrototype, "constructor", GeneratorFunction);
  GeneratorFunction.displayName = define(
    GeneratorFunctionPrototype,
    toStringTagSymbol,
    "GeneratorFunction"
  );

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      define(prototype, method, function(arg) {
        return this._invoke(method, arg);
      });
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      define(genFun, toStringTagSymbol, "GeneratorFunction");
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
    return this;
  });
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined$1) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined$1;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined$1;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  define(Gp, toStringTagSymbol, "Generator");

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  define(Gp, iteratorSymbol, function() {
    return this;
  });

  define(Gp, "toString", function() {
    return "[object Generator]";
  });

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined$1;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined$1, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined$1;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined$1;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined$1;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined$1;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined$1;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   module.exports 
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, in modern engines
  // we can explicitly access globalThis. In older engines we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  if (typeof globalThis === "object") {
    globalThis.regeneratorRuntime = runtime;
  } else {
    Function("r", "regeneratorRuntime = r")(runtime);
  }
}
});

var isAuthenticated = /*#__PURE__*/graphqlShield.rule({
  cache: 'contextual'
})( /*#__PURE__*/function () {
  var _ref = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(parent, args, ctx, info) {
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", ctx.isAuthenticated);

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x, _x2, _x3, _x4) {
    return _ref.apply(this, arguments);
  };
}());
var isModeDev = /*#__PURE__*/graphqlShield.rule({
  cache: 'contextual'
})( /*#__PURE__*/function () {
  var _ref2 = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(parent, args, ctx, info) {
    var _ctx$options$mode;

    return runtime_1.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            return _context2.abrupt("return", !!((_ctx$options$mode = ctx.options.mode) != null && _ctx$options$mode.isDev));

          case 1:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x5, _x6, _x7, _x8) {
    return _ref2.apply(this, arguments);
  };
}());
var isModeProd = /*#__PURE__*/graphqlShield.rule({
  cache: 'contextual'
})( /*#__PURE__*/function () {
  var _ref3 = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3(parent, args, ctx, info) {
    var _ctx$options$mode2;

    return runtime_1.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            return _context3.abrupt("return", !!((_ctx$options$mode2 = ctx.options.mode) != null && _ctx$options$mode2.isProd));

          case 1:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function (_x9, _x10, _x11, _x12) {
    return _ref3.apply(this, arguments);
  };
}());
var isModeTest = /*#__PURE__*/graphqlShield.rule({
  cache: 'contextual'
})( /*#__PURE__*/function () {
  var _ref4 = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee4(parent, args, ctx, info) {
    var _ctx$options$mode3;

    return runtime_1.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            return _context4.abrupt("return", !!((_ctx$options$mode3 = ctx.options.mode) != null && _ctx$options$mode3.isTest));

          case 1:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function (_x13, _x14, _x15, _x16) {
    return _ref4.apply(this, arguments);
  };
}());
var isRoleSystem = /*#__PURE__*/graphqlShield.rule({
  cache: 'contextual'
})( /*#__PURE__*/function () {
  var _ref5 = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee5(parent, args, ctx, info) {
    return runtime_1.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            return _context5.abrupt("return", ctx.role === 'SYSTEM');

          case 1:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function (_x17, _x18, _x19, _x20) {
    return _ref5.apply(this, arguments);
  };
}());
var isRoleSysAdmin = /*#__PURE__*/graphqlShield.rule({
  cache: 'contextual'
})( /*#__PURE__*/function () {
  var _ref6 = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee6(parent, args, ctx, info) {
    return runtime_1.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            return _context6.abrupt("return", ctx.role === 'SYSADMIN');

          case 1:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));

  return function (_x21, _x22, _x23, _x24) {
    return _ref6.apply(this, arguments);
  };
}());
var isRoleAdmin = /*#__PURE__*/graphqlShield.rule({
  cache: 'contextual'
})( /*#__PURE__*/function () {
  var _ref7 = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee7(parent, args, ctx, info) {
    return runtime_1.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            return _context7.abrupt("return", ctx.role === 'ADMIN');

          case 1:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));

  return function (_x25, _x26, _x27, _x28) {
    return _ref7.apply(this, arguments);
  };
}());
var isRoleEmployee = /*#__PURE__*/graphqlShield.rule({
  cache: 'contextual'
})( /*#__PURE__*/function () {
  var _ref8 = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee8(parent, args, ctx, info) {
    return runtime_1.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            return _context8.abrupt("return", ctx.role === 'EMPLOYEE');

          case 1:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8);
  }));

  return function (_x29, _x30, _x31, _x32) {
    return _ref8.apply(this, arguments);
  };
}());
var isRolePatient = /*#__PURE__*/graphqlShield.rule({
  cache: 'contextual'
})( /*#__PURE__*/function () {
  var _ref9 = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee9(parent, args, ctx, info) {
    return runtime_1.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            return _context9.abrupt("return", ctx.role === 'PATIENT');

          case 1:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9);
  }));

  return function (_x33, _x34, _x35, _x36) {
    return _ref9.apply(this, arguments);
  };
}());
var isRoleUser = /*#__PURE__*/graphqlShield.rule({
  cache: 'contextual'
})( /*#__PURE__*/function () {
  var _ref10 = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee10(parent, args, ctx, info) {
    return runtime_1.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            return _context10.abrupt("return", ctx.role === 'USER');

          case 1:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10);
  }));

  return function (_x37, _x38, _x39, _x40) {
    return _ref10.apply(this, arguments);
  };
}());
var isRoleAnonym = /*#__PURE__*/graphqlShield.rule({
  cache: 'contextual'
})( /*#__PURE__*/function () {
  var _ref11 = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee11(parent, args, ctx, info) {
    return runtime_1.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            return _context11.abrupt("return", !ctx.role || ctx.role === 'ANONYM');

          case 1:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11);
  }));

  return function (_x41, _x42, _x43, _x44) {
    return _ref11.apply(this, arguments);
  };
}());
var isRoleSysAdminOrMore = /*#__PURE__*/graphqlShield.or(isRoleSysAdmin, isRoleSystem);
var isRoleAdminOrMore = /*#__PURE__*/graphqlShield.or(isRoleAdmin, isRoleSysAdmin, isRoleSystem);
var isRoleEmployeeOrMore = /*#__PURE__*/graphqlShield.or(isRoleEmployee, isRoleAdmin, isRoleSysAdmin, isRoleSystem);
var isRolePatientOrMore = /*#__PURE__*/graphqlShield.or(isRolePatient, isRoleEmployee, isRoleAdmin, isRoleSysAdmin, isRoleSystem);
var isRoleUserOrMore = /*#__PURE__*/graphqlShield.or(isRoleUser, isRolePatient, isRoleEmployee, isRoleAdmin, isRoleSysAdmin, isRoleSystem);

var rules = {
  __proto__: null,
  isAuthenticated: isAuthenticated,
  isModeDev: isModeDev,
  isModeProd: isModeProd,
  isModeTest: isModeTest,
  isRoleSystem: isRoleSystem,
  isRoleSysAdmin: isRoleSysAdmin,
  isRoleAdmin: isRoleAdmin,
  isRoleEmployee: isRoleEmployee,
  isRolePatient: isRolePatient,
  isRoleUser: isRoleUser,
  isRoleAnonym: isRoleAnonym,
  isRoleSysAdminOrMore: isRoleSysAdminOrMore,
  isRoleAdminOrMore: isRoleAdminOrMore,
  isRoleEmployeeOrMore: isRoleEmployeeOrMore,
  isRolePatientOrMore: isRolePatientOrMore,
  isRoleUserOrMore: isRoleUserOrMore
};

var ruleAllow = /*#__PURE__*/graphqlShield.or(isModeDev, isModeTest, isAuthenticated);
var SHIELD = {
  ruleTree: {
    Query: {
      '*': ruleAllow
    },
    Mutation: {
      '*': ruleAllow
    }
  },
  options: {
    debug: true,
    allowExternalErrors: true
  }
};

var SCHEMA = {
  typeDefs: 'type Query{hello:String} type Mutation{ping:String}',
  resolvers: [{
    Query: {
      hello: function hello() {
        return 'Hi!';
      }
    },
    Mutation: {
      ping: function ping() {
        return 'Pong';
      }
    }
  }]
};

require('dotenv-flow').config({
  silent: true
});

var ENV = {
  AIT_PLATFORM_TENANT_ID: process.env.AIT_PLATFORM_TENANT_ID,
  AIT_MIGRATE_ON_STARTUP: process.env.AIT_MIGRATE_ON_STARTUP,
  APP_SCHEMA: process.env.AIT__APP_SCHEMA || process.env.APP_SCHEMA,
  NODE_ENV: "development",
  AIT_KUBE: process.env.AIT_KUBE,
  AIT_EXTERNAL_URL: process.env.AIT_EXTERNAL_URL,
  AIT_APP_VERSION: process.env.AIT_APP_VERSION,
  AIT_JWT_SECRET: process.env.AIT_JWT_SECRET,
  PORT: process.env.AIT__PORT || process.env.PORT,
  AIT_LOGLEVEL: process.env.AIT_LOGLEVEL,
  AIT_POSTGRES_HOST: process.env.AIT_POSTGRES_HOST,
  AIT_POSTGRES_DB: process.env.AIT_POSTGRES_DB,
  AIT_POSTGRES_USER: process.env.AIT_POSTGRES_USER,
  AIT_POSTGRES_PASSWORD: process.env.AIT_POSTGRES_PASSWORD,
  AIT_POSTGRES_USER_TENANT: process.env.AIT_POSTGRES_USER_TENANT,
  AIT_POSTGRES_PASSWORD_TENANT: process.env.AIT_POSTGRES_PASSWORD_TENANT,
  AIT_POSTGRES_PORT: process.env.AIT_POSTGRES_PORT,
  AIT_PGBOUNCER_PORT: process.env.AIT_PGBOUNCER_PORT,
  AIT_MEDPORTAL_URL: process.env.AIT_MEDPORTAL_URL,
  AIT_MAIL_TEST_ADDRESS: process.env.AIT_MAIL_TEST_ADDRESS,
  npm_package_version: process.env.npm_package_version,
  AIT_MODE_TENANT: process.env.AIT__MODE_TENANT
};

var MIGRATION = {
  PRISMA_DEPLOY_CMD: 'migrate deploy',
  PRISMA_VERSION_CMD: '-v'
};

var CLIENT = {
  INGRESS_REWRITE_SCHEMAS: ['fserv', 'cron', 'xqconv', 'egisz', 'printx', 'cmon', 'bash'],
  ROLE: 'SYSTEM',
  EXP: 52560000
};

var TENANT = {
  PG_CURRENT_TENANT_PARAM_NAME: 'app.current_tenant',
  AIT_PLATFORM_TENANT_ID: '00000000-0000-0000-1000-000000000001'
};

var SETTINGS = {
  ENV_LOCAL_PREFIX: 'ait.'
};

var constants = {
  ROOT: ROOT,
  JWT: JWT,
  APOLLO: APOLLO,
  SERVER: SERVER,
  DEV: DEV,
  SHIELD: SHIELD,
  SCHEMA: SCHEMA,
  ENV: ENV,
  MIGRATION: MIGRATION,
  CLIENT: CLIENT,
  TENANT: TENANT,
  SETTINGS: SETTINGS
};

var dummy = function dummy(o) {
  throw new Error('Core is not initialized!');
};

var _core = {
  app: dummy,
  apollo: dummy,
  config: dummy,
  express: dummy,
  dbConnection: dummy,
  prismaOptions: dummy,
  prisma: dummy,
  context: dummy,
  schema: dummy,
  server: dummy,
  getUrl: dummy,
  clientOptions: dummy,
  gqlClient: dummy,
  shield: dummy,
  httpHeader: dummy,
  migrate: dummy,
  dbInstall: dummy,
  dbSeed: dummy,
  dbDeploy: dummy,
  tenants: dummy,
  useRlsMiddleware: dummy,
  settings: dummy,
  lookups: dummy
};
function initCore(core) {
  _core = _extends({}, _core, core);
  return _core;
}
function presetCore(opts) {
  return _extends({}, _core, opts == null ? void 0 : opts.core);
}

function parseUrl(dbUrl) {
  if (!dbUrl) return {
    match: false
  }; // postgresql://userId:Password@Server:5489/DbName?schema=dbSchema&pgbouncer=true

  var re = /^(postgresql):\/\/([^:]*):([^:]*)@([^:]*):(\d+)\/([^?]*)\?([^#]+)/;
  var r = dbUrl.match(re);

  if (r) {
    var p = split(r[7], '&');
    return {
      match: true,
      host: r[4],
      port: r[5],
      db: r[6],
      user: r[2],
      password: r[3],
      schema: p['schema'],
      bouncer: p['pgbouncer'] !== undefined ? p['pgbouncer'] === 'true' : undefined
    };
  } // "Server=92.63.98.130;Port=5432;Database=aitjwt;User Id=ait;Password=475@1;Pooling=true;MinPoolSize=10;MaxPoolSize=100;Enlist=true;Timeout=300;CommandTimeout=300"


  var sobj = split(dbUrl, ';');
  return {
    match: !!sobj['database'] && !!sobj['server'],
    host: sobj['server'],
    port: sobj['port'],
    db: sobj['database'],
    user: sobj['user id'],
    password: sobj['password'],
    schema: sobj['schema']
  };
}

function split(dbUrl, splitter) {
  return dbUrl.split(splitter).reduce(function (acc, c) {
    var kv = c.split('=');
    if (kv.length === 2) acc[kv[0].trim().toLowerCase()] = kv[1].trim();
    return acc;
  }, {});
}

function buildUrl(opts) {
  var dbUrl = opts.dbUrl,
      port = opts.port,
      db = opts.db,
      user = opts.user,
      password = opts.password,
      schema = opts.schema,
      server = opts.host,
      bouncer = opts.bouncer;

  if (dbUrl) {
    var _m$port, _m$db, _m$user, _m$password, _m$schema, _m$host, _m$bouncer;

    var m = parseUrl(dbUrl);

    if (!m.match) {
      throw new Error("Parse DbUrl error '" + dbUrl + "'");
    }

    port = (_m$port = m.port) != null ? _m$port : port;
    db = (_m$db = m.db) != null ? _m$db : db;
    user = (_m$user = m.user) != null ? _m$user : user;
    password = (_m$password = m.password) != null ? _m$password : password;
    schema = (_m$schema = m.schema) != null ? _m$schema : schema;
    server = (_m$host = m.host) != null ? _m$host : server;
    bouncer = (_m$bouncer = m.bouncer) != null ? _m$bouncer : bouncer;
  }

  var pgbouncer = bouncer ? 'true' : 'false';
  return "postgresql://" + user + ":" + password + "@" + server + ":" + port + "/" + db + "?schema=" + schema + "&pgbouncer=" + pgbouncer;
}

function dropSchema(_x, _x2) {
  return _dropSchema.apply(this, arguments);
}

function _dropSchema() {
  _dropSchema = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(dbUrl, schema) {
    var p;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (dbUrl) {
              _context.next = 2;
              break;
            }

            throw new Error('Cannot read property  dbUrl');

          case 2:
            p = parseUrl(dbUrl);
            schema = schema || p.schema;

            if (schema) {
              _context.next = 6;
              break;
            }

            throw new Error('schema not named');

          case 6:
            dbUrl = buildUrl(_extends({}, p, {
              schema: 'public',
              user: constants.ENV.AIT_POSTGRES_USER,
              password: constants.ENV.AIT_POSTGRES_PASSWORD,
              bouncer: false,
              port: constants.ENV.AIT_POSTGRES_PORT
            }));
            _context.next = 9;
            return query({
              dbUrl: dbUrl,
              queryText: "DROP SCHEMA IF EXISTS \"" + schema + "\" CASCADE"
            });

          case 9:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _dropSchema.apply(this, arguments);
}

function ping(_x3) {
  return _ping.apply(this, arguments);
}

function _ping() {
  _ping = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(dbUrl) {
    var r;
    return runtime_1.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (dbUrl) {
              _context2.next = 2;
              break;
            }

            throw new Error('Cannot read property  dbUrl');

          case 2:
            _context2.next = 4;
            return query({
              dbUrl: dbUrl,
              queryText: "SELECT NOW()"
            });

          case 4:
            r = _context2.sent;
            return _context2.abrupt("return", r.rows[0]);

          case 6:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _ping.apply(this, arguments);
}

function checkSchema(_x4, _x5) {
  return _checkSchema.apply(this, arguments);
}

function _checkSchema() {
  _checkSchema = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3(dbUrl, schema) {
    var _schema;

    var p, r;
    return runtime_1.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            p = parseUrl(dbUrl);

            if (p.match) {
              _context3.next = 3;
              break;
            }

            throw new Error('Cannot read property dbUrl');

          case 3:
            schema = (_schema = schema) != null ? _schema : p.schema;
            dbUrl = buildUrl(_extends({}, p, {
              schema: 'public',
              user: constants.ENV.AIT_POSTGRES_USER,
              password: constants.ENV.AIT_POSTGRES_PASSWORD,
              bouncer: false,
              port: constants.ENV.AIT_POSTGRES_PORT
            }));
            _context3.next = 7;
            return query({
              dbUrl: dbUrl,
              queryText: "SELECT EXISTS(SELECT 1 FROM pg_namespace WHERE nspname = '" + schema + "');"
            });

          case 7:
            r = _context3.sent;
            return _context3.abrupt("return", !!r.rows[0].exists);

          case 9:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _checkSchema.apply(this, arguments);
}

function lastMigration(_x6, _x7) {
  return _lastMigration.apply(this, arguments);
}

function _lastMigration() {
  _lastMigration = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee4(dbUrl, schema) {
    var _ref2, _ref3, _schema2;

    var p, r;
    return runtime_1.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            p = parseUrl(dbUrl);

            if (p.match) {
              _context4.next = 3;
              break;
            }

            throw new Error('Cannot read property dbUrl');

          case 3:
            schema = (_ref2 = (_ref3 = (_schema2 = schema) != null ? _schema2 : p.schema) != null ? _ref3 : constants.ENV.APP_SCHEMA) != null ? _ref2 : 'public';
            dbUrl = buildUrl(_extends({}, p, {
              schema: schema,
              user: constants.ENV.AIT_POSTGRES_USER,
              password: constants.ENV.AIT_POSTGRES_PASSWORD,
              bouncer: false,
              port: constants.ENV.AIT_POSTGRES_PORT
            }));
            _context4.next = 7;
            return query({
              dbUrl: dbUrl,
              queryText: "select id, migration_name, started_at, finished_at, applied_steps_count\n  FROM " + schema + ".\"_prisma_migrations\"\n  order by started_at desc limit 1"
            });

          case 7:
            r = _context4.sent;
            return _context4.abrupt("return", r.rows[0]);

          case 9:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _lastMigration.apply(this, arguments);
}

function checkAitInstallation(_x8) {
  return _checkAitInstallation.apply(this, arguments);
}

function _checkAitInstallation() {
  _checkAitInstallation = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee5(dbUrl) {
    var p, queryText, r;
    return runtime_1.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            p = parseUrl(dbUrl);

            if (p.match) {
              _context5.next = 3;
              break;
            }

            throw new Error('Cannot read property dbUrl');

          case 3:
            queryText = "\n  SELECT EXISTS (\n    SELECT *\n    FROM pg_catalog.pg_proc\n    JOIN pg_namespace ON pg_catalog.pg_proc.pronamespace = pg_namespace.oid\n            WHERE proname = 'ait_tenant_table_grant'\n        AND pg_namespace.nspname = 'public'\n)\n";
            _context5.prev = 4;
            _context5.next = 7;
            return query({
              dbUrl: dbUrl,
              queryText: queryText
            });

          case 7:
            r = _context5.sent;
            return _context5.abrupt("return", !!r.rows[0].exists);

          case 11:
            _context5.prev = 11;
            _context5.t0 = _context5["catch"](4);
            return _context5.abrupt("return", false);

          case 14:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5, null, [[4, 11]]);
  }));
  return _checkAitInstallation.apply(this, arguments);
}

function query(_x9) {
  return _query.apply(this, arguments);
}

function _query() {
  _query = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee6(_ref) {
    var dbUrl, queryText, values, client;
    return runtime_1.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            dbUrl = _ref.dbUrl, queryText = _ref.queryText, values = _ref.values;

            if (dbUrl) {
              _context6.next = 3;
              break;
            }

            throw new Error('Cannot read property  dbUrl');

          case 3:
            client = new pg.Client({
              connectionString: dbUrl
            }); // `CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

            _context6.next = 6;
            return client.connect();

          case 6:
            _context6.prev = 6;
            _context6.next = 9;
            return client.query(queryText, values);

          case 9:
            return _context6.abrupt("return", _context6.sent);

          case 10:
            _context6.prev = 10;
            _context6.next = 13;
            return client.end();

          case 13:
            return _context6.finish(10);

          case 14:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6, null, [[6,, 10, 14]]);
  }));
  return _query.apply(this, arguments);
}

var db = {
  parseUrl: parseUrl,
  buildUrl: buildUrl,
  dropSchema: dropSchema,
  ping: ping,
  checkSchema: checkSchema,
  lastMigration: lastMigration,
  checkAitInstallation: checkAitInstallation,
  query: query
};

function createToken(opts) {
  var sub = (opts == null ? void 0 : opts.userId) || '';
  var name = (opts == null ? void 0 : opts.name) || '';
  var secret = (opts == null ? void 0 : opts.secret) || constants.JWT.SECRET;
  var exp = (opts == null ? void 0 : opts.exp) || 3600;
  var role = (opts == null ? void 0 : opts.role) || constants.JWT.ANONYM;
  var clientId = opts == null ? void 0 : opts.clientId;
  var issuer = (opts == null ? void 0 : opts.issuer) || constants.JWT.ISSUER;
  var sessionId = opts == null ? void 0 : opts.sessionId;
  var profileId = opts == null ? void 0 : opts.profileId;
  var tenantId = opts == null ? void 0 : opts.tenantId;
  return new Promise(function (resolve, reject) {
    var payload = {
      jti: share2.utils.uuidv4(),
      sub: sub,
      // jwt (iat, exp) измеряется в секундах
      exp: Math.floor(Date.now() / 1000) + exp,
      name: name,
      role: role,
      clientId: clientId || '',
      sessionId: sessionId,
      tenantId: tenantId,
      profileId: profileId
    };
    var options = {
      // expiresIn: `1d`,
      issuer: issuer
    };
    JWT$1.sign(payload, secret, options, function (err, token) {
      if (err) {
        console.log(err.message);
        reject(new createError.InternalServerError());
        return;
      }

      resolve(token);
    });
  });
}

function verifyToken(opts) {
  var token = opts.token,
      secret = opts.secret;
  return new Promise(function (resolve, reject) {
    var secretOrPublicKey = secret || constants.JWT.TOKEN.SECRET;
    JWT$1.verify(token, secretOrPublicKey, function (err, payload) {
      if (err) {
        var message = err.name === 'JsonWebTokenError' ? 'Unauthorized' : err.message;
        reject(new createError.Unauthorized(message));
        return;
      }

      resolve(payload);
    });
  });
}

function createAccessToken(opts) {
  return createToken({
    userId: opts == null ? void 0 : opts.userId,
    name: opts == null ? void 0 : opts.name,
    role: opts == null ? void 0 : opts.role,
    exp: (opts == null ? void 0 : opts.exp) || constants.JWT.TOKEN.EXPIRES_IN,
    secret: (opts == null ? void 0 : opts.secret) || constants.JWT.TOKEN.SECRET,
    clientId: opts == null ? void 0 : opts.clientId,
    issuer: opts == null ? void 0 : opts.issuer,
    sessionId: opts == null ? void 0 : opts.sessionId,
    profileId: opts == null ? void 0 : opts.profileId,
    tenantId: opts == null ? void 0 : opts.tenantId,
    ip: opts == null ? void 0 : opts.ip
  });
}

function createRefreshToken(opts) {
  return createToken({
    userId: opts == null ? void 0 : opts.userId,
    name: opts == null ? void 0 : opts.name,
    role: opts == null ? void 0 : opts.role,
    exp: (opts == null ? void 0 : opts.exp) || constants.JWT.REFRESH_TOKEN.EXPIRES_IN,
    secret: (opts == null ? void 0 : opts.secret) || constants.JWT.REFRESH_TOKEN.SECRET,
    clientId: opts == null ? void 0 : opts.clientId,
    issuer: opts == null ? void 0 : opts.issuer,
    sessionId: opts == null ? void 0 : opts.sessionId,
    profileId: opts == null ? void 0 : opts.profileId,
    tenantId: opts == null ? void 0 : opts.tenantId,
    ip: opts == null ? void 0 : opts.ip
  });
}

function getSafePayload(_x) {
  return _getSafePayload.apply(this, arguments);
}

function _getSafePayload() {
  _getSafePayload = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return verifyToken({
              token: opts.token,
              secret: opts.secret
            });

          case 3:
            return _context.abrupt("return", _context.sent);

          case 6:
            _context.prev = 6;
            _context.t0 = _context["catch"](0);

          case 8:
            return _context.abrupt("return", undefined);

          case 9:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 6]]);
  }));
  return _getSafePayload.apply(this, arguments);
}

function getPayload(_x2) {
  return _getPayload.apply(this, arguments);
}

function _getPayload() {
  _getPayload = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(opts) {
    var _auth$refreshToken$se, _auth$refreshToken, _auth$token$secret, _auth$token, _yield$getSafePayload;

    var token, auth, refreshTokenSecret, tokenSecret;
    return runtime_1.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            token = opts.token, auth = opts.auth;

            if (token) {
              _context2.next = 3;
              break;
            }

            return _context2.abrupt("return", undefined);

          case 3:
            refreshTokenSecret = (_auth$refreshToken$se = (_auth$refreshToken = auth.refreshToken) == null ? void 0 : _auth$refreshToken.secret) != null ? _auth$refreshToken$se : constants.JWT.REFRESH_TOKEN.SECRET;
            tokenSecret = (_auth$token$secret = (_auth$token = auth.token) == null ? void 0 : _auth$token.secret) != null ? _auth$token$secret : constants.JWT.TOKEN.SECRET;
            _context2.next = 7;
            return getSafePayload({
              token: token,
              secret: refreshTokenSecret
            });

          case 7:
            _context2.t0 = _yield$getSafePayload = _context2.sent;

            if (!(_context2.t0 != null)) {
              _context2.next = 12;
              break;
            }

            _context2.t1 = _yield$getSafePayload;
            _context2.next = 13;
            break;

          case 12:
            _context2.t1 = getSafePayload({
              token: token,
              secret: tokenSecret
            });

          case 13:
            return _context2.abrupt("return", _context2.t1);

          case 14:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _getPayload.apply(this, arguments);
}

function createTokens(_x3) {
  return _createTokens.apply(this, arguments);
}

function _createTokens() {
  _createTokens = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3(opts) {
    var _auth$token2, _root$schema, _auth$token3, _auth$refreshToken2;

    var user, auth, root, tOpts, accessToken, refreshToken;
    return runtime_1.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            user = opts.user, auth = opts.auth, root = opts.root;
            tOpts = _extends({
              userId: user == null ? void 0 : user.id,
              name: user == null ? void 0 : user.name,
              role: user == null ? void 0 : user.role,
              secret: (auth == null ? void 0 : (_auth$token2 = auth.token) == null ? void 0 : _auth$token2.secret) || constants.JWT.TOKEN.SECRET,
              clientId: (_root$schema = root == null ? void 0 : root.schema) != null ? _root$schema : constants.ROOT.SCHEMA
            }, opts == null ? void 0 : opts.data);
            _context3.next = 4;
            return createAccessToken(_extends({}, tOpts, {
              secret: (auth == null ? void 0 : (_auth$token3 = auth.token) == null ? void 0 : _auth$token3.secret) || constants.JWT.TOKEN.SECRET
            }));

          case 4:
            accessToken = _context3.sent;
            _context3.next = 7;
            return createRefreshToken(_extends({}, tOpts, {
              secret: (auth == null ? void 0 : (_auth$refreshToken2 = auth.refreshToken) == null ? void 0 : _auth$refreshToken2.secret) || constants.JWT.REFRESH_TOKEN.SECRET
            }));

          case 7:
            refreshToken = _context3.sent;
            return _context3.abrupt("return", {
              accessToken: accessToken,
              refreshToken: refreshToken
            });

          case 9:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _createTokens.apply(this, arguments);
}

var jwtHelper = {
  createToken: createToken,
  createTokens: createTokens,
  verifyToken: verifyToken,
  getPayload: getPayload
};

function hashPassword(_x) {
  return _hashPassword.apply(this, arguments);
}

function _hashPassword() {
  _hashPassword = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(password) {
    var salt, hashedPassword;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return bcrypt.genSalt(10);

          case 2:
            salt = _context.sent;
            _context.next = 5;
            return bcrypt.hash(password, salt);

          case 5:
            hashedPassword = _context.sent;
            return _context.abrupt("return", hashedPassword);

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _hashPassword.apply(this, arguments);
}

function isValidPassword(_x2, _x3) {
  return _isValidPassword.apply(this, arguments);
}

function _isValidPassword() {
  _isValidPassword = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(noencrypted, encrypted) {
    return runtime_1.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return bcrypt.compare(noencrypted, encrypted);

          case 3:
            return _context2.abrupt("return", _context2.sent);

          case 6:
            _context2.prev = 6;
            _context2.t0 = _context2["catch"](0);
            return _context2.abrupt("return", false);

          case 9:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 6]]);
  }));
  return _isValidPassword.apply(this, arguments);
}

var psw = {
  hashPassword: hashPassword,
  isValidPassword: isValidPassword
};

var httpsAgent = /*#__PURE__*/new https.Agent({
  rejectUnauthorized: false
});
var PostError = /*#__PURE__*/function (_Error) {
  _inheritsLoose(PostError, _Error);

  function PostError(url, status, message) {
    var _this;

    _this = _Error.call(this, message) || this;
    _this.name = 'AitPostError';
    _this.url = url;
    _this.status = status;
    return _this;
  }

  return PostError;
}( /*#__PURE__*/_wrapNativeSuper(Error));

function getResult(_x, _x2) {
  return _getResult.apply(this, arguments);
}

function _getResult() {
  _getResult = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(r, asText) {
    var data, headers, msg;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!(r.status >= 200 && r.status < 300)) {
              _context.next = 4;
              break;
            }

            if (!(r.status === 204)) {
              _context.next = 3;
              break;
            }

            return _context.abrupt("return");

          case 3:
            return _context.abrupt("return", asText ? r.text() : r.json());

          case 4:
            _context.next = 6;
            return r.text();

          case 6:
            data = _context.sent;
            headers = r.headers || {};
            msg = r.url + " status: " + (r == null ? void 0 : r.status) + "\n    headers:\n" + Object.keys(headers).map(function (k) {
              return "   " + k + ": " + headers.get(k);
            }).join('\r\n') + "\n\n    data:\n" + data + "\n\n";
            throw new PostError(r.url, r.status, msg);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _getResult.apply(this, arguments);
}

function fetchEx(_x3, _x4, _x5) {
  return _fetchEx.apply(this, arguments);
}

function _fetchEx() {
  _fetchEx = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(url, data, init) {
    var _init$contentType, _init$method;

    var contentType, method, body, formData, name, headers, res;
    return runtime_1.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            contentType = (_init$contentType = init == null ? void 0 : init.contentType) != null ? _init$contentType : 'application/json';
            method = (_init$method = init == null ? void 0 : init.method) != null ? _init$method : 'POST';
            body = init == null ? void 0 : init.body;

            if (body) {
              _context2.next = 16;
              break;
            }

            if (!(typeof data === 'string')) {
              _context2.next = 8;
              break;
            }

            body = data;
            _context2.next = 16;
            break;

          case 8:
            if (!contentType) {
              _context2.next = 16;
              break;
            }

            _context2.t0 = contentType;
            _context2.next = _context2.t0 === 'application/json' ? 12 : _context2.t0 === 'multipart/form-data' ? 14 : 16;
            break;

          case 12:
            if (method !== 'GET') body = JSON.stringify(data);
            return _context2.abrupt("break", 16);

          case 14:
            if (typeof data === 'object') {
              // DO NOT supply headers with 'Content-Type' if it's using FormData.
              contentType = undefined;
              formData = new FormData();

              for (name in data) {
                if (data[name] !== undefined) formData.append(name, data[name]);
              }

              body = formData;
            }

            return _context2.abrupt("break", 16);

          case 16:
            if (contentType || init != null && init.headers) {
              headers = _extends({}, contentType && {
                'Content-Type': contentType
              }, init == null ? void 0 : init.headers);
            }

            _context2.next = 19;
            return fetch(url, {
              method: method,
              headers: headers,
              body: body,
              agent: url.startsWith('https://') ? httpsAgent : undefined
            });

          case 19:
            res = _context2.sent;

            if (!(init != null && init.getResult)) {
              _context2.next = 22;
              break;
            }

            return _context2.abrupt("return", init.getResult(res, init));

          case 22:
            return _context2.abrupt("return", getResult(res, init == null ? void 0 : init.responseText));

          case 23:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _fetchEx.apply(this, arguments);
}

var exec = /*#__PURE__*/util.promisify( /*#__PURE__*/require('child_process').exec);
function npxPrisma(_x, _x2) {
  return _npxPrisma.apply(this, arguments);
}

function _npxPrisma() {
  _npxPrisma = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(cmd, env) {
    var ENV, p;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (cmd) {
              _context.next = 3;
              break;
            }

            console.error('cmd is empty');
            return _context.abrupt("return", false);

          case 3:
            ENV = _extends({}, process.env, env);
            console.log("npx prisma " + cmd);
            _context.prev = 5;
            _context.next = 8;
            return exec(getPrismaBinPath() + " " + cmd, {
              env: ENV
            });

          case 8:
            p = _context.sent;

            if (p.stderr) {
              if (p.stderr && p.stderr.startsWith && !p.stderr.startsWith('Debugger attached.')) {
                console.error(p.stderr);
              }
            }

            if (p.stdout) console.log(p.stdout);
            return _context.abrupt("return", !!p.stdout);

          case 14:
            _context.prev = 14;
            _context.t0 = _context["catch"](5);
            console.error('npx', _context.t0);
            return _context.abrupt("return", false);

          case 18:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[5, 14]]);
  }));
  return _npxPrisma.apply(this, arguments);
}

function getPrismaBinPath() {
  var PRISMA_BINARY = path.join(process.cwd(), 'node_modules', '.bin', 'prisma');

  if (!fs.existsSync(PRISMA_BINARY)) {
    PRISMA_BINARY = path.join(process.cwd(), 'server', 'node_modules', '.bin', 'prisma');
  }

  if (!fs.existsSync(PRISMA_BINARY)) throw new Error("\u041F\u0443\u0442\u044C \u043A .binprisma \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D '" + PRISMA_BINARY + "'!");
  return PRISMA_BINARY;
}

function prismaVersion(_x) {
  return _prismaVersion.apply(this, arguments);
}

function _prismaVersion() {
  _prismaVersion = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(_ref) {
    var _cmdVersion;

    var cmdVersion;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            cmdVersion = _ref.cmdVersion;
            cmdVersion = (_cmdVersion = cmdVersion) != null ? _cmdVersion : constants.MIGRATION.PRISMA_VERSION_CMD; // log info...

            _context.next = 4;
            return npxPrisma(cmdVersion);

          case 4:
            return _context.abrupt("return", _context.sent);

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _prismaVersion.apply(this, arguments);
}

function prismaDeploy(_x) {
  return _prismaDeploy.apply(this, arguments);
}

function _prismaDeploy() {
  _prismaDeploy = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(_ref) {
    var _cmdDeploy;

    var dbUrl, cmdDeploy, ENV;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            dbUrl = _ref.dbUrl, cmdDeploy = _ref.cmdDeploy;
            // console.log('db deploy - start');
            cmdDeploy = (_cmdDeploy = cmdDeploy) != null ? _cmdDeploy : constants.MIGRATION.PRISMA_DEPLOY_CMD; // Set the required environment variable to contain the connection string
            // to our database test schema
            // Run the migrations to ensure our schema has the required structure

            ENV = dbUrl && {
              DATABASE_URL: dbUrl
            };
            _context.next = 5;
            return npxPrisma(cmdDeploy, ENV);

          case 5:
            return _context.abrupt("return", _context.sent);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _prismaDeploy.apply(this, arguments);
}

var npx = {
  prismaDeploy: prismaDeploy,
  prismaVersion: prismaVersion,
  npxPrisma: npxPrisma
};

var storage = /*#__PURE__*/new async_hooks.AsyncLocalStorage();

function presetStorage(opts) {
  var _opts$contextStorage;

  return (_opts$contextStorage = opts == null ? void 0 : opts.contextStorage) != null ? _opts$contextStorage : storage;
}

function createScope(create) {
  return /*#__PURE__*/function () {
    var _inTenantScope = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(opts, tenantId, callback) {
      var storage, tenant;
      return runtime_1.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              storage = presetStorage(opts);
              tenant = _extends({}, storage.getStore(), {
                tenantId: tenantId
              });
              return _context2.abrupt("return", storage.run(tenant, /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee() {
                var _yield$create, prisma, dbUrl;

                return runtime_1.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return create(opts);

                      case 2:
                        _yield$create = _context.sent;
                        prisma = _yield$create.prisma;
                        dbUrl = _yield$create.dbUrl;
                        _context.prev = 5;
                        _context.next = 8;
                        return callback({
                          tenantId: tenantId,
                          tenant: tenant,
                          prisma: prisma,
                          dbUrl: dbUrl
                        });

                      case 8:
                        return _context.abrupt("return", _context.sent);

                      case 9:
                        _context.prev = 9;

                        if (!prisma) {
                          _context.next = 13;
                          break;
                        }

                        _context.next = 13;
                        return prisma.$disconnect();

                      case 13:
                        return _context.finish(9);

                      case 14:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, null, [[5,, 9, 14]]);
              }))));

            case 3:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function inTenantScope(_x, _x2, _x3) {
      return _inTenantScope.apply(this, arguments);
    }

    return inTenantScope;
  }();
}

function newAitPrisma(_x4) {
  return _newAitPrisma.apply(this, arguments);
}

function _newAitPrisma() {
  _newAitPrisma = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3(options) {
    var _options$prisma, _options$prisma2;

    var core, _yield$core$prismaOpt, prisma, dbUrl;

    return runtime_1.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            core = presetCore(options);
            _context3.next = 3;
            return core.prismaOptions(_extends({}, options, {
              mode: _extends({}, options.mode, {
                isMigration: false,
                isTenant: false
              }),
              prisma: {
                isLogged: (_options$prisma = options.prisma) == null ? void 0 : _options$prisma.isLogged,
                middlewares: _extends({}, (_options$prisma2 = options.prisma) == null ? void 0 : _options$prisma2.middlewares, {
                  rls: true
                })
              },
              database: _extends({}, options.database, {
                dbUrl: undefined
              })
            }));

          case 3:
            _yield$core$prismaOpt = _context3.sent;
            prisma = _yield$core$prismaOpt.prisma;
            dbUrl = _yield$core$prismaOpt.dbUrl;

            if (dbUrl) {
              _context3.next = 8;
              break;
            }

            throw new Error('dbUrl is null');

          case 8:
            return _context3.abrupt("return", {
              prisma: prisma,
              dbUrl: dbUrl
            });

          case 9:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _newAitPrisma.apply(this, arguments);
}

function newTenantPrisma(_x5) {
  return _newTenantPrisma.apply(this, arguments);
}

function _newTenantPrisma() {
  _newTenantPrisma = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee4(options) {
    var _options$mode, _options$prisma3, _options$prisma4;

    var core, _yield$core$prismaOpt2, prisma, dbUrl;

    return runtime_1.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            core = presetCore(options);
            _context4.next = 3;
            return core.prismaOptions(_extends({}, options, {
              mode: _extends({}, options.mode, {
                isMigration: false,
                isTenant: (_options$mode = options.mode) == null ? void 0 : _options$mode.isTenant
              }),
              prisma: {
                isLogged: (_options$prisma3 = options.prisma) == null ? void 0 : _options$prisma3.isLogged,
                middlewares: _extends({}, (_options$prisma4 = options.prisma) == null ? void 0 : _options$prisma4.middlewares, {
                  rls: true
                })
              },
              database: _extends({}, options.database, {
                dbUrl: undefined
              })
            }));

          case 3:
            _yield$core$prismaOpt2 = _context4.sent;
            prisma = _yield$core$prismaOpt2.prisma;
            dbUrl = _yield$core$prismaOpt2.dbUrl;

            if (dbUrl) {
              _context4.next = 8;
              break;
            }

            throw new Error('dbUrl is null');

          case 8:
            return _context4.abrupt("return", {
              prisma: prisma,
              dbUrl: dbUrl
            });

          case 9:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _newTenantPrisma.apply(this, arguments);
}

var ctx = {
  inTenantScope: /*#__PURE__*/createScope(newTenantPrisma),
  inAitScope: /*#__PURE__*/createScope(newAitPrisma)
};

function startTimer() {
  // import 'acemodes'
  var startTime = 0;
  var endTime = 0;

  function endT() {
    endTime = perf_hooks.performance.now();
    return Math.round(((endTime - startTime) / 1000 + Number.EPSILON) * 100) / 100;
  }

  function start() {
    startTime = perf_hooks.performance.now();
    return endT;
  }

  return start();
}

var helper = {
  db: db,
  jwt: jwtHelper,
  psw: psw,
  fetch: fetchEx,
  ctx: ctx,
  npx: npx,
  timer: {
    start: startTimer
  }
};

function createApp(_x) {
  return _createApp.apply(this, arguments);
}

function _createApp() {
  _createApp = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee5(opts) {
    var _options$server, _options$server$expre, _options$server2, _options$server2$expr;

    var core, options, _options$prisma, prisma, dbUrl, appServer, _ref, isMigration, isTest, _run, _stop, app, baseUrl;

    return runtime_1.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            core = presetCore(opts); // load congif

            _context5.next = 3;
            return core.config(opts);

          case 3:
            options = _context5.sent;
            _context5.next = 6;
            return core.prismaOptions(options);

          case 6:
            _options$prisma = options.prisma = _context5.sent;
            prisma = _options$prisma.prisma;
            dbUrl = _options$prisma.dbUrl;
            _context5.next = 11;
            return core.clientOptions(options);

          case 11:
            options.client = _context5.sent;
            _context5.next = 14;
            return core.server(options);

          case 14:
            options.server = _context5.sent;
            appServer = options.server.appServer;

            if (appServer) {
              _context5.next = 18;
              break;
            }

            throw new Error('appServer not initialized');

          case 18:
            _ref = options.mode || {}, isMigration = _ref.isMigration, isTest = _ref.isTest;
            _run = appServer.run; // override run

            appServer.run = /*#__PURE__*/function () {
              var _ref2 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts, listeningListener) {
                return runtime_1.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        if (!isMigration) {
                          _context.next = 15;
                          break;
                        }

                        if (!isTest) {
                          listeningListener && listeningListener({});
                        }

                        _context.prev = 2;
                        _context.next = 5;
                        return core.migrate(options);

                      case 5:
                        _context.next = 15;
                        break;

                      case 7:
                        _context.prev = 7;
                        _context.t0 = _context["catch"](2);

                        if (isTest) {
                          _context.next = 13;
                          break;
                        }

                        console.error(_context.t0, function () {
                          return process.exit(1);
                        });
                        _context.next = 15;
                        break;

                      case 13:
                        console.error(_context.t0);
                        throw _context.t0;

                      case 15:
                        if (!(isTest || !isMigration)) {
                          _context.next = 18;
                          break;
                        }

                        _context.next = 18;
                        return _run(null, listeningListener);

                      case 18:
                        if (!(isMigration && !isTest)) {
                          _context.next = 21;
                          break;
                        }

                        _context.next = 21;
                        return appServer.stop();

                      case 21:
                        return _context.abrupt("return", options);

                      case 22:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, null, [[2, 7]]);
              }));

              return function (_x2, _x3) {
                return _ref2.apply(this, arguments);
              };
            }();

            _stop = appServer.stop;
            appServer.stop = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2() {
              var _options$test, _options$mode;

              return runtime_1.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (!(prisma != null && prisma.$disconnect)) {
                        _context2.next = 3;
                        break;
                      }

                      _context2.next = 3;
                      return prisma.$disconnect();

                    case 3:
                      if (!_stop) {
                        _context2.next = 6;
                        break;
                      }

                      _context2.next = 6;
                      return _stop();

                    case 6:
                      if (!isTest) {
                        _context2.next = 13;
                        break;
                      }

                      if (!(!(options != null && (_options$test = options.test) != null && _options$test.nodrop) && options != null && (_options$mode = options.mode) != null && _options$mode.isMigration && dbUrl)) {
                        _context2.next = 13;
                        break;
                      }

                      _context2.next = 10;
                      return helper.db.checkSchema(dbUrl);

                    case 10:
                      if (!_context2.sent) {
                        _context2.next = 13;
                        break;
                      }

                      _context2.next = 13;
                      return helper.db.dropSchema(dbUrl);

                    case 13:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2);
            }));
            app = (_options$server = options.server) == null ? void 0 : (_options$server$expre = _options$server.express) == null ? void 0 : _options$server$expre.app;
            baseUrl = (_options$server2 = options.server) == null ? void 0 : (_options$server2$expr = _options$server2.express) == null ? void 0 : _options$server2$expr.baseUrl;

            if (app) {
              _context5.next = 27;
              break;
            }

            throw new Error('express mot initialized!');

          case 27:
            app.all(baseUrl + "/api/ping", /*#__PURE__*/function () {
              var _ref4 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3(req, res) {
                var r, error;
                return runtime_1.wrap(function _callee3$(_context3) {
                  while (1) {
                    switch (_context3.prev = _context3.next) {
                      case 0:
                        _context3.prev = 0;

                        if (dbUrl) {
                          _context3.next = 3;
                          break;
                        }

                        throw new Error('dbUrl not initialized');

                      case 3:
                        _context3.next = 5;
                        return helper.db.ping(dbUrl);

                      case 5:
                        r = _context3.sent;
                        res.json(r);
                        _context3.next = 14;
                        break;

                      case 9:
                        _context3.prev = 9;
                        _context3.t0 = _context3["catch"](0);
                        error = _context3.t0 instanceof Error ? _context3.t0 == null ? void 0 : _context3.t0.message : _context3.t0 + '';
                        console.error(error);
                        res.status(500).json({
                          error: error
                        });

                      case 14:
                      case "end":
                        return _context3.stop();
                    }
                  }
                }, _callee3, null, [[0, 9]]);
              }));

              return function (_x4, _x5) {
                return _ref4.apply(this, arguments);
              };
            }());
            app.all(baseUrl + "/api/version", /*#__PURE__*/function () {
              var _ref5 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee4(req, res) {
                var r, error;
                return runtime_1.wrap(function _callee4$(_context4) {
                  while (1) {
                    switch (_context4.prev = _context4.next) {
                      case 0:
                        _context4.prev = 0;

                        if (dbUrl) {
                          _context4.next = 3;
                          break;
                        }

                        throw new Error('dbUrl not initialized');

                      case 3:
                        _context4.next = 5;
                        return helper.db.lastMigration(dbUrl);

                      case 5:
                        r = _context4.sent;
                        res.json(r);
                        _context4.next = 14;
                        break;

                      case 9:
                        _context4.prev = 9;
                        _context4.t0 = _context4["catch"](0);
                        error = _context4.t0 instanceof Error ? _context4.t0 == null ? void 0 : _context4.t0.message : _context4.t0 + '';
                        console.error(error);
                        res.status(500).json({
                          error: error
                        });

                      case 14:
                      case "end":
                        return _context4.stop();
                    }
                  }
                }, _callee4, null, [[0, 9]]);
              }));

              return function (_x6, _x7) {
                return _ref5.apply(this, arguments);
              };
            }());
            return _context5.abrupt("return", options);

          case 30:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));
  return _createApp.apply(this, arguments);
}

function makeRemoteExecutor(url, options) {
  if (options === void 0) {
    options = {
      log: false
    };
  }

  var headers = _extends({}, options.headers, {
    'Content-Type': 'application/json'
  });

  return /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(_ref) {
      var document, variables, query, fetchResult;
      return runtime_1.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              document = _ref.document, variables = _ref.variables;
              query = typeof document === 'string' ? document : graphql$1.print(document);
              if (options.log) console.log("# -- OPERATION " + new Date().toISOString() + ":\n" + query);
              _context.next = 5;
              return crossFetch.fetch(url, {
                method: 'POST',
                headers: headers,
                body: JSON.stringify({
                  query: query,
                  variables: variables
                })
              });

            case 5:
              fetchResult = _context.sent;
              return _context.abrupt("return", fetchResult.json());

            case 7:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x) {
      return _ref2.apply(this, arguments);
    };
  }();
}

var ExecutorStore = function ExecutorStore() {}; // static executors: Map<{ url: string }, any> = new Map<{ url: string }, any>();


ExecutorStore.executors = {};
var defaultRemoteTypeFuncArgs = function defaultRemoteTypeFuncArgs(ids, remoteTypePkName) {
  var _where;

  return {
    where: (_where = {}, _where[remoteTypePkName] = {
      "in": ids
    }, _where)
  };
};
function subSchema(_x2, _x3, _x4) {
  return _subSchema.apply(this, arguments);
}

function _subSchema() {
  _subSchema = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(url, remotes, options) {
    var executor, config;
    return runtime_1.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            if (options === void 0) {
              options = {
                log: false,
                batch: true,
                valuesFromResults: undefined
              };
            }

            if (options.log) console.log('# -- MERGING URL: ' + url);
            executor = ExecutorStore.executors[url];

            if (!executor) {
              executor = makeRemoteExecutor(url, {
                log: options.log,
                headers: options.headers
              });
              ExecutorStore.executors[url] = executor;
            }

            config = {
              schema: wrap.introspectSchema(executor),
              executor: executor,
              batch: options.batch,
              merge: {//   [remoteTypeName]: {
                //     selectionSet: `{ ${remoteTypePkName} }`,
                //     fieldName: remoteTypeFunc,
                //     key: (obj) => obj[remoteTypePkName], // pluck a key from each record in the array
                //     argsFromKeys: remoteTypeFuncArgs, // format all plucked keys into query args
                //   },
              }
            };
            remotes.forEach(function (remote) {
              var _Object$assign;

              Object.assign(config.merge, (_Object$assign = {}, _Object$assign[remote.remoteTypeName] = {
                selectionSet: "{ " + remote.remoteTypePkName + " }",
                fieldName: remote.remoteTypeFunc,
                key: function key(obj) {
                  return obj[remote.remoteTypePkName];
                },
                argsFromKeys: remote.remoteTypeFuncArgs,
                valuesFromResults: function valuesFromResults(results, keys) {
                  // юзаем внешнюю сотртировалку если есть
                  if (options.valuesFromResults) return options.valuesFromResults(results, keys); // graphql-tools bugfix: при сортировке по внешнему полю подтянутые в batching зависимости не сопоставляются как надо
                  // поэтому сами сортируем массив объектов results в соответствии с массивом keys

                  var sortedArray = keys.map(function (i) {
                    return results.find(function (j) {
                      return j[remote.remoteTypePkName] === i;
                    });
                  });
                  return sortedArray;
                }
              }, _Object$assign));
            });
            if (options.log) console.log('# -- MERGE: ', config.merge);
            return _context2.abrupt("return", config);

          case 8:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _subSchema.apply(this, arguments);
}

function makeGatewaySchema(_x5, _x6, _x7, _x8) {
  return _makeGatewaySchema.apply(this, arguments);
}

function _makeGatewaySchema() {
  _makeGatewaySchema = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3(typeDefs, resolvers, subSchemas, typeMergingOptions) {
    var localSchema, resolvedSubSchemas, stitchedSchema;
    return runtime_1.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            localSchema = schema.makeExecutableSchema({
              typeDefs: typeDefs,
              resolvers: resolvers != null ? resolvers : []
            }); // TODO: ?? Promise.allSettled(all);

            _context3.next = 3;
            return Promise.all(subSchemas);

          case 3:
            resolvedSubSchemas = _context3.sent;
            stitchedSchema = stitch.stitchSchemas({
              subschemas: [].concat(resolvedSubSchemas, [localSchema]),
              typeMergingOptions: typeMergingOptions
            });
            return _context3.abrupt("return", stitchedSchema);

          case 6:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _makeGatewaySchema.apply(this, arguments);
}

var graphqlTools = {
  __proto__: null,
  defaultRemoteTypeFuncArgs: defaultRemoteTypeFuncArgs,
  subSchema: subSchema,
  makeGatewaySchema: makeGatewaySchema
};

function presetSchema(opts) {
  return _extends({}, opts == null ? void 0 : opts.schema);
}

var ROLES = ['ANONYM', 'USER', 'PATIENT', 'EMPLOYEE', 'ADMIN', 'SYSADMIN', 'SYSTEM'];

var _sessions = /*#__PURE__*/new Map();

var resolvers = {
  Query: {
    me: /*#__PURE__*/function () {
      var _me = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(_, args, _ref, _any) {
        var userId, cti, user;
        return runtime_1.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                userId = _ref.userId;
                cti = _sessions.get(userId || '');

                if (cti) {
                  _context.next = 4;
                  break;
                }

                throw new createError.NotFound('Пользователь не зарегистрирован');

              case 4:
                user = {
                  id: userId,
                  name: cti.name,
                  createdAt: cti.createdAt,
                  email: cti.name + "@test.ru",
                  isDisabled: false,
                  personId: cti.userId,
                  role: cti.role,
                  profile: {
                    id: cti.profileId,
                    role: cti.role,
                    name: 'Тестовый ' + cti.role
                  }
                };
                return _context.abrupt("return", user);

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function me(_x, _x2, _x3, _x4) {
        return _me.apply(this, arguments);
      }

      return me;
    }()
  },
  Mutation: {
    login: /*#__PURE__*/function () {
      var _login = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(_, args, _ref2, _any) {
        var _ref2$options, auth, root, name, password, tenantId, profileId, role, userId, data, _yield$helper$jwt$cre, accessToken, refreshToken;

        return runtime_1.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _ref2$options = _ref2.options, auth = _ref2$options.auth, root = _ref2$options.root;
                _context2.prev = 1;
                name = args.name, password = args.password, tenantId = args.tenantId, profileId = args.profileId;

                if (!(password && password !== name)) {
                  _context2.next = 5;
                  break;
                }

                throw new createError.Unauthorized('Введено неверное имя пользователя или пароль');

              case 5:
                role = 'SYSADMIN';

                if (ROLES.includes(password)) {
                  role = password;
                }

                userId = share2.utils.uuidv4();
                data = {
                  sessionId: userId,
                  userId: userId,
                  name: name,
                  tenantId: tenantId,
                  profileId: profileId,
                  role: role
                };

                _sessions.set(userId, data);

                _context2.next = 12;
                return helper.jwt.createTokens({
                  data: data,
                  auth: auth,
                  root: root
                });

              case 12:
                _yield$helper$jwt$cre = _context2.sent;
                accessToken = _yield$helper$jwt$cre.accessToken;
                refreshToken = _yield$helper$jwt$cre.refreshToken;
                return _context2.abrupt("return", {
                  accessToken: accessToken,
                  refreshToken: refreshToken
                });

              case 18:
                _context2.prev = 18;
                _context2.t0 = _context2["catch"](1);
                console.error('login', _context2.t0);
                throw _context2.t0;

              case 22:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[1, 18]]);
      }));

      function login(_x5, _x6, _x7, _x8) {
        return _login.apply(this, arguments);
      }

      return login;
    }(),
    register: /*#__PURE__*/function () {
      var _register = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3() {
        return runtime_1.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                throw new createError.NotImplemented();

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function register() {
        return _register.apply(this, arguments);
      }

      return register;
    }(),
    refreshToken: /*#__PURE__*/function () {
      var _refreshToken2 = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee4(_, _ref3, _ref4, __) {
        var _auth$refreshToken;

        var _refreshToken, _ref4$options, auth, root, payload, userId, cti;

        return runtime_1.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _refreshToken = _ref3.refreshToken;
                _ref4$options = _ref4.options, auth = _ref4$options.auth, root = _ref4$options.root;

                if (_refreshToken) {
                  _context4.next = 4;
                  break;
                }

                throw new createError.BadRequest();

              case 4:
                _context4.next = 6;
                return helper.jwt.verifyToken({
                  token: _refreshToken,
                  secret: auth == null ? void 0 : (_auth$refreshToken = auth.refreshToken) == null ? void 0 : _auth$refreshToken.secret
                });

              case 6:
                payload = _context4.sent;

                if (payload) {
                  _context4.next = 9;
                  break;
                }

                throw new createError.NotFound('Пользователь не зарегистрирован');

              case 9:
                userId = payload.sub; //userId

                cti = _sessions.get(userId);

                if (cti) {
                  _context4.next = 13;
                  break;
                }

                throw new createError.NotFound('Пользователь не зарегистрирован');

              case 13:
                return _context4.abrupt("return", helper.jwt.createTokens({
                  data: cti,
                  auth: auth,
                  root: root
                }));

              case 14:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function refreshToken(_x9, _x10, _x11, _x12) {
        return _refreshToken2.apply(this, arguments);
      }

      return refreshToken;
    }(),
    logout: /*#__PURE__*/function () {
      var _logout = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee5(_, __any, _ref5, __) {
        var userId;
        return runtime_1.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                userId = _ref5.userId;
                return _context5.abrupt("return", _sessions["delete"](userId || ''));

              case 2:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }));

      function logout(_x13, _x14, _x15, _x16) {
        return _logout.apply(this, arguments);
      }

      return logout;
    }()
  }
};

var Auth = {
  Query: {
    me: /*#__PURE__*/function () {
      var _me = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(p, args, ctx, meta) {
        var _ctx$options$mode;

        return runtime_1.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!((_ctx$options$mode = ctx.options.mode) != null && _ctx$options$mode.isTest)) {
                  _context.next = 2;
                  break;
                }

                return _context.abrupt("return", resolvers.Query.me(p, args, ctx, meta));

              case 2:
                throw new createError.NotImplemented();

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function me(_x, _x2, _x3, _x4) {
        return _me.apply(this, arguments);
      }

      return me;
    }()
  },
  Mutation: {
    login: /*#__PURE__*/function () {
      var _login = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(p, args, ctx, meta) {
        var _ctx$options$mode2;

        return runtime_1.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!((_ctx$options$mode2 = ctx.options.mode) != null && _ctx$options$mode2.isTest)) {
                  _context2.next = 2;
                  break;
                }

                return _context2.abrupt("return", resolvers.Mutation.login(p, args, ctx, meta));

              case 2:
                throw new createError.NotImplemented();

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function login(_x5, _x6, _x7, _x8) {
        return _login.apply(this, arguments);
      }

      return login;
    }(),
    register: /*#__PURE__*/function () {
      var _register = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3() {
        return runtime_1.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                throw new createError.NotImplemented();

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function register() {
        return _register.apply(this, arguments);
      }

      return register;
    }(),
    refreshToken: /*#__PURE__*/function () {
      var _refreshToken = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee4(p, args, ctx, meta) {
        var _ctx$options$mode3;

        return runtime_1.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                if (!((_ctx$options$mode3 = ctx.options.mode) != null && _ctx$options$mode3.isTest)) {
                  _context4.next = 2;
                  break;
                }

                return _context4.abrupt("return", resolvers.Mutation.refreshToken(p, args, ctx, meta));

              case 2:
                throw new createError.NotImplemented();

              case 3:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function refreshToken(_x9, _x10, _x11, _x12) {
        return _refreshToken.apply(this, arguments);
      }

      return refreshToken;
    }(),
    logout: /*#__PURE__*/function () {
      var _logout = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee5(p, args, ctx, meta) {
        var _ctx$options$mode4;

        return runtime_1.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                if (!((_ctx$options$mode4 = ctx.options.mode) != null && _ctx$options$mode4.isTest)) {
                  _context5.next = 2;
                  break;
                }

                return _context5.abrupt("return", resolvers.Mutation.logout(p, args, ctx, meta));

              case 2:
                throw new createError.NotImplemented();

              case 3:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }));

      function logout(_x13, _x14, _x15, _x16) {
        return _logout.apply(this, arguments);
      }

      return logout;
    }()
  }
};

var returnOnError = function returnOnError(operation, alternative) {
  try {
    return operation();
  } catch (e) {
    return alternative;
  }
}; // serialize: Эта функция вызывается, когда значение передается клиенту. Здесь вы можете вернуть все, что угодно, если это может быть действительный JSON. Это означает, что вы можете сериализовать его в строку, числа, объекты и массивы.
// parseValue: Эта функция вызывается, когда необходимо проанализировать входной параметр.
// parseLiteral: Эта функция вызывается, когда необходимо проанализировать встроенный входной параметр. Вместо того, чтобы возвращать значение, он вернет узел AST. (GraphQL использует абстрактное синтаксическое дерево для анализа запроса)


var LocalDateTimeResolver = /*#__PURE__*/new graphql$1.GraphQLScalarType({
  name: 'LocalDateTime',
  description: 'Дата-время без часового пояса в календарной системе ISO-8601, например 2007-12-03T10:15:30, используется для дней рождений...',
  parseValue: function parseValue(value) {
    // console.log('parseValue', value);
    return returnOnError(function () {
      return value == null ? null : new Date(value);
    }, null);
  },
  serialize: function serialize(value) {
    if (value instanceof Date) {
      return getYYYYMMDD(value);
    } else if (typeof value === 'string') {
      return value; // const m = moment(value);
      // return m.format("DD.MM.YYYY HH:mm:ss");
    } else if (typeof value === 'number') {
      returnOnError(function () {
        return value == null ? null : new Date(value);
      }, null);
    } // tslint:disable-next-line:no-null-keyword


    return undefined;
  },
  parseLiteral: function parseLiteral(ast) {
    if (ast.kind === graphql$1.Kind.STRING) {
      var _sdate;

      var sdate = ast.value; //
      // yyyy-mm-dd

      if (((_sdate = sdate) == null ? void 0 : _sdate.length) === 10) {
        // fixed 2000-01-01  => 2000-01-01 00:00:00
        sdate = sdate + 'T00:00:00';
      }

      return getYYYYMMDD(new Date(sdate)); // oddValue(parseInt(ast.value, 10));
    } else if (ast.kind === graphql$1.Kind.INT) {
      return new Date(ast.value);
    }

    return null;
  }
});

function getYYYYMMDD(value) {
  var yyyy = value.getFullYear();
  var MM = padLeft(value.getMonth() + 1);
  var dd = padLeft(value.getDate());
  var hh = padLeft(value.getHours());
  var mm = padLeft(value.getMinutes());
  var ss = padLeft(value.getSeconds());
  return yyyy + "-" + MM + "-" + dd + "T" + hh + ":" + mm + ":" + ss;
}

function padStart(value, length, _char) {
  value = value + '';
  var len = length - value.length;

  if (len <= 0) {
    return value;
  }

  return Array(len + 1).join(_char) + value;
}

function padLeft(num) {
  return padStart(num, 2, '0');
}

var returnOnError$1 = function returnOnError(operation, alternative) {
  try {
    return operation();
  } catch (e) {
    return alternative;
  }
}; // serialize: Эта функция вызывается, когда значение передается клиенту. Здесь вы можете вернуть все, что угодно, если это может быть действительный JSON. Это означает, что вы можете сериализовать его в строку, числа, объекты и массивы.
// parseValue: Эта функция вызывается, когда необходимо проанализировать входной параметр.
// parseLiteral: Эта функция вызывается, когда необходимо проанализировать встроенный входной параметр. Вместо того, чтобы возвращать значение, он вернет узел AST. (GraphQL использует абстрактное синтаксическое дерево для анализа запроса)


var DateTimeResolver = /*#__PURE__*/new graphql$1.GraphQLScalarType({
  name: 'DateTime',
  description: 'Дата-время без часового пояса в календарной системе ISO-8601, например 2007-12-03T10:15:30, используется для дней рождений...',
  parseValue: function parseValue(value) {
    // console.log('parseValue', value);
    return returnOnError$1(function () {
      return value == null ? null : new Date(value);
    }, null);
  },
  serialize: function serialize(value) {
    // console.log("serialize", value);
    if (value instanceof Date) {
      return getYYYYMMDD$1(value);
    } else if (typeof value === 'string') {
      return value; // const m = moment(value);
      // return m.format("DD.MM.YYYY HH:mm:ss");
    } else if (typeof value === 'number') {
      returnOnError$1(function () {
        return value == null ? null : new Date(value);
      }, null);
    } // tslint:disable-next-line:no-null-keyword


    return undefined;
  },
  parseLiteral: function parseLiteral(ast) {
    // console.log("parseLiteral", ast);
    if (ast.kind === graphql$1.Kind.STRING) {
      var _sdate;

      var sdate = ast.value; //
      // yyyy-mm-dd

      if (((_sdate = sdate) == null ? void 0 : _sdate.length) === 10) {
        // fixed 2000-01-01  => 2000-01-01T00:00:00
        sdate = sdate + 'T00:00:00';
      }

      return getYYYYMMDD$1(new Date(sdate)); // oddValue(parseInt(ast.value, 10));
    } else if (ast.kind === graphql$1.Kind.INT) {
      return new Date(ast.value);
    }

    return null;
  }
});

function getYYYYMMDD$1(value) {
  var yyyy = value.getFullYear();
  var MM = padLeft$1(value.getMonth() + 1);
  var dd = padLeft$1(value.getDate());
  var hh = padLeft$1(value.getHours());
  var mm = padLeft$1(value.getMinutes());
  var ss = padLeft$1(value.getSeconds());
  return yyyy + "-" + MM + "-" + dd + "T" + hh + ":" + mm + ":" + ss;
}

function padStart$1(value, length, _char) {
  value = value + '';
  var len = length - value.length;

  if (len <= 0) {
    return value;
  }

  return Array(len + 1).join(_char) + value;
}

function padLeft$1(num) {
  return padStart$1(num, 2, '0');
}

var resolvers$1 = {
  LocalDateTime: LocalDateTimeResolver,
  // JSON: JSONResolver,
  GUID: graphqlScalars.GUIDResolver,
  EmailAddress: graphqlScalars.EmailAddressResolver,
  LocalTime: graphqlScalars.LocalTimeResolver,
  // ----------
  DateTime: DateTimeResolver,
  JSONObject: graphqlScalars.JSONObjectResolver,
  LocalDate: graphqlScalars.LocalDateResolver,
  Json: /*#__PURE__*/_extends({}, graphqlScalars.JSONResolver, {
    name: 'Json'
  })
};

var typeDefs = "\n  scalar DateTime\n  scalar JSONObject\n  scalar LocalDateTime\n  scalar LocalDate\n  scalar GUID\n  scalar EmailAddress\n  scalar LocalTime\n  scalar Json\n";

var scalars = {
  resolvers: resolvers$1,
  typeDefs: typeDefs
};

function presetMode(opts) {
  var _opts$mode$isDev, _opts$mode, _opts$mode$isProd, _opts$mode2, _opts$mode$isTest, _opts$mode3, _opts$mode$isKube, _opts$mode4, _opts$mode$isMigratio, _opts$mode5, _opts$mode$isTenant, _opts$mode6;

  var isDev = (_opts$mode$isDev = opts == null ? void 0 : (_opts$mode = opts.mode) == null ? void 0 : _opts$mode.isDev) != null ? _opts$mode$isDev : constants.ENV.NODE_ENV === 'development';
  var isProd = (_opts$mode$isProd = opts == null ? void 0 : (_opts$mode2 = opts.mode) == null ? void 0 : _opts$mode2.isProd) != null ? _opts$mode$isProd : constants.ENV.NODE_ENV === 'production';
  var isTest = (_opts$mode$isTest = opts == null ? void 0 : (_opts$mode3 = opts.mode) == null ? void 0 : _opts$mode3.isTest) != null ? _opts$mode$isTest : constants.ENV.NODE_ENV === 'test';
  var isKube = (_opts$mode$isKube = opts == null ? void 0 : (_opts$mode4 = opts.mode) == null ? void 0 : _opts$mode4.isKube) != null ? _opts$mode$isKube : constants.ENV.AIT_KUBE === 'true';
  var isMigration = (_opts$mode$isMigratio = opts == null ? void 0 : (_opts$mode5 = opts.mode) == null ? void 0 : _opts$mode5.isMigration) != null ? _opts$mode$isMigratio : constants.ENV.AIT_MIGRATE_ON_STARTUP === 'true';
  var isTenant = (_opts$mode$isTenant = opts == null ? void 0 : (_opts$mode6 = opts.mode) == null ? void 0 : _opts$mode6.isTenant) != null ? _opts$mode$isTenant : constants.ENV.AIT_MODE_TENANT === 'true';
  return {
    isKube: isKube,
    isMigration: isMigration,
    isDev: isDev,
    isProd: isProd,
    isTest: isTest,
    isTenant: isTenant
  };
}

function presetRoot(opts) {
  var _ref, _opts$root$schema, _opts$root, _ref2, _opts$root$externalUr, _opts$root2, _opts$root$mailTestAd, _opts$root3, _constants$ENV$AIT_MA, _ref3, _opts$root$appVersion, _opts$root4, _opts$root$version, _opts$root5;

  var mode = presetMode(opts);
  var isTest = mode.isTest;
  var schema = (_ref = (_opts$root$schema = opts == null ? void 0 : (_opts$root = opts.root) == null ? void 0 : _opts$root.schema) != null ? _opts$root$schema : constants.ENV.APP_SCHEMA) != null ? _ref : constants.ROOT.SCHEMA;
  var externalUrl = (_ref2 = (_opts$root$externalUr = opts == null ? void 0 : (_opts$root2 = opts.root) == null ? void 0 : _opts$root2.externalUrl) != null ? _opts$root$externalUr : constants.ENV.AIT_EXTERNAL_URL) != null ? _ref2 : constants.ROOT.AIT_EXTERNAL_URL;
  var mailTestAddress = ((_opts$root$mailTestAd = opts == null ? void 0 : (_opts$root3 = opts.root) == null ? void 0 : _opts$root3.mailTestAddress) != null ? _opts$root$mailTestAd : isTest) ? (_constants$ENV$AIT_MA = constants.ENV.AIT_MAIL_TEST_ADDRESS) != null ? _constants$ENV$AIT_MA : constants.ROOT.AIT_MAIL_TEST_ADDRESS : constants.ENV.AIT_MAIL_TEST_ADDRESS;
  var appVersion = (_ref3 = (_opts$root$appVersion = opts == null ? void 0 : (_opts$root4 = opts.root) == null ? void 0 : _opts$root4.appVersion) != null ? _opts$root$appVersion : constants.ENV.AIT_APP_VERSION) != null ? _ref3 : constants.ROOT.AIT_APP_VERSION;
  var version = (_opts$root$version = opts == null ? void 0 : (_opts$root5 = opts.root) == null ? void 0 : _opts$root5.version) != null ? _opts$root$version : constants.ENV.npm_package_version;
  return _extends({
    schema: schema,
    externalUrl: externalUrl,
    appVersion: appVersion,
    version: version,
    mailTestAddress: mailTestAddress
  }, opts == null ? void 0 : opts.root);
}

function presetAuth(opts) {
  var _ref, _opts$auth$secretKey, _opts$auth;

  var secretKey = (_ref = (_opts$auth$secretKey = opts == null ? void 0 : (_opts$auth = opts.auth) == null ? void 0 : _opts$auth.secretKey) != null ? _opts$auth$secretKey : constants.ENV.AIT_JWT_SECRET) != null ? _ref : constants.JWT.SECRET;
  return {
    secretKey: secretKey,
    token: {
      secret: secretKey
    },
    refreshToken: {
      secret: secretKey
    }
  };
}

function presetClient(opts) {
  var _opts$client;

  return _extends({}, opts == null ? void 0 : opts.client, {
    headers: opts == null ? void 0 : (_opts$client = opts.client) == null ? void 0 : _opts$client.headers
  });
}

/**
 * Global header
 */

function createHttpHeader(_x) {
  return _createHttpHeader.apply(this, arguments);
}

function _createHttpHeader() {
  _createHttpHeader = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var _root$schema, _auth$token;

    var mode, root, auth, client, clientId, headers, token;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            mode = presetMode(opts);
            root = presetRoot(_extends({}, opts, {
              mode: mode
            }));
            auth = presetAuth(opts);
            client = presetClient(opts);
            clientId = (_root$schema = root == null ? void 0 : root.schema) != null ? _root$schema : constants.ROOT.SCHEMA;
            headers = _extends({
              'x-client-id': clientId
            }, client == null ? void 0 : client.headers);
            _context.next = 8;
            return createClientToken({
              clientId: clientId,
              secret: auth == null ? void 0 : (_auth$token = auth.token) == null ? void 0 : _auth$token.secret
            });

          case 8:
            token = _context.sent;

            if (token) {
              headers['Authorization'] = "Bearer " + token;
            }

            return _context.abrupt("return", headers);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _createHttpHeader.apply(this, arguments);
}

function createClientToken(_x2) {
  return _createClientToken.apply(this, arguments);
}

function _createClientToken() {
  _createClientToken = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(opts) {
    return runtime_1.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            return _context2.abrupt("return", jwtHelper.createToken(_extends({}, opts, {
              role: constants.CLIENT.ROLE,
              exp: constants.CLIENT.EXP
            })));

          case 1:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _createClientToken.apply(this, arguments);
}

var _templateObject, _templateObject2;
var POOL_JOB_SIZE = 5;
var GET_JOBS = /*#__PURE__*/gql(_templateObject || (_templateObject = /*#__PURE__*/_taggedTemplateLiteralLoose(["\n  query jobs($ids: [String!]) {\n    jobs(where: { id: { in: $ids } }) {\n      id\n      name\n      cronTime\n      curl\n      enabled\n    }\n  }\n"])));
var SAVE_JOB = /*#__PURE__*/gql(_templateObject2 || (_templateObject2 = /*#__PURE__*/_taggedTemplateLiteralLoose(["\n  mutation saveJob(\n    $id: ID!\n    $name: String\n    $cronTime: String!\n    $curl: String!\n    $enabled: Boolean!\n  ) {\n    saveJob(\n      job: {\n        id: $id\n        cronTime: $cronTime\n        name: $name\n        curl: $curl\n        enabled: $enabled\n      }\n    ) {\n      id\n      name\n      curl\n      enabled\n      cronTime\n    }\n  }\n"])));
/**
 * Команда для периодического выполнения заданий
 * @example
const c = new Job<any>();
c.add({
      id: 'monitor2.mail-dispatch',
      cronTime: "* * * * * *",
      name: 'рассылка писем',
      runFirst: true,
      resolve: async (_parent, args, context) => {...}
});
 */

var Job = /*#__PURE__*/function () {
  function Job() {}

  Job.getCurl = function getCurl(url, id, data, headers) {
    var h = headers && Object.keys(headers).map(function (k) {
      return "-H \"" + k + ": " + headers[k] + "\"";
    }).join(' ') || '';
    return "curl -X POST " + url + " -H \"Content-Type: application/json\" " + h + " -d '" + JSON.stringify({
      query: "mutation runCronJob($id:String!,$data:JSONObject){runCronJob(id:$id,data:$data)}",
      variables: {
        data: data,
        id: id
      }
    }) + "'";
  }
  /**
  * Добавить задачу
  * @example
  c.add({
        id: 'monitor2.mail-dispatch',
        cronTime: "* * * * * *",
        name: 'рассылка писем',
        runFirst: true,
        resolve: async (_parent, args, context) => {...}
  });
  */
  ;

  var _proto = Job.prototype;

  _proto.add = function add(opts) {
    Job.jobs[opts.id] = {
      opts: opts,
      dump: {
        errors: [],
        logs: []
      }
    };
    return this;
  };

  Job.initJobs = /*#__PURE__*/function () {
    var _initJobs = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(parent, args, context, info) {
      var _context$options$root, _context$options$auth, _context$options$auth2;

      var jobs, client, clientId, url, ids, items, jwt, h, cid, copts, headers, curl, item;
      return runtime_1.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              jobs = Job.jobs;
              client = context.gqlClient.get('cron');
              clientId = ((_context$options$root = context.options.root) == null ? void 0 : _context$options$root.schema) || '';
              url = context.gqlClient.getUrl(clientId) + '/graphql';
              ids = Job.getIds();
              _context.next = 7;
              return client.request(GET_JOBS, ids);

            case 7:
              items = _context.sent.jobs;
              items.forEach(function (item) {
                var c = jobs[item.id];
                if (c) c.item = item;
              });
              _context.next = 11;
              return createClientToken({
                clientId: 'cron',
                tenantId: context.tenantId,
                secret: (_context$options$auth = context.options.auth) == null ? void 0 : (_context$options$auth2 = _context$options$auth.token) == null ? void 0 : _context$options$auth2.secret
              });

            case 11:
              jwt = _context.sent;
              _context.next = 14;
              return presetCore(context.options).httpHeader();

            case 14:
              h = _context.sent;
              h['Authorization'] = 'Bearer ' + jwt;
              _context.t0 = runtime_1.keys(jobs);

            case 17:
              if ((_context.t1 = _context.t0()).done) {
                _context.next = 34;
                break;
              }

              cid = _context.t1.value;
              copts = jobs[cid].opts;
              headers = _extends({}, h, copts == null ? void 0 : copts.headers);
              _context.prev = 21;
              curl = Job.getCurl(url, cid, copts.data, headers);
              _context.next = 25;
              return client.request(SAVE_JOB, {
                id: cid,
                name: copts.name,
                cronTime: copts.cronTime,
                curl: curl,
                enabled: !!copts.runFirst
              });

            case 25:
              item = _context.sent.saveJob;
              jobs[cid].item = item;
              _context.next = 32;
              break;

            case 29:
              _context.prev = 29;
              _context.t2 = _context["catch"](21);
              console.error('initCronJobs', cid, _context.t2);

            case 32:
              _context.next = 17;
              break;

            case 34:
              return _context.abrupt("return", {
                date: new Date(),
                jobs: ids.map(function (cid) {
                  var _jobs$cid;

                  return (_jobs$cid = jobs[cid]) == null ? void 0 : _jobs$cid.item;
                }).filter(function (c) {
                  return !!c;
                }).map(function (c) {
                  return {
                    id: c == null ? void 0 : c.id,
                    name: c == null ? void 0 : c.name,
                    enabled: c == null ? void 0 : c.enabled,
                    cronTime: c == null ? void 0 : c.cronTime
                  };
                })
              });

            case 35:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[21, 29]]);
    }));

    function initJobs(_x, _x2, _x3, _x4) {
      return _initJobs.apply(this, arguments);
    }

    return initJobs;
  }();

  Job.getIds = function getIds() {
    return Object.keys(Job.jobs);
  };

  Job.run = /*#__PURE__*/function () {
    var _run = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(parent, args, context, info) {
      var id, data, endTimer, time, job, resolve, result, duration, len, _duration;

      return runtime_1.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              id = args.id, data = args.data;
              endTimer = startTimer();
              time = new Date();
              _context2.prev = 3;
              job = Job.jobs[id];

              if (job) {
                _context2.next = 7;
                break;
              }

              throw new Error('Job not found');

            case 7:
              resolve = job.opts.resolve;
              _context2.t0 = resolve;

              if (!_context2.t0) {
                _context2.next = 13;
                break;
              }

              _context2.next = 12;
              return resolve(parent, args, context, info);

            case 12:
              _context2.t0 = _context2.sent;

            case 13:
              result = _context2.t0;
              duration = endTimer();
              len = Array.isArray(result) ? result.length : undefined;
              return _context2.abrupt("return", {
                id: id,
                len: len,
                result: result,
                data: data,
                duration: duration,
                time: time
              });

            case 19:
              _context2.prev = 19;
              _context2.t1 = _context2["catch"](3);
              _duration = endTimer();
              console.error('runCronJob', id, _context2.t1);
              return _context2.abrupt("return", {
                id: id,
                duration: _duration,
                data: data,
                error: _context2.t1 instanceof Error ? _context2.t1 == null ? void 0 : _context2.t1.message : _context2.t1 + '',
                time: time
              });

            case 24:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[3, 19]]);
    }));

    function run(_x5, _x6, _x7, _x8) {
      return _run.apply(this, arguments);
    }

    return run;
  }();

  Job.trimSize = function trimSize(items) {
    if (items.length > POOL_JOB_SIZE) return items.shift();
  };

  Job.runJob = /*#__PURE__*/function () {
    var _runJob = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3(parent, args, context, info) {
      var ativity, job, prev;
      return runtime_1.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return Job.run(parent, args, context, info);

            case 2:
              ativity = _context3.sent;
              job = Job.jobs[args.id];

              if (job) {
                prev = job.lastActivity;
                job.lastActivity = ativity;

                if (prev) {
                  job.dump.logs.push(prev);

                  if (prev.error) {
                    job.dump.errors.push(prev);
                  }

                  Job.trimSize(job.dump.logs);
                  Job.trimSize(job.dump.errors);
                }
              }

              return _context3.abrupt("return", ativity);

            case 6:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    }));

    function runJob(_x9, _x10, _x11, _x12) {
      return _runJob.apply(this, arguments);
    }

    return runJob;
  }();

  Job.getJobs = function getJobs() {
    return {
      jobs: Job.getIds().map(function (id) {
        var _j$item$name, _j$item;

        var j = Job.jobs[id];
        return {
          id: id,
          name: (_j$item$name = (_j$item = j.item) == null ? void 0 : _j$item.name) != null ? _j$item$name : j.opts.name,
          lastActivity: j.lastActivity,
          dump: j.dump
        };
      })
    };
  };

  return Job;
}();
Job.jobs = {};
Job.typeDefs = "\n  type Query {\n    getCronJobs: JSONObject\n  }\n  type Mutation {\n    \"CRON \u041A\u043E\u043C\u0430\u043D\u0434\u0430 \u0434\u043B\u044F \u043F\u0435\u0440\u0438\u043E\u0434\u0438\u0447\u0435\u0441\u043A\u043E\u0433\u043E \u0432\u044B\u043F\u043E\u043B\u043D\u0435\u043D\u0438\u044F \u0437\u0430\u0434\u0430\u043D\u0438\u0439 \u0432 \u043E\u043F\u0440\u0435\u0434\u0435\u043B\u0451\u043D\u043D\u043E\u0435 \u0432\u0440\u0435\u043C\u044F\"\n    runCronJob(\n      \"id \u0438\u0441\u043F\u043E\u043B\u043D\u044F\u0435\u043C\u0430\u044F-\u043A\u043E\u043C\u0430\u043D\u0434\u0430\"\n      id: String!\n      \"\u0434\u043E\u043F. \u0434\u0430\u043D\u043D\u044B\u0435\"\n      data: JSONObject\n    ): JSONObject\n    \"\u0438\u043D\u0438\u0446\u0438\u0430\u043B\u0438\u0437\u0430\u0446\u0438\u044F (\u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F) CRON \u0441\u043B\u0443\u0436\u0431\"\n    initCronJobs: JSONObject\n  }\n  ";
Job.resolvers = {
  JSONObject: graphqlScalars.JSONObjectResolver,
  Mutation: {
    runCronJob: Job.runJob,
    initCronJobs: Job.initJobs
  },
  Query: {
    getCronJobs: Job.getJobs
  }
};
var CronResolvers = Job.resolvers;
var CronTypeDefs = Job.typeDefs;

var index = {
  __proto__: null,
  Job: Job,
  CronResolvers: CronResolvers,
  CronTypeDefs: CronTypeDefs
};

var resolvers$2 = [scalars.resolvers, Auth, CronResolvers];

var _templateObject$1;
var Auth$1 = /*#__PURE__*/gql(_templateObject$1 || (_templateObject$1 = /*#__PURE__*/_taggedTemplateLiteralLoose(["\n  type Token {\n    accessToken: String\n    refreshToken: String\n  }\n\n  type Query {\n    me: JSONObject\n  }\n\n  type Mutation {\n    login(\n      name: String!\n      password: String!\n      tenantId: String!\n      profileId: String\n    ): Token\n    register(name: String!, password: String!, email: String): Token\n    refreshToken(refreshToken: String!): Token\n    logout: Boolean\n  }\n"])));

var typeDefs$1 = /*#__PURE__*/merge.mergeTypeDefs([scalars.typeDefs, Auth$1, CronTypeDefs]);

var graphql = {
  resolvers: resolvers$2,
  typeDefs: typeDefs$1
};

function createSchema(_x) {
  return _createSchema.apply(this, arguments);
} // stitch

function _createSchema() {
  _createSchema = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var _schemaOpts$middlewar, _opts$shield;

    var schemaOpts, schema$1, _opts$lookups, _lookup$typeDefs, _schemaOpts$typeDefs, _schemaOpts$resolvers, _schemaOpts$subSchema, lookup, middlewares;

    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            schemaOpts = presetSchema(opts); // as is

            schema$1 = schemaOpts == null ? void 0 : schemaOpts.schema;

            if (schema$1) {
              _context.next = 13;
              break;
            }

            lookup = opts == null ? void 0 : (_opts$lookups = opts.lookups) == null ? void 0 : _opts$lookups.lookup;
            schemaOpts.typeDefs = merge.mergeTypeDefs([(_lookup$typeDefs = lookup == null ? void 0 : lookup.typeDefs) != null ? _lookup$typeDefs : '', graphql.typeDefs, (_schemaOpts$typeDefs = schemaOpts == null ? void 0 : schemaOpts.typeDefs) != null ? _schemaOpts$typeDefs : constants.SCHEMA.typeDefs]);
            schemaOpts.resolvers = [(lookup == null ? void 0 : lookup.resolvers()) || {}].concat(graphql.resolvers, (_schemaOpts$resolvers = schemaOpts == null ? void 0 : schemaOpts.resolvers) != null ? _schemaOpts$resolvers : constants.SCHEMA.resolvers);

            if (!(opts && schemaOpts != null && (_schemaOpts$subSchema = schemaOpts.subSchemas) != null && _schemaOpts$subSchema.length)) {
              _context.next = 12;
              break;
            }

            _context.next = 9;
            return makeStitchSchema(opts);

          case 9:
            schema$1 = _context.sent;
            _context.next = 13;
            break;

          case 12:
            // standart
            schema$1 = schema.makeExecutableSchema({
              typeDefs: schemaOpts == null ? void 0 : schemaOpts.typeDefs,
              resolvers: schemaOpts == null ? void 0 : schemaOpts.resolvers
            });

          case 13:
            // Build one sdl file have all types you can delete if you not need
            if (schema$1 && schemaOpts != null && schemaOpts.isPaljs && schemaOpts != null && schemaOpts.isGenerateGraphQlSDLFile) plugins.generateGraphQlSDLFile(schema$1);
            middlewares = [schemaOpts != null && schemaOpts.isPaljs ? paljsMiddleware : null].concat((_schemaOpts$middlewar = schemaOpts == null ? void 0 : schemaOpts.middlewares) != null ? _schemaOpts$middlewar : [], [opts == null ? void 0 : (_opts$shield = opts.shield) == null ? void 0 : _opts$shield.permissions]).filter(function (c) {
              return !!c;
            }); // paljs preset...

            schemaOpts.schema = graphqlMiddleware.applyMiddleware.apply(void 0, [schema$1].concat(middlewares));
            return _context.abrupt("return", schemaOpts);

          case 17:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _createSchema.apply(this, arguments);
}

function makeStitchSchema(_x2) {
  return _makeStitchSchema.apply(this, arguments);
} // paljs preset...


function _makeStitchSchema() {
  _makeStitchSchema = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(opts) {
    var _opts$client$getUrl, _opts$client, _opts$client2, _schemaOpts$resolvers2;

    var schemaOpts, getUrl, headers, subSchemas, schema;
    return runtime_1.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            schemaOpts = presetSchema(opts);
            getUrl = (_opts$client$getUrl = (_opts$client = opts.client) == null ? void 0 : _opts$client.getUrl) != null ? _opts$client$getUrl : presetCore(opts).getUrl;
            headers = (_opts$client2 = opts.client) == null ? void 0 : _opts$client2.headers;
            subSchemas = ((schemaOpts == null ? void 0 : schemaOpts.subSchemas) || []).map(function (c) {
              //
              var remotes = c.remotes.map(function (cr) {
                return _extends({}, cr, {
                  remoteTypeFunc: cr.remoteTypeFunc || "findMany" + cr.remoteTypeName,
                  remoteTypeFuncArgs: cr.remoteTypeFuncArgs || function (ids) {
                    return defaultRemoteTypeFuncArgs(ids, cr.remoteTypePkName);
                  }
                });
              });
              var url = getUrl(c.schemaName) + "/graphql";

              var options = _extends({
                log: false,
                batch: true,
                headers: headers
              }, c.options);

              return subSchema(url, remotes, options);
            });
            _context2.next = 6;
            return makeGatewaySchema(schemaOpts == null ? void 0 : schemaOpts.typeDefs, (_schemaOpts$resolvers2 = schemaOpts == null ? void 0 : schemaOpts.resolvers) != null ? _schemaOpts$resolvers2 : [], subSchemas, {
              validationSettings: {
                validationLevel: 'off'
              }
            });

          case 6:
            schema = _context2.sent;
            return _context2.abrupt("return", schema);

          case 8:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _makeStitchSchema.apply(this, arguments);
}

function paljsMiddleware(resolve, root, args, context, info) {
  var result = new plugins.PrismaSelect(info).value;

  if (!result.select || Object.keys(result.select).length > 0) {
    args = _extends({}, args, result);
  }

  return resolve(root, args, context, info);
}

var ApolloLogging = {
  requestDidStart: function requestDidStart(requestContext) {
    return _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee5() {
      return runtime_1.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              return _context5.abrupt("return", {
                parsingDidStart: function parsingDidStart(reg) {
                  return _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee() {
                    return runtime_1.wrap(function _callee$(_context) {
                      while (1) {
                        switch (_context.prev = _context.next) {
                          case 0:
                            return _context.abrupt("return", function () {
                              for (var _len = arguments.length, errors = new Array(_len), _key = 0; _key < _len; _key++) {
                                errors[_key] = arguments[_key];
                              }

                              // If there are no errors, we log in willSendResponse instead.
                              if (errors.length) {
                                errors.forEach(function (err) {
                                  return console.error('parsingDidStart', err);
                                });
                              }
                            });

                          case 1:
                          case "end":
                            return _context.stop();
                        }
                      }
                    }, _callee);
                  }))();
                },
                validationDidStart: function validationDidStart() {
                  return _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2() {
                    return runtime_1.wrap(function _callee2$(_context2) {
                      while (1) {
                        switch (_context2.prev = _context2.next) {
                          case 0:
                            return _context2.abrupt("return", function (errs) {
                              if (errs) {
                                errs.forEach(function (err) {
                                  return console.error('validationDidStart', err);
                                });
                              }
                            });

                          case 1:
                          case "end":
                            return _context2.stop();
                        }
                      }
                    }, _callee2);
                  }))();
                },
                executionDidStart: function executionDidStart() {
                  return _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3() {
                    return runtime_1.wrap(function _callee3$(_context3) {
                      while (1) {
                        switch (_context3.prev = _context3.next) {
                          case 0:
                            return _context3.abrupt("return", function (err) {
                              if (err) {
                                console.error('executionDidStart', err);
                              }
                            });

                          case 1:
                          case "end":
                            return _context3.stop();
                        }
                      }
                    }, _callee3);
                  }))();
                },
                didEncounterErrors: function didEncounterErrors(reg) {
                  return _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee4() {
                    return runtime_1.wrap(function _callee4$(_context4) {
                      while (1) {
                        switch (_context4.prev = _context4.next) {
                          case 0:
                            if (reg != null && reg.errors.length) {
                              // TODO: traceId
                              console.error('\n', '🎈 errors:', reg.operationName + "\n", reg == null ? void 0 : reg.errors);
                              console.log(reg.source, '\nvariables:', reg == null ? void 0 : reg.request.variables, '\n-----------\n');
                            }

                          case 1:
                          case "end":
                            return _context4.stop();
                        }
                      }
                    }, _callee4);
                  }))();
                }
              });

            case 1:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5);
    }))();
  }
};

function presetExpress(opts) {
  var _opts$server, _express$publicDir, _express$jsonLimit;

  var root = presetRoot(opts);
  var express = opts == null ? void 0 : (_opts$server = opts.server) == null ? void 0 : _opts$server.express;
  var baseUrl = (express == null ? void 0 : express.baseUrl) || (root == null ? void 0 : root.schema) || '';

  if (baseUrl && !baseUrl.startsWith("/")) {
    baseUrl = "/" + baseUrl;
  }

  var publicDir = (_express$publicDir = express == null ? void 0 : express.publicDir) != null ? _express$publicDir : constants.SERVER.PUBLIC_DIR;
  var jsonLimit = (_express$jsonLimit = express == null ? void 0 : express.jsonLimit) != null ? _express$jsonLimit : constants.SERVER.JSON_LIMIT;
  return _extends({}, express, {
    baseUrl: baseUrl,
    publicDir: publicDir,
    jsonLimit: jsonLimit
  });
}

function presetApollo(opts) {
  var _opts$server, _apollo$graphqlPostfi;

  var _presetExpress = presetExpress(opts),
      baseUrl = _presetExpress.baseUrl;

  var apollo = _extends({}, opts == null ? void 0 : (_opts$server = opts.server) == null ? void 0 : _opts$server.apollo);

  apollo.graphqlPostfixUrl = (_apollo$graphqlPostfi = apollo.graphqlPostfixUrl) != null ? _apollo$graphqlPostfi : constants.APOLLO.GRAPHQL_POSTFIX_URL;

  if (!apollo.graphqlUrl) {
    apollo.graphqlUrl = baseUrl ? "" + baseUrl + apollo.graphqlPostfixUrl : apollo.graphqlPostfixUrl;
  }

  return apollo;
}

function createApollo(_x) {
  return _createApollo.apply(this, arguments);
}

function _createApollo() {
  _createApollo = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var apollo, _presetSchema, schema, apolloConfig;

    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            apollo = presetApollo(opts);
            _presetSchema = presetSchema(opts), schema = _presetSchema.schema;

            if (!apollo.apolloServer) {
              _context.next = 4;
              break;
            }

            return _context.abrupt("return", apollo.apolloServer);

          case 4:
            apolloConfig = {
              schema: schema,
              context: apollo == null ? void 0 : apollo.context,
              plugins: [ApolloLogging, // "development" === 'production' ?
              //   ApolloServerPluginLandingPageProductionDefault({ footer: false }) :
              apolloServerCore.ApolloServerPluginLandingPageLocalDefault({
                footer: false
              })],
              introspection: true
            };
            return _context.abrupt("return", new apolloServerExpress.ApolloServer(apolloConfig));

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _createApollo.apply(this, arguments);
}

function presetPrisma(opts) {
  var prisma = opts == null ? void 0 : opts.prisma;
  return _extends({}, prisma, {
    middlewares: _extends({}, prisma == null ? void 0 : prisma.middlewares)
  });
}

function createContext(_x) {
  return _createContext.apply(this, arguments);
}

function _createContext() {
  _createContext = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3(opts) {
    var _settings$provider$ge, _settings$provider;

    var core, options, _presetExpress, app, apollo, auth, storage, settings, getSettings, _presetClient, gqlClient, _presetPrisma, prisma, dbUrl, _presetSchema, schema, context;

    return runtime_1.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            // set GLOBAL context for App instance
            core = presetCore(opts);
            _context3.next = 3;
            return core.config(opts);

          case 3:
            options = _context3.sent;
            _presetExpress = presetExpress(options), app = _presetExpress.app;
            apollo = presetApollo(options); // check context exsits

            if (!apollo.context) {
              _context3.next = 8;
              break;
            }

            return _context3.abrupt("return", apollo);

          case 8:
            auth = presetAuth(options);
            storage = presetStorage(options);
            _context3.next = 12;
            return core.settings(options);

          case 12:
            settings = _context3.sent;
            getSettings = (_settings$provider$ge = (_settings$provider = settings.provider) == null ? void 0 : _settings$provider.getSettings) != null ? _settings$provider$ge : function () {
              return Promise.resolve({});
            }; // **************************************
            // prepare Tenant Context
            // **************************************

            if (app) {
              app.use( /*#__PURE__*/function () {
                var _ref = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(req, res, next) {
                  var _req$get;

                  var clientId, requestId, traceId, token, payload, userId, role, tenantId, sessionId, profileId, ip, ctx;
                  return runtime_1.wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          clientId = req.headers['x-client-id'];
                          requestId = req.headers['x-request-id'] || share2.utils.uuidv4();
                          traceId = req.headers['x-trace-id'] || requestId;
                          token = ((_req$get = req.get('Authorization')) != null ? _req$get : '').replace('Bearer ', ''); // fix Bearer

                          if (!auth) {
                            _context.next = 10;
                            break;
                          }

                          _context.next = 7;
                          return helper.jwt.getPayload({
                            token: token,
                            auth: auth
                          });

                        case 7:
                          _context.t0 = _context.sent;
                          _context.next = 11;
                          break;

                        case 10:
                          _context.t0 = undefined;

                        case 11:
                          payload = _context.t0;
                          userId = payload == null ? void 0 : payload.sub;
                          role = (payload == null ? void 0 : payload.role) || constants.JWT.ANONYM;
                          tenantId = payload == null ? void 0 : payload.tenantId;
                          sessionId = payload == null ? void 0 : payload.sessionId;
                          profileId = payload == null ? void 0 : payload.profileId;
                          ip = req.ip;
                          ctx = {
                            clientId: clientId,
                            requestId: requestId,
                            traceId: traceId,
                            tenantId: tenantId,
                            userId: userId,
                            sessionId: sessionId,
                            role: role,
                            profileId: profileId,
                            token: token,
                            ip: ip
                          };
                          storage.run(ctx, next);

                        case 20:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee);
                }));

                return function (_x2, _x3, _x4) {
                  return _ref.apply(this, arguments);
                };
              }());
            }

            _presetClient = presetClient(options), gqlClient = _presetClient.gqlClient;
            _presetPrisma = presetPrisma(options), prisma = _presetPrisma.prisma, dbUrl = _presetPrisma.dbUrl;
            _presetSchema = presetSchema(options), schema = _presetSchema.schema; // *******************************************
            // prepare Apollo Context
            // *******************************************

            context = /*#__PURE__*/function () {
              var _ref2 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2() {
                var tenant, role, tenantId, userId, isAuthenticated;
                return runtime_1.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        if (gqlClient) {
                          _context2.next = 2;
                          break;
                        }

                        throw new Error('gqlClient is not initialized');

                      case 2:
                        tenant = storage.getStore();
                        role = tenant && tenant.role || constants.JWT.ANONYM;
                        tenantId = tenant == null ? void 0 : tenant.tenantId; // TODO checkUser

                        userId = tenant == null ? void 0 : tenant.userId;
                        isAuthenticated = role !== constants.JWT.ANONYM;
                        return _context2.abrupt("return", {
                          schema: schema,
                          prisma: prisma,
                          dataloaders: new WeakMap(),
                          isAuthenticated: isAuthenticated,
                          role: role,
                          options: options,
                          tenant: tenant,
                          tenantId: tenantId,
                          userId: userId,
                          select: {},
                          // request: req,
                          gqlClient: gqlClient,
                          store: new Map(),
                          dbUrl: dbUrl,
                          getSettings: getSettings
                        });

                      case 8:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2);
              }));

              return function context() {
                return _ref2.apply(this, arguments);
              };
            }();

            return _context3.abrupt("return", _extends({}, apollo, {
              context: context
            }));

          case 20:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _createContext.apply(this, arguments);
}

var cors = /*#__PURE__*/require('cors');

function createExpress(_x) {
  return _createExpress.apply(this, arguments);
}

function _createExpress() {
  _createExpress = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var _expessOptions$app, _expessOptions$baseUr;

    var root, expessOptions, app, baseUrl, publicDir;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            root = presetRoot(opts);
            expessOptions = presetExpress(_extends({}, opts, {
              root: root
            }));
            app = expessOptions.app = (_expessOptions$app = expessOptions.app) != null ? _expessOptions$app : express(); // compress all responses

            app.use(compression());
            app.use(express.json({
              limit: expessOptions.jsonLimit
            }));
            app.use(express.urlencoded({
              extended: true
            }));
            app.options('*', cors());
            baseUrl = (_expessOptions$baseUr = expessOptions == null ? void 0 : expessOptions.baseUrl) != null ? _expessOptions$baseUr : "";
            publicDir = expessOptions.publicDir || '';

            if (baseUrl) {
              app.use(baseUrl, express["static"](publicDir));
            } else {
              app.use(express["static"](publicDir));
            }

            app.all(baseUrl + "/ping", function (req, res) {
              var d = new Date();
              var data = {
                now: share2.utils.formatDateTime(d),
                utc: d
              };
              console.log(baseUrl + "/ping", data);
              res.json(data);
            });
            app.all(baseUrl + "/version", function (req, res) {
              var now = new Date();
              res.json({
                npm: root.version,
                app: root.appVersion,
                today: now.toString(),
                todayISO: now.toISOString(),
                todayUTC: now.toUTCString(),
                timeZone: now.getTimezoneOffset()
              });
            });
            return _context.abrupt("return", expessOptions);

          case 13:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _createExpress.apply(this, arguments);
}

function presetServer(_x) {
  return _presetServer.apply(this, arguments);
}

function _presetServer() {
  _presetServer = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var _opts$server;

    var port, mode, _constants$ENV$PORT;

    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            port = opts == null ? void 0 : (_opts$server = opts.server) == null ? void 0 : _opts$server.port;

            if (port) {
              _context.next = 10;
              break;
            }

            mode = presetMode(opts);

            if (!mode.isTest) {
              _context.next = 9;
              break;
            }

            _context.next = 6;
            return getPort({
              port: share2.utils.getRandomInt(5000, 5500)
            });

          case 6:
            port = _context.sent;
            _context.next = 10;
            break;

          case 9:
            port = (_constants$ENV$PORT = constants.ENV.PORT) != null ? _constants$ENV$PORT : constants.SERVER.PORT;

          case 10:
            return _context.abrupt("return", _extends({}, opts == null ? void 0 : opts.server, {
              port: port,
              express: presetExpress(opts),
              apollo: presetApollo(opts)
            }));

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _presetServer.apply(this, arguments);
}

function createServer(_x) {
  return _createServer.apply(this, arguments);
}

function _createServer() {
  _createServer = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3(opts) {
    var _server$port;

    var core, options, server, express, app, apollo, apolloServer, port, started, httpServer;
    return runtime_1.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            core = presetCore(opts);
            _context3.next = 3;
            return core.config(opts);

          case 3:
            options = _context3.sent;
            _context3.next = 6;
            return presetServer(options);

          case 6:
            server = options.server = _context3.sent;
            _context3.next = 9;
            return core.express(options);

          case 9:
            express = server.express = _context3.sent;
            app = express.app;

            if (app) {
              _context3.next = 13;
              break;
            }

            throw new Error('express.Application not initialize');

          case 13:
            _context3.next = 15;
            return core.lookups(options);

          case 15:
            options.lookups = _context3.sent;
            options.shield = core.shield(options);
            _context3.next = 19;
            return core.schema(options);

          case 19:
            options.schema = _context3.sent;
            _context3.next = 22;
            return core.context(options);

          case 22:
            apollo = options.server.apollo = _context3.sent;
            _context3.next = 25;
            return core.apollo(options);

          case 25:
            apolloServer = apollo.apolloServer = _context3.sent;

            if (!server.appServer) {
              _context3.next = 28;
              break;
            }

            return _context3.abrupt("return", server);

          case 28:
            if (apolloServer) {
              _context3.next = 30;
              break;
            }

            throw Error('apolloServer should be init');

          case 30:
            port = (_server$port = server == null ? void 0 : server.port) != null ? _server$port : 0; // handles

            started = null;
            server.appServer = {
              run: function () {
                var _run = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(_, listeningListener) {
                  return runtime_1.wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          _context.next = 2;
                          return apolloServer.start();

                        case 2:
                          started = _context.sent;
                          apolloServer.applyMiddleware({
                            app: app,
                            path: apollo.graphqlUrl
                          });
                          return _context.abrupt("return", new Promise(function (resolve) {
                            httpServer = app.listen({
                              port: port
                            }, function () {
                              var _options$root;

                              listeningListener && listeningListener(core);
                              console.log("\n\n******************************************************************************************\n      \uD83D\uDE80 Server v" + (options == null ? void 0 : (_options$root = options.root) == null ? void 0 : _options$root.version) + " ready at http://localhost:" + port + apolloServer.graphqlPath + "\n                          www - http://localhost:" + port + (express == null ? void 0 : express.baseUrl) + "\n******************************************************************************************\n");
                            });
                            resolve(options);
                          }));

                        case 5:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee);
                }));

                function run(_x2, _x3) {
                  return _run.apply(this, arguments);
                }

                return run;
              }(),
              stop: function () {
                var _stop = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2() {
                  var _httpServer;

                  return runtime_1.wrap(function _callee2$(_context2) {
                    while (1) {
                      switch (_context2.prev = _context2.next) {
                        case 0:
                          if ((_httpServer = httpServer) != null && _httpServer.close) httpServer.close();

                          if (!started) {
                            _context2.next = 4;
                            break;
                          }

                          _context2.next = 4;
                          return apolloServer == null ? void 0 : apolloServer.stop();

                        case 4:
                        case "end":
                          return _context2.stop();
                      }
                    }
                  }, _callee2);
                }));

                function stop() {
                  return _stop.apply(this, arguments);
                }

                return stop;
              }()
            };
            return _context3.abrupt("return", server);

          case 34:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _createServer.apply(this, arguments);
}

function presetShield(opts) {
  return _extends({
    ruleTree: {}
  }, opts == null ? void 0 : opts.shield);
}

function createShield(opts) {
  var _scheld$permissions, _scheld$ruleTree;

  var _scheld = presetShield(opts);

  var permissions = (_scheld$permissions = _scheld.permissions) != null ? _scheld$permissions : graphqlShield.shield((_scheld$ruleTree = _scheld.ruleTree) != null ? _scheld$ruleTree : {}, _scheld.options);
  _scheld.permissions = permissions;
  return _scheld;
}

var rand = /*#__PURE__*/nanoid.customAlphabet('qwertyuioplkjhgfdsazxcvbnm', 3);
function presetDatabase(opts) {
  var _opts$database, _ref, _opts$database$host, _opts$database2, _ref2, _opts$database$db, _opts$database3, _ref3, _opts$database$user, _opts$database4, _ref4, _opts$database$passwo, _opts$database5, _ref5, _opts$database$port, _opts$database6, _opts$database$pgboun, _opts$database7, _opts$database$userTe, _opts$database8, _opts$database$passwo2, _opts$database9;

  var dbSchema = opts == null ? void 0 : (_opts$database = opts.database) == null ? void 0 : _opts$database.schema;

  if (!dbSchema) {
    var mode = presetMode(opts);
    var root = presetRoot(_extends({}, opts, {
      mode: mode
    }));
    dbSchema = root.schema;

    if (mode != null && mode.isTest) {
      dbSchema = dbSchema + "_" + share2.utils.formatDate(new Date(), 'yyyyMMddHHmmss') + "_" + rand();
    }
  }

  return {
    host: (_ref = (_opts$database$host = opts == null ? void 0 : (_opts$database2 = opts.database) == null ? void 0 : _opts$database2.host) != null ? _opts$database$host : constants.ENV.AIT_POSTGRES_HOST) != null ? _ref : '',
    db: (_ref2 = (_opts$database$db = opts == null ? void 0 : (_opts$database3 = opts.database) == null ? void 0 : _opts$database3.db) != null ? _opts$database$db : constants.ENV.AIT_POSTGRES_DB) != null ? _ref2 : '',
    user: (_ref3 = (_opts$database$user = opts == null ? void 0 : (_opts$database4 = opts.database) == null ? void 0 : _opts$database4.user) != null ? _opts$database$user : constants.ENV.AIT_POSTGRES_USER) != null ? _ref3 : '',
    password: (_ref4 = (_opts$database$passwo = opts == null ? void 0 : (_opts$database5 = opts.database) == null ? void 0 : _opts$database5.password) != null ? _opts$database$passwo : constants.ENV.AIT_POSTGRES_PASSWORD) != null ? _ref4 : '',
    schema: dbSchema,
    port: (_ref5 = (_opts$database$port = opts == null ? void 0 : (_opts$database6 = opts.database) == null ? void 0 : _opts$database6.port) != null ? _opts$database$port : constants.ENV.AIT_POSTGRES_PORT) != null ? _ref5 : 5432,
    // BOUNCER PORT
    pgbouncer: (_opts$database$pgboun = opts == null ? void 0 : (_opts$database7 = opts.database) == null ? void 0 : _opts$database7.pgbouncer) != null ? _opts$database$pgboun : constants.ENV.AIT_PGBOUNCER_PORT,
    userTenant: (_opts$database$userTe = opts == null ? void 0 : (_opts$database8 = opts.database) == null ? void 0 : _opts$database8.userTenant) != null ? _opts$database$userTe : constants.ENV.AIT_POSTGRES_USER_TENANT,
    passwordTenant: (_opts$database$passwo2 = opts == null ? void 0 : (_opts$database9 = opts.database) == null ? void 0 : _opts$database9.passwordTenant) != null ? _opts$database$passwo2 : constants.ENV.AIT_POSTGRES_PASSWORD_TENANT
  };
}

function createDbConnection(opts) {
  var mode = presetMode(opts);
  var root = presetRoot(_extends({}, opts, {
    mode: mode
  }));
  var database = presetDatabase(_extends({}, opts, {
    mode: mode,
    root: root
  })); // BOUNCER PORT

  var hasBouncer = !(mode != null && mode.isMigration) && (database == null ? void 0 : database.pgbouncer) && database.host !== 'localhost' && database.host !== '127.0 0.1'; // PG PORT

  var port = hasBouncer ? database == null ? void 0 : database.pgbouncer : database == null ? void 0 : database.port; // миграцию делаем напрямую в порт 5432

  var dbOpts = _extends({}, database, {
    bouncer: !!hasBouncer,
    port: port
  }); // check tenant user connection


  if (mode.isTenant && database.userTenant) dbOpts.user = database.userTenant;
  if (mode.isTenant && database.passwordTenant) dbOpts.password = database.passwordTenant;
  return helper.db.buildUrl(dbOpts);
}

function createPrismaOptions(_x) {
  return _createPrismaOptions.apply(this, arguments);
}

function _createPrismaOptions() {
  _createPrismaOptions = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var core, dbUrl, prisma, _prisma$middlewares;

    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            core = presetCore(opts);
            dbUrl = core.dbConnection(opts);
            prisma = presetPrisma(_extends({}, opts, {
              prisma: _extends({}, opts == null ? void 0 : opts.prisma, {
                dbUrl: dbUrl
              })
            }));

            if (prisma.prisma) {
              _context.next = 9;
              break;
            }

            prisma.prisma = core.prisma(prisma, opts == null ? void 0 : opts.contextStorage);

            if (!prisma.prisma) {
              _context.next = 9;
              break;
            }

            if (!((_prisma$middlewares = prisma.middlewares) != null && _prisma$middlewares.rls)) {
              _context.next = 9;
              break;
            }

            _context.next = 9;
            return core.useRlsMiddleware(_extends({}, opts, {
              prisma: prisma
            }));

          case 9:
            return _context.abrupt("return", prisma);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _createPrismaOptions.apply(this, arguments);
}

function presetMigration(opts) {
  var _opts$migration$cmdDe, _opts$migration, _opts$migration$cmdVe, _opts$migration2;

  var cmdDeploy = (_opts$migration$cmdDe = opts == null ? void 0 : (_opts$migration = opts.migration) == null ? void 0 : _opts$migration.cmdDeploy) != null ? _opts$migration$cmdDe : constants.MIGRATION.PRISMA_DEPLOY_CMD;
  var cmdVersion = (_opts$migration$cmdVe = opts == null ? void 0 : (_opts$migration2 = opts.migration) == null ? void 0 : _opts$migration2.cmdVersion) != null ? _opts$migration$cmdVe : constants.MIGRATION.PRISMA_VERSION_CMD;
  return _extends({}, opts == null ? void 0 : opts.migration, {
    cmdDeploy: cmdDeploy,
    cmdVersion: cmdVersion
  });
}

function dbDeploy(_x) {
  return _dbDeploy.apply(this, arguments);
}

function _dbDeploy() {
  _dbDeploy = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var _opts;

    var core, options, migration, dbUrl, v, up;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            // set AIT Connection + bouncer off
            opts = _extends({}, opts, {
              mode: _extends({}, (_opts = opts) == null ? void 0 : _opts.mode, {
                isTenant: false,
                isMigration: true
              })
            });
            core = presetCore(opts);
            _context.next = 4;
            return core.config(opts);

          case 4:
            options = _context.sent;
            migration = presetMigration(options);
            dbUrl = core.dbConnection(options);

            if (!(migration != null && migration.skipDeploy)) {
              _context.next = 11;
              break;
            }

            console.log('db deploy - SKIPPED...');
            _context.next = 23;
            break;

          case 11:
            _context.next = 13;
            return prismaVersion({
              cmdVersion: migration == null ? void 0 : migration.cmdVersion
            });

          case 13:
            v = _context.sent;

            if (v) {
              _context.next = 16;
              break;
            }

            throw new Error('db version - FAIL');

          case 16:
            console.log("----- ----- DEPLOY - \"" + dbUrl + "\" START...");
            _context.next = 19;
            return prismaDeploy({
              dbUrl: dbUrl,
              cmdDeploy: migration == null ? void 0 : migration.cmdDeploy
            });

          case 19:
            up = _context.sent;

            if (up) {
              _context.next = 22;
              break;
            }

            throw new Error('db deploy - FAIL');

          case 22:
            console.log("----- ----- DEPLOY - OK");

          case 23:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _dbDeploy.apply(this, arguments);
}

function grantSchemaTenant(_x, _x2) {
  return _grantSchemaTenant.apply(this, arguments);
}

function _grantSchemaTenant() {
  _grantSchemaTenant = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(dbUrl, schema) {
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return helper.db.query({
              dbUrl: dbUrl,
              queryText: "CALL public.ait_tenant_schema_grant('" + schema + "')"
            });

          case 2:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _grantSchemaTenant.apply(this, arguments);
}

function grantTableTenant(_x, _x2, _x3) {
  return _grantTableTenant.apply(this, arguments);
}

function _grantTableTenant() {
  _grantTableTenant = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(dbUrl, schema, tables) {
    var index, t;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            console.log('ait_tenant_table_grant', tables);
            index = 0;

          case 2:
            if (!(index < tables.length)) {
              _context.next = 9;
              break;
            }

            t = tables[index];
            _context.next = 6;
            return helper.db.query({
              dbUrl: dbUrl,
              queryText: "CALL public.ait_tenant_table_grant('" + schema + "','" + t + "')"
            });

          case 6:
            index++;
            _context.next = 2;
            break;

          case 9:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _grantTableTenant.apply(this, arguments);
}

function grantTenant(_x, _x2) {
  return _grantTenant.apply(this, arguments);
}

function _grantTenant() {
  _grantTenant = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(dbUrl, options) {
    var _options$database;

    var schema, _options$prisma, _options$prisma$prism, _options$prisma$prism2, _options$prisma$prism3, models, tables;

    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            schema = (_options$database = options.database) == null ? void 0 : _options$database.schema;
            _context.t0 = schema;

            if (!_context.t0) {
              _context.next = 6;
              break;
            }

            _context.next = 5;
            return helper.db.checkSchema(dbUrl, schema);

          case 5:
            _context.t0 = _context.sent;

          case 6:
            if (!_context.t0) {
              _context.next = 14;
              break;
            }

            _context.next = 9;
            return grantSchemaTenant(dbUrl, schema);

          case 9:
            models = (_options$prisma = options.prisma) == null ? void 0 : (_options$prisma$prism = _options$prisma.prisma) == null ? void 0 : (_options$prisma$prism2 = _options$prisma$prism._dmmf) == null ? void 0 : (_options$prisma$prism3 = _options$prisma$prism2.datamodel) == null ? void 0 : _options$prisma$prism3.models;

            if (!Array.isArray(models)) {
              _context.next = 14;
              break;
            }

            tables = models.map(function (c) {
              return c.dbName || c.name;
            });
            _context.next = 14;
            return grantTableTenant(dbUrl, schema, tables);

          case 14:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _grantTenant.apply(this, arguments);
}

function afterDeploy(_x) {
  return _afterDeploy.apply(this, arguments);
}

function _afterDeploy() {
  _afterDeploy = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(options) {
    var core, dbUrl;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            core = presetCore(options);
            dbUrl = core.dbConnection(_extends({}, options, {
              database: _extends({}, options.database, {
                schema: 'public'
              }),
              mode: {
                isMigration: true,
                isTenant: false
              }
            }));
            _context.next = 4;
            return grantTenant(dbUrl, options);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _afterDeploy.apply(this, arguments);
}

function migrate(_x) {
  return _migrate.apply(this, arguments);
}

function _migrate() {
  _migrate = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var _options$prisma;

    var core, options;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            core = presetCore(opts);
            _context.next = 3;
            return core.config(opts);

          case 3:
            options = _context.sent;
            console.log("----- Start migration: " + ((_options$prisma = options.prisma) == null ? void 0 : _options$prisma.dbUrl) + " ------");
            _context.prev = 5;
            _context.next = 8;
            return core.dbInstall(options);

          case 8:
            _context.next = 10;
            return core.dbDeploy(options);

          case 10:
            _context.next = 12;
            return afterDeploy(options);

          case 12:
            _context.next = 14;
            return core.dbSeed(options);

          case 14:
            console.log('MIGRATE - OK');
            _context.next = 21;
            break;

          case 17:
            _context.prev = 17;
            _context.t0 = _context["catch"](5);
            console.error('MIGRATE - FAIL', _context.t0);
            throw _context.t0;

          case 21:
            _context.prev = 21;
            console.log('----- End migration ------');
            return _context.finish(21);

          case 24:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[5, 17, 21, 24]]);
  }));
  return _migrate.apply(this, arguments);
}

function dbSeed(_x) {
  return _dbSeed.apply(this, arguments);
}

function _dbSeed() {
  _dbSeed = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee5(opts) {
    var core, options, migration, settings;
    return runtime_1.wrap(function _callee5$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            core = presetCore(opts);
            _context6.next = 3;
            return core.config(opts);

          case 3:
            options = _context6.sent;
            migration = presetMigration(options);
            _context6.next = 7;
            return core.settings(options);

          case 7:
            settings = _context6.sent;
            options.settings = settings; // const isSeed =
            //   migration?.seed || migration?.seedPlatform || migration.seedTenant;
            // if (!isSeed) {
            //   console.log('SEED - IGNORE...');
            //   return;
            // }

            _context6.prev = 9;
            return _context6.delegateYield( /*#__PURE__*/runtime_1.mark(function _callee4() {
              var _options$root, _settings$provider, _options$tenant;

              var schema, getSettings, settingKeys, tenantIds, platformTenantId, migrationName, len, _loop, index;

              return runtime_1.wrap(function _callee4$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      console.log('----- Start SEED');
                      schema = ((_options$root = options.root) == null ? void 0 : _options$root.schema) || '.';
                      getSettings = settings == null ? void 0 : (_settings$provider = settings.provider) == null ? void 0 : _settings$provider.getSettings;
                      settingKeys = getSettings ? Object.keys((settings == null ? void 0 : settings.keys) || {}).filter(function (c) {
                        return c.startsWith(schema);
                      }) : [];
                      _context5.next = 6;
                      return core.tenants(options);

                    case 6:
                      tenantIds = _context5.sent;
                      platformTenantId = ((_options$tenant = options.tenant) == null ? void 0 : _options$tenant.platformTenantId) || '';
                      _context5.next = 10;
                      return helper.ctx.inAitScope(options, platformTenantId, /*#__PURE__*/function () {
                        var _ref2 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(_ref) {
                          var dbUrl, prisma, tenant, tenantId, migrName, migrationInfo;
                          return runtime_1.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  dbUrl = _ref.dbUrl, prisma = _ref.prisma, tenant = _ref.tenant, tenantId = _ref.tenantId;
                                  migrName = '';

                                  if (migration != null && migration.skipDeploy) {
                                    _context.next = 16;
                                    break;
                                  }

                                  _context.prev = 3;
                                  _context.next = 6;
                                  return helper.db.checkSchema(dbUrl);

                                case 6:
                                  if (!_context.sent) {
                                    _context.next = 11;
                                    break;
                                  }

                                  _context.next = 9;
                                  return helper.db.lastMigration(dbUrl);

                                case 9:
                                  migrationInfo = _context.sent;
                                  migrName = (migrationInfo == null ? void 0 : migrationInfo.migration_name) || '';

                                case 11:
                                  _context.next = 16;
                                  break;

                                case 13:
                                  _context.prev = 13;
                                  _context.t0 = _context["catch"](3);
                                  console.error('Ошибка получения версии', dbUrl);

                                case 16:
                                  if (!migration.seed) {
                                    _context.next = 21;
                                    break;
                                  }

                                  console.log("----- ----- seed AIT \"" + dbUrl + "\"");
                                  _context.next = 20;
                                  return migration.seed({
                                    dbUrl: dbUrl,
                                    prisma: prisma,
                                    migrationName: migrName,
                                    options: options,
                                    tenant: tenant,
                                    tenantId: tenantId,
                                    tenantIds: tenantIds
                                  });

                                case 20:
                                  console.log('----- ----- seed AIT - OK');

                                case 21:
                                  return _context.abrupt("return", migrName);

                                case 22:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, null, [[3, 13]]);
                        }));

                        return function (_x2) {
                          return _ref2.apply(this, arguments);
                        };
                      }());

                    case 10:
                      migrationName = _context5.sent;

                      if (!(migration.seedPlatform || settingKeys.length)) {
                        _context5.next = 14;
                        break;
                      }

                      _context5.next = 14;
                      return helper.ctx.inTenantScope(options, platformTenantId, /*#__PURE__*/function () {
                        var _ref4 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(_ref3) {
                          var dbUrl, prisma, tenant, tenantId, setts;
                          return runtime_1.wrap(function _callee2$(_context2) {
                            while (1) {
                              switch (_context2.prev = _context2.next) {
                                case 0:
                                  dbUrl = _ref3.dbUrl, prisma = _ref3.prisma, tenant = _ref3.tenant, tenantId = _ref3.tenantId;
                                  console.log("----- ----- seed PLATFORM \"" + dbUrl + "\"");

                                  if (!getSettings) {
                                    _context2.next = 8;
                                    break;
                                  }

                                  _context2.next = 5;
                                  return getSettings();

                                case 5:
                                  _context2.t0 = _context2.sent;
                                  _context2.next = 9;
                                  break;

                                case 8:
                                  _context2.t0 = {};

                                case 9:
                                  setts = _context2.t0;

                                  if (!migration.seedPlatform) {
                                    _context2.next = 13;
                                    break;
                                  }

                                  _context2.next = 13;
                                  return migration.seedPlatform({
                                    dbUrl: dbUrl,
                                    migrationName: migrationName,
                                    prisma: prisma,
                                    tenantId: tenantId,
                                    options: options,
                                    tenant: tenant,
                                    tenantIds: tenantIds,
                                    settings: setts
                                  });

                                case 13:
                                  console.log('----- -----  OK');

                                case 14:
                                case "end":
                                  return _context2.stop();
                              }
                            }
                          }, _callee2);
                        }));

                        return function (_x3) {
                          return _ref4.apply(this, arguments);
                        };
                      }());

                    case 14:
                      len = tenantIds.length;

                      if (!(len && (migration.seedTenant || settingKeys.length))) {
                        _context5.next = 24;
                        break;
                      }

                      console.log("----- ----- seed TENANTS = " + len);
                      _loop = /*#__PURE__*/runtime_1.mark(function _loop(index) {
                        return runtime_1.wrap(function _loop$(_context4) {
                          while (1) {
                            switch (_context4.prev = _context4.next) {
                              case 0:
                                _context4.next = 2;
                                return helper.ctx.inTenantScope(options, tenantIds[index], /*#__PURE__*/function () {
                                  var _ref6 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3(_ref5) {
                                    var dbUrl, prisma, tenant, tenantId, setts;
                                    return runtime_1.wrap(function _callee3$(_context3) {
                                      while (1) {
                                        switch (_context3.prev = _context3.next) {
                                          case 0:
                                            dbUrl = _ref5.dbUrl, prisma = _ref5.prisma, tenant = _ref5.tenant, tenantId = _ref5.tenantId;
                                            console.log("----- ----- ----- seed " + (index + 1) + "/" + len + " \"" + tenantId);

                                            if (!getSettings) {
                                              _context3.next = 8;
                                              break;
                                            }

                                            _context3.next = 5;
                                            return getSettings();

                                          case 5:
                                            _context3.t0 = _context3.sent;
                                            _context3.next = 9;
                                            break;

                                          case 8:
                                            _context3.t0 = {};

                                          case 9:
                                            setts = _context3.t0;

                                            if (!migration.seedTenant) {
                                              _context3.next = 13;
                                              break;
                                            }

                                            _context3.next = 13;
                                            return migration.seedTenant({
                                              dbUrl: dbUrl,
                                              migrationName: migrationName,
                                              prisma: prisma,
                                              tenantId: tenantId,
                                              options: options,
                                              tenant: tenant,
                                              tenantIds: tenantIds,
                                              settings: setts
                                            });

                                          case 13:
                                            console.log('----- ----- -----  OK');

                                          case 14:
                                          case "end":
                                            return _context3.stop();
                                        }
                                      }
                                    }, _callee3);
                                  }));

                                  return function (_x4) {
                                    return _ref6.apply(this, arguments);
                                  };
                                }());

                              case 2:
                              case "end":
                                return _context4.stop();
                            }
                          }
                        }, _loop);
                      });
                      index = 0;

                    case 19:
                      if (!(index < len)) {
                        _context5.next = 24;
                        break;
                      }

                      return _context5.delegateYield(_loop(index), "t0", 21);

                    case 21:
                      index++;
                      _context5.next = 19;
                      break;

                    case 24:
                      console.log('----- SEED - OK');

                    case 25:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee4);
            })(), "t0", 11);

          case 11:
            _context6.next = 17;
            break;

          case 13:
            _context6.prev = 13;
            _context6.t1 = _context6["catch"](9);
            console.error('SEED - FAIL', _context6.t1);
            throw _context6.t1;

          case 17:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee5, null, [[9, 13]]);
  }));
  return _dbSeed.apply(this, arguments);
}

function presetTenant(opts) {
  var _ref, _opts$tenant$platform, _opts$tenant;

  var platformTenantId = (_ref = (_opts$tenant$platform = opts == null ? void 0 : (_opts$tenant = opts.tenant) == null ? void 0 : _opts$tenant.platformTenantId) != null ? _opts$tenant$platform : constants.ENV.AIT_PLATFORM_TENANT_ID) != null ? _ref : constants.TENANT.AIT_PLATFORM_TENANT_ID;
  var pgCurrentTenantParamName = constants.TENANT.PG_CURRENT_TENANT_PARAM_NAME;
  return _extends({
    platformTenantId: platformTenantId,
    pgCurrentTenantParamName: pgCurrentTenantParamName
  }, opts == null ? void 0 : opts.tenant);
}

function getTenants(_x) {
  return _getTenants.apply(this, arguments);
}

function _getTenants() {
  _getTenants = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(opts) {
    var core, options, tenant, platformTenantId, tenantIds;
    return runtime_1.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            core = presetCore(opts);
            _context2.next = 3;
            return core.config(opts);

          case 3:
            options = _context2.sent;
            tenant = presetTenant(options);
            platformTenantId = tenant.platformTenantId || '';
            _context2.next = 8;
            return helper.ctx.inAitScope(options, platformTenantId, /*#__PURE__*/function () {
              var _ref2 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(_ref) {
                var dbUrl, rows;
                return runtime_1.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        dbUrl = _ref.dbUrl;
                        _context.prev = 1;
                        _context.next = 4;
                        return helper.db.checkSchema(dbUrl, 'platform');

                      case 4:
                        if (!_context.sent) {
                          _context.next = 9;
                          break;
                        }

                        _context.next = 7;
                        return helper.db.query({
                          dbUrl: dbUrl,
                          queryText: "SELECT id FROM platform.\"Tenant\" WHERE \"isDisabled\"=false"
                        });

                      case 7:
                        rows = _context.sent;
                        return _context.abrupt("return", rows.rows.map(function (c) {
                          return c.id;
                        }));

                      case 9:
                        _context.next = 14;
                        break;

                      case 11:
                        _context.prev = 11;
                        _context.t0 = _context["catch"](1);
                        console.error('Ошибка получения списка platform."Tenant"', dbUrl);

                      case 14:
                        return _context.abrupt("return", []);

                      case 15:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, null, [[1, 11]]);
              }));

              return function (_x2) {
                return _ref2.apply(this, arguments);
              };
            }());

          case 8:
            tenantIds = _context2.sent;

            if (!tenantIds || !tenantIds.length) {
              // default lpu ids if platform was not initialized
              tenantIds = ['00000000-0000-0000-1000-000000000002', '00000000-0000-0000-1000-000000000003'];
            }

            return _context2.abrupt("return", share2.utils.distinct(tenantIds.filter(function (c) {
              return c && c !== platformTenantId;
            })));

          case 11:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _getTenants.apply(this, arguments);
}

function createClientOptions(_x) {
  return _createClientOptions.apply(this, arguments);
}

function _createClientOptions() {
  _createClientOptions = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var _client$getUrl, _client$gqlClient;

    var core, headers, client;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            core = presetCore(opts);
            _context.next = 3;
            return core.httpHeader(opts);

          case 3:
            headers = _context.sent;
            client = presetClient(opts);
            client.headers = _extends({}, client == null ? void 0 : client.headers, headers);

            if (!((_client$getUrl = client.getUrl) != null)) {
              _context.next = 10;
              break;
            }

            _context.t0 = _client$getUrl;
            _context.next = 13;
            break;

          case 10:
            _context.next = 12;
            return core.getUrl(opts);

          case 12:
            _context.t0 = _context.sent;

          case 13:
            client.getUrl = _context.t0;

            if (!((_client$gqlClient = client.gqlClient) != null)) {
              _context.next = 18;
              break;
            }

            _context.t1 = _client$gqlClient;
            _context.next = 21;
            break;

          case 18:
            _context.next = 20;
            return core.gqlClient(_extends({}, opts, {
              client: client,
              core: core
            }));

          case 20:
            _context.t1 = _context.sent;

          case 21:
            client.gqlClient = _context.t1;
            return _context.abrupt("return", client);

          case 23:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _createClientOptions.apply(this, arguments);
}

function createGqlClient(_x) {
  return _createGqlClient.apply(this, arguments);
}

function _createGqlClient() {
  _createGqlClient = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var _options$client$getUr, _options$client;

    var core, options, clientOpts, globalHeaders, storage, getHeaders, getUrl, get, post;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            post = function _post(url, data, init) {
              var headers = getHeaders(init == null ? void 0 : init.headers, init == null ? void 0 : init.noProxyJwt);
              return fetchEx(url, data, _extends({}, init, {
                headers: headers
              }));
            };

            get = function _get(schema, options) {
              schema = schema || '';
              var url = getUrl(schema) + "/graphql";
              var headers = getHeaders(options == null ? void 0 : options.headers, options == null ? void 0 : options.noProxyJwt);
              return new graphqlRequest.GraphQLClient(url, {
                headers: headers
              });
            };

            getHeaders = function _getHeaders(headers, noProxyTenant) {
              var xRequestId = share2.utils.uuidv4();
              var h = {
                'x-request-id': xRequestId,
                'x-trace-id': xRequestId,
                Authorization: globalHeaders == null ? void 0 : globalHeaders.Authorization
              };
              var tenantContext = storage.getStore();

              if (tenantContext) {
                if (tenantContext.traceId) h['x-trace-id'] = tenantContext.traceId;
                if (tenantContext.sessionId) h['x-session-id'] = tenantContext.sessionId;
                if (tenantContext.token) h['Authorization'] = 'Bearer ' + tenantContext.token;
              }

              if (noProxyTenant) {
                h['Authorization'] = 'Bearer ' + createClientToken({
                  sessionId: tenantContext == null ? void 0 : tenantContext.sessionId,
                  profileId: tenantContext == null ? void 0 : tenantContext.profileId,
                  tenantId: tenantContext == null ? void 0 : tenantContext.tenantId,
                  userId: tenantContext == null ? void 0 : tenantContext.userId
                });
              }

              return _extends({}, globalHeaders, h, headers);
            };

            core = presetCore(opts);
            _context.next = 6;
            return core.config(opts);

          case 6:
            options = _context.sent;
            clientOpts = presetClient(options);
            globalHeaders = clientOpts == null ? void 0 : clientOpts.headers;
            storage = presetStorage(options); // per request call

            if (!((_options$client$getUr = options == null ? void 0 : (_options$client = options.client) == null ? void 0 : _options$client.getUrl) != null)) {
              _context.next = 14;
              break;
            }

            _context.t0 = _options$client$getUr;
            _context.next = 17;
            break;

          case 14:
            _context.next = 16;
            return presetCore(options).getUrl(options);

          case 16:
            _context.t0 = _context.sent;

          case 17:
            getUrl = _context.t0;
            return _context.abrupt("return", {
              options: options,
              get: get,
              post: post,
              getUrl: getUrl
            });

          case 19:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _createGqlClient.apply(this, arguments);
}

function presetDev(opts) {
  var _ref, _opts$dev$medportalUr, _opts$dev;

  return {
    medportalUrl: (_ref = (_opts$dev$medportalUr = opts == null ? void 0 : (_opts$dev = opts.dev) == null ? void 0 : _opts$dev.medportalUrl) != null ? _opts$dev$medportalUr : constants.ENV.AIT_MEDPORTAL_URL) != null ? _ref : constants.DEV.MEDPORTAL_URL
  };
}

var INGRESS_REWRITE_SCHEMAS = constants.CLIENT.INGRESS_REWRITE_SCHEMAS;
function createUrl(_x) {
  return _createUrl.apply(this, arguments);
}

function _createUrl() {
  _createUrl = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var dev, mode, root, server, SCHEMA;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            dev = presetDev(opts);
            mode = presetMode(opts);
            root = presetRoot(_extends({}, opts, {
              mode: mode
            }));
            _context.next = 5;
            return presetServer(_extends({}, opts, {
              root: root,
              mode: mode
            }));

          case 5:
            server = _context.sent;
            SCHEMA = (root == null ? void 0 : root.schema) || constants.ROOT.SCHEMA;
            return _context.abrupt("return", function (schema) {
              var _schema, _schema2, _dev$medportalUrl;

              schema = schema || SCHEMA;
              if ((_schema = schema) != null && _schema.startsWith('http:') || (_schema2 = schema) != null && _schema2.startsWith('https:')) return schema;

              if (mode != null && mode.isKube) {
                if (INGRESS_REWRITE_SCHEMAS.includes(schema)) {
                  return "http://everest2" + schema + "-service";
                }

                return "http://everest2" + schema + "-service/" + schema;
              }

              if (schema === SCHEMA) {
                return "http://localhost:" + (server == null ? void 0 : server.port) + "/" + schema;
              }

              return ((_dev$medportalUrl = dev == null ? void 0 : dev.medportalUrl) != null ? _dev$medportalUrl : '') + "/" + schema;
            });

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _createUrl.apply(this, arguments);
}

function presetLog(opts) {
  var _opts$log$level, _opts$log, _constants$ENV$AIT_LO;

  return {
    level: (_opts$log$level = opts == null ? void 0 : (_opts$log = opts.log) == null ? void 0 : _opts$log.level) != null ? _opts$log$level : parseInt((_constants$ENV$AIT_LO = constants.ENV.AIT_LOGLEVEL) != null ? _constants$ENV$AIT_LO : '2', 10)
  };
}

function presetTest(opts) {
  return _extends({}, opts == null ? void 0 : opts.test);
}

var ENV$1 = null;
function presetSettings(_x) {
  return _presetSettings.apply(this, arguments);
}

function _presetSettings() {
  _presetSettings = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var _opts$settings, _opts$settings2;

    var consts, env, settings, keys;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            consts = _extends({
              ENV_LOCAL_PREFIX: constants.SETTINGS.ENV_LOCAL_PREFIX
            }, opts == null ? void 0 : (_opts$settings = opts.settings) == null ? void 0 : _opts$settings.constants);
            env = ENV$1 || (ENV$1 = loadEnv(consts.ENV_LOCAL_PREFIX));
            settings = (opts == null ? void 0 : (_opts$settings2 = opts.settings) == null ? void 0 : _opts$settings2.keys) || {};
            keys = share2.utils.mergeDeep(env, settings);
            return _context.abrupt("return", _extends({
              constants: consts
            }, opts == null ? void 0 : opts.settings, {
              keys: keys
            }));

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _presetSettings.apply(this, arguments);
}

function loadEnv(prefix) {
  var _process;

  if (!prefix || !((_process = process) != null && _process.env)) return {};
  return Object.keys(process.env).filter(function (key) {
    return key.startsWith(prefix);
  }).reduce(function (r, ckey) {
    var v = process.env[ckey];
    var pureKey = ckey.substring(prefix.length);

    if (pureKey) {
      r[pureKey] = {
        defValue: v
      };
    }

    return r;
  }, {});
}

function presetLookups(opts) {
  var lookups = opts == null ? void 0 : opts.lookups;
  return _extends({}, lookups, {
    types: _extends({}, lookups == null ? void 0 : lookups.types)
  });
}

function presetApp(_x) {
  return _presetApp.apply(this, arguments);
}

function _presetApp() {
  _presetApp = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var mode, auth, log, dev, test, prisma, shield, migration, core, tenant, contextStorage, settings, lookups, root, schema, server, database;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            mode = presetMode(opts);
            auth = presetAuth(opts);
            log = presetLog(opts);
            dev = presetDev(opts);
            test = presetTest(opts);
            prisma = presetPrisma(opts);
            shield = presetShield(opts);
            migration = presetMigration(opts);
            core = presetCore(opts);
            tenant = presetTenant(opts);
            contextStorage = presetStorage(opts);
            _context.next = 13;
            return presetSettings(opts);

          case 13:
            settings = _context.sent;
            lookups = presetLookups(opts);
            opts = _extends({}, opts, {
              mode: mode,
              auth: auth,
              log: log,
              dev: dev,
              test: test,
              prisma: prisma,
              shield: shield,
              tenant: tenant,
              migration: migration,
              core: core,
              contextStorage: contextStorage,
              settings: settings,
              lookups: lookups
            });
            root = presetRoot(opts);
            schema = presetSchema(opts);
            opts = _extends({}, opts, {
              root: root,
              schema: schema
            });
            _context.next = 21;
            return presetServer(opts);

          case 21:
            server = _context.sent;
            database = presetDatabase(opts);
            opts = _extends({}, opts, {
              server: server,
              database: database
            });
            return _context.abrupt("return", opts);

          case 25:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _presetApp.apply(this, arguments);
}

function createConfig(opts) {
  return presetApp(opts);
}

var sql_0 = (function () {
  return "\n\nCREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";\n\nCREATE OR REPLACE PROCEDURE public.ait_tenant_role_check(rolename text, psw text)\n LANGUAGE plpgsql\nAS $$\nbegin\n\tIF NOT EXISTS (SELECT FROM pg_catalog.pg_roles WHERE rolname = rolename) THEN\n\t\tEXECUTE format('CREATE ROLE %s NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN PASSWORD ''%s'' NOREPLICATION NOBYPASSRLS', rolename, psw);\n\tEND IF;\nend\n$$\n;\n\n\ncreate or replace function public.ait_default_tenant()\n\treturns uuid\n\timmutable\n\tlanguage plpgsql\nas\n$$\nbegin\n   return current_setting('app.current_tenant')::uuid;\nend;\n$$;\n\n\nCREATE OR REPLACE PROCEDURE public.ait_tenant_schema_grant(schema_name text) AS $$\nDECLARE\n\tsql varchar(1024);\n\ttenant_user text := 'tenant';\nBEGIN\n    RAISE NOTICE USING MESSAGE=format('Granting schema: %s', schema_name);\n\n\t-- grant access to schema for tenant user (CREATE | USAGE)\n\tsql := format('GRANT ALL ON SCHEMA %s TO %s;', schema_name, tenant_user);\n    RAISE NOTICE USING MESSAGE=sql;\n\tEXECUTE(sql);\n\n\t-- grant access to tables for tenant user\n\tsql := format('GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA %s TO %s;', schema_name, tenant_user);\n    RAISE NOTICE USING MESSAGE=sql;\n\tEXECUTE(sql);\nEND\n$$\n\nLANGUAGE plpgsql;\n\ncreate or replace function public.ait_rls_check_dict(tenant_id uuid)\n  returns bool\n  stable\n  cost 100000\nas\n$$\n  select case\n      when tenant_id::text = current_setting('app.current_tenant')::text\n          or tenant_id::text = '00000000-0000-0000-1000-000000000001' --current_setting('app.platform')::text\n      then true\n      else false\n  end;\n$$\nlanguage sql;\n\nCREATE OR REPLACE PROCEDURE public.ait_tenant_table_grant(p_schemaname text, p_tablename text)\n    LANGUAGE plpgsql\nAS $procedure$\nDECLARE\n    sql varchar(1024);\nBEGIN\n\n    IF NOT EXISTS (SELECT 1\n        FROM  information_schema.columns\n            join information_schema.tables t on t.table_schema = columns.table_schema and t.table_name = columns.table_name and t.table_type <> 'VIEW'\n        WHERE  columns.table_schema = p_schemaname\n        AND  columns.table_name = p_tablename\n        AND  columns.column_name = 'tenant_id') THEN\n            RETURN;\n    END IF;\n\n    sql := format('ALTER TABLE %s.\"%s\" ENABLE ROW LEVEL SECURITY;', p_schemaname, p_tablename);\n    RAISE NOTICE USING MESSAGE=sql;\n    EXECUTE(sql);\n    IF NOT EXISTS (select 1 from pg_policies where schemaname = p_schemaname and tablename = p_tablename and policyname = 'tenant_isolation_policy') THEN\n        IF (p_schemaname = 'dict2') THEN\n            sql := format('CREATE POLICY tenant_isolation_policy ON %s.\"%s\" USING (public.ait_rls_check_dict(tenant_id));', p_schemaname, p_tablename);\n        ELSE\n            sql := format('CREATE POLICY tenant_isolation_policy ON %s.\"%s\" USING (tenant_id = current_setting(''app.current_tenant'')::uuid);', p_schemaname, p_tablename);\n        END IF;\n        RAISE NOTICE USING MESSAGE=sql;\n        EXECUTE(sql);\n    END IF;\nEND\n$procedure$;\n;\n\n";
});

var sql_1 = (function () {
  return "\n-- ----------------------------------------\n  CREATE SCHEMA IF NOT EXISTS core2;\n\n  CREATE TABLE IF NOT EXISTS core2.\"Setting\" (\n    id text NOT NULL,\n    value text NULL,\n    \"label\" text NULL,\n    note text NULL,\n    meta jsonb NULL,\n    sort int4 NULL DEFAULT 0,\n    public bool NOT NULL DEFAULT false,\n    \"createdAt\" timestamp(3) NULL DEFAULT CURRENT_TIMESTAMP,\n    \"updatedAt\" timestamp(3) NULL,\n    \"isDisabled\" bool NULL DEFAULT false,\n    \"key\" text NOT NULL DEFAULT 'dummy'::text,\n    tenant_id uuid NOT NULL DEFAULT ait_default_tenant(),\n    CONSTRAINT \"Setting_pkey\" PRIMARY KEY (id)\n  );\n\n  CREATE UNIQUE INDEX  IF NOT EXISTS  \"Setting_key_tenantId_unique_constraint\" ON core2.\"Setting\" USING btree (tenant_id, key);\n  CREATE INDEX  IF NOT EXISTS \"Setting_tenant_id_idx\" ON core2.\"Setting\" USING btree (tenant_id);\n\n  -- Permissions\n  CALL public.ait_tenant_schema_grant('core2');\n  CALL public.ait_tenant_table_grant('core2','\"Setting\"');\n\n";
});

function execSql (_x) {
  return _ref.apply(this, arguments);
}

function _ref() {
  _ref = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var result, dbUrl, hasInstallation, queryText, hasCore2, _queryText;

    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            result = 1;
            dbUrl = opts.dbUrl;
            _context.next = 4;
            return helper.db.checkAitInstallation(dbUrl);

          case 4:
            hasInstallation = _context.sent;

            if (hasInstallation) {
              _context.next = 22;
              break;
            }

            console.log("----- INSTALL - \"" + dbUrl + "\" START...");
            _context.prev = 7;
            queryText = sql_0();
            console.log(queryText);
            _context.next = 12;
            return helper.db.query({
              dbUrl: dbUrl,
              queryText: queryText
            });

          case 12:
            _context.next = 14;
            return helper.db.query({
              dbUrl: dbUrl,
              queryText: "CALL public.ait_tenant_role_check('" + opts.userTenant + "','" + opts.passwordTenant + "')"
            });

          case 14:
            console.log("----- INSTALL - OK");
            _context.next = 21;
            break;

          case 17:
            _context.prev = 17;
            _context.t0 = _context["catch"](7);
            console.error('INSTALL', _context.t0);
            throw new Error('Ошибка инсталяции');

          case 21:
            result = 2;

          case 22:
            _context.next = 24;
            return helper.db.checkSchema(dbUrl, 'core2');

          case 24:
            hasCore2 = _context.sent;

            if (hasCore2) {
              _context.next = 40;
              break;
            }

            console.log("----- INIT CORE2 START...");
            _context.prev = 27;
            _queryText = sql_1();
            console.log(_queryText);
            _context.next = 32;
            return helper.db.query({
              dbUrl: dbUrl,
              queryText: _queryText
            });

          case 32:
            console.log("----- INIT - OK");
            _context.next = 39;
            break;

          case 35:
            _context.prev = 35;
            _context.t1 = _context["catch"](27);
            console.error('INIT CORE', _context.t1);
            throw new Error('Ошибка установки core2');

          case 39:
            result = 4;

          case 40:
            return _context.abrupt("return", result);

          case 41:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[7, 17], [27, 35]]);
  }));
  return _ref.apply(this, arguments);
}

function dbInstall(_x) {
  return _dbInstall.apply(this, arguments);
}

function _dbInstall() {
  _dbInstall = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var core, database, dbUrl;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            core = presetCore(opts);
            database = presetDatabase(opts); // set AIT + off bouncer

            dbUrl = core.dbConnection(_extends({}, opts, {
              mode: _extends({}, opts == null ? void 0 : opts.mode, {
                isTenant: false,
                isMigration: true
              }),
              database: _extends({}, database, {
                pgbouncer: undefined,
                schema: 'public'
              })
            }));
            _context.next = 5;
            return execSql({
              dbUrl: dbUrl,
              passwordTenant: database.passwordTenant || 'tenant',
              userTenant: database.userTenant || 'tenant'
            });

          case 5:
            return _context.abrupt("return", dbUrl);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _dbInstall.apply(this, arguments);
}

var RLS = /*#__PURE__*/Symbol('RLS');
function useRlsMiddleware(_x) {
  return _useRlsMiddleware.apply(this, arguments);
}

function _useRlsMiddleware() {
  _useRlsMiddleware = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3(opts) {
    var _presetPrisma, prisma, tenant, storage, currentTenantParamName, forceTenantId, $BASE_TRANSACTION, rls;

    return runtime_1.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _presetPrisma = presetPrisma(opts), prisma = _presetPrisma.prisma;

            if (prisma) {
              _context3.next = 3;
              break;
            }

            throw new Error('prisma should be set');

          case 3:
            tenant = presetTenant(opts);
            storage = presetStorage(opts);
            currentTenantParamName = tenant.pgCurrentTenantParamName || constants.TENANT.PG_CURRENT_TENANT_PARAM_NAME;
            forceTenantId = tenant.forceTenantId; // wrap func $transaction<P extends PrismaPromise<any>[]>(arg: [...P]): Promise<UnwrapTuple<P>>;

            if (!prisma[RLS]) {
              prisma[RLS] = true; // save base method

              $BASE_TRANSACTION = prisma.$transaction.bind(prisma); // override $transaction

              prisma.$transaction = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee() {
                var _storage$getStore;

                var tenantId,
                    values,
                    SQL_SET_LOCAL,
                    setLocal,
                    results,
                    _args = arguments;
                return runtime_1.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        if (Array.isArray(_args.length <= 0 ? undefined : _args[0])) {
                          _context.next = 2;
                          break;
                        }

                        return _context.abrupt("return", $BASE_TRANSACTION.apply(void 0, _args));

                      case 2:
                        tenantId = forceTenantId != null ? forceTenantId : (_storage$getStore = storage.getStore()) == null ? void 0 : _storage$getStore.tenantId;

                        if (tenantId) {
                          _context.next = 5;
                          break;
                        }

                        throw new Error('tenant_id should be set');

                      case 5:
                        values = _args.length <= 0 ? undefined : _args[0];
                        SQL_SET_LOCAL = "SET LOCAL " + currentTenantParamName + " TO '" + tenantId + "'";
                        setLocal = prisma.$queryRawUnsafe(SQL_SET_LOCAL);
                        _context.next = 10;
                        return $BASE_TRANSACTION([setLocal].concat(values));

                      case 10:
                        results = _context.sent;
                        results.shift();
                        return _context.abrupt("return", results);

                      case 13:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee);
              }));
            }

            rls = /*#__PURE__*/function () {
              var _ref2 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(params, next) {
                var _params$args, _params$args$paramete, _params$args2, _params$args2$paramet;

                var values, vals, queryRaw, r, values1, vals1, executeRaw, r1, model, modelName, prismaAction, res;
                return runtime_1.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        if (!params.runInTransaction) {
                          _context2.next = 2;
                          break;
                        }

                        return _context2.abrupt("return", next(params));

                      case 2:
                        _context2.t0 = params.action;
                        _context2.next = _context2.t0 === 'queryRaw' ? 5 : _context2.t0 === 'executeRaw' ? 12 : 19;
                        break;

                      case 5:
                        values = ((_params$args = params.args) == null ? void 0 : (_params$args$paramete = _params$args.parameters) == null ? void 0 : _params$args$paramete.values) || '[]';
                        vals = JSON.parse(values);
                        queryRaw = prisma.$queryRawUnsafe.apply(prisma, [params.args.query].concat(vals));
                        _context2.next = 10;
                        return prisma.$transaction([queryRaw]);

                      case 10:
                        r = _context2.sent;
                        return _context2.abrupt("return", r[0]);

                      case 12:
                        values1 = ((_params$args2 = params.args) == null ? void 0 : (_params$args2$paramet = _params$args2.parameters) == null ? void 0 : _params$args2$paramet.values) || '[]';
                        vals1 = JSON.parse(values1);
                        executeRaw = prisma.$executeRawUnsafe.apply(prisma, [params.args.query].concat(vals1));
                        _context2.next = 17;
                        return prisma.$transaction([executeRaw]);

                      case 17:
                        r1 = _context2.sent;
                        return _context2.abrupt("return", r1[0]);

                      case 19:
                        model = params.model;

                        if (!(model == null)) {
                          _context2.next = 22;
                          break;
                        }

                        return _context2.abrupt("return", next(params));

                      case 22:
                        // Generate model class name from model params (PascalCase to camelCase)
                        modelName = model.charAt(0).toLowerCase() + model.slice(1); // @ts-ignore

                        prismaAction = prisma[modelName][params.action](params.args);
                        _context2.next = 26;
                        return prisma.$transaction([prismaAction]);

                      case 26:
                        res = _context2.sent;
                        return _context2.abrupt("return", res[0]);

                      case 28:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2);
              }));

              return function rls(_x2, _x3) {
                return _ref2.apply(this, arguments);
              };
            }();

            prisma.$use(rls);

          case 10:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _useRlsMiddleware.apply(this, arguments);
}

var TABLE_NAME = 'core2."Setting"';

function escapeString(val) {
  if (!val) return '';
  val = val.replace(/[\0\n\r\b\t\\'"\x1a]/g, function (s) {
    switch (s) {
      case '\0':
        return '\\0';

      case '\n':
        return '\\n';

      case '\r':
        return '\\r';

      case '\b':
        return '\\b';

      case '\t':
        return '\\t';

      case '\x1a':
        return '\\Z';

      case "'":
        return "''";

      case '"':
        return '""';

      default:
        return '\\' + s;
    }
  });
  return val;
}

function createGetSettings(opts, tablename) {
  var _opts$settings;

  if (tablename === void 0) {
    tablename = TABLE_NAME;
  }

  // cache
  var allKeys = Object.keys(((_opts$settings = opts.settings) == null ? void 0 : _opts$settings.keys) || {});
  if (!allKeys.length) return function () {
    return Promise.resolve({});
  };
  var sqlIn = allKeys.filter(function (c) {
    return c;
  }).map(function (c) {
    return "'" + escapeString(c) + "'";
  }).join(',');
  return /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(appId) {
      var _recs$1$rows;

      var _getDbContext, tenantId, dbUrl, sql_SET_LOCAL, OR_LIKE, sql_SELECT, recs, rdata, newValues, values, queryText, r;

      return runtime_1.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!(!sqlIn && !appId)) {
                _context.next = 2;
                break;
              }

              return _context.abrupt("return", {});

            case 2:
              _getDbContext = getDbContext(opts), tenantId = _getDbContext.tenantId, dbUrl = _getDbContext.dbUrl, sql_SET_LOCAL = _getDbContext.sql_SET_LOCAL;
              OR_LIKE = '';

              if (appId) {
                OR_LIKE = " OR \"key\" LIKE '" + escapeString(appId) + ".%' ";
              }

              sql_SELECT = "\n    SELECT\n      \"key\", value\n    FROM\n      " + tablename + "\n    WHERE\n      tenant_id='" + tenantId + "'\n      AND (\"key\" IN (" + sqlIn + ") " + OR_LIKE + ")\n    ORDER BY sort, \"label\";\n    ";
              _context.next = 8;
              return helper.db.query({
                dbUrl: dbUrl,
                queryText: sql_SET_LOCAL + sql_SELECT
              });

            case 8:
              recs = _context.sent;
              rdata = ((_recs$1$rows = recs[1].rows) != null ? _recs$1$rows : []).reduce(function (d, c) {
                d[c.key] = c.value;
                return d;
              }, {}); // check new settings for registered

              newValues = allKeys.filter(function (key) {
                var _opts$root;

                return ((_opts$root = opts.root) == null ? void 0 : _opts$root.schema) && key.startsWith(opts.root.schema) && !share2.utils.has(rdata, key);
              }).map(function (key) {
                var _opts$settings2, _opts$settings2$keys;

                var sett = (_opts$settings2 = opts.settings) == null ? void 0 : (_opts$settings2$keys = _opts$settings2.keys) == null ? void 0 : _opts$settings2$keys[key];
                return {
                  key: key,
                  sett: sett
                };
              });

              if (!(newValues != null && newValues.length)) {
                _context.next = 18;
                break;
              }

              values = newValues.map(function (kv) {
                var _kv$sett, _kv$sett2, _kv$sett3, _kv$sett4, _kv$sett5, _kv$sett6, _kv$sett7, _kv$sett8;

                return "\n(\n'" + share2.utils.uuidv4() + "',\n'" + escapeString(kv.key) + "',\n'" + escapeString(((_kv$sett = kv.sett) == null ? void 0 : _kv$sett.value) || ((_kv$sett2 = kv.sett) == null ? void 0 : _kv$sett2.defValue)) + "',\n'" + escapeString((_kv$sett3 = kv.sett) == null ? void 0 : _kv$sett3.label) + "',\n" + ((_kv$sett4 = kv.sett) != null && _kv$sett4["public"] ? 'true' : 'false') + ",\n'" + escapeString((_kv$sett5 = kv.sett) == null ? void 0 : _kv$sett5.note) + "',\n" + (((_kv$sett6 = kv.sett) == null ? void 0 : _kv$sett6.sort) || 10) + ",\n" + ((_kv$sett7 = kv.sett) != null && _kv$sett7.meta ? "'" + JSON.stringify((_kv$sett8 = kv.sett) == null ? void 0 : _kv$sett8.meta) + "'" : 'NULL') + ",\n'" + tenantId + "'\n)";
              }).join(',');
              queryText = "\n" + sql_SET_LOCAL + "\nINSERT INTO " + tablename + "\n  (id,\"key\",value,label,public,note,sort,meta,tenant_id)\nVALUES\n  " + values + "\nON CONFLICT (tenant_id,\"key\") DO NOTHING;\n" + sql_SELECT;
              _context.next = 16;
              return helper.db.query({
                dbUrl: dbUrl,
                queryText: queryText
              });

            case 16:
              r = _context.sent;
              // reread
              rdata = (r[2].rows || []).reduce(function (d, c) {
                d[c.key] = c.value;
                return d;
              }, {});

            case 18:
              return _context.abrupt("return", rdata);

            case 19:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }();
}
function createGetSettingList(opts, tablename) {
  if (tablename === void 0) {
    tablename = TABLE_NAME;
  }

  return /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(options) {
      var _getDbContext2, tenantId, dbUrl, sql_SET_LOCAL, AND, sql_SELECT, recs;

      return runtime_1.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _getDbContext2 = getDbContext(opts), tenantId = _getDbContext2.tenantId, dbUrl = _getDbContext2.dbUrl, sql_SET_LOCAL = _getDbContext2.sql_SET_LOCAL;
              AND = '';

              if (options != null && options.appId) {
                AND = " AND \"key\" LIKE '" + escapeString(options == null ? void 0 : options.appId) + ".%' ";
              }

              if (options != null && options["public"]) {
                AND = " AND \"public\"=true ";
              } else if ((options == null ? void 0 : options["public"]) === false) {
                AND = " AND \"public\"=false ";
              }

              sql_SELECT = "\n  SELECT\n    id, value, \"label\", note, meta, sort, public, \"key\"\n  FROM\n    " + tablename + "\n  WHERE\n    tenant_id='" + tenantId + "' " + AND + "\n  ORDER BY sort, \"label\";\n  ";
              _context2.next = 7;
              return helper.db.query({
                dbUrl: dbUrl,
                queryText: sql_SET_LOCAL + sql_SELECT
              });

            case 7:
              recs = _context2.sent;
              return _context2.abrupt("return", recs == null ? void 0 : recs[1].rows);

            case 9:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function (_x2) {
      return _ref2.apply(this, arguments);
    };
  }();
}
function createSaveSettings(opts, tablename) {
  if (tablename === void 0) {
    tablename = TABLE_NAME;
  }

  return /*#__PURE__*/function () {
    var _ref3 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3(data) {
      var keys, _getDbContext3, tenantId, dbUrl, sql_SET_LOCAL, upd, _iterator, _step, key, sql_UPDATE;

      return runtime_1.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              keys = data && Object.keys(data);

              if (keys != null && keys.length) {
                _context3.next = 3;
                break;
              }

              return _context3.abrupt("return", Promise.resolve(0));

            case 3:
              _getDbContext3 = getDbContext(opts), tenantId = _getDbContext3.tenantId, dbUrl = _getDbContext3.dbUrl, sql_SET_LOCAL = _getDbContext3.sql_SET_LOCAL;
              upd = keys.length;
              _iterator = _createForOfIteratorHelperLoose(keys);

            case 6:
              if ((_step = _iterator()).done) {
                _context3.next = 13;
                break;
              }

              key = _step.value;
              sql_UPDATE = "UPDATE " + tablename + "\n      SET value='" + escapeString(data[key]) + "'\n      WHERE\n        (\"key\"='" + escapeString(key) + "'\n          OR id='" + escapeString(key) + "')\n        AND tenant_id='" + tenantId + "'";
              _context3.next = 11;
              return helper.db.query({
                dbUrl: dbUrl,
                queryText: sql_SET_LOCAL + sql_UPDATE
              });

            case 11:
              _context3.next = 6;
              break;

            case 13:
              return _context3.abrupt("return", upd);

            case 14:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    }));

    return function (_x3) {
      return _ref3.apply(this, arguments);
    };
  }();
}

function getDbContext(opts) {
  var _opts$tenant, _opts$tenant2, _opts$contextStorage, _opts$contextStorage$;

  var core = presetCore(opts);
  var dbUrl = core.dbConnection(opts);
  var currentTenantParamName = (opts == null ? void 0 : (_opts$tenant = opts.tenant) == null ? void 0 : _opts$tenant.pgCurrentTenantParamName) || constants.TENANT.PG_CURRENT_TENANT_PARAM_NAME;
  var tenantId = ((_opts$tenant2 = opts.tenant) == null ? void 0 : _opts$tenant2.forceTenantId) || ((_opts$contextStorage = opts.contextStorage) == null ? void 0 : (_opts$contextStorage$ = _opts$contextStorage.getStore()) == null ? void 0 : _opts$contextStorage$.tenantId) || '';
  var sql_SET_LOCAL = tenantId ? "SET LOCAL " + currentTenantParamName + " TO '" + tenantId + "';" : '';
  return {
    tenantId: tenantId,
    dbUrl: dbUrl,
    sql_SET_LOCAL: sql_SET_LOCAL
  };
}

function createSettings(_x) {
  return _createSettings.apply(this, arguments);
}

function _createSettings() {
  _createSettings = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var core, settings;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            core = presetCore(opts);
            _context.next = 3;
            return core.config(opts);

          case 3:
            opts = _context.sent;
            _context.next = 6;
            return presetSettings(opts);

          case 6:
            settings = _context.sent;
            settings.provider = buildProvider(settings, opts);
            return _context.abrupt("return", settings);

          case 9:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _createSettings.apply(this, arguments);
}

function buildProvider(settings, opts) {
  var provider = settings.provider || {};

  if (!provider.getSettings) {
    provider.getSettings = createGetSettings(opts);
  }

  if (!provider.saveSettings) {
    provider.saveSettings = createSaveSettings(opts);
  }

  if (!provider.getSettingList) {
    provider.getSettingList = createGetSettingList(opts);
  }

  return provider;
}

var _excluded = ["q"];

function getTypeName(type) {
  type = type.charAt(0).toUpperCase() + type.slice(1);
  return "Lookup" + type + "Payload";
}

function getTypeDefs(type) {
  return "\n  type " + getTypeName(type) + " {\n    id: ID!\n    record: JSONObject\n    error: String\n  }\n";
}

var DEF = "\n  scalar JSONObject\n\n  input LookupSortInput {\n    by: String\n    desc: Boolean\n  }\n\n  input LookupPaginationInput {\n    page: Int\n    perPage: Int\n  }\n\n  input LookupSearchInput {\n    filter: JSONObject\n    pagination: LookupPaginationInput\n    sort: [LookupSortInput!]\n  }\n\n  type Query {\n    findLookup(\n      ids: LookupInput!\n    ): LookupResult!\n    findLookupList(\n      type: String!\n      where: LookupSearchInput\n    ): [JSONObject!]\n    getLookupTypes: JSONObject!\n  }\n";

function createGetList(type, propText, select, searchFields, prepareArgs) {
  return /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
      var _opts$where, _pagination$page, _pagination$perPage, _opts$where2, _opts$where3;

      var pagination, page, take, skip, sort, filter, _q$filter, q, otherFilter, where, orderBy, context, args, OR, _OR, _yield$prepareArgs;

      return runtime_1.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              pagination = opts == null ? void 0 : (_opts$where = opts.where) == null ? void 0 : _opts$where.pagination;
              page = (_pagination$page = pagination == null ? void 0 : pagination.page) != null ? _pagination$page : 1;
              take = (_pagination$perPage = pagination == null ? void 0 : pagination.perPage) != null ? _pagination$perPage : 100;
              skip = (page - 1) * take;
              sort = opts == null ? void 0 : (_opts$where2 = opts.where) == null ? void 0 : _opts$where2.sort;
              filter = (opts == null ? void 0 : (_opts$where3 = opts.where) == null ? void 0 : _opts$where3.filter) || {};
              _q$filter = _extends({
                q: undefined
              }, filter), q = _q$filter.q, otherFilter = _objectWithoutPropertiesLoose(_q$filter, _excluded);
              where = _extends({}, otherFilter);
              orderBy = (sort == null ? void 0 : sort.length) && sort.map(function (c) {
                var _ref2;

                return _ref2 = {}, _ref2[c.by] = c.desc ? 'desc' : 'asc', _ref2;
              }) || Array.isArray(searchFields) && searchFields.map(function (c) {
                var _ref3;

                return _ref3 = {}, _ref3[c] = 'asc', _ref3;
              });
              context = opts.context;
              args = {
                where: where,
                skip: skip,
                take: take,
                orderBy: orderBy,
                select: select
              };

              if (!q) {
                _context.next = 21;
                break;
              }

              if (!Array.isArray(searchFields)) {
                _context.next = 16;
                break;
              }

              if (searchFields.length) {
                OR = searchFields.map(function (c) {
                  var _ref4;

                  return _ref4 = {}, _ref4[c] = {
                    startsWith: q,
                    mode: 'insensitive'
                  }, _ref4;
                });
                args.where.OR = [].concat(where.OR, OR);
              }

              _context.next = 21;
              break;

            case 16:
              if (!(typeof searchFields === 'function')) {
                _context.next = 21;
                break;
              }

              _context.next = 19;
              return searchFields({
                args: args,
                input: q,
                context: context,
                type: type,
                opts: opts
              });

            case 19:
              _OR = _context.sent;
              args.where.OR = [].concat(where.OR, [_OR]);

            case 21:
              if (!prepareArgs) {
                _context.next = 31;
                break;
              }

              _context.next = 24;
              return prepareArgs({
                args: args,
                input: q,
                context: context,
                type: type,
                opts: opts
              });

            case 24:
              _context.t0 = _yield$prepareArgs = _context.sent;

              if (!(_context.t0 != null)) {
                _context.next = 29;
                break;
              }

              _context.t1 = _yield$prepareArgs;
              _context.next = 30;
              break;

            case 29:
              _context.t1 = args;

            case 30:
              args = _context.t1;

            case 31:
              return _context.abrupt("return", context.prisma[type].findMany(args));

            case 32:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }();
}

function buildDisplay(propText, list) {
  if (propText) {
    if (typeof propText === 'function') {
      list.forEach(function (c) {
        return c['_display'] = propText(c);
      });
    } else {
      list.forEach(function (c) {
        return c['_display'] = c[propText];
      });
    }
  }

  return list;
}

function createGetByIds(type, propId, propText, select, prepareArgs // where?: any
) {
  return /*#__PURE__*/function () {
    var _ref5 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(opts) {
      var _where;

      var context, ids, pargs, _yield$prepareArgs2;

      return runtime_1.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              context = opts.context, ids = opts.ids;
              pargs = {
                where: (_where = {}, _where[propId] = {
                  "in": ids
                }, _where),
                select: select
              };

              if (!prepareArgs) {
                _context2.next = 12;
                break;
              }

              _context2.next = 5;
              return prepareArgs({
                args: pargs,
                context: context,
                type: type,
                input: ids,
                opts: opts
              });

            case 5:
              _context2.t0 = _yield$prepareArgs2 = _context2.sent;

              if (!(_context2.t0 != null)) {
                _context2.next = 10;
                break;
              }

              _context2.t1 = _yield$prepareArgs2;
              _context2.next = 11;
              break;

            case 10:
              _context2.t1 = pargs;

            case 11:
              pargs = _context2.t1;

            case 12:
              return _context2.abrupt("return", context.prisma[type].findMany(pargs));

            case 13:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function (_x2) {
      return _ref5.apply(this, arguments);
    };
  }();
}
/**
 *@example
  const lookup = new Lookup();
  lookup.add({
    type: "gender",
    propId: "genderId",
    select: { name: true },
  })
  //gql
  {
    findLookup(ids: { gender: ["00000000-0000-0000-0006-000000000001"] }) {
      gender {
        id
        record
      }
    }
    findLookupList(type: "gender")
  }
 */


var Lookup = /*#__PURE__*/function () {
  function Lookup() {
    this.types = {};
  }

  var _proto = Lookup.prototype;

  _proto.add = function add(opts) {
    var type = opts.type;

    if (type) {
      var _opts$propId, _opts$propText, _opts$resolvers$ids, _opts$resolvers, _opts$prepareArgs, _ref6, _opts$resolvers$list, _opts$resolvers2, _opts$prepareArgs2;

      var propId = (_opts$propId = opts.propId) != null ? _opts$propId : 'id';
      var propText = (_opts$propText = opts.propText) != null ? _opts$propText : 'name';
      var typeName = getTypeName(type);
      var mapper = opts.map;

      var select = _extends({}, opts.select);

      select[propId] = true;
      var arrSearchFields = Array.isArray(opts.searchFields) && [].concat(opts.searchFields) || undefined;

      if (typeof propText === 'string') {
        select[propText] = true;
        if (arrSearchFields && !arrSearchFields.includes(propText)) arrSearchFields.push(propText);
      }

      if (arrSearchFields) arrSearchFields.forEach(function (c) {
        return select[c] = true;
      });

      var _ids = (_opts$resolvers$ids = opts == null ? void 0 : (_opts$resolvers = opts.resolvers) == null ? void 0 : _opts$resolvers.ids) != null ? _opts$resolvers$ids : createGetByIds(type, propId, propText, select, opts == null ? void 0 : (_opts$prepareArgs = opts.prepareArgs) == null ? void 0 : _opts$prepareArgs.ids);

      var searchFields = (_ref6 = arrSearchFields != null ? arrSearchFields : opts == null ? void 0 : opts.searchFields) != null ? _ref6 : [];

      var _list = (_opts$resolvers$list = opts == null ? void 0 : (_opts$resolvers2 = opts.resolvers) == null ? void 0 : _opts$resolvers2.list) != null ? _opts$resolvers$list : createGetList(type, propText, select, searchFields, opts == null ? void 0 : (_opts$prepareArgs2 = opts.prepareArgs) == null ? void 0 : _opts$prepareArgs2.list); // buildDisplay


      var resolvers = {
        ids: function () {
          var _ids2 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee3(idsOpts) {
            var items;
            return runtime_1.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return _ids(idsOpts);

                  case 2:
                    items = _context3.sent;
                    if (mapper) items = items.map(function (c) {
                      return mapper(c);
                    });
                    return _context3.abrupt("return", buildDisplay(propText, items));

                  case 5:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3);
          }));

          function ids(_x3) {
            return _ids2.apply(this, arguments);
          }

          return ids;
        }(),
        list: function () {
          var _list2 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee4(listOpts) {
            var items;
            return runtime_1.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return _list(listOpts);

                  case 2:
                    items = _context4.sent;
                    if (mapper) items = items.map(function (c) {
                      return mapper(c);
                    });
                    return _context4.abrupt("return", buildDisplay(propText, items));

                  case 5:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4);
          }));

          function list(_x4) {
            return _list2.apply(this, arguments);
          }

          return list;
        }()
      };
      var typeDefs = getTypeDefs(type);
      this.types[type] = {
        type: type,
        propId: propId,
        // propText,
        typeName: typeName,
        resolvers: resolvers,
        typeDefs: typeDefs
      };
    }

    return this;
  };

  _proto.getTypes = function getTypes() {
    return Object.getOwnPropertyNames(this.types);
  };

  _proto.toPayload = function toPayload() {
    var _this = this;

    return this.getTypes().map(function (t) {
      return _this.types[t].typeDefs;
    }).join();
  };

  _proto.toInput = function toInput() {
    var flds = this.getTypes().map(function (t) {
      return "    " + t + ": [ID!]";
    }).join('\r\n');
    return "\n  input LookupInput {\n" + flds + "\n  }\n";
  };

  _proto.toResult = function toResult() {
    var _this2 = this;

    var flds = this.getTypes().map(function (t) {
      return "    " + t + ": [" + _this2.types[t].typeName + "!]";
    }).join('\r\n');
    return "\n  type LookupResult {\n" + flds + "\n  }\n";
  };

  _proto.toString = function toString() {
    return this.typeDefs;
  };

  _proto.resolvers = function resolvers() {
    var _this3 = this;

    if (!this.getTypes().length) return {};
    return {
      JSONObject: graphqlScalars.JSONObjectResolver,
      Query: {
        findLookup: function () {
          var _findLookup = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee6(source, args, context, info) {
            var typesIds, promises, results, result;
            return runtime_1.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    typesIds = args.ids || {};
                    promises = Object.keys(typesIds).map( /*#__PURE__*/function () {
                      var _ref7 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee5(ctype) {
                        var _args$ids$ctype, _ti$resolvers;

                        var ids, ti, propId, getIds, items, error, r;
                        return runtime_1.wrap(function _callee5$(_context5) {
                          while (1) {
                            switch (_context5.prev = _context5.next) {
                              case 0:
                                ids = (_args$ids$ctype = args == null ? void 0 : args.ids[ctype]) != null ? _args$ids$ctype : [];
                                ti = _this3.types[ctype];
                                propId = ti == null ? void 0 : ti.propId;
                                getIds = ti == null ? void 0 : (_ti$resolvers = ti.resolvers) == null ? void 0 : _ti$resolvers.ids;
                                items = [];
                                error = null;

                                if (!getIds) {
                                  _context5.next = 17;
                                  break;
                                }

                                _context5.prev = 7;
                                _context5.next = 10;
                                return getIds({
                                  source: source,
                                  args: args,
                                  context: context,
                                  info: info,
                                  ids: [].concat(ids)
                                });

                              case 10:
                                items = _context5.sent;
                                _context5.next = 17;
                                break;

                              case 13:
                                _context5.prev = 13;
                                _context5.t0 = _context5["catch"](7);
                                console.error('findLookup getIds', _context5.t0);

                                if (_context5.t0 instanceof Error) {
                                  error = _context5.t0.message;
                                } else {
                                  error = 'error';
                                }

                              case 17:
                                r = ids.map(function (id) {
                                  return {
                                    id: id,
                                    record: items.find(function (citem) {
                                      return propId && (citem == null ? void 0 : citem[propId]) === id;
                                    }) || null,
                                    error: error
                                  };
                                });
                                return _context5.abrupt("return", {
                                  ctype: ctype,
                                  r: r
                                });

                              case 19:
                              case "end":
                                return _context5.stop();
                            }
                          }
                        }, _callee5, null, [[7, 13]]);
                      }));

                      return function (_x9) {
                        return _ref7.apply(this, arguments);
                      };
                    }());
                    _context6.next = 4;
                    return Promise.all(promises);

                  case 4:
                    results = _context6.sent;
                    result = results.reduce(function (p, c) {
                      p[c.ctype] = c.r;
                      return p;
                    }, {});
                    return _context6.abrupt("return", result);

                  case 7:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6);
          }));

          function findLookup(_x5, _x6, _x7, _x8) {
            return _findLookup.apply(this, arguments);
          }

          return findLookup;
        }(),
        findLookupList: function findLookupList(source, args, context, info) {
          var _this3$types$args$typ, _getList;

          var getList = (_this3$types$args$typ = _this3.types[args.type].resolvers) == null ? void 0 : _this3$types$args$typ.list;
          if (!getList) return null;
          return (_getList = getList({
            source: source,
            context: context,
            info: info,
            type: args.type,
            where: args == null ? void 0 : args.where
          })) != null ? _getList : null;
        },
        getLookupTypes: function getLookupTypes() {
          return _this3.getTypes().reduce(function (p, t) {
            p[t] = {
              propId: _this3.types[t]
            };
            return p;
          }, {});
        }
      }
    };
  };

  _createClass(Lookup, [{
    key: "typeDefs",
    get: function get() {
      if (!this.getTypes().length) return '';
      return "\n    " + this.toInput() + "\n    " + this.toPayload() + "\n    " + this.toResult() + "\n    " + DEF + "\n";
    }
  }]);

  return Lookup;
}();

function createLookups(_x) {
  return _createLookups.apply(this, arguments);
}

function _createLookups() {
  _createLookups = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var lookupsOptions, lookup, types;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            lookupsOptions = presetLookups(opts);

            if (!lookupsOptions.lookup) {
              _context.next = 3;
              break;
            }

            return _context.abrupt("return", lookupsOptions);

          case 3:
            lookup = new Lookup();
            types = lookupsOptions.types || {}; // fill lookup

            Object.keys(types).forEach(function (type) {
              lookup.add(_extends({
                type: type
              }, types[type]));
            });
            return _context.abrupt("return", _extends({}, lookupsOptions, {
              types: types,
              lookup: lookup
            }));

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _createLookups.apply(this, arguments);
}

var dummy$1 = function dummy(o) {
  throw new Error('Core is not initialized!');
};

var _core$1;

function initApp() {
  var _core2;

  _core$1 = (_core2 = _core$1) != null ? _core2 : initCore({
    app: createApp,
    context: createContext,
    apollo: createApollo,
    config: createConfig,
    express: createExpress,
    dbConnection: createDbConnection,
    prismaOptions: createPrismaOptions,
    prisma: dummy$1,
    schema: createSchema,
    server: createServer,
    getUrl: createUrl,
    clientOptions: createClientOptions,
    gqlClient: createGqlClient,
    shield: createShield,
    httpHeader: createHttpHeader,
    dbInstall: dbInstall,
    dbSeed: dbSeed,
    dbDeploy: dbDeploy,
    migrate: migrate,
    tenants: getTenants,
    useRlsMiddleware: useRlsMiddleware,
    settings: createSettings,
    lookups: createLookups
  });
  return _core$1;
}
function createApp$1(_x) {
  return _createApp$1.apply(this, arguments);
}

function _createApp$1() {
  _createApp$1 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(opts) {
    var _appOpts$server;

    var appOpts;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return initApp().app(opts);

          case 2:
            appOpts = _context.sent;

            if ((_appOpts$server = appOpts.server) != null && _appOpts$server.appServer) {
              _context.next = 5;
              break;
            }

            throw new Error('appServer not initialized');

          case 5:
            return _context.abrupt("return", appOpts.server.appServer);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _createApp$1.apply(this, arguments);
}



var options = {
  __proto__: null
};

var presets = {
  core: presetCore,
  database: presetDatabase,
  auth: presetAuth,
  dev: presetDev,
  log: presetLog,
  migration: presetMigration,
  mode: presetMode,
  prisma: presetPrisma,
  root: presetRoot,
  shield: presetShield,
  test: presetTest,
  tenant: presetTenant,
  client: presetClient,
  server: presetServer,
  schema: presetSchema,
  apollo: presetApollo,
  express: presetExpress,
  storage: presetStorage,
  settings: presetSettings
};



var index$1 = {
  __proto__: null
};



var Context = {
  __proto__: null
};

function mockConsole(method, value) {
  if (!method) return null;
  var methods = Array.isArray(method) ? method : [method];
  return methods.map(function (c) {
    return jest.spyOn(console, c).mockReturnValue(value).mockRestore;
  });
}
function createTestContext(opts) {
  var _opts, _opts2, _opts3, _opts3$test;

  var context = {};
  var server = null;
  var restores = null;
  opts = _extends({}, opts, {
    mode: _extends({
      isMigration: true,
      isTest: true
    }, (_opts = opts) == null ? void 0 : _opts.mode),
    shield: _extends({}, (_opts2 = opts) == null ? void 0 : _opts2.shield, {
      options: {
        allowExternalErrors: true,
        debug: true
      }
    })
  });

  if ((_opts3 = opts) != null && (_opts3$test = _opts3.test) != null && _opts3$test.isBeforeEach) {
    beforeEach(doBefore());
    afterEach(doAfter());
  } else {
    beforeAll(doBefore());
    afterAll(doAfter());
  }

  return context;

  function doBefore() {
    return /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee() {
      var _opts4, _opts4$test, _options$client;

      var options, core, _yield$core$prismaOpt, dbUrl, prisma;

      return runtime_1.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              restores = mockConsole((_opts4 = opts) == null ? void 0 : (_opts4$test = _opts4.test) == null ? void 0 : _opts4$test.suppressLog);
              _context.next = 3;
              return createApp$1(opts);

            case 3:
              server = _context.sent;
              _context.next = 6;
              return server.run();

            case 6:
              options = _context.sent;
              core = presetCore(options);
              _context.next = 10;
              return core.prismaOptions(_extends({}, options, {
                mode: {
                  isTenant: false
                },
                prisma: {}
              }));

            case 10:
              _yield$core$prismaOpt = _context.sent;
              dbUrl = _yield$core$prismaOpt.dbUrl;
              prisma = _yield$core$prismaOpt.prisma;
              context.dbUrl = dbUrl;
              context.prisma = prisma;
              context.options = options;
              context.gqlClient = (_options$client = options.client) == null ? void 0 : _options$client.gqlClient;

            case 17:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));
  }

  function doAfter() {
    return /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2() {
      var _context2;

      return runtime_1.wrap(function _callee2$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              if (!((_context2 = context) != null && _context2.prisma && context.prisma.$disconnect)) {
                _context3.next = 3;
                break;
              }

              _context3.next = 3;
              return context.prisma.$disconnect();

            case 3:
              context = null;

              if (!server) {
                _context3.next = 10;
                break;
              }

              _context3.next = 7;
              return server.stop();

            case 7:
              server = null;
              _context3.next = 10;
              return sleep(1000);

            case 10:
              // log restore
              if (restores) restores.forEach(function (c) {
                return c();
              });

            case 11:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee2);
    }));
  }
}

function sleep(ms) {
  return new Promise(function (resolve) {
    return setTimeout(resolve, ms);
  });
}

var index$2 = {
  __proto__: null,
  mockConsole: mockConsole,
  createTestContext: createTestContext
};



var index$3 = {
  __proto__: null,
  rules: rules
};

function sendMail(gqlClient, data) {
  // post api
  var url = gqlClient.getUrl('fserv') + '/postman/api/send_mail';
  return gqlClient.post(url, data, {
    contentType: 'multipart/form-data',
    responseText: true
  });
}

function create(client) {
  return {
    mail: function mail(data) {
      var _client$options, _client$options$root;

      var addresses = (_client$options = client.options) == null ? void 0 : (_client$options$root = _client$options.root) == null ? void 0 : _client$options$root.mailTestAddress;

      if (!addresses) {
        addresses = Array.isArray(data.addresses) ? data.addresses.join(';') : data.addresses;
      }

      return sendMail(client, {
        addresses: addresses,
        subject: data.subject.length > 255 ? data.subject.substring(0, 254) + '…' : data.subject,
        body: data.html
      });
    }
  };
}

var postman = {
  __proto__: null,
  create: create
};

function _getNumeratorCode(docType, context) {
  var _context$deptCode;

  var res = docType;
  var deptCode = (_context$deptCode = context == null ? void 0 : context.deptCode) != null ? _context$deptCode : process.env.AIT_DEV_DEPT_CODE; // TODO: из глобального контекста здесь?

  if (deptCode) res += "." + deptCode;
  return res;
}

function _getDocNum2(_x, _x2, _x3) {
  return _getDocNum.apply(this, arguments);
}

function _getDocNum() {
  _getDocNum = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(docType, client, context) {
    var numeratorCode, docNumUrl;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            numeratorCode = _getNumeratorCode(docType);
            docNumUrl = client.getUrl('cmon') + '/num/api/numerator/execute';
            return _context.abrupt("return", client.post(docNumUrl, {
              code: numeratorCode
            }));

          case 3:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _getDocNum.apply(this, arguments);
}

function create$1(client, context) {
  return {
    getNumeratorCode: function getNumeratorCode(docType) {
      return _getNumeratorCode(docType, context);
    },
    getDocNum: function getDocNum(docType) {
      return _getDocNum2(docType, client, context);
    }
  };
}

var cmon = {
  __proto__: null,
  create: create$1
};

var _templateObject$2;
//   let res = docType;
//   const deptCode = context?.deptCode ?? process.env.AIT_DEV_DEPT_CODE; // TODO: из глобального контекста здесь?
//   if (deptCode) res += `.${deptCode}`;
//   return res;
// }

function _getDocNum2$1(_x, _x2, _x3, _x4) {
  return _getDocNum$1.apply(this, arguments);
}

function _getDocNum$1() {
  _getDocNum$1 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(numerator_id, client, context, onDate) {
    var q, res;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            q = graphqlRequest.gql(_templateObject$2 || (_templateObject$2 = _taggedTemplateLiteralLoose(["\n    mutation execNumerator($input: ExecNumeratorInput!) {\n      execNumerator(input: $input) {\n        ... on ExecNumeratorResultSuccess {\n          value\n          numerator_id\n          issued_number_id\n        }\n        ... on ResultError {\n          errorCode\n          errorText\n        }\n      }\n    }\n  "])));
            _context.next = 3;
            return client.get('common').request(q, {
              input: {
                numerator_id: numerator_id,
                onDate: onDate
              }
            });

          case 3:
            res = _context.sent.execNumerator;
            console.log('execNumerator res', res);
            return _context.abrupt("return", res);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _getDocNum$1.apply(this, arguments);
}

function create$2(client, context) {
  return {
    //getNumeratorCode: (docType: string) => getNumeratorCode(docType, context),
    getDocNum: function getDocNum(numerator_id, onDate) {
      return _getDocNum2$1(numerator_id, client, context, onDate);
    }
  };
}

var common = {
  __proto__: null,
  create: create$2
};

function _exec2(_x, _x2) {
  return _exec.apply(this, arguments);
}

function _exec() {
  _exec = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(client, data) {
    var url;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            url = client.getUrl('bash') + '/Scripts/execute';
            return _context.abrupt("return", client.post(url, data, {
              contentType: 'application/json',
              responseText: true
            }));

          case 2:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _exec.apply(this, arguments);
}

function create$3(client) {
  return {
    exec: function exec(data) {
      return _exec2(client, data);
    }
  };
}

var bash = {
  __proto__: null,
  create: create$3
};

function _ping2(_x, _x2) {
  return _ping$1.apply(this, arguments);
}

function _ping$1() {
  _ping$1 = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(client, data) {
    var cmd, path, url;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            cmd = data != null && data.version ? 'version' : 'ping';
            path = data != null && data.api ? "/api/" + cmd : "/" + cmd;
            url = client.getUrl((data == null ? void 0 : data.schema) || '') + path;
            return _context.abrupt("return", client.post(url, undefined, {
              method: 'GET',
              responseText: !(data != null && data.json)
            }));

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _ping$1.apply(this, arguments);
}

function create$4(client) {
  return {
    ping: function ping(data) {
      return _ping2(client, data);
    }
  };
}

var ping$1 = {
  __proto__: null,
  create: create$4
};

var getQuery = function getQuery(types) {
  return "query FindLookup($ids: LookupInput!) {\n  findLookup(ids: $ids) {\n    " + types.map(function (k) {
    return k + " {id record}";
  }).join(' ') + "\n  }\n}";
};

function _getIds2(_x, _x2) {
  return _getIds.apply(this, arguments);
}

function _getIds() {
  _getIds = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(client, data) {
    var r, result;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (!(!data || !data.schema)) {
              _context.next = 2;
              break;
            }

            return _context.abrupt("return", {});

          case 2:
            _context.next = 4;
            return client.get(data.schema || '').request(getQuery(Object.keys(data.ids)), {
              ids: data.ids
            }, data.headers);

          case 4:
            r = _context.sent.findLookup;
            result = Object.keys(r).reduce(function (p, typ) {
              var payload = r[typ].reduce(function (p1, idRecord) {
                p1[idRecord.id] = idRecord.record || null;
                return p1;
              }, {});
              p[typ] = payload;
              return p;
            }, {});
            return _context.abrupt("return", result);

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _getIds.apply(this, arguments);
}

function create$5(client) {
  return {
    getIds: function getIds(data) {
      return _getIds2(client, data);
    }
  };
}

var lookup = {
  __proto__: null,
  create: create$5
};

function post(_x, _x2, _x3) {
  return _post.apply(this, arguments);
}

function _post() {
  _post = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee(func, client, data) {
    var url;
    return runtime_1.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            url = client.getUrl('printx') + '/api/' + func;
            return _context.abrupt("return", client.post(url, data, {
              contentType: 'application/json'
            }));

          case 2:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _post.apply(this, arguments);
}

function get(_x4, _x5) {
  return _get.apply(this, arguments);
}

function _get() {
  _get = _asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2(func, client) {
    var url;
    return runtime_1.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            url = client.getUrl('printx') + '/api/' + func;
            return _context2.abrupt("return", client.post(url, null, {
              method: 'GET',
              contentType: 'application/json'
            }));

          case 2:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _get.apply(this, arguments);
}

function create$6(client) {
  return {
    saveSection: function saveSection(data) {
      return post('save_section', client, data);
    },
    saveProperties: function saveProperties(data) {
      return post('save_properties', client, data);
    },
    selectProperties: function selectProperties(sectionId) {
      return get("select_properties/" + sectionId, client);
    },
    selectSections: function selectSections(data) {
      return post("select_sections", client, data);
    },
    selectTemplates: function selectTemplates(data) {
      return post("select_templates", client, data);
    },
    getDocumentValues: function getDocumentValues(sectionId) {
      var postfix = sectionId ? "?sectionId=" + sectionId : '';
      return get("get_document_values" + postfix, client);
    },
    saveTemplate: function saveTemplate(data) {
      return post("save_template", client, data);
    },
    saveTemplateData: function saveTemplateData(data) {
      return post("save_template_data", client, data);
    },
    selectTemplateData: function selectTemplateData(id) {
      return get("select_template_data/" + id, client);
    },
    createDocuments: function createDocuments(data) {
      return post("create_documents", client, data);
    },
    createWorkbook: function createWorkbook(data) {
      return post("create_workbook", client, data);
    },
    convertRtfToPdf: function convertRtfToPdf(data) {
      return post("convert_rtf_to_pdf", client, data);
    }
  };
}

var printx = {
  __proto__: null,
  create: create$6
};



var index$4 = {
  __proto__: null,
  postman: postman,
  cmon: cmon,
  common: common,
  bash: bash,
  ping: ping$1,
  lookup: lookup,
  printx: printx
};

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

exports.Cron = index;
exports.Lookup = Lookup;
exports.aitScalars = scalars;
exports.clients = index$4;
exports.constants = constants;
exports.context = Context;
exports.createApp = createApp$1;
exports.gqlTools = graphqlTools;
exports.helper = helper;
exports.options = options;
exports.presets = presets;
exports.shield = index$3;
exports.testing = index$2;
exports.types = index$1;
//# sourceMappingURL=serv2.cjs.development.js.map
