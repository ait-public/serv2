
'use strict'

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./serv2.cjs.production.min.js')
} else {
  module.exports = require('./serv2.cjs.development.js')
}
