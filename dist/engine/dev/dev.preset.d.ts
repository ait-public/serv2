import { DevOptions } from './dev.options';
export declare function presetDev(opts?: {
    dev?: DevOptions;
}): DevOptions;
