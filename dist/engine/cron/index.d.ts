import { GraphQLResolveInfo } from 'graphql';
export declare type Resolver<TContext> = (parent: any, args: any, context: TContext, info: GraphQLResolveInfo) => Promise<any>;
export declare type JobOpts<TContext> = {
    /**
     * id исполняемая-команда
     */
    id: string;
    /**
     * наименование команды
     */
    name: string;
    /**
     * Формат cronjob-выражения '*\/30 * * * * *'
     */
    cronTime: string;
    /**
     * доп. данные
     */
    data?: any;
    /**
     * пользовательский заголовок
     */
    headers?: Record<string, string>;
    /**
     * автоматический запуск при первой регистрации службы
     */
    runFirst?: boolean;
    /**
     * Выполнение команды
     */
    resolve?: Resolver<TContext>;
};
export declare type JobActivity = {
    /**
     * id исполняемая-команда
     */
    id: string;
    /**
     * длина массива
     */
    len?: number;
    /**
     * результат
     */
    result?: any;
    /**
     * доп. данные команды
     */
    data?: any;
    /**
     * длительность выполнения
     */
    duration: number;
    /**
     * Дата исполнения
     */
    time: Date;
    /**
     * Ошибка
     */
    error?: string;
};
export declare type JobDump = {
    jobs: {
        id: string;
        name: string;
        lastActivity?: JobActivity;
    }[];
};
/**
 * Команда для периодического выполнения заданий
 * @example
const c = new Job<any>();
c.add({
      id: 'monitor2.mail-dispatch',
      cronTime: "* * * * * *",
      name: 'рассылка писем',
      runFirst: true,
      resolve: async (_parent, args, context) => {...}
});
 */
export declare class Job<TContext> {
    private static jobs;
    static readonly typeDefs = "\n  type Query {\n    getCronJobs: JSONObject\n  }\n  type Mutation {\n    \"CRON \u041A\u043E\u043C\u0430\u043D\u0434\u0430 \u0434\u043B\u044F \u043F\u0435\u0440\u0438\u043E\u0434\u0438\u0447\u0435\u0441\u043A\u043E\u0433\u043E \u0432\u044B\u043F\u043E\u043B\u043D\u0435\u043D\u0438\u044F \u0437\u0430\u0434\u0430\u043D\u0438\u0439 \u0432 \u043E\u043F\u0440\u0435\u0434\u0435\u043B\u0451\u043D\u043D\u043E\u0435 \u0432\u0440\u0435\u043C\u044F\"\n    runCronJob(\n      \"id \u0438\u0441\u043F\u043E\u043B\u043D\u044F\u0435\u043C\u0430\u044F-\u043A\u043E\u043C\u0430\u043D\u0434\u0430\"\n      id: String!\n      \"\u0434\u043E\u043F. \u0434\u0430\u043D\u043D\u044B\u0435\"\n      data: JSONObject\n    ): JSONObject\n    \"\u0438\u043D\u0438\u0446\u0438\u0430\u043B\u0438\u0437\u0430\u0446\u0438\u044F (\u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F) CRON \u0441\u043B\u0443\u0436\u0431\"\n    initCronJobs: JSONObject\n  }\n  ";
    static getCurl(url: string, id: string, data?: any, headers?: {
        [key: string]: string;
    }): string;
    /**
    * Добавить задачу
    * @example
    c.add({
          id: 'monitor2.mail-dispatch',
          cronTime: "* * * * * *",
          name: 'рассылка писем',
          runFirst: true,
          resolve: async (_parent, args, context) => {...}
    });
    */
    add(opts: JobOpts<TContext>): this;
    private static initJobs;
    private static getIds;
    private static run;
    private static trimSize;
    private static runJob;
    private static getJobs;
    static resolvers: {
        JSONObject: import("graphql").GraphQLScalarType;
        Mutation: {
            runCronJob: typeof Job.runJob;
            initCronJobs: typeof Job.initJobs;
        };
        Query: {
            getCronJobs: typeof Job.getJobs;
        };
    };
}
export declare const CronResolvers: {
    JSONObject: import("graphql").GraphQLScalarType;
    Mutation: {
        runCronJob: typeof Job.runJob;
        initCronJobs: typeof Job.initJobs;
    };
    Query: {
        getCronJobs: typeof Job.getJobs;
    };
};
export declare const CronTypeDefs = "\n  type Query {\n    getCronJobs: JSONObject\n  }\n  type Mutation {\n    \"CRON \u041A\u043E\u043C\u0430\u043D\u0434\u0430 \u0434\u043B\u044F \u043F\u0435\u0440\u0438\u043E\u0434\u0438\u0447\u0435\u0441\u043A\u043E\u0433\u043E \u0432\u044B\u043F\u043E\u043B\u043D\u0435\u043D\u0438\u044F \u0437\u0430\u0434\u0430\u043D\u0438\u0439 \u0432 \u043E\u043F\u0440\u0435\u0434\u0435\u043B\u0451\u043D\u043D\u043E\u0435 \u0432\u0440\u0435\u043C\u044F\"\n    runCronJob(\n      \"id \u0438\u0441\u043F\u043E\u043B\u043D\u044F\u0435\u043C\u0430\u044F-\u043A\u043E\u043C\u0430\u043D\u0434\u0430\"\n      id: String!\n      \"\u0434\u043E\u043F. \u0434\u0430\u043D\u043D\u044B\u0435\"\n      data: JSONObject\n    ): JSONObject\n    \"\u0438\u043D\u0438\u0446\u0438\u0430\u043B\u0438\u0437\u0430\u0446\u0438\u044F (\u0440\u0435\u0433\u0438\u0441\u0442\u0440\u0430\u0446\u0438\u044F) CRON \u0441\u043B\u0443\u0436\u0431\"\n    initCronJobs: JSONObject\n  }\n  ";
