import { Core } from './Core';
import { CoreOptions } from './core.options';
export declare function initCore(core: Core): Core;
export declare function presetCore(opts?: {
    core?: CoreOptions;
}): Core;
