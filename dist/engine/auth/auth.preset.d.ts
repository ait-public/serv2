import { AuthOptions } from '../options';
export declare function presetAuth(opts?: {
    auth?: AuthOptions;
}): AuthOptions;
