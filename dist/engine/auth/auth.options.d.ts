export declare type AuthOptions = {
    secretKey?: string;
    token?: {
        secret: string;
    };
    refreshToken?: {
        secret: string;
    };
};
