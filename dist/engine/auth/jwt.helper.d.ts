import { ID } from '../types';
import { AuthOptions } from './auth.options';
import { RootOptions } from '../root/root.options';
export declare type JwtPayload = {
    [key: string]: string | number | undefined;
    jti: ID;
    sub: ID;
    exp: number;
    name: string;
    role: string;
    clientId: string;
    sessionId?: string;
    profileId?: string;
    tenantId?: string;
};
export declare type CreateTokenInput = {
    [key: string]: string | number | undefined;
    userId?: string;
    name?: string;
    role?: string;
    secret?: string;
    clientId?: string;
    issuer?: string;
    exp?: number;
    sessionId?: string;
    profileId?: string;
    tenantId?: string;
    ip?: string;
};
declare function createToken(opts?: CreateTokenInput): Promise<string | undefined>;
export declare type TokenSecretPair = {
    token: string;
    secret?: string;
};
declare function verifyToken(opts: TokenSecretPair): Promise<JwtPayload | undefined>;
declare function getPayload(opts: {
    token: string;
    auth: AuthOptions;
}): Promise<JwtPayload | undefined>;
declare function createTokens(opts: {
    /** deprecated use data instead*/
    user?: {
        id?: ID;
        name?: string;
        role?: string;
    };
    data?: CreateTokenInput;
    auth?: AuthOptions;
    root?: RootOptions;
}): Promise<{
    accessToken: string | undefined;
    refreshToken: string | undefined;
}>;
declare const _default: {
    createToken: typeof createToken;
    createTokens: typeof createTokens;
    verifyToken: typeof verifyToken;
    getPayload: typeof getPayload;
};
export default _default;
