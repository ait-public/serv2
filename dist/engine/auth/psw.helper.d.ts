declare function hashPassword(password: string): Promise<string>;
declare function isValidPassword(noencrypted: string, encrypted: string): Promise<boolean>;
declare const _default: {
    hashPassword: typeof hashPassword;
    isValidPassword: typeof isValidPassword;
};
export default _default;
