declare const _default: {
    ruleTree: {
        Query: {
            '*': import("graphql-shield/dist/rules").RuleOr;
        };
        Mutation: {
            '*': import("graphql-shield/dist/rules").RuleOr;
        };
    };
    options: {
        debug: boolean;
        allowExternalErrors: boolean;
    };
};
export default _default;
