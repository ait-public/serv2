import { ShieldOptions } from './shield.options';
export declare type ShieldOptionsInput = {
    shield?: ShieldOptions;
};
