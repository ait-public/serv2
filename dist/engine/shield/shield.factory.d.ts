import { ShieldOptions } from './shield.options';
export declare function createShield(opts?: {
    shield?: ShieldOptions;
}): ShieldOptions;
