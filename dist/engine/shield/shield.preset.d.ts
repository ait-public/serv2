import { ShieldOptions } from '../options';
export declare function presetShield(opts?: {
    shield?: ShieldOptions;
}): ShieldOptions;
