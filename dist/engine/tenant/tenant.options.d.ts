export declare type TenantOptions = {
    platformTenantId?: string;
    forceTenantId?: string;
    /** Check tenantId before apply */
    strict?: boolean;
    /** 'app.current_tenant' */
    pgCurrentTenantParamName?: string;
};
