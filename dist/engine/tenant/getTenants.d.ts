import { AppOptions } from '../options';
export declare function getTenants(opts?: AppOptions): Promise<string[]>;
