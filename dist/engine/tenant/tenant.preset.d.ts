import { TenantOptions } from './tenant.options';
export declare function presetTenant(opts?: {
    tenant?: TenantOptions;
}): TenantOptions;
