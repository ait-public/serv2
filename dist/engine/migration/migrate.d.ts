import { MigrateOptionsInput } from './migrate.options.input';
export declare function migrate(opts?: MigrateOptionsInput): Promise<void>;
