import { MigrationOptions } from '../options';
export declare function presetMigration(opts?: {
    migration?: MigrationOptions<any>;
}): MigrationOptions<any>;
