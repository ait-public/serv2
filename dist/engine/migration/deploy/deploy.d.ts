import { DeployOptionsInput } from './deploy.options.input';
export declare function dbDeploy(opts?: DeployOptionsInput): Promise<void>;
