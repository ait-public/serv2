import { DbInstallOptionsInput } from './dbInstall.options.input';
export declare function dbInstall(opts?: DbInstallOptionsInput): Promise<string>;
