export default function (opts: {
    dbUrl: string;
    userTenant: string;
    passwordTenant: string;
}): Promise<number>;
