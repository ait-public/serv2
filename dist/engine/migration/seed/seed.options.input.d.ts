import { MigrationOptions } from '../migration.options';
import { AppOptions, AuthOptions, DbConnectionOptions, DbOptions, GetUrl, HttpHeaderOptions, PrismaOptions, UrlOptions } from '../../options';
import { IGqlClient } from '../../types';
import { IContextStorage } from '../../storage/IContextStorage';
export declare type SeedOptionsInput = {
    root?: {
        schema?: string;
    };
    tenant?: {
        platformTenantId?: string;
    };
    migration?: MigrationOptions<any>;
    database?: DbOptions;
    contextStorage?: IContextStorage;
    prisma?: PrismaOptions;
    mode?: {
        isTest?: boolean;
        isMigration?: boolean;
        isTenant?: boolean;
        isKube?: boolean;
    };
    dev?: {
        medportalUrl?: string;
    };
    server?: {
        port?: string | number;
    };
    auth?: AuthOptions;
    client?: {
        headers?: Record<string, string>;
        gqlClient?: IGqlClient;
        getUrl?: GetUrl;
    };
    core?: {
        dbConnection?: (opts?: DbConnectionOptions) => string;
        prisma?: (opts?: PrismaOptions, storage?: IContextStorage) => any;
        httpHeader?: (opts?: HttpHeaderOptions) => Promise<Record<string, string>>;
        getUrl?: (opts?: UrlOptions) => Promise<GetUrl>;
        tenants?: (opts?: AppOptions) => Promise<string[]>;
    };
};
