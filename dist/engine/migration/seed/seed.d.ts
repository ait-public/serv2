import { SeedOptionsInput } from './seed.options.input';
export declare function dbSeed(opts?: SeedOptionsInput): Promise<void>;
