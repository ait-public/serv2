import { AppOptions } from '../../options';
export declare function grantTenant(dbUrl: string, options: AppOptions): Promise<void>;
