import { SeedContext } from '../Context';
export declare type SeedFunc<T> = (ctx: SeedContext<T>) => Promise<void>;
export declare type MigrationOptions<T> = {
    /** skip npx Prisma deploy */
    skipDeploy?: boolean;
    /** seed by AIT User */
    seed?: SeedFunc<T>;
    /** seed platform by Tenant User */
    seedPlatform?: SeedFunc<T>;
    /** seed each lpu by Tenant User */
    seedTenant?: SeedFunc<T>;
    /** npx prisma migrate deploy */
    cmdDeploy?: string;
    /** npx prisma -v */
    cmdVersion?: string;
    /** migrate after ping all dependencie schemas {dict2:"*",fserv:"^3.0.0"} */
    dependencies?: {
        [key: string]: string;
    };
};
