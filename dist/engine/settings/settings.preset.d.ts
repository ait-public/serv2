import { SettingsOptions } from './settings.options';
export declare function presetSettings(opts?: {
    settings?: SettingsOptions;
}): Promise<SettingsOptions>;
