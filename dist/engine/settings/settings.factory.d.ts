import { SettingsOptions, SettingsOptionsInput } from './settings.options';
export declare function createSettings(opts?: SettingsOptionsInput): Promise<SettingsOptions>;
