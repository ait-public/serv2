import { AppOptions } from '../options';
export declare function createGetSettings(opts: AppOptions, tablename?: string): (appId?: string | undefined) => Promise<any>;
export declare function createGetSettingList(opts: AppOptions, tablename?: string): (options?: {
    appId?: string | undefined;
    public?: boolean | undefined;
} | undefined) => Promise<any>;
export declare function createSaveSettings(opts: AppOptions, tablename?: string): (data: Record<string, string>) => Promise<number>;
