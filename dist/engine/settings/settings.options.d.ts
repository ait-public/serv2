import { AppOptions } from '../options';
export declare type Setting = {
    label?: string;
    defValue?: string;
    value?: string;
    note?: string;
    meta?: any;
    sort?: number;
    public?: boolean;
};
export declare type KeySetting = {
    id: string;
    key: string;
} & Setting;
export declare type SettingNullable = Setting | undefined | null;
export declare type KeySettings = Record<string, SettingNullable>;
export declare type SettingValue = string | undefined | null;
export declare type SettingsProvider = {
    getSettings?: (appId?: string) => Promise<Record<string, SettingValue>>;
    saveSettings?: (data: Record<string, string>) => Promise<number>;
    getSettingList?: (options?: {
        appId?: string;
        public?: boolean;
    }) => Promise<KeySetting[]>;
};
export declare type SettingsOptions = {
    keys?: KeySettings;
    provider?: SettingsProvider;
    constants?: {
        ENV_LOCAL_PREFIX?: string;
    };
};
export declare type SettingsOptionsInput = AppOptions;
