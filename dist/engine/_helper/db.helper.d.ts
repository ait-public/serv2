declare function parseUrl(dbUrl: string): {
    match: boolean;
    port?: string | number;
    db?: string;
    user?: string;
    password?: string;
    schema?: string;
    host?: string;
    bouncer?: boolean;
};
declare function buildUrl(opts: {
    dbUrl?: string;
    port?: string | number;
    db?: string;
    user?: string;
    password?: string;
    schema?: string;
    host?: string;
    bouncer?: boolean;
}): string;
declare function dropSchema(dbUrl: string, schema?: string): Promise<void>;
declare function ping(dbUrl: string): Promise<any>;
declare function checkSchema(dbUrl: string, schema?: string): Promise<boolean>;
declare function lastMigration(dbUrl: string, schema?: string): Promise<any>;
declare function checkAitInstallation(dbUrl: string): Promise<boolean>;
declare function query({ dbUrl, queryText, values, }: {
    dbUrl: string;
    queryText: string;
    values?: any[];
}): Promise<import("pg").QueryResult<any>>;
declare const _default: {
    parseUrl: typeof parseUrl;
    buildUrl: typeof buildUrl;
    dropSchema: typeof dropSchema;
    ping: typeof ping;
    checkSchema: typeof checkSchema;
    lastMigration: typeof lastMigration;
    checkAitInstallation: typeof checkAitInstallation;
    query: typeof query;
};
export default _default;
