import { Response } from 'node-fetch';
export declare class PostError extends Error {
    url: string;
    status: number;
    constructor(url: string, status: number, message: string);
}
declare function fetchEx(url: string, data?: any, init?: {
    method?: string;
    headers?: Record<string, string>;
    contentType?: string;
    responseText?: boolean;
    getResult?: (r: Response, init?: any) => Promise<any>;
    body?: any;
}): Promise<any>;
export default fetchEx;
