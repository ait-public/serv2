import fetch from './fetch.helper';
import { startTimer as start } from './startTimer';
declare const _default: {
    db: {
        parseUrl: (dbUrl: string) => {
            match: boolean;
            port?: string | number | undefined;
            db?: string | undefined;
            user?: string | undefined;
            password?: string | undefined;
            schema?: string | undefined;
            host?: string | undefined;
            bouncer?: boolean | undefined;
        };
        buildUrl: (opts: {
            dbUrl?: string | undefined;
            port?: string | number | undefined;
            db?: string | undefined;
            user?: string | undefined;
            password?: string | undefined;
            schema?: string | undefined;
            host?: string | undefined;
            bouncer?: boolean | undefined;
        }) => string;
        dropSchema: (dbUrl: string, schema?: string | undefined) => Promise<void>;
        ping: (dbUrl: string) => Promise<any>;
        checkSchema: (dbUrl: string, schema?: string | undefined) => Promise<boolean>;
        lastMigration: (dbUrl: string, schema?: string | undefined) => Promise<any>;
        checkAitInstallation: (dbUrl: string) => Promise<boolean>;
        query: ({ dbUrl, queryText, values, }: {
            dbUrl: string;
            queryText: string;
            values?: any[] | undefined;
        }) => Promise<import("pg").QueryResult<any>>;
    };
    jwt: {
        createToken: (opts?: import("../auth/jwt.helper").CreateTokenInput | undefined) => Promise<string | undefined>;
        createTokens: (opts: {
            user?: {
                id?: string | undefined;
                name?: string | undefined;
                role?: string | undefined;
            } | undefined;
            data?: import("../auth/jwt.helper").CreateTokenInput | undefined;
            auth?: import("../options").AuthOptions | undefined;
            root?: import("../options").RootOptions | undefined;
        }) => Promise<{
            accessToken: string | undefined;
            refreshToken: string | undefined;
        }>;
        verifyToken: (opts: import("../auth/jwt.helper").TokenSecretPair) => Promise<import("../auth/jwt.helper").JwtPayload | undefined>;
        getPayload: (opts: {
            token: string;
            auth: import("../options").AuthOptions;
        }) => Promise<import("../auth/jwt.helper").JwtPayload | undefined>;
    };
    psw: {
        hashPassword: (password: string) => Promise<string>;
        isValidPassword: (noencrypted: string, encrypted: string) => Promise<boolean>;
    };
    fetch: typeof fetch;
    ctx: {
        inTenantScope: <T extends import("./ctx").IPrisma, R = any>(opts: import("../options").AppOptions, tenantId: string, callback: (ctx: import("./ctx").TenantScopeContext<T>) => Promise<R>) => Promise<R>;
        inAitScope: <T extends import("./ctx").IPrisma, R = any>(opts: import("../options").AppOptions, tenantId: string, callback: (ctx: import("./ctx").TenantScopeContext<T>) => Promise<R>) => Promise<R>;
    };
    npx: {
        prismaDeploy: typeof import("../prisma/npx/prismaDeploy").prismaDeploy;
        prismaVersion: typeof import("../prisma/npx/prismaVersion").prismaVersion;
        npxPrisma: typeof import("../prisma/npx/npxPrisma").npxPrisma;
    };
    timer: {
        start: typeof start;
    };
};
export default _default;
