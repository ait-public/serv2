import { TenantContext } from '../Context';
import { AppOptions } from '../options';
export interface IPrisma {
    $disconnect(): Promise<any>;
    $executeRawUnsafe(query: string, ...values: any[]): Promise<any>;
    $queryRawUnsafe(query: string, ...values: any[]): Promise<any>;
}
export declare type TenantScopeContext<TPrisma extends IPrisma> = {
    tenantId: string;
    tenant: TenantContext;
    prisma: TPrisma;
    dbUrl: string;
};
declare const _default: {
    inTenantScope: <T extends IPrisma, R = any>(opts: AppOptions, tenantId: string, callback: (ctx: TenantScopeContext<T>) => Promise<R>) => Promise<R>;
    inAitScope: <T extends IPrisma, R = any>(opts: AppOptions, tenantId: string, callback: (ctx: TenantScopeContext<T>) => Promise<R>) => Promise<R>;
};
export default _default;
