declare type endTimer = () => number;
export declare function startTimer(): endTimer;
export {};
