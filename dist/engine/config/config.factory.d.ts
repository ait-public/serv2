import { AppOptions } from '../app/app.options';
export declare function createConfig(opts?: AppOptions): Promise<AppOptions>;
