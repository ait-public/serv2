export declare type ModeOptions = {
    /** in kubernetes scope **/
    isKube?: boolean;
    /** if true direct connection AIT_POSTGRES_PORT=5432 else AIT_PGBOUNCER_PORT=6432 */
    isMigration?: boolean;
    /** if true  AIT_POSTGRES_USER_TENANT = "tenant" else AIT_POSTGRES_USER="ait" */
    isTenant?: boolean;
    isDev?: boolean;
    isProd?: boolean;
    isTest?: boolean;
};
