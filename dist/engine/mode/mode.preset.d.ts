import { ModeOptions } from '../options';
export declare function presetMode(opts?: {
    mode?: ModeOptions;
}): ModeOptions;
