import { AppOptions } from './app.options';
export declare function createApp(opts?: AppOptions): Promise<AppOptions>;
