import { TestOptions } from '../options';
export declare function presetTest(opts?: {
    test?: TestOptions;
}): TestOptions;
