import { IGqlClient } from '../types';
import { AppOptions } from '../options';
export declare type TestContext<TPrisma> = {
    gqlClient: IGqlClient;
    prisma: TPrisma;
    options: AppOptions;
    dbUrl: string;
};
export declare function mockConsole(method?: any, value?: any): (() => void)[] | null;
export declare function createTestContext<TPrisma>(opts?: AppOptions): TestContext<TPrisma>;
