declare const _default: {
    typeDefs: string;
    resolvers: {
        Query: {
            hello: () => string;
        };
        Mutation: {
            ping: () => string;
        };
    }[];
};
export default _default;
