declare const _default: {
    PORT: number;
    PUBLIC_DIR: string;
    JSON_LIMIT: string;
};
export default _default;
