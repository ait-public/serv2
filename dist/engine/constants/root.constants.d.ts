declare const _default: {
    SCHEMA: string;
    AIT_EXTERNAL_URL: string;
    AIT_APP_VERSION: string;
    AIT_MAIL_TEST_ADDRESS: string;
};
export default _default;
