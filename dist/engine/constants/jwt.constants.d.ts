declare const _default: {
    ISSUER: string;
    SECRET: string;
    ANONYM: string;
    TOKEN: {
        SECRET: string;
        EXPIRES_IN: number;
    };
    /**
     * Configuration of refresh token.
     * expiresIn - The time in minutes before the code token expires.  Default is 100 years.  Most if
     *             all refresh tokens are expected to not expire.  However, I give it a very long shelf
     *             life instead.
     */
    REFRESH_TOKEN: {
        SECRET: string;
        EXPIRES_IN: number;
    };
    /**
     * Configuration of code token.
     * expiresIn - The time in minutes before the code token expires.  Default is 5 minutes.
     */
    CODE_TOKEN: {
        EXPIRES_IN: number;
    };
};
export default _default;
