declare const _default: {
    ROOT: {
        SCHEMA: string;
        AIT_EXTERNAL_URL: string;
        AIT_APP_VERSION: string;
        AIT_MAIL_TEST_ADDRESS: string;
    };
    JWT: {
        ISSUER: string;
        SECRET: string;
        ANONYM: string;
        TOKEN: {
            SECRET: string;
            EXPIRES_IN: number;
        };
        REFRESH_TOKEN: {
            SECRET: string;
            EXPIRES_IN: number;
        };
        CODE_TOKEN: {
            EXPIRES_IN: number;
        };
    };
    APOLLO: {
        BG: string;
        GRAPHQL_POSTFIX_URL: string;
    };
    SERVER: {
        PORT: number;
        PUBLIC_DIR: string;
        JSON_LIMIT: string;
    };
    DEV: {
        MEDPORTAL_URL: string;
    };
    SHIELD: {
        ruleTree: {
            Query: {
                '*': import("graphql-shield/dist/rules").RuleOr;
            };
            Mutation: {
                '*': import("graphql-shield/dist/rules").RuleOr;
            };
        };
        options: {
            debug: boolean;
            allowExternalErrors: boolean;
        };
    };
    SCHEMA: {
        typeDefs: string;
        resolvers: {
            Query: {
                hello: () => string;
            };
            Mutation: {
                ping: () => string;
            };
        }[];
    };
    ENV: {
        AIT_PLATFORM_TENANT_ID: string | undefined;
        AIT_MIGRATE_ON_STARTUP: string | undefined;
        APP_SCHEMA: string | undefined;
        NODE_ENV: string | undefined;
        AIT_KUBE: string | undefined;
        AIT_EXTERNAL_URL: string | undefined;
        AIT_APP_VERSION: string | undefined;
        AIT_JWT_SECRET: string | undefined;
        PORT: string | undefined;
        AIT_LOGLEVEL: string | undefined;
        AIT_POSTGRES_HOST: string | undefined;
        AIT_POSTGRES_DB: string | undefined;
        AIT_POSTGRES_USER: string | undefined;
        AIT_POSTGRES_PASSWORD: string | undefined;
        AIT_POSTGRES_USER_TENANT: string | undefined;
        AIT_POSTGRES_PASSWORD_TENANT: string | undefined;
        AIT_POSTGRES_PORT: string | undefined;
        AIT_PGBOUNCER_PORT: string | undefined;
        AIT_MEDPORTAL_URL: string | undefined;
        AIT_MAIL_TEST_ADDRESS: string | undefined;
        npm_package_version: string | undefined;
        AIT_MODE_TENANT: string | undefined;
    };
    MIGRATION: {
        PRISMA_DEPLOY_CMD: string;
        PRISMA_VERSION_CMD: string;
    };
    CLIENT: {
        INGRESS_REWRITE_SCHEMAS: string[];
        ROLE: string;
        EXP: number;
    };
    TENANT: {
        PG_CURRENT_TENANT_PARAM_NAME: string;
        AIT_PLATFORM_TENANT_ID: string;
    };
    SETTINGS: {
        ENV_LOCAL_PREFIX: string;
    };
};
export default _default;
