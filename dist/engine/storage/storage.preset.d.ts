import { IContextStorage } from './IContextStorage';
export declare function presetStorage(opts?: {
    contextStorage?: IContextStorage;
}): IContextStorage;
