import { presetAuth } from './auth/auth.preset';
import { presetClient } from './client/client.preset';
import { presetCore } from './core/core.preset';
import { presetDatabase } from './database/database.preset';
import { presetDev } from './dev/dev.preset';
import { presetLog } from './log/log.preset';
import { presetMigration } from './migration/migration.preset';
import { presetMode } from './mode/mode.preset';
import { presetPrisma } from './prisma/prisma.preset';
import { presetRoot } from './root/root.preset';
import { presetSchema } from './schema/schema.preset';
import { presetApollo } from './server/apollo/apollo.preset';
import { presetExpress } from './server/express/express.preset';
import { presetServer } from './server/server.preset';
import { presetShield } from './shield/shield.preset';
import { presetTenant } from './tenant/tenant.preset';
import { presetTest } from './testing/test.preset';
import { presetStorage } from './storage/storage.preset';
import { presetSettings } from './settings/settings.preset';
export declare const presets: {
    core: typeof presetCore;
    database: typeof presetDatabase;
    auth: typeof presetAuth;
    dev: typeof presetDev;
    log: typeof presetLog;
    migration: typeof presetMigration;
    mode: typeof presetMode;
    prisma: typeof presetPrisma;
    root: typeof presetRoot;
    shield: typeof presetShield;
    test: typeof presetTest;
    tenant: typeof presetTenant;
    client: typeof presetClient;
    server: typeof presetServer;
    schema: typeof presetSchema;
    apollo: typeof presetApollo;
    express: typeof presetExpress;
    storage: typeof presetStorage;
    settings: typeof presetSettings;
};
