import { IContextStorage } from '../storage/IContextStorage';
import { AuthOptions, UrlOptions } from '../options';
export declare type GetUrl = (schema: string) => string;
export declare type GqlClientOptions = {
    root?: {
        schema?: string;
    };
    mode?: {
        isTest?: boolean;
        isKube?: boolean;
    };
    dev?: {
        medportalUrl?: string;
    };
    server?: {
        port?: string | number;
    };
    auth?: AuthOptions;
    contextStorage?: IContextStorage;
    client?: {
        headers?: Record<string, string>;
        getUrl?: GetUrl;
    };
    core?: {
        getUrl?: (opts?: UrlOptions) => Promise<GetUrl>;
    };
};
