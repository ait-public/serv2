import { GetUrl } from '../options';
import { IGqlClient } from '../types';
export declare type ClientOptions = {
    headers?: Record<string, string>;
    gqlClient?: IGqlClient;
    getUrl?: GetUrl;
};
