import { GetUrl, UrlOptions } from '../options';
export declare function createUrl(opts?: UrlOptions): Promise<GetUrl>;
