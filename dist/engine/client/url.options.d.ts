export declare type UrlOptions = {
    root?: {
        schema?: string;
    };
    dev?: {
        medportalUrl?: string;
    };
    mode?: {
        isKube?: boolean;
        isTest?: boolean;
    };
    server?: {
        port?: string | number;
    };
};
