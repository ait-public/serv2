import { ClientOptions } from '../options';
export declare function presetClient(opts?: {
    client?: ClientOptions;
}): ClientOptions;
