import { IGqlClient } from '../types';
import { GqlClientOptions } from '../options';
export declare function createGqlClient(opts?: GqlClientOptions): Promise<IGqlClient>;
