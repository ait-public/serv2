import { AuthOptions } from '../auth/auth.options';
export declare type HttpHeaderOptions = {
    root?: {
        schema?: string;
    };
    mode?: {
        isTest?: boolean;
    };
    auth?: AuthOptions;
    client?: {
        headers?: Record<string, string>;
    };
};
