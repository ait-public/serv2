import { ClientOptions } from '../options';
import { ClientOptionsInput } from './client.options.input';
export declare function createClientOptions(opts?: ClientOptionsInput): Promise<ClientOptions>;
