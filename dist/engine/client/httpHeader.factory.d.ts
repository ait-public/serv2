import { HttpHeaderOptions } from '../options';
import { CreateTokenInput } from '../auth/jwt.helper';
/**
 * Global header
 */
export declare function createHttpHeader(opts?: HttpHeaderOptions): Promise<Record<string, string>>;
export declare function createClientToken(opts?: CreateTokenInput): Promise<string | undefined>;
