import { Core } from './core/Core';
import { AppOptions } from './options';
import { IServer } from './types';
export declare function initApp(): Core;
export declare function createApp(opts?: AppOptions): Promise<IServer>;
