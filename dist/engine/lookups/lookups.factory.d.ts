import { LookupsOptions } from './lookups.options';
import { LookupsOptionsInput } from './lookups.options.input';
export declare function createLookups(opts?: LookupsOptionsInput): Promise<LookupsOptions>;
