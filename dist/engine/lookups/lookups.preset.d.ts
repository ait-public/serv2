import { LookupsOptions } from './lookups.options';
export declare function presetLookups(opts?: {
    lookups?: LookupsOptions;
}): LookupsOptions;
