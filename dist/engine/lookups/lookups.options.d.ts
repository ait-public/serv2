import Lookup, { FindManyArgs, LookupDict, LookupResolvers, PrepareArgs, PrepareSearchCondition } from '../lookup/Lookup';
/** Описание Лукап типа */
export declare type LookupType<TFindManyArgs = FindManyArgs> = {
    /** PK - `id` по умолчанию */
    propId?: string;
    /**  задаем _display name  - имя для отображения */
    propText?: string | ((item: any) => string);
    /** задаем поле для поиска => сортировку по умолчанию */
    searchFields?: string[] | PrepareSearchCondition;
    /**
     * выбираем поля для лукапа в UI
     * - https://www.prisma.io/docs/concepts/components/prisma-client/select-fields
     * */
    select?: LookupDict<boolean>;
    /**
     * Подготовить выборку для Prisma через - `args` своими хотелками,
     * где уже есть пагинация, сортировка, поиск по строке, фильтры ...
     * например - задать свою сортировку
     */
    prepareArgs?: PrepareArgs<TFindManyArgs>;
    /** Выборка из Prismа */
    resolvers?: LookupResolvers;
    /**
     * Подмешать в готовую выборку свои поля
     */
    map?: (item: any) => any;
};
export declare type LookupsOptions = {
    types?: Record<string, LookupType>;
    lookup?: Lookup;
};
