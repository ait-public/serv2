import { LookupsOptions } from './lookups.options';
export declare type LookupsOptionsInput = {
    lookups?: LookupsOptions;
};
