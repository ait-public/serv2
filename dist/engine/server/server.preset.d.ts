import { ServerOptions } from '../options';
export declare function presetServer(opts?: {
    server?: ServerOptions;
    mode?: {
        isTest?: boolean;
    };
    root?: {
        schema?: string;
    };
}): Promise<ServerOptions>;
