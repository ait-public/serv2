import { ExpressOptionsInput } from '../../options';
export declare function createExpress(opts?: ExpressOptionsInput): Promise<import("./express.options").ExpressOptions>;
