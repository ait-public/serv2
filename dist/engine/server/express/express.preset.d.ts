import { ExpressOptions } from '../../options';
export declare function presetExpress(opts?: {
    server?: {
        express?: ExpressOptions;
    };
    root?: {
        schema?: string;
    };
}): ExpressOptions;
