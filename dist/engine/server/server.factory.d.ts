import { ServerOptions } from './server.options';
import { ServerOptionsInput } from './server.options.input';
export declare function createServer(opts?: ServerOptionsInput): Promise<ServerOptions>;
