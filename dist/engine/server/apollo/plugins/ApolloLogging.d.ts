export declare const ApolloLogging: {
    requestDidStart(requestContext: any): Promise<{
        parsingDidStart(reg: any): Promise<(...errors: any) => any>;
        validationDidStart(): Promise<(errs: any) => void>;
        executionDidStart(): Promise<(err: any) => void>;
        didEncounterErrors(reg: any): Promise<void>;
    }>;
};
