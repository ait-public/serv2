import { ApolloOptions } from '../../options';
import { ExpressOptions } from '../express/express.options';
export declare function presetApollo(opts?: {
    server?: {
        express?: ExpressOptions;
        apollo?: ApolloOptions;
    };
    root?: {
        schema?: string;
    };
}): ApolloOptions;
