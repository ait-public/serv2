import { GraphQLSchema } from 'graphql';
import { ApolloOptions } from '../../options';
export declare type ApolloOptionsInput = {
    root?: {
        schema?: string;
    };
    schema?: {
        schema?: GraphQLSchema;
    };
    server?: {
        express?: {
            baseUrl?: string;
        };
        apollo?: ApolloOptions;
    };
};
