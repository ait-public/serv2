import { ApolloServer, ExpressContext } from 'apollo-server-express';
import { ApolloOptionsInput } from './apollo.options.input';
export declare function createApollo(opts?: ApolloOptionsInput): Promise<ApolloServer<ExpressContext>>;
