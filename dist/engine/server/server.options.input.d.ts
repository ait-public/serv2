import { ApolloServer, ExpressContext } from 'apollo-server-express';
import { ServerOptions } from './server.options';
import { ApolloOptions, ApolloOptionsInput, AppOptions, ExpressOptions, ExpressOptionsInput, LookupsOptions, LookupsOptionsInput, SchemaOptions, SchemaOptionsInput, ShieldOptions } from '../options';
export declare type ServerOptionsInput = {
    server?: ServerOptions;
    root?: {
        schema?: string;
        /** ENV production version */
        appVersion?: string;
        /** npm server package.json */
        version?: string;
    };
    mode?: {
        isTest?: boolean;
    };
    shield?: ShieldOptions;
    schema?: SchemaOptions;
    lookups?: LookupsOptions;
    core?: {
        config?: (opts?: AppOptions) => Promise<AppOptions>;
        express?: (opts?: ExpressOptionsInput) => Promise<ExpressOptions>;
        apollo?: (opts?: ApolloOptionsInput) => Promise<ApolloServer<ExpressContext>>;
        context?: (opts?: AppOptions) => Promise<ApolloOptions>;
        lookups?: (opts?: LookupsOptionsInput) => Promise<LookupsOptions>;
        shield?: (opts?: SchemaOptionsInput) => ShieldOptions;
        schema?: (opts?: SchemaOptionsInput) => Promise<SchemaOptions>;
    };
};
