import { ExpressOptions } from './express/express.options';
import { ApolloOptions } from './apollo/apollo.options';
import { IServer } from '../types';
export declare type ServerOptions = {
    port?: string | number;
    appServer?: IServer;
    express?: ExpressOptions;
    apollo?: ApolloOptions;
};
