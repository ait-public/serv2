import { IGqlClient } from '../types';
export interface IDocNum {
    value: string;
    numeratorId: string;
    issuedNumberId: string;
}
export interface ICmonApi {
    getNumeratorCode(docType: string): string;
    getDocNum(docType: string): Promise<IDocNum>;
}
export interface ICmonContext {
    deptCode?: string;
}
export declare function create(client: IGqlClient, context?: ICmonContext): ICmonApi;
