import { IGqlClient } from '../types';
export interface IBashApi {
    exec(data: {
        script: string;
        database: string;
        params: any;
    }): Promise<any>;
}
export declare function create(client: IGqlClient): IBashApi;
