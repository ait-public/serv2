import { IGqlClient } from '../types';
export interface IDocNum {
    value: string;
    numerator_id: string;
    issued_number_id: string;
}
export interface ICommonApi {
    getDocNum(numerator_id: string, onDate?: Date): Promise<IDocNum>;
}
export interface ICommonContext {
    deptCode?: string;
}
export declare function create(client: IGqlClient, context?: ICommonContext): ICommonApi;
