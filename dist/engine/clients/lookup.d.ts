import { IGqlClient } from '../types';
export declare type LookupIdsInput = {
    schema: string;
    ids: {
        [type: string]: string[];
    };
    headers?: Record<string, string>;
};
export declare type LookupIdsResult = {
    [type: string]: {
        [id: string]: any;
    } | null;
};
export interface ILookupApi {
    getIds(data: LookupIdsInput): Promise<LookupIdsResult>;
}
export declare function create(client: IGqlClient): ILookupApi;
