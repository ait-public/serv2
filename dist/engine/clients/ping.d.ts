import { IGqlClient } from '../types';
export declare type PingOptionsInput = {
    schema: string;
    api?: boolean;
    version?: boolean;
    json?: boolean;
};
export interface IPingApi {
    ping(data?: PingOptionsInput): Promise<any>;
}
export declare function create(client: IGqlClient): IPingApi;
