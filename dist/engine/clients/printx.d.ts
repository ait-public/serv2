import { IGqlClient } from '../types';
export interface IPrintxBaseData {
    id?: string;
    tenantId?: string;
    createdId?: string;
    modifiedId?: string;
    created?: string;
    creator?: string;
    modified?: string;
    modifier?: string;
    org?: string;
}
/** Раздел для добавления или обновления */
export interface IPrintxSaveSectionInput extends IPrintxBaseData {
    Name?: string;
    Description?: string;
    prefix?: string;
}
/** Свойства раздела для добавления или обновления */
export interface IPrintxSavePropertiesInput {
    SectionId?: string;
    ParentId?: string;
    Name?: string;
    Description?: string;
    Kind?: string;
    DefaultValue?: string;
}
/** Получить список разделов по указанным параметрам */
export interface IPrintxSelectSectionsInput {
    ids?: string[];
    likeText?: string;
    orderBy?: string;
    isDescending?: boolean;
    pageIndex?: number;
    pageSize?: number;
}
/**
 * Получить список шаблонов по указанным параметрам
 */
export interface IPrintxSelectTemplatesInput {
    ids?: string[];
    sectionIds?: [];
    likeText?: string;
    orderBy?: string;
    isDescending?: boolean;
    pageIndex?: number;
    pageSize?: number;
}
/** Шаблон для добавления или обновления */
export interface IPrintxSaveTemplateInput extends IPrintxBaseData {
    SectionId?: string;
    Name?: string;
    Description?: string;
    Prefix?: string;
}
/** Добавить новый или обновить данные шаблона */
export interface IPrintxSaveTemplateDataInput extends IPrintxBaseData {
    data?: string;
}
/** Список шаблонов и данные для создания документов */
export interface IPrintxCreateDocumentsInput {
    templateIds?: string[];
    format?: string;
    values?: Record<string, string>;
}
/** Создать рабочую книгу Excel по заданным параметрам */
export interface IPrintxCreateWorkbookInput {
    WorksheetInfos?: {
        Name?: string;
        ShowHeader?: boolean;
        ColumnInfos?: {
            Title?: string;
            Color?: any;
            FontSize?: number;
            FontStyle?: number;
            BackgroundColor?: any;
            VerticalAlignmenut?: number;
            HorizontalAlignment?: number;
            NumberFormat?: string;
            columnWidth?: number;
        }[];
        Data?: any;
    }[];
}
/** Конвертировать документ rtf в документ pdf */
export interface IPrintxConvertRtfToPdfInput {
    fileName?: string;
    fileData?: string;
}
/** Сервис API: Шаблоны печатных форм */
export interface IPrintxApi {
    /** Добавить новый или обновить существующий раздел */
    saveSection(data: IPrintxSaveSectionInput): Promise<any>;
    /** Добавить новый или обновить существующие свойства раздела */
    saveProperties(data: IPrintxSavePropertiesInput[]): Promise<any>;
    /** Получить список свойств для раздела */
    selectProperties(sectionId: string): Promise<any>;
    /** Получить список разделов по указанным параметрам */
    selectSections(data: IPrintxSelectSectionsInput): Promise<any>;
    /** Получить список шаблонов по указанным параметрам */
    selectTemplates(data: IPrintxSelectTemplatesInput): Promise<any>;
    /** Получить список значений документа для указанного раздела */
    getDocumentValues(sectionId?: string): Promise<any>;
    /** Добавить новый или обновить существующий шаблон */
    saveTemplate(data: IPrintxSaveTemplateInput): Promise<any>;
    /** Добавить новый или обновить данные шаблона */
    saveTemplateData(data: IPrintxSaveTemplateDataInput): Promise<any>;
    /** Получить данные шаблона по идентификатору */
    selectTemplateData(id: string): Promise<any>;
    /** Список шаблонов и данные для создания документов */
    createDocuments(data: IPrintxCreateDocumentsInput): Promise<any>;
    /** Создать рабочую книгу Excel по заданным параметрам */
    createWorkbook(data: IPrintxCreateWorkbookInput): Promise<any>;
    /** Конвертировать документ rtf в документ pdf*/
    convertRtfToPdf(data: IPrintxConvertRtfToPdfInput): Promise<any>;
}
export declare function create(client: IGqlClient): IPrintxApi;
