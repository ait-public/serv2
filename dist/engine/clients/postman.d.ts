import { IGqlClient } from '../types';
export interface IPostmanApi {
    mail(data: {
        addresses: string[] | string;
        subject: string;
        html?: string;
    }): Promise<any>;
}
export declare function create(client: IGqlClient): IPostmanApi;
