import { Context } from '../Context';
export interface LookupDict<T> {
    [Key: string]: T | any;
}
declare type LookupTypeInfo = {
    type: string;
    propId: string;
    propText: string | ((item: any) => string);
    typeDefs: string;
    typeName: string;
    select: LookupDict<boolean>;
    resolvers: LookupResolvers;
};
export declare type LookupSort = {
    by: string;
    desc: boolean;
};
/**
 * @default
 *  {page: 1, perPage: 100}
 */
export declare type LookupPaginationInput = {
    page?: number;
    perPage?: number;
};
export declare type LookupSearchInput = {
    filter?: LookupDict<any>;
    pagination?: LookupPaginationInput;
    sort?: LookupSort[];
};
export declare type LookupGetByIdsParams = {
    source: any;
    args: any;
    context: Context<any>;
    info: any;
    ids: string[];
};
export declare type LookupGetByIds = (ctx: LookupGetByIdsParams) => Promise<any[]>;
export declare type LookupGetListParams = {
    source: any;
    context: Context<any>;
    info: any;
    type: string;
    where?: LookupSearchInput;
};
export declare type LookupGetList = (ctx: LookupGetListParams) => Promise<any[]>;
export declare type LookupResolvers = {
    ids?: LookupGetByIds;
    list?: LookupGetList;
};
export declare type FindManyArgs = {
    where?: any;
    skip?: number;
    take?: number;
    orderBy?: any;
    select?: LookupDict<boolean>;
} | undefined;
export declare type PrepareOpts<TFindManyArgs = FindManyArgs> = {
    input?: string | string[];
    args: TFindManyArgs;
    type: string;
    context: Context<any>;
    opts: LookupGetListParams | LookupGetByIdsParams;
};
export declare type PrepareFindManyArgs<TFindManyArgs = FindManyArgs> = (opts: PrepareOpts<TFindManyArgs>) => Promise<TFindManyArgs>;
export declare type PrepareArgs<TFindManyArgs = FindManyArgs> = {
    list?: PrepareFindManyArgs<TFindManyArgs>;
    ids?: PrepareFindManyArgs<TFindManyArgs>;
};
export declare type PrepareSearchCondition = (opts: PrepareOpts) => Promise<any>;
/**
 * @default
 *  {propId: "id", propText: "name"}
 */
export declare type LookupOptions = {
    type: string;
    propId?: string;
    propText?: string | ((item: any) => string);
    searchFields?: string[] | PrepareSearchCondition;
    select?: LookupDict<boolean>;
    resolvers?: LookupResolvers;
    prepareArgs?: PrepareArgs;
    map?: (item: any) => any;
};
/**
 *@example
  const lookup = new Lookup();
  lookup.add({
    type: "gender",
    propId: "genderId",
    select: { name: true },
  })
  //gql
  {
    findLookup(ids: { gender: ["00000000-0000-0000-0006-000000000001"] }) {
      gender {
        id
        record
      }
    }
    findLookupList(type: "gender")
  }
 */
declare class Lookup {
    readonly types: LookupDict<LookupTypeInfo>;
    add(opts: LookupOptions): this;
    getTypes(): string[];
    toPayload(): string;
    toInput(): string;
    toResult(): string;
    get typeDefs(): string;
    toString(): string;
    resolvers(): {
        JSONObject?: undefined;
        Query?: undefined;
    } | {
        JSONObject: import("graphql").GraphQLScalarType;
        Query: {
            findLookup: (source: any, args: any, context: any, info: any) => Promise<any>;
            findLookupList: (source: any, args: any, context: any, info: any) => any;
            getLookupTypes: () => any;
        };
    };
}
export default Lookup;
