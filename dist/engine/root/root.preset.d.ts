import { RootOptions } from '../options';
export declare function presetRoot(opts?: {
    root?: RootOptions;
    mode?: {
        isTest?: boolean;
    };
}): RootOptions;
