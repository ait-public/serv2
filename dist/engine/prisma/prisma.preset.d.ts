import { PrismaOptions } from '../options';
export declare function presetPrisma(opts?: {
    prisma?: PrismaOptions;
}): PrismaOptions;
