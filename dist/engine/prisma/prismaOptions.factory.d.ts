import { PrismaOptions } from '../options';
import { PrismaOptionsInput } from './prisma.options.input';
export declare function createPrismaOptions(opts?: PrismaOptionsInput): Promise<PrismaOptions>;
