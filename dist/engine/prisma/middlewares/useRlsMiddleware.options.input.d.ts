import { PrismaOptions } from '../../options';
import { IContextStorage } from '../../storage/IContextStorage';
import { TenantOptions } from '../../tenant/tenant.options';
export declare type userRlsMiddlewareOptionsInput = {
    prisma?: PrismaOptions;
    tenant?: TenantOptions;
    contextStorage?: IContextStorage;
};
