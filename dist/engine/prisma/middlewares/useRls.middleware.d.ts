import { userRlsMiddlewareOptionsInput as useRlsMiddlewareOptionsInput } from './useRlsMiddleware.options.input';
export declare function useRlsMiddleware(opts?: useRlsMiddlewareOptionsInput): Promise<void>;
