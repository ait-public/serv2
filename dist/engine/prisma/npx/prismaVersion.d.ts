export declare function prismaVersion({ cmdVersion }: {
    cmdVersion?: string;
}): Promise<boolean>;
