import { prismaVersion } from './prismaVersion';
import { prismaDeploy } from './prismaDeploy';
import { npxPrisma } from './npxPrisma';
declare const _default: {
    prismaDeploy: typeof prismaDeploy;
    prismaVersion: typeof prismaVersion;
    npxPrisma: typeof npxPrisma;
};
export default _default;
