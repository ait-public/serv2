export declare function prismaDeploy({ dbUrl, cmdDeploy, }: {
    dbUrl?: string;
    cmdDeploy?: string;
}): Promise<boolean>;
