export declare type PrismaOptions = {
    dbUrl?: string;
    isLogged?: boolean;
    prisma?: any;
    middlewares?: {
        rls?: boolean;
    };
};
