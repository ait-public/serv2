import { GraphQLClient } from 'graphql-request';
import { Context } from '../Context';
import { GetUrl, RootOptions, ModeOptions } from '../options';
import { ContextParameters } from '../server/apollo/apollo.options';
export declare type CallBackReqContext = ({ req, }: ContextParameters) => Promise<Context<any>>;
export interface IServer {
    run(opts?: any, listeningListener?: (opts: any) => void): Promise<any>;
    stop(): Promise<void>;
}
export declare type ID = string;
export declare type ValueOrPromise<T> = T | Promise<T>;
export interface IGqlClient {
    get(schema: string, options?: {
        headers?: Record<string, string>;
        noProxyTenant?: boolean;
    }): GraphQLClient;
    post(url: string, data?: string | object | null, init?: {
        /** GET | POST */
        method?: string;
        headers?: Record<string, string>;
        contentType?: string;
        responseText?: boolean;
        noProxyTenant?: boolean;
    }): Promise<any>;
    getUrl: GetUrl;
    options?: {
        root?: RootOptions;
        mode?: ModeOptions;
    };
}
