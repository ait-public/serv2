import { ApolloOptions } from '../server/apollo/apollo.options';
import { AppOptions } from '../options';
export declare function createContext(opts?: AppOptions): Promise<ApolloOptions>;
