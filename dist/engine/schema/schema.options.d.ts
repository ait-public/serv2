import { GraphQLSchema } from 'graphql';
import { IMiddlewareGenerator } from 'graphql-middleware';
export declare type SubSchemaInfo = {
    schemaName: string;
    remotes: {
        remoteTypeName: string;
        remoteTypePkName: string;
        remoteTypeFunc?: string;
        remoteTypeFuncArgs?: (ids: any[]) => any;
    }[];
    /**
     * @default { log: false, batch: true, valuesFromResults: undefined }
     */
    options?: {
        /**
         * @default false
         */
        log?: boolean;
        /**
         * @default true
         */
        batch?: boolean;
        valuesFromResults?: (results: any[], keys: any[]) => any[];
    };
};
export declare type SchemaOptions = {
    schema?: GraphQLSchema;
    typeDefs?: any;
    resolvers?: any;
    isGenerateGraphQlSDLFile?: boolean;
    isPaljs?: boolean;
    subSchemas?: SubSchemaInfo[];
    middlewares?: IMiddlewareGenerator<any, any, any>[];
};
