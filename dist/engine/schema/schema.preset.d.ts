import { SchemaOptions } from '../options';
export declare function presetSchema(opts?: {
    schema?: SchemaOptions;
    mode?: {
        isDev?: boolean;
    };
}): SchemaOptions;
