import { SchemaOptionsInput } from '../options';
export declare function createSchema(opts?: SchemaOptionsInput): Promise<import("./schema.options").SchemaOptions>;
