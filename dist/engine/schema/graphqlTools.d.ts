import { SubschemaConfig } from '@graphql-tools/delegate';
import { TypeMergingOptions } from '@graphql-tools/stitch/types';
export declare const defaultRemoteTypeFuncArgs: (ids: any[], remoteTypePkName: string) => object;
export declare function subSchema(url: string, remotes: {
    remoteTypeName: string;
    remoteTypePkName: string;
    remoteTypeFunc: string;
    remoteTypeFuncArgs: (ids: any[]) => object;
}[], options?: {
    log?: boolean;
    batch?: boolean;
    valuesFromResults?: (results: any[], keys: any[]) => any[];
    headers?: Record<string, string>;
}): Promise<SubschemaConfig>;
export declare function makeGatewaySchema(typeDefs: any, resolvers: any, subSchemas: Promise<SubschemaConfig>[], typeMergingOptions?: TypeMergingOptions): Promise<import("graphql").GraphQLSchema>;
