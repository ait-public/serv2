import { DbConnectionOptions } from './dbConnection.options';
export declare function createDbConnection(opts?: DbConnectionOptions): string;
