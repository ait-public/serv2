export declare type DbOptions = {
    dbUrl?: string;
    host?: string;
    db?: string;
    user?: string;
    password?: string;
    schema?: string;
    port?: string | number;
    pgbouncer?: string | number;
    userTenant?: string;
    passwordTenant?: string;
};
