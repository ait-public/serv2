import { DbOptions } from '../options';
export declare function presetDatabase(opts?: {
    database?: DbOptions;
    mode?: {
        isTest?: boolean;
    };
    root?: {
        schema?: string;
    };
}): DbOptions;
