import { Context } from '../../Context';
declare const _default: {
    Query: {
        me: (p: any, args: any, ctx: Context<any>, meta: any) => Promise<{
            id: string | undefined;
            name: string | undefined;
            createdAt: string | number | undefined;
            email: string;
            isDisabled: boolean;
            personId: string | undefined;
            role: string | undefined;
            profile: {
                id: string | undefined;
                role: string | undefined;
                name: string;
            };
        }>;
    };
    Mutation: {
        login: (p: any, args: any, ctx: Context<any>, meta: any) => Promise<{
            accessToken: string | undefined;
            refreshToken: string | undefined;
        }>;
        register: () => Promise<never>;
        refreshToken: (p: any, args: any, ctx: Context<any>, meta: any) => Promise<{
            accessToken: string | undefined;
            refreshToken: string | undefined;
        }>;
        logout: (p: any, args: any, ctx: Context<any>, meta: any) => Promise<boolean>;
    };
};
export default _default;
