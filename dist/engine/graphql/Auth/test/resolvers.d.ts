import { Context } from '../../../Context';
import { AppOptions } from '../../../options';
declare const resolvers: {
    Query: {
        me: (_: any, args: any, { userId }: Context<any>, _any: any) => Promise<{
            id: string | undefined;
            name: string | undefined;
            createdAt: string | number | undefined;
            email: string;
            isDisabled: boolean;
            personId: string | undefined;
            role: string | undefined;
            profile: {
                id: string | undefined;
                role: string | undefined;
                name: string;
            };
        }>;
    };
    Mutation: {
        login: (_: any, args: any, { options: { auth, root } }: {
            options: AppOptions;
        }, _any: any) => Promise<{
            accessToken: string | undefined;
            refreshToken: string | undefined;
        }>;
        register: () => Promise<never>;
        refreshToken: (_: any, { refreshToken }: any, { options: { auth, root } }: Context<any>, __: any) => Promise<{
            accessToken: string | undefined;
            refreshToken: string | undefined;
        }>;
        logout: (_: any, __any: any, { userId }: Context<any>, __: any) => Promise<boolean>;
    };
};
export default resolvers;
