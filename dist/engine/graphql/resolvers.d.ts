declare const _default: ({
    LocalDateTime: import("graphql").GraphQLScalarType;
    GUID: import("graphql").GraphQLScalarType;
    EmailAddress: import("graphql").GraphQLScalarType;
    LocalTime: import("graphql").GraphQLScalarType;
    DateTime: import("graphql").GraphQLScalarType;
    JSONObject: import("graphql").GraphQLScalarType;
    LocalDate: import("graphql").GraphQLScalarType;
    Json: {
        name: string;
        description: import("graphql/jsutils/Maybe").Maybe<string>;
        specifiedByUrl: import("graphql/jsutils/Maybe").Maybe<string>;
        serialize: import("graphql").GraphQLScalarSerializer<any>;
        parseValue: import("graphql").GraphQLScalarValueParser<any>;
        parseLiteral: import("graphql").GraphQLScalarLiteralParser<any>;
        extensions: import("graphql/jsutils/Maybe").Maybe<Readonly<import("graphql").GraphQLScalarTypeExtensions>>;
        astNode: import("graphql/jsutils/Maybe").Maybe<import("graphql").ScalarTypeDefinitionNode>;
        extensionASTNodes: import("graphql/jsutils/Maybe").Maybe<readonly import("graphql").ScalarTypeExtensionNode[]>;
    };
} | {
    Query: {
        me: (p: any, args: any, ctx: import("../Context").Context<any>, meta: any) => Promise<{
            id: string | undefined;
            name: string | undefined;
            createdAt: string | number | undefined;
            email: string;
            isDisabled: boolean;
            personId: string | undefined;
            role: string | undefined;
            profile: {
                id: string | undefined;
                role: string | undefined;
                name: string;
            };
        }>;
    };
    Mutation: {
        login: (p: any, args: any, ctx: import("../Context").Context<any>, meta: any) => Promise<{
            accessToken: string | undefined;
            refreshToken: string | undefined;
        }>;
        register: () => Promise<never>;
        refreshToken: (p: any, args: any, ctx: import("../Context").Context<any>, meta: any) => Promise<{
            accessToken: string | undefined;
            refreshToken: string | undefined;
        }>;
        logout: (p: any, args: any, ctx: import("../Context").Context<any>, meta: any) => Promise<boolean>;
    };
} | {
    JSONObject: import("graphql").GraphQLScalarType;
    Mutation: {
        runCronJob: typeof import("../cron").Job.runJob;
        initCronJobs: typeof import("../cron").Job.initJobs;
    };
    Query: {
        getCronJobs: typeof import("../cron").Job.getJobs;
    };
})[];
export default _default;
