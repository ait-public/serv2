declare const _default: {
    LocalDateTime: import("graphql").GraphQLScalarType;
    GUID: import("graphql").GraphQLScalarType;
    EmailAddress: import("graphql").GraphQLScalarType;
    LocalTime: import("graphql").GraphQLScalarType;
    DateTime: import("graphql").GraphQLScalarType;
    JSONObject: import("graphql").GraphQLScalarType;
    LocalDate: import("graphql").GraphQLScalarType;
    Json: {
        name: string;
        description: import("graphql/jsutils/Maybe").Maybe<string>;
        specifiedByUrl: import("graphql/jsutils/Maybe").Maybe<string>;
        serialize: import("graphql").GraphQLScalarSerializer<any>;
        parseValue: import("graphql").GraphQLScalarValueParser<any>;
        parseLiteral: import("graphql").GraphQLScalarLiteralParser<any>;
        extensions: import("graphql/jsutils/Maybe").Maybe<Readonly<import("graphql").GraphQLScalarTypeExtensions>>;
        astNode: import("graphql/jsutils/Maybe").Maybe<import("graphql").ScalarTypeDefinitionNode>;
        extensionASTNodes: import("graphql/jsutils/Maybe").Maybe<readonly import("graphql").ScalarTypeExtensionNode[]>;
    };
};
export default _default;
