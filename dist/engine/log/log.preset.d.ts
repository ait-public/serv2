import { LogOptions } from '../options';
export declare function presetLog(opts?: {
    log?: LogOptions;
}): LogOptions;
